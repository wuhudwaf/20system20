package com.example.system.config;

import com.example.system.dto.Result;
import com.example.system.exception.CommonException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * 全局异常处理器
 */
@Slf4j
@RestControllerAdvice
@SuppressWarnings("all")
public class WebExceptionAdvice {

    @ExceptionHandler(RuntimeException.class)
    public Result handleRuntimeException(RuntimeException e) {
        log.error(e.toString(), e);
        return Result.fail(e.getMessage());
    }

    /**
     * 统一异常
     *
     * @param commonException
     * @return
     */
    @ExceptionHandler(CommonException.class)
    public Result CommonException(CommonException commonException) {
        // 提示
        log.error("异常位置 --> " + commonException.getHead());
        log.error("异常信息 --> " + commonException.getMessage());
        log.error(commonException.toString(), commonException);
        // 异常信息
        return Result.fail(commonException.getMessage());
    }

    /**
     * 实体类字段为空或长度异常
     *
     * @param methodArgumentNotValidException
     * @return
     */
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public Result MethodArgumentNotValidException(MethodArgumentNotValidException methodArgumentNotValidException) {
        log.error(methodArgumentNotValidException.toString(), methodArgumentNotValidException);
        return Result.fail(methodArgumentNotValidException.getBindingResult().getFieldError().getDefaultMessage());
    }
}
