package com.example.system.config;


import com.example.system.interceotor.LoginInterceptor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

import java.util.ArrayList;
import java.util.List;

/**
 * 拦截器配置类
 */
@SuppressWarnings({"all"})
@Slf4j
@Configuration
public class InterceptorConfigurerr extends WebMvcConfigurationSupport {

    @Value("${img.path}")
    String path;

    @Value("${filess.file}")
    String file;

    @Value("${bjt.bjts}")
    String bjt;


    /**
     * 拦截器，拦截需要登录的路径
     */
    @Autowired
    private LoginInterceptor loginInterceptor;

    /**
     * 注册拦截器
     *
     * @param registry
     */
    @Override
    public void addInterceptors(InterceptorRegistry registry) {

        /**
         * List 白名单: 放行
         */
        // 拦截需要登录的路径
        List<String> loginInterceptorList = new ArrayList<>();
        // 登录接口
        loginInterceptorList.add("/**");
        loginInterceptorList.add("/common/sys/user/login");
        loginInterceptorList.add("/vue-admin-template/user/login");
        loginInterceptorList.add("/error");
        // 静态资源
        loginInterceptorList.add("/static/**");
        // 图片上传接口
        loginInterceptorList.add("/common/upload");
        loginInterceptorList.add("/favicon.ico");

        loginInterceptorList.add("/swagger-resources/**");
        loginInterceptorList.add("/webjars/**");
        loginInterceptorList.add("/file/**");
        loginInterceptorList.add("/v2/**");
        loginInterceptorList.add("/v3/**");
        loginInterceptorList.add("/swagger-ui.html/**");
        loginInterceptorList.add("/api");
        loginInterceptorList.add("/api-docs");
        loginInterceptorList.add("/api-docs/**");
        loginInterceptorList.add("/doc.html/**");
        //注册拦截器类，添加黑名单(addPathPatterns("/**")),‘/*’只拦截一个层级，'/**'拦截全部
        // 和白名单(excludePathPatterns("List类型参数"))，将不必拦截的路径添加到List列表中
        registry.addInterceptor(loginInterceptor).addPathPatterns("/**").excludePathPatterns(loginInterceptorList);
    }

    /**
     * 设置静态资源映射
     *
     * @param registry
     */
    @Override
    protected void addResourceHandlers(ResourceHandlerRegistry registry) {
        // 输出日志
        log.info("开始进行静态资源映射");

        //存放上传文件的文件夹
//        File file = new File(baseFilePath + "\\");
        // 找图片，相对路径与绝对路径映射，然后使用相对路径找图片即可
        registry.addResourceHandler("/file/**").addResourceLocations("file:" + path +  "/");
//        registry.addResourceHandler("/picbook/statics/uploadFile/**").addResourceLocations("file:C:\\image\\");
        registry.addResourceHandler("/fisles/**").addResourceLocations("file:" + file +  "/");
        registry.addResourceHandler("/bgt/**").addResourceLocations("file:" + bjt +  "/");

        registry.addResourceHandler("/**").addResourceLocations("classpath:/static/");
        registry.addResourceHandler("doc.html").addResourceLocations("classpath:/META-INF/resources/");
        registry.addResourceHandler("/favicon.ico").addResourceLocations("classpath:/META-INF/resources/");
        registry.addResourceHandler("/webjars/**").addResourceLocations("classpath:/META-INF/resources/webjars/");
    }

    /**
     * 扩展转换器
     *
     * @param converters
     */
    @Override
    protected void extendMessageConverters(List<HttpMessageConverter<?>> converters) {
        log.info("扩展消息转换器");
        // 创建消息转换器
        MappingJackson2HttpMessageConverter mappingJackson2HttpMessageConverter = new MappingJackson2HttpMessageConverter();
        // 设置具体的对象映射器
        mappingJackson2HttpMessageConverter.setObjectMapper(new JacksonObjectMapper());
        // 通过索引设置, 优先使用我们扩展的转换器
        converters.add(0, mappingJackson2HttpMessageConverter);
    }
}
