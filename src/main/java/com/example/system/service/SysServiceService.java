package com.example.system.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.system.pojo.SysService;

public interface SysServiceService {

    /**
     * 售后服务列表
     * @param page
     * @param pageSize
     * @return
     */
    Page<SysService> getPage(Long page, Long pageSize);
}