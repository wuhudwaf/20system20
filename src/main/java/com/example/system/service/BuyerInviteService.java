package com.example.system.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.system.pojo.BuyerInvite;

public interface BuyerInviteService {

    /**
     * 用户邀请列表
     * @param page
     * @param pageSize
     * @return
     */
    Page<BuyerInvite> getList(Long page, Long pageSize);
}