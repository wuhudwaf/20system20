package com.example.system.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.system.pojo.SellerInventory;

public interface SellerInventoryService {

    /**
     * 查看sku库存列表
     *
     * @param page
     * @param pageSize
     * @return
     */
    Page<SellerInventory> getPage(Long page, Long pageSize);
}