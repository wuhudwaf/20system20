package com.example.system.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.system.pojo.BuyerScore;
import com.example.system.pojo.page.NewPage;
import com.example.system.pojo.vo.BuyerUserBuyerScoreVo;

import java.util.List;

public interface BuyerScoreService {

    /**
     * 用户积分列表
     *
     * @param page
     * @param pageSize
     * @return
     */
    Page<BuyerScore> getPage(Long page, Long pageSize);

    /**
     * 积分明细
     *
     * @param newPage
     * @return
     */
    Page<BuyerUserBuyerScoreVo> buyerUserScore(NewPage newPage);
}