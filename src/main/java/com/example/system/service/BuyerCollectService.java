package com.example.system.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.system.pojo.BuyerCollect;

public interface BuyerCollectService {
    /**
     * 查询用户收藏列表
     *
     * @param page
     * @param pageSize
     * @return
     */
    Page<BuyerCollect> getList(Long page, Long pageSize);
}