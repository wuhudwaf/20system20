package com.example.system.service;

import com.example.system.dto.SysRoleDto;
import com.example.system.dto.SysRoleStatus;
import com.example.system.pojo.SysRole;

import java.util.List;

public interface SysRoleService {

    /**
     * 查询角色
     *
     * @return
     */
    List<SysRoleDto> getPage(Long page, Long pageSize);

    /**
     * 删除角色
     *
     * @param id
     * @return
     */
    void delete(String id);

    /**
     * 查看角色详情
     *
     * @param id
     * @return
     */
    SysRoleDto getInfo(String id);

    /**
     * 启用/关闭角色
     *
     * @param sysRoleStatus
     * @return
     */
    void setStatus(SysRoleStatus sysRoleStatus);

    /**
     * 新增角色
     *
     * @param sysRoleDto
     * @return
     */
    void add(SysRoleDto sysRoleDto);

    /**
     * 编辑角色
     *
     * @param sysRoleDto
     * @return
     */
    void set(SysRoleDto sysRoleDto);

    /**
     * 重置角色权限
     *
     * @return
     */
    void resetting(String id);

    /**
     * 查询所有角色，新增管理员时选择可以使用的角色
     *
     * @return
     */
    List<SysRole> getList();
}