package com.example.system.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.system.pojo.GoodsSku;

import javax.servlet.http.HttpServletRequest;

public interface GoodsSkuService {

    /**
     * 新增商品sku
     *
     * @param goodsSku
     * @return
     */
    void add( GoodsSku goodsSku);

    /**
     * 删除商品sku
     *
     * @param skuId
     * @return
     */
    void delete(String skuId);

    /**
     * 修改商品sku
     *
     * @param goodsSku
     * @return
     */
    void set(GoodsSku goodsSku);

    /**
     * 查看商品sku详情
     *
     * @param skuId
     * @return
     */
    GoodsSku getBySkuId(String skuId);

    /**
     * 商品sku列表（管理）
     *
     * @param page
     * @param pageSize
     * @return
     */
    Page<GoodsSku> getPage(Long page, Long pageSize);
}