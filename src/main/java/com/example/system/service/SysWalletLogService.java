package com.example.system.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.system.pojo.SysWalletLog;

public interface SysWalletLogService {

    /**
     * 门店id查询余额变动日志列表
     *
     * @param shopId
     * @param page
     * @param pageSize
     * @return
     */
    Page<SysWalletLog> getPage(String shopId,Long page, Long pageSize);
}