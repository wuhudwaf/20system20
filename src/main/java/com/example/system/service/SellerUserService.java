package com.example.system.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.system.dto.SellerUserDto;
import com.example.system.dto.SetInfoSellerUser;
import com.example.system.pojo.SellerUser;
import com.example.system.dto.SetPwd;


public interface SellerUserService {

    /**
     * 新增门店员工
     *
     * @param sellerUser
     * @return
     */
    String add(SellerUser sellerUser);

    /**
     * 冻结
     *
     * @param id
     * @return
     */
    void delete(String id);

    /**
     * 解冻
     *
     * @param id
     * @return
     */
    void set(String id);

    /**
     * 数据回显 (修改门店员工信息)
     *
     * @param id
     * @return
     */
    SellerUserDto get(String id);

    /**
     * 修改门店员工
     *
     * @param setInfoSellerUser
     * @return
     */
    void setInfo(SetInfoSellerUser setInfoSellerUser);

    /**
     * 查询，根据手机号查询
     *
     * @param phone
     * @return
     */
    SellerUserDto getByPhone(String phone);

    /**
     * 查询某一门店的员工
     *
     * @param shopId
     * @param page
     * @param pageSize
     * @return
     */
    Page<SellerUserDto> getList(String shopId, Long page, Long pageSize);

    /**
     * 重置密码
     *
     * @param setPwd
     * @return
     */
    String setPwd(SetPwd setPwd);
}
