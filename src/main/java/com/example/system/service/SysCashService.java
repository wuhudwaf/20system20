package com.example.system.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.system.pojo.SysCash;

public interface SysCashService {

    /**
     * 提现申请列表
     *
     * @param page
     * @param pageSize
     * @return
     */
    Page<SysCash> getPage(Long page, Long pageSize);

    /**
     * 新增提现申请
     *
     * @param sysCash
     * @return
     */
    void addSysCash(SysCash sysCash);

    /**
     * 同意申请/申请驳回
     *
     * @param id
     * @param status
     * @return
     */
    void deleteSysCash(String id,Integer status);

    /**
     * 提现申请详情
     *
     * @param id
     * @return
     */
    SysCash getBySysCashId(String id);

    /**
     * 测试
     *
     * @param page
     * @param pageSize
     * @return
     */
    Page<SysCash> getPageTest(Long page, Long pageSize);
}