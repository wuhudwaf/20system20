package com.example.system.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.system.pojo.SellerUser;
import com.example.system.pojo.SysShop;
import com.example.system.pojo.export.ExportSysShop;
import com.example.system.pojo.vo.add.SysShopAdd;

import java.util.List;


public interface SysShopService {
    /**
     * 新增门店
     *
     * @param sysShop
     * @return
     */
    void add(SysShop sysShop);

    /**
     * 删除门店
     *
     * @param shopId
     * @return
     */
    void delete(String shopId);

    /**
     * 获取门店详情
     *
     * @return
     */
    SysShop getByShopId(String shopId);

    /**
     * 获取门店列表
     *
     * @return
     */
    Page<SysShop> getList(Long page, Long pageSize);

    /**
     * 修改门店信息
     *
     * @param sysShop
     * @return
     */
    void update(SysShop sysShop);

    /**
     * 查询指定门店未审批员工列表
     *
     * @param shopId
     * @param page
     * @param pageSize
     * @return
     */
    Page<SellerUser> byShopIdSelectSellerUserId(String shopId, Long page, Long pageSize);

    /**
     * 批量设置门店状态(1：开店，0：闭店)
     *
     * @param shops
     * @return
     */
    void setStatusList(List<SysShop> shops);

    /**
     * 批量设置门店营业时间
     *
     * @param shops
     * @return
     */
    void setStartTimeAndEndTimeList(List<SysShop> shops);

    /**
     * 门店导出
     *
     * @return
     */
    List<SysShop> diyGetSysGoodsList();

    /**
     * 商品导出(页数)
     *
     * @param page
     * @return
     */
    List<SysShop> diyGetSysShopPage(com.example.system.pojo.page.Page page);

    /**
     * 批量更新门店
     *
     * @param sysShopAdds
     */
    void insertBatchSomeColumn(List<SysShopAdd> sysShopAdds);

    /**
     * 门店导入
     *
     * @param sysShopAdds
     */
    void importShop(List<SysShopAdd> sysShopAdds);

    List<ExportSysShop> diyGetSysShopListNew();
}
