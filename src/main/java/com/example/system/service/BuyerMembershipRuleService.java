package com.example.system.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.system.pojo.BuyerMembershipRule;
import com.example.system.pojo.BuyerUser;

import javax.validation.constraints.NotNull;
import java.util.List;

public interface BuyerMembershipRuleService {

    /**
     * 添加会员规则
     *
     * @param buyerMembershipRule
     * @return
     */
    void add(BuyerMembershipRule buyerMembershipRule);

    /**
     * 删除会员规则
     *
     * @param ruleId
     * @return
     */
    void delete(String ruleId);

    /**
     * 修改会员规则
     *
     * @param buyerMembershipRule
     * @return
     */
    void set(BuyerMembershipRule buyerMembershipRule);

    /**
     * 查看详情
     *
     * @param ruleId
     * @return
     */
    BuyerMembershipRule getInfo(String ruleId);

    /**
     * 会员等级列表
     *
     * @return
     */
    List<BuyerMembershipRule> getList();


    Page<BuyerUser> getUserBuyerMembershipRulePage(Long page, Long pageSize);
}