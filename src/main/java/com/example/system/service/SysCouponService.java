package com.example.system.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.system.dto.SysCouponDto;
import com.example.system.pojo.SysCoupon;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public interface SysCouponService {

    /**
     * 添加优惠券
     *
     * @param sysCouponDto
     * @return
     */
    void add(SysCouponDto sysCouponDto);

    /**
     * 删除优惠券
     *
     * @param couponId
     * @return
     */
    void delete(String couponId);

    /**
     * 修改优惠券
     *
     * @param sysCouponDto
     * @return
     */
    void update(SysCouponDto sysCouponDto);

    /**
     * 分页查询优惠券
     *
     * @param page
     * @param pageSize
     * @return
     */
    Page<SysCouponDto> getPage(Long page, Long pageSize);

    /**
     * 查看优惠券详情
     *
     * @param couponId
     * @return
     */
    SysCouponDto get(String couponId);

    /**
     * 批量设置上/下架
     *
     * @param sysCoupons
     * @return
     */
    void setStatusList(List<SysCoupon> sysCoupons);
}