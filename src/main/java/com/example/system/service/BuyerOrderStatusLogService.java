package com.example.system.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.system.pojo.BuyerOrderStatusLog;

public interface BuyerOrderStatusLogService {

    /**
     * 订单状态变化列表
     *
     * @param page
     * @param pageSize
     * @return
     */
    Page<BuyerOrderStatusLog> getPage(Long page, Long pageSize);
}