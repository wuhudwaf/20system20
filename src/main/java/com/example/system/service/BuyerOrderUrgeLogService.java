package com.example.system.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.system.pojo.BuyerOrderUrgeLog;

public interface BuyerOrderUrgeLogService {

    /**
     * 买家订单催促日志列表
     *
     * @param page
     * @param pageSize
     * @return
     */
    Page<BuyerOrderUrgeLog> getPage(Long page, Long pageSize);
}