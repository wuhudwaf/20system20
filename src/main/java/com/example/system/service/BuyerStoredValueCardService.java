package com.example.system.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.system.pojo.BuyerStoredValueCard;

public interface BuyerStoredValueCardService {

    /**
     * 用户储值卡记录列表
     * @param page
     * @param pageSize
     * @return
     */
    Page<BuyerStoredValueCard> getPage(Long page, Long pageSize);
}