package com.example.system.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.system.pojo.GoodsQueryLog;

public interface GoodsQueryLogService {

    /**
     * 商品搜索记录列表
     *
     * @param page
     * @param pageSize
     * @return
     */
    Page<GoodsQueryLog> getPage(Long page, Long pageSize);
}