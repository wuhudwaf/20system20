package com.example.system.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.system.pojo.SysNotice;

import javax.servlet.http.HttpServletRequest;

public interface SysNoticeService {

    /**
     * 新增通知公告
     *
     * @param sysNotice
     * @return
     */
    void add(SysNotice sysNotice);

    /**
     * 删除指定的通知公告
     *
     * @param noticeId
     * @return
     */
    void delete(String noticeId);

    /**
     * 修改指定的通知公告
     *
     * @param sysNotice
     * @return
     */
    void update(SysNotice sysNotice);

    /**
     * 获取通知公告列表
     *
     * @return
     */
    Page<SysNotice> getList(Long page, Long pageSize);

    /**
     * 获取通知公告详情
     *
     * @param noticeId
     * @return
     */
    SysNotice get(String noticeId);
}