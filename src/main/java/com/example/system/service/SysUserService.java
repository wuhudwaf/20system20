package com.example.system.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.system.dto.AddSysUser;
import com.example.system.dto.LogInUser;
import com.example.system.dto.SysUserDto;
import com.example.system.pojo.SysUser;

import javax.servlet.http.HttpServletRequest;

public interface SysUserService {

    /**
     * 登录
     *
     * @param logInUser
     * @return
     */
    String login(LogInUser logInUser);

    /**
     * 退出登录
     *
     * @return
     */
    void exit();

    /**
     * 获取所有的门店管理员
     *
     * @return
     */
    Page<SysUserDto> getShopUser(Long page, Long pageSize);

    /**
     * 新增管理员
     *
     * @param addSysUser
     * @return
     */
    void add(AddSysUser addSysUser);

    /**
     * 查看我的信息
     *
     * @return
     */
    SysUserDto getMyInfo();
}
