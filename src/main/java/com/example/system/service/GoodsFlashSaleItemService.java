package com.example.system.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.system.pojo.GoodsFlashSaleItem;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public interface GoodsFlashSaleItemService {

    /**
     * 秒杀和商品sku关联接口
     *
     * @param goodsFlashSaleItem
     * @return
     */
    void addGoodsFlashSaleItem(GoodsFlashSaleItem goodsFlashSaleItem);

    /**
     * 删除一个秒杀和商品sku关联
     *
     * @param skuId
     * @return
     */
    void deleteGoodsFlashSaleItem(String skuId);

    /**
     * 修改商品限时秒杀项目信息
     *
     * @param goodsFlashSaleItem
     * @return
     */
    void updateGoodsFlashSaleItem(GoodsFlashSaleItem goodsFlashSaleItem);

    /**
     * 根据itemId获取商品限时秒杀项目信息
     *
     * @param itemId
     * @return
     */
    GoodsFlashSaleItem getGoodsFlashSaleItem(String itemId);

    /**
     * 获取商品限时秒杀项目信息列表
     *
     * @param page
     * @param pageSize
     * @return
     */
    Page<GoodsFlashSaleItem> getGoodsFlashSaleItemPage(Long page, Long pageSize);
}