package com.example.system.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.system.pojo.SellerGoods;

public interface SellerGoodsService {

    /**
     * 查询门店商品列表
     *
     * @param page
     * @param pageSize
     * @return
     */
    Page<SellerGoods> getPage(String shopId,Long page, Long pageSize);
}