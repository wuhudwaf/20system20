package com.example.system.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.system.pojo.BuyerOrder;

public interface BuyerOrderService {

    /**
     * 查询订单
     *
     * @param page
     * @param pageSize
     * @return
     */
    Page<BuyerOrder> getPage(Long page, Long pageSize);

    /**
     * 取消订单
     *
     * @param orderId
     * @return
     */
    void setOrderStatus(String orderId);

    /**
     * 退款记录
     *
     * @param page
     * @return
     */
    Page<BuyerOrder> getRefundLog(com.example.system.pojo.page.Page page);
}