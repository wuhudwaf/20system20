package com.example.system.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.system.pojo.GoodsSku;
import com.example.system.pojo.OrderReviews;
import com.example.system.pojo.SysGoods;
import com.example.system.pojo.imports.SysGoodsImport;
import com.example.system.pojo.page.NewPage;
import com.example.system.pojo.vo.add.SysGoodsAdd;
import com.example.system.pojo.where.SysGoodsQuery;

import java.text.ParseException;
import java.util.List;

public interface SysGoodsService {

    /**
     * 新增商品
     *
     * @param sysGoodsAdd
     * @return
     */
    void add(SysGoodsAdd sysGoodsAdd);

    /**
     * 删除商品
     *
     * @param goodsId
     * @return
     */
    void delete(String goodsId);

    /**
     * 修改商品
     *
     * @param sysGoods
     * @return
     */
    void set(SysGoodsAdd sysGoods, GoodsSkuService goodsSkuService);

    /**
     * 查看商品详情(商品携带当前商品的sku列表)
     *
     * @param goodsId
     * @return
     */
    SysGoods getByGoodsId(String goodsId);

    /**
     * 查看商品列表(每个商品携带当前商品的sku列表)
     *
     * @return
     */
    Page<SysGoods> getByGoodsPage(SysGoodsQuery sysGoodsQuery);

    /**
     * 查询指定分类的商品
     *
     * @param cateId
     * @return
     */
    List<SysGoods> categoryIdSelectGoodsList(String cateId);

    /**
     * 获取指定商品的sku对象列表
     *
     * @param goodsId
     * @return
     */
    List<GoodsSku> getGoodsSku(String goodsId);

    /**
     * 放入仓库/上架销售：1放入仓库, 2上架销售
     *
     * @param goodsId
     * @return
     */
    void setStatus(String goodsId);

    /**
     * 筛选分页查询
     *
     * @param page
     * @param pageSize
     * @param name
     * @param cateId
     * @return
     */
    Page<SysGoods> getSysGoodsPageNew(Long page, Long pageSize, String name, String cateId);

    List<SysGoodsImport> diyGetSysGoodsList();

    List<SysGoodsImport> diyGetSysGoodsPage(Integer pageOne, Integer pageTwo, Integer pageSize);

    /**
     * 商品导入
     *
     * @return
     */
    void autoAddSysGoodsList(List<SysGoods> sysGoods, List<GoodsSku> goodsSkus);

    /**
     * 商品导入
     *
     * @param sysGoodsImports
     */
    void autoAddSysGoodsListTwo(List<SysGoodsImport> sysGoodsImports) throws ParseException;

    /**
     * 商品评价
     *
     * @return
     */
    List<OrderReviews> getGoodsEvaluate(NewPage newPage);

    /**
     * 商品海报
     *
     * @param goodsId
     * @return
     */
    String getPoster(Long goodsId);
}