package com.example.system.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.system.dto.InterfaceDto;
import com.example.system.dto.SysRoleDto;
import com.example.system.dto.SysRoleStatus;
import com.example.system.exception.CommonException;
import com.example.system.mapper.SysInterfaceMapper;
import com.example.system.mapper.SysInterfaceRoleMapper;
import com.example.system.mapper.SysRoleMapper;
import com.example.system.pojo.SysInterfaceRole;
import com.example.system.pojo.SysRole;
import com.example.system.service.SysRoleService;
import com.example.system.utils.GetUser;
import com.example.system.utils.RedisIdWorker;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

import static com.example.system.utils.Constants.*;


@Service
@RequiredArgsConstructor
@SuppressWarnings("all")
public class SysRoleServiceImpl implements SysRoleService {

    private final SysRoleMapper sysRoleMapper;

    private final SysInterfaceRoleMapper sysInterfaceRoleMapper;

    private final SysInterfaceMapper sysInterfaceMapper;

    private final RedisIdWorker redisIdWorker;

    /**
     * 查询角色
     *
     * @return
     */
    @Override
    public List<SysRoleDto> getPage(Long page, Long pageSize) {
        // interfaceDtos为SysRoleDto中的一个属性
        List<InterfaceDto> interfaceDtos = null;
        // 去MySQL查询
        LambdaQueryWrapper<SysRole> sysRoleLambdaQueryWrapper = Wrappers.lambdaQuery(SysRole.class);
        sysRoleLambdaQueryWrapper.orderByAsc(SysRole::getStatus);
        Page<SysRole> sysRolePage = sysRoleMapper.selectPage(new Page<SysRole>(page, pageSize), sysRoleLambdaQueryWrapper);
        // 角色列表
        List<SysRole> sysRoles = sysRolePage.getRecords();
        // 权限角色关系列表
        List<SysInterfaceRole> sysInterfaceRoles = sysInterfaceRoleMapper.getList();
        // 权限列表
        List<InterfaceDto> urlAll = GetUser.getUserDto().getUrlAll();
        // 最终返回得数据
        ArrayList<SysRoleDto> sysRoleDtos = new ArrayList<>();
        for (SysRole sysRole : sysRoles) {
            interfaceDtos = new ArrayList<>();
            for (SysInterfaceRole sysInterfaceRole : sysInterfaceRoles) {
                if (sysRole.getId().equals(sysInterfaceRole.getRoleId())) {
                    // 该角色对应的权限id，可能有多个
                    String menuId = sysInterfaceRole.getMenuId();
                    for (InterfaceDto interfaceDto : urlAll) {
                        if (interfaceDto.getId().equals(menuId)) {
                            interfaceDtos.add(interfaceDto);
                        }
                    }
                }
            }
            SysRoleDto sysRoleDto = BeanUtil.copyProperties(sysRole, SysRoleDto.class);
            sysRoleDto.setInterfaceDto(interfaceDtos);
            sysRoleDtos.add(sysRoleDto);
        }
        // sysRoleDtos封装好的数据
        return sysRoleDtos;
    }

    /**
     * 删除角色
     *
     * @param id
     * @return
     */
    @Override
    @Transactional // 事务管理
    public void delete(String id) {
        if (id.equals(ONE)) {
            throw new CommonException("超级管理员无法被删除", "无法删除超级管理员");
        }
        // 去MySQL查询，
        SysRole sysRole = sysRoleMapper.selectById(id);
        if (sysRole == null) {
            throw new CommonException("删除角色", "角色不存在");
        }
        // 有数据,执行删除
        sysRoleMapper.deleteById(id);
        // 删除角色对应权限
        sysInterfaceRoleMapper.deleteByRoleId(id);
    }

    /**
     * 查看角色详情
     *
     * @param id
     * @return
     */
    @Override
    public SysRoleDto getInfo(String id) {
        SysRole sysRole = null;
        // 去MySQL查询，
        sysRole = sysRoleMapper.selectById(id);
        if (sysRole == null) {
            throw new CommonException("查看角色详情", "角色不存在");
        }
        // 角色存在，根据角色获取权限id，（角色权限关系）
        List<SysInterfaceRole> byRoleId = sysInterfaceRoleMapper.getByRoleId(id);
        // 拿到所有权限
        List<InterfaceDto> urlAll = GetUser.getUserDto().getUrlAll();
        // 该角色拥有的权限 interfaceDtos
        ArrayList<InterfaceDto> interfaceDtos = new ArrayList<>();
        for (SysInterfaceRole sysInterfaceRole : byRoleId) {
            String menuId = sysInterfaceRole.getMenuId();
            for (InterfaceDto interfaceDto : urlAll) {
                if (menuId.equals(interfaceDto.getId())) {
                    interfaceDtos.add(interfaceDto);
                }
            }
        }
        SysRoleDto sysRoleDto = BeanUtil.copyProperties(sysRole, SysRoleDto.class);
        sysRoleDto.setInterfaceDto(interfaceDtos);
        return sysRoleDto;
    }

    /**
     * 启用/关闭角色
     *
     * @param sysRoleStatus
     * @return
     */
    @Override
    public void setStatus(SysRoleStatus sysRoleStatus) {
        SysRole sysRole = BeanUtil.copyProperties(sysRoleStatus, SysRole.class);
        if (sysRoleMapper.updateById(sysRole) <= 0) {
            throw new CommonException("启用/关闭角色", "无效的操作");
        }
    }

    /**
     * 新增角色
     *
     * @param sysRoleDto
     * @return
     */
    @Override
    @Transactional // 事务管理
    public void add(SysRoleDto sysRoleDto) {
        // 角色
        SysRole sysRole = BeanUtil.copyProperties(sysRoleDto, SysRole.class);
        // 角色id
        String id = redisIdWorker.nextId("roledto");
        sysRole.setId(id);
        sysRole.setStatus(1);
        // 新增角色
        sysRoleMapper.insert(sysRole);
        // 新增角色权限关系
        List<InterfaceDto> interfaceDto = sysRoleDto.getInterfaceDto();
        for (InterfaceDto dto : interfaceDto) {
            sysInterfaceRoleMapper.insert(new SysInterfaceRole(id, dto.getId()));
        }
    }

    /**
     * 编辑角色
     *
     * @param sysRoleDto
     * @return
     */
    @Override
    @Transactional // 事务管理
    public void set(SysRoleDto sysRoleDto) {
        SysRole sysRole = sysRoleMapper.selectById(sysRoleDto.getId());
        if (sysRole == null) {
            throw new CommonException("编辑角色", "角色不存在");
        }
        // 根据id修改角色
        sysRoleMapper.updateById(BeanUtil.copyProperties(sysRoleDto, SysRole.class));
        // 该角色的新权限
        List<InterfaceDto> interfaceDto = sysRoleDto.getInterfaceDto();
        if (interfaceDto.size() != 0) {
            // 确实需要更新权限,删除该角色对应的权限关系，重新构建权限
            LambdaQueryWrapper<SysInterfaceRole> sysInterfaceRoleLambdaQueryWrapper = new LambdaQueryWrapper<>();
            sysInterfaceRoleLambdaQueryWrapper.eq(SysInterfaceRole::getRoleId, sysRoleDto.getId());
            sysInterfaceRoleMapper.delete(sysInterfaceRoleLambdaQueryWrapper);
            for (InterfaceDto dto : interfaceDto) {
                // 新增角色权限关系
                sysInterfaceRoleMapper.insert(new SysInterfaceRole(sysRoleDto.getId(), dto.getId()));
            }
        }
    }

    /**
     * 重置角色权限
     *
     * @return
     */
    @Override
    public void resetting(String id) {
        LambdaQueryWrapper<SysInterfaceRole> sysInterfaceRoleLambdaQueryWrapper = Wrappers.lambdaQuery(SysInterfaceRole.class);
        sysInterfaceRoleLambdaQueryWrapper.eq(SysInterfaceRole::getRoleId, id);
        if (sysInterfaceRoleMapper.delete(sysInterfaceRoleLambdaQueryWrapper) <= 0) {
            throw new CommonException("重置角色权限", "网络异常，请稍后再试");
        }
    }

    /**
     * 查询所有角色，新增管理员时选择可以使用的角色
     *
     * @return
     */
    @Override
    public List<SysRole> getList() {
        LambdaQueryWrapper<SysRole> sysRoleLambdaQueryWrapper = Wrappers.lambdaQuery(SysRole.class);
        sysRoleLambdaQueryWrapper.eq(SysRole::getStatus, 1);
        return sysRoleMapper.selectList(sysRoleLambdaQueryWrapper);
    }
}