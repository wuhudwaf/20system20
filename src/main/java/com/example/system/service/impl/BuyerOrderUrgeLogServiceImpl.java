package com.example.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.system.exception.CommonException;
import com.example.system.mapper.BuyerOrderUrgeLogMapper;
import com.example.system.pojo.BuyerOrderStatusLog;
import com.example.system.pojo.BuyerOrderUrgeLog;
import com.example.system.service.BuyerOrderUrgeLogService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@SuppressWarnings("all")
public class BuyerOrderUrgeLogServiceImpl implements BuyerOrderUrgeLogService {

    private final BuyerOrderUrgeLogMapper buyerOrderUrgeLogMapper;

    /**
     * 买家订单催促日志列表
     *
     * @param page
     * @param pageSize
     * @return
     */
    @Override
    public Page<BuyerOrderUrgeLog> getPage(Long page, Long pageSize) {
        LambdaQueryWrapper<BuyerOrderUrgeLog> buyerUserLambdaQueryWrapper = Wrappers.lambdaQuery(BuyerOrderUrgeLog.class);
        buyerUserLambdaQueryWrapper.orderByAsc(BuyerOrderUrgeLog::getCreateTime);
        Page<BuyerOrderUrgeLog> buyerOrderUrgeLogPage = buyerOrderUrgeLogMapper.selectPage(new Page<BuyerOrderUrgeLog>(page, pageSize), buyerUserLambdaQueryWrapper);
        return buyerOrderUrgeLogPage;
    }
}