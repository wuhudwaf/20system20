package com.example.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.example.system.exception.CommonException;
import com.example.system.mapper.SysCategoryMapper;
import com.example.system.pojo.SysCategory;
import com.example.system.service.SysCategoryService;
import com.example.system.utils.GetUser;
import com.example.system.utils.RedisIdWorker;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.*;

@Service
@RequiredArgsConstructor
@SuppressWarnings("all")
public class SysCategoryServiceImpl implements SysCategoryService {

    private final SysCategoryMapper sysCategoryMapper;

    private final RedisIdWorker redisIdWorker;

    /**
     * 添加商品类别
     *
     * @param sysCategory
     * @return
     */
    @Override
    public void add(SysCategory sysCategory) {
        sysCategory.setCateId(redisIdWorker.nextId("category"));
        sysCategory.setTenantId(String.valueOf(redisIdWorker.nextId("category")));
        sysCategory.setRevision(1);
        String id = GetUser.getUserId();
        sysCategory.setCreateBy(id);
        sysCategory.setCreateTime(LocalDateTime.now());
        sysCategory.setUpdateBy(id);
        sysCategory.setUpdateTime(LocalDateTime.now());
        sysCategory.setStatus(1);
        // 新增
        if (sysCategoryMapper.insert(sysCategory) <= 0) {
            throw new CommonException("添加商品类别", "网络异常，请稍后再试");
        }
    }

    /**
     * 废弃分类
     *
     * @param cateId
     * @return
     */
    @Override
    public void delete(String cateId) {
        // 修改前，若当前分类有子分类，则无法进行废除
        List<SysCategory> byParentId = sysCategoryMapper.getByParentId(cateId);
        if (byParentId.size() != 0) {
            throw new CommonException("删除分类", "当前分类存有子分类，无法删除");
        }
        SysCategory sysCategory = new SysCategory();
        sysCategory.setCateId(cateId);
        sysCategory.setStatus(2);
        if (sysCategoryMapper.updateById(sysCategory) <= 0) {
            throw new CommonException("删除分类", "网络异常，请稍后再试");
        }
    }

    /**
     * 修改分类
     *
     * @param sysCategory
     * @return
     */
    @Override
    public void set(SysCategory sysCategory) {
        LambdaQueryWrapper<SysCategory> sysCategoryLambdaQueryWrapper = Wrappers.lambdaQuery(SysCategory.class);
        sysCategoryLambdaQueryWrapper.eq(SysCategory::getCateId, sysCategory.getCateId());
        // 先查询
        SysCategory sqSysCategory = sysCategoryMapper.selectOne(sysCategoryLambdaQueryWrapper);
        if (sqSysCategory == null) {
            throw new CommonException("修改分类", "分类不存在");
        }
        // 更新乐观锁
        sysCategory.setRevision(sqSysCategory.getRevision() + 1);
        sysCategory.setUpdateBy(GetUser.getUserId());
        sysCategory.setUpdateTime(LocalDateTime.now());
        sysCategoryLambdaQueryWrapper.eq(SysCategory::getRevision, sqSysCategory.getRevision());
        // 修改分类
        if (sysCategoryMapper.update(sysCategory, sysCategoryLambdaQueryWrapper) <= 0) {
            throw new CommonException("修改分类", "访问人数过多，请稍后再试");
        }
    }

    /**
     * 分类详情
     *
     * @param cateId
     * @return
     */
    @Override
    public SysCategory getByCateId(String cateId) {
        LambdaQueryWrapper<SysCategory> sysCategoryLambdaQueryWrapper = new LambdaQueryWrapper<>();
        sysCategoryLambdaQueryWrapper.eq(SysCategory::getCateId, cateId);
        SysCategory sysCategory = sysCategoryMapper.selectOne(sysCategoryLambdaQueryWrapper);
        if (sysCategory == null) {
            throw new CommonException("分类详情 -->", "分类不存在");
        }
        return sysCategory;
    }

    /**
     * 查询分类列表
     *
     * @return
     */
    public List<SysCategory> getList() {
        // 准备拿到所有分类
        LambdaQueryWrapper<SysCategory> sysCategoryLambdaQueryWrapper = Wrappers.lambdaQuery(SysCategory.class);
        // 只需要可用的分类 --> 类别状态1-正常,2-已废弃
        sysCategoryLambdaQueryWrapper.eq(SysCategory::getStatus, 1);
        // 排序
        sysCategoryLambdaQueryWrapper.orderByAsc(SysCategory::getSortOrder);
        // 所有分类数据
        List<SysCategory> categoryList = sysCategoryMapper.selectList(sysCategoryLambdaQueryWrapper);
        // 根分类集合
        ArrayList<SysCategory> fatherSysCategoryList = new ArrayList<>();
        // 获取根分类
        for (SysCategory sysCategory : categoryList) {
            if (sysCategory.getParentId().equals("0")) {
                // 表示根分类
                fatherSysCategoryList.add(sysCategory);
            }
        }

        //递归
        recursionSelect(fatherSysCategoryList, categoryList);

        return fatherSysCategoryList;
    }

    /**
     * 递归查询
     *
     * @param fatherSysCategoryList 根分类
     * @param categoryList          所有分类
     */
    public void recursionSelect(List<SysCategory> fatherSysCategoryList, List<SysCategory> categoryList) {
        // 循环父分类
        for (SysCategory sysCategory : fatherSysCategoryList) {
            //  sysCategories集合是sysCategory的子分类
            ArrayList<SysCategory> sysCategories = new ArrayList<>();
            // 循环所有分类集合数据
            for (SysCategory category : categoryList) {
                // 判断category是否为sysCategory的子类
                if (sysCategory.getCateId().equals(category.getParentId())) {
                    // 是
                    sysCategories.add(category);
                }
                // 降序排序
                sysCategories.sort(Comparator.comparing(SysCategory::getSortOrder).reversed());
                sysCategory.setList(sysCategories);
                // 继续查
                recursionSelect(sysCategories, categoryList);
            }
        }
    }


    /**
     * 查询子分类（没有被废弃的）
     *
     * @param cateId
     * @return
     */
    @Override
    public List<SysCategory> getList(List<SysCategory> sysCategories, String cateId) {
        SysCategory byCateId = sysCategoryMapper.getByCateId(cateId);
        if (byCateId == null) {
            throw new CommonException("分类层级选择（没有被废弃的）", "选择的分类不存在");
        }
        // 查询子分类
        sysCategories = sysCategoryMapper.getByParentId(cateId);
        return sysCategories;
    }

    /**
     * 查询根节点分类
     *
     * @return
     */
    @Override
    public List<SysCategory> getRoot() {
        return sysCategoryMapper.getRoot();
    }
}