package com.example.system.service.impl;


import cn.hutool.json.JSONUtil;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.system.exception.CommonException;
import com.example.system.mapper.*;
import com.example.system.pojo.*;
import com.example.system.service.BuyerOrderService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.*;

@RequiredArgsConstructor
@Service
@SuppressWarnings("all")
public class BuyerOrderServiceImpl implements BuyerOrderService {

    /**
     * 订单
     */
    private final BuyerOrderMapper buyerOrderMapper;

    /**
     * 商品订单表
     */
    private final SellerOrderItemMapper sellerOrderItemMapper;

    /**
     * 商品
     */
    private final SysGoodsMapper sysGoodsMapper;

    /**
     * 商品sku
     */
    private final GoodsSkuMapper goodsSkuMapper;

    /**
     * 微信用户信息
     */
    private final BuyerUserMapper buyerUserMapper;

    /**
     * 门店
     */
    private final SysShopMapper sysShopMapper;

    /**
     * 商品限时秒杀
     */
    private final GoodsFlashSaleMapper goodsFlashSaleMapper;

    /**
     * 商品限时秒杀项目信息
     */
    private final GoodsFlashSaleItemMapper goodsFlashSaleItemMapper;

    /**
     * 查询订单
     *
     * @param page
     * @param pageSize
     * @return
     */
    @Override
    public Page<BuyerOrder> getPage(Long page, Long pageSize) {
        // 获取订单条件构造器
        LambdaQueryWrapper<BuyerOrder> buyerUserLambdaQueryWrapper = Wrappers.lambdaQuery(BuyerOrder.class);
        // 排序
        buyerUserLambdaQueryWrapper.orderByAsc(BuyerOrder::getCreateTime);
        // 获取订单分页对象
        Page<BuyerOrder> buyerUserPage = buyerOrderMapper.selectPage(new Page<BuyerOrder>(page, pageSize), buyerUserLambdaQueryWrapper);
        // 获取订单集合列表
        List<BuyerOrder> records = buyerUserPage.getRecords();
        if (records.size() == 0) {
            // 直接返回，没有订单数据就没有后续的数据，所以直接返回即可，避免报错
            return buyerUserPage;
        }
        // 根据 records获取下单人id
        TreeSet<String> userIds = new TreeSet<>();
        // 根据 records获取订单id
        TreeSet<String> orderids = new TreeSet<>();
        // 根据 records获取门店id
        TreeSet<String> shopId = new TreeSet<>();
        // 循环 订单集合列表添加下单人id和订单id
        for (BuyerOrder record : records) {
            userIds.add(record.getUserId());
            orderids.add(record.getOrderId());
            shopId.add(record.getShopId());
        }
        // 获取商品订单条件构造器
        LambdaQueryWrapper<SellerOrderItem> sellerOrderItemLambdaQueryWrapper = Wrappers.lambdaQuery(SellerOrderItem.class);
        // in OrderId
        sellerOrderItemLambdaQueryWrapper.in(SellerOrderItem::getOrderId, orderids);
        // 根据 orderids获取 商品订单列表
        List<SellerOrderItem> sellerOrderItems = sellerOrderItemMapper.selectList(sellerOrderItemLambdaQueryWrapper);
        // 根据 商品订单列表sellerOrderItems 获取商品id 和商品skuid
        TreeSet<String> goodsId = new TreeSet<>();
        TreeSet<String> skuId = new TreeSet<>();
        // 循环添加，循环的商品订单列表，其中包含商品和skuid
        for (SellerOrderItem sellerOrderItem : sellerOrderItems) {
            goodsId.add(sellerOrderItem.getGoodsId());
            skuId.add(sellerOrderItem.getSkuId());
        }
        // 根据 goodsId获取商品列表
        List<SysGoods> sysGoods = sysGoodsMapper.selectBatchIds(goodsId);
        // 根据 skuId列表获取sku对象列表
        List<GoodsSku> goodsSkus = goodsSkuMapper.selectBatchIds(skuId);
        // 拿到商品限时秒杀
        LambdaQueryWrapper<GoodsFlashSale> goodsFlashSaleLambdaQueryWrapper = Wrappers.lambdaQuery(GoodsFlashSale.class);
        goodsFlashSaleLambdaQueryWrapper.eq(GoodsFlashSale::getStatus, 1);
        GoodsFlashSale goodsFlashSale = goodsFlashSaleMapper.selectOne(goodsFlashSaleLambdaQueryWrapper);
        // 有秒杀才校验哪些sku是秒杀，若没秒杀则无需校验，另外是避免报错
        if (goodsFlashSale != null) {
            // 根据skuid列表获取商品限时秒杀项目信息列表
            LambdaQueryWrapper<GoodsFlashSaleItem> goodsFlashSaleItemLambdaQueryWrapper = Wrappers.lambdaQuery(GoodsFlashSaleItem.class);
            goodsFlashSaleItemLambdaQueryWrapper.eq(GoodsFlashSaleItem::getFlashSaleId, goodsFlashSale.getFlashSaleId());
            goodsFlashSaleItemLambdaQueryWrapper.in(GoodsFlashSaleItem::getGoodsSkuId, skuId);
            // 设置为秒杀的sku对应的商品限时秒杀项目信息
            List<GoodsFlashSaleItem> goodsFlashSaleItems = goodsFlashSaleItemMapper.selectList(goodsFlashSaleItemLambdaQueryWrapper);
            // 判断每个sku是否为秒杀，并且修改sku状态值和秒杀价格
            for (GoodsFlashSaleItem goodsFlashSaleItem : goodsFlashSaleItems) {
                for (GoodsSku skus : goodsSkus) {
                    if (goodsFlashSaleItem.getGoodsSkuId().equals(skus.getSkuId())) {
                        skus.setStatus(1);
                        skus.setFlashSalePrice(goodsFlashSaleItem.getFlashSalePrice());
                    }
                }
            }
        }
        int i = 0;
        // 遍历商品订单列表,封装商品sku到商品对象中
        for (SellerOrderItem sellerOrderItem : sellerOrderItems) {
            // 遍历商品列表
            for (SysGoods sysGood : sysGoods) {
                i += 1;
//                List<GoodsSku> goodsSkus1 = sysGood.getGoodsSkus();
                // 遍历商品sku列表
                for (GoodsSku skus : goodsSkus) {
                    if (i == 1) {
                        skus.setSkuValJson(JSONObject.parseObject(skus.getSkuVal()));
                        skus.setSkuVal("");
                    }
                    // 商品对应的sku
                    if (sellerOrderItem.getGoodsId().equals(sysGood.getGoodsId()) && sellerOrderItem.getSkuId().equals(skus.getSkuId())) {
                        // 商品对应的sku
                        sysGood.setGoodsSkuObj(skus);
                        break;
                    }
                }
            }
        }
        // 遍历商品订单列表,封装商品到订单对象中
        for (SellerOrderItem sellerOrderItem : sellerOrderItems) {
            // 遍历订单列表
            for (BuyerOrder record : records) {
                // 遍历商品列表
                for (SysGoods sysGood : sysGoods) {
                    if (sysGood.getSellerOrderItemId() != null) {
                        continue;
                    }
                    // 订单对应的商品
                    if (sellerOrderItem.getOrderId().equals(record.getOrderId())
                            && sellerOrderItem.getGoodsId().equals(sysGood.getGoodsId())) {
                        // 商品订单数据中存储商品数据
                        sellerOrderItem.setSysGoods(sysGood);
                        // 订单中存储当前商品订单数据
                        List<SellerOrderItem> sellerOrderItemsResult = record.getSellerOrderItems();
                        // 添加商品订单列表
                        sellerOrderItemsResult.add(sellerOrderItem);
                        // 再将列表存储回去
                        record.setSellerOrderItems(sellerOrderItemsResult);
                    }
                }
            }
        }
        // 根据 userIds获取下单人对象集合
        List<BuyerUser> buyerUsers = buyerUserMapper.selectBatchIds(userIds);
        // 遍历商品订单列表，将下单人对象封装到订单对象中
        for (SellerOrderItem sellerOrderItem : sellerOrderItems) {
            // 遍历订单列表
            for (BuyerOrder record : records) {
                // 遍历 buyerUsers下单人对象
                for (BuyerUser buyerUser : buyerUsers) {
                    // 订单对应的下单人
                    if (sellerOrderItem.getOrderId().equals(record.getOrderId()) && sellerOrderItem.getUserId().equals(buyerUser.getUserId())) {
                        // 将下单人对象存储到订单中
                        record.setBuyerUser(buyerUser);
                    }
                }
            }
        }
        // 根据 shopId获取门店列表
        List<SysShop> sysShops = sysShopMapper.selectBatchIds(shopId);
        // 遍历商品订单列表，将门店对象封装到订单对象中
        for (SellerOrderItem sellerOrderItem : sellerOrderItems) {
            // 遍历订单列表
            for (BuyerOrder record : records) {
                // 遍历门店对象
                for (SysShop sysShop : sysShops) {
                    // 订单对应的门店
                    if (sellerOrderItem.getOrderId().equals(record.getOrderId()) && sellerOrderItem.getShopId().equals(sysShop.getShopId())) {
                        // 将门店对象存储到订单中
                        Set<SysShop> sysShopSet = record.getSysShopSet();
                        sysShopSet.add(sysShop);
                        record.setSysShopSet(sysShopSet);
                    }
                }
            }
        }
        int count = 0;
        for (BuyerOrder record : records) {
            for (SellerOrderItem sellerOrderItem : record.getSellerOrderItems()) {
                Integer quantity = sellerOrderItem.getQuantity();
                count += quantity;
            }
            record.setQuantity(count);
            count = 0;
        }
        // 将新的订单列表数据封装到page对象中
        buyerUserPage.setRecords(records);
        // 返回分页对象
        return buyerUserPage;
    }

    /**
     * 取消订单
     * 可以取消的： 1，3，4
     * 不能取消的： 2，5，6，7，0
     *
     * @param orderId
     * @return
     */
    @Override
    public void setOrderStatus(String orderId) {
        LambdaQueryWrapper<BuyerOrder> buyerOrderLambdaQueryWrapper = Wrappers.lambdaQuery(BuyerOrder.class);
        buyerOrderLambdaQueryWrapper.eq(BuyerOrder::getOrderId, orderId);
        BuyerOrder buyerOrder = buyerOrderMapper.selectOne(buyerOrderLambdaQueryWrapper);
        if (buyerOrder == null) throw new CommonException("取消订单", "订单不存在");
        // 获取当前订单状态
        int orderStatus = buyerOrder.getOrderStatus();
        if (orderStatus == 2 || orderStatus == 5 || orderStatus == 6 || orderStatus == 7 || orderStatus == 0) {
            // 表示不能取消
            throw new CommonException("取消订单", "该订单不可取消");
        }
        if (orderStatus == 99) {
            // 已经取消的订单
            throw new CommonException("取消订单", "该订单已取消，请忽重复点击");
        }
        // 可以取消的订单
        if (orderStatus == 3 || orderStatus == 4) {
            // 表示已付款，需要进行退款
        }
        // 修改状态值
        buyerOrder.setOrderStatus(99);
        // 更新
        buyerOrderMapper.updateById(buyerOrder);
    }

    /**
     * 退款记录
     *
     * @param page
     * @return
     */
    @Override
    public Page<BuyerOrder> getRefundLog(com.example.system.pojo.page.Page page) {
        LambdaQueryWrapper<BuyerOrder> buyerOrderLambdaQueryWrapper = Wrappers.lambdaQuery(BuyerOrder.class);
        buyerOrderLambdaQueryWrapper.orderByAsc(BuyerOrder::getCreateTime);
        buyerOrderLambdaQueryWrapper.isNotNull(BuyerOrder::getRefundTime);
        Page<BuyerOrder> buyerOrderPage = buyerOrderMapper.selectPage(new Page<BuyerOrder>(page.getPage(), page.getPageSize()), buyerOrderLambdaQueryWrapper);
        return buyerOrderPage;
    }
}