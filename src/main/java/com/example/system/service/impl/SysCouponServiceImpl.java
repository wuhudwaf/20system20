package com.example.system.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.RandomUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.system.dto.SysCouponDto;
import com.example.system.exception.CommonException;
import com.example.system.mapper.SysCouponMapper;
import com.example.system.pojo.SysCoupon;
import com.example.system.service.SysCouponService;
import com.example.system.utils.GetUser;
import com.example.system.utils.RedisIdWorker;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
@SuppressWarnings("all")
public class SysCouponServiceImpl extends ServiceImpl<SysCouponMapper, SysCoupon> implements SysCouponService {

    private final SysCouponMapper sysCouponMapper;

    private final RedisIdWorker redisIdWorker;

    /**
     * 添加优惠券
     *
     * @param sysCoupon
     * @return
     */
    @Override
    public void add(SysCouponDto sysCouponDto) {
        // 拷贝对象，拿到SysCoupon对象，忽略开始时间和结束时间
        SysCoupon sysCoupon = BeanUtil.copyProperties(sysCouponDto, SysCoupon.class, "startTime", "endTime");
        // 将sysCouponDto中时间戳转换为localdatetime并存储到sysCoupon对象中
        Long startTime = sysCouponDto.getStartTime();
        Long endTime = sysCouponDto.getEndTime();
        sysCoupon.setStartTime(LocalDateTime.ofInstant(Instant.ofEpochMilli(startTime), ZoneId.systemDefault()));
        sysCoupon.setEndTime(LocalDateTime.ofInstant(Instant.ofEpochMilli(endTime), ZoneId.systemDefault()));
        if (sysCoupon.getAllowRedeemed().equals(1) && sysCoupon.getRedeemNeedScore() == null) {
            throw new CommonException("添加优惠券", "请设置兑换所需积分");
        }
        if (sysCoupon.getAllowRedeemed().equals(0)) {
            sysCoupon.setRedeemNeedScore(null);
        }
        // 登录者id
        String id = GetUser.getUserId();
        // 当前时间
        LocalDateTime now = LocalDateTime.now();
        // 优惠券类型
        sysCoupon.setCouponType("1");
        // 主键
        sysCoupon.setCouponId(redisIdWorker.nextId("coupon"));
        // 租户号
        sysCoupon.setTenantId(String.valueOf(redisIdWorker.nextId("coupon")));
        // 乐观锁
        sysCoupon.setRevision(RandomUtil.randomInt());
        // 创建人
        sysCoupon.setCreateBy(id);
        // 创建时间
        sysCoupon.setCreateTime(now);
        // 更新人
        sysCoupon.setUpdateBy(id);
        // 更新时间
        sysCoupon.setUpdateTime(now);
        // 新增
        if (sysCouponMapper.insert(sysCoupon) <= 0) {
            throw new CommonException("添加优惠券", "网络异常，请稍后再试");
        }
    }

    /**
     * 删除优惠券
     *
     * @param couponId
     * @return
     */
    @Override
    public void delete(String couponId) {
        LambdaQueryWrapper<SysCoupon> sysCouponLambdaQueryWrapper = Wrappers.lambdaQuery(SysCoupon.class);
        sysCouponLambdaQueryWrapper.eq(SysCoupon::getCouponId, couponId);

        SysCoupon sysCoupon = sysCouponMapper.selectOne(sysCouponLambdaQueryWrapper);
        if (sysCoupon == null) {
            throw new CommonException("删除优惠券", "优惠券不存在");
        }

        if (sysCouponMapper.delete(sysCouponLambdaQueryWrapper) <= 0) {
            throw new CommonException("删除优惠券", "网络异常，请稍后再试");
        }
    }

    /**
     * 修改优惠券
     *
     * @param sysCoupon
     * @return
     */
    @Override
    public void update(SysCouponDto sysCouponDto) {
        // 拷贝对象，拿到SysCoupon对象，忽略开始时间和结束时间
        SysCoupon sysCoupon = BeanUtil.copyProperties(sysCouponDto, SysCoupon.class, "startTime", "endTime");
        // 将sysCouponDto中时间戳转换为localdatetime并存储到sysCoupon对象中
        Long startTime = sysCouponDto.getStartTime();
        Long endTime = sysCouponDto.getEndTime();
        sysCoupon.setStartTime(LocalDateTime.ofInstant(Instant.ofEpochMilli(startTime), ZoneId.systemDefault()));
        sysCoupon.setEndTime(LocalDateTime.ofInstant(Instant.ofEpochMilli(endTime), ZoneId.systemDefault()));

        if (sysCoupon.getAllowRedeemed().equals(1) && sysCoupon.getRedeemNeedScore() == null) {
            throw new CommonException("修改优惠券", "请设置兑换所需积分");
        }
        if (sysCoupon.getAllowRedeemed().equals(0)) {
            sysCoupon.setRedeemNeedScore(null);
        }
        LambdaQueryWrapper<SysCoupon> sysCouponLambdaQueryWrapper = Wrappers.lambdaQuery(SysCoupon.class);
        sysCouponLambdaQueryWrapper.eq(SysCoupon::getCouponId, sysCoupon.getCouponId());
        // 乐观锁
        SysCoupon sysCoupon1 = sysCouponMapper.selectOne(sysCouponLambdaQueryWrapper);
        if (sysCoupon1 == null) {
            throw new CommonException("修改优惠券", "优惠券不存在");
        }
        // 更新数据
        sysCoupon.setUpdateBy(GetUser.getUserId());
        sysCoupon.setUpdateTime(LocalDateTime.now());
        sysCoupon.setRevision(RandomUtil.randomInt());
        // 更新
        LambdaQueryWrapper<SysCoupon> sysCouponLambdaQueryWrapper1 = new LambdaQueryWrapper<>();
        sysCouponLambdaQueryWrapper1.eq(SysCoupon::getCouponId, sysCoupon.getCouponId());
        sysCouponLambdaQueryWrapper1.eq(SysCoupon::getRevision, sysCoupon1.getRevision());
        if (sysCouponMapper.update(sysCoupon, sysCouponLambdaQueryWrapper1) <= 0) {
            throw new CommonException("修改优惠券", "访问人数过多，请稍后再试");
        }
    }

    /**
     * 分页查询优惠券
     *
     * @param page
     * @param pageSize
     * @return
     */
    @Override
    public Page<SysCouponDto> getPage(Long page, Long pageSize) {
        LambdaQueryWrapper<SysCoupon> sysCouponLambdaQueryWrapper = Wrappers.lambdaQuery(SysCoupon.class);
        sysCouponLambdaQueryWrapper.orderByAsc(SysCoupon::getRedeemNeedScore);
        Page<SysCoupon> sysCouponPage = sysCouponMapper.selectPage(new Page<SysCoupon>(page, pageSize), sysCouponLambdaQueryWrapper);
        List<SysCoupon> records = sysCouponPage.getRecords();

        List<SysCouponDto> list = new ArrayList<>();
        for (SysCoupon record : records) {
            SysCouponDto sysCouponDto = BeanUtil.copyProperties(record, SysCouponDto.class, "startTime", "endTime");
            LocalDateTime startTime = record.getStartTime();
            LocalDateTime endTime = record.getEndTime();
            sysCouponDto.setStartTime(startTime.toInstant(ZoneOffset.of("+8")).toEpochMilli());
            sysCouponDto.setEndTime(endTime.toInstant(ZoneOffset.of("+8")).toEpochMilli());
            list.add(sysCouponDto);
        }
        Page<SysCouponDto> sysCouponDtoPage = new Page<>();
        BeanUtil.copyProperties(sysCouponPage, sysCouponDtoPage, "records");
        sysCouponDtoPage.setRecords(list);
        return sysCouponDtoPage;
    }

    /**
     * 查看优惠券详情
     *
     * @param couponId
     * @return
     */
    @Override
    public SysCouponDto get(String couponId) {
        LambdaQueryWrapper<SysCoupon> sysCouponLambdaQueryWrapper = Wrappers.lambdaQuery(SysCoupon.class);
        sysCouponLambdaQueryWrapper.eq(SysCoupon::getCouponId, couponId);
        SysCoupon sysCoupon = sysCouponMapper.selectOne(sysCouponLambdaQueryWrapper);
        if (sysCoupon == null) throw new CommonException("分页查询优惠券 -->", "优惠券不存在");
        // 存在,拷贝对象，传递时间戳对象
        SysCouponDto sysCouponDto = BeanUtil.copyProperties(sysCoupon, SysCouponDto.class, "startTime", "endTime");
        // 将开始时间和结束时间转为时间戳
        LocalDateTime startTime = sysCoupon.getStartTime();
        LocalDateTime endTime = sysCoupon.getEndTime();
        sysCouponDto.setStartTime(startTime.toInstant(ZoneOffset.of("+8")).toEpochMilli());
        sysCouponDto.setEndTime(endTime.toInstant(ZoneOffset.of("+8")).toEpochMilli());
        return sysCouponDto;
    }

    /**
     * 批量设置上/下架
     *
     * @param sysCoupons
     * @return
     */
    @Override
    public void setStatusList(List<SysCoupon> sysCoupons) {
        // 批量更新
        updateBatchById(sysCoupons);
    }
}