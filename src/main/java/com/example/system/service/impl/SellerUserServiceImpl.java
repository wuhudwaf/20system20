package com.example.system.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.RandomUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.system.dto.SellerUserDto;
import com.example.system.dto.SetInfoSellerUser;
import com.example.system.exception.CommonException;
import com.example.system.mapper.SellerUserMapper;
import com.example.system.mapper.SysShopMapper;
import com.example.system.pojo.SellerOrderItem;
import com.example.system.pojo.SellerUser;
import com.example.system.dto.SetPwd;
import com.example.system.pojo.SysShop;
import com.example.system.service.SellerUserService;
import com.example.system.utils.MD5Str;
import com.example.system.utils.RedisIdWorker;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static com.example.system.utils.Constants.*;

@Service
@RequiredArgsConstructor
@SuppressWarnings("All")
public class SellerUserServiceImpl implements SellerUserService {

    private final SellerUserMapper sellerUserMapper;

    private final SysShopMapper sysShopMapper;

    private final StringRedisTemplate stringRedisTemplate;

    private final RedisIdWorker redisIdWorker;

    /**
     * 新增门店员工
     *
     * @param sellerUser
     * @return
     */
    @Override
    public String add(SellerUser sellerUser) {
        // 查询指定门店
        LambdaQueryWrapper<SysShop> sysShopLambdaQueryWrapper = Wrappers.lambdaQuery(SysShop.class);
        sysShopLambdaQueryWrapper.eq(SysShop::getShopId, sellerUser.getShopId());
        SysShop sysShop = sysShopMapper.selectOne(sysShopLambdaQueryWrapper);
        // 门店是否不存在
        if (sysShop == null) throw new CommonException("新增门店员工", "指定的门店不存在");
        // 该门店店铺是否审批员工
        if (sysShop.getStaffSet().equals(0)) throw new CommonException("新增门店员工", "该门店未开启审批员工");
        // 当前手机号是否被注册
        SellerUser byPhone = sellerUserMapper.getByPhone(sellerUser.getPhone());
        if (byPhone != null) {
            // 已注册，提示管理员
            throw new CommonException("新增门店员工", "该手机号已被注册");
        }
        // 未注册,封装 员工对象数据并返回员工id
        String id = setSellerUser(sellerUser);
        // 自动生成密码
        String pwd = RandomUtil.randomString(5) + RandomUtil.randomNumbers(5);
        // 对密码进行 MD5加密 16位小写
        sellerUser.setPwd(MD5Str.MD5(pwd, MD5_SIXTEEN));
        // 新增员工
        if (sellerUserMapper.insert(sellerUser) <= 0) {
            throw new CommonException("新增门店员工", "网络异常，请稍后再试");
        }
        // 拷贝 sellerUser对象获取 sellerUserDto对象
        SellerUserDto sellerUserDto = BeanUtil.copyProperties(sellerUser, SellerUserDto.class);
        // 返回密码
        return pwd;
    }

    /**
     * 冻结
     *
     * @param id
     * @return
     */
    @Override
    public void delete(String id) {
        SellerUser sellerUser = sellerUserMapper.selectById(id);
        sellerUser.setDelFlag(0);
        if (sellerUserMapper.updateById(sellerUser) <= 0) {
            throw new CommonException("冻结", "无效的id");
        }
    }

    /**
     * 解冻
     *
     * @param id
     */
    @Override
    public void set(String id) {
        SellerUser sellerUser = sellerUserMapper.selectById(id);
        sellerUser.setDelFlag(1);
        if (sellerUserMapper.updateById(sellerUser) <= 0) {
            throw new CommonException("解冻", "无效的id");
        }
    }

    /**
     * 数据回显 (修改门店员工信息)
     *
     * @param id
     * @return
     */
    @Override
    public SellerUserDto get(String id) {
        // 去mysql中查询
        SellerUser sellerUser = sellerUserMapper.selectById(id);
        // 判断是否为null
        if (sellerUser == null) {
            throw new CommonException("数据回显 -->", "门店员工不存在");
        }
        SellerUserDto sellerUserDto = BeanUtil.copyProperties(sellerUser, SellerUserDto.class);
        // 返回数据
        return sellerUserDto;
    }

    /**
     * 修改门店员工
     *
     * @param setInfoSellerUser
     * @return
     */
    @Override
    public void setInfo(SetInfoSellerUser setInfoSellerUser) {
        // 手机号是否为空
        if (setInfoSellerUser.getPhone() != null) {
            // 不为空。判断改手机号是否被注册
            SellerUser byPhone = sellerUserMapper.getByPhone(setInfoSellerUser.getPhone());
            if (byPhone != null)
                // 被注册，直接结束提示
                throw new CommonException("修改门店员工 ->", "该手机号已被注册");
        }
        // 去mysql中查询
        SellerUser sellerUser = sellerUserMapper.selectById(setInfoSellerUser.getId());
        if (sellerUser == null)
            throw new CommonException("修改门店员工 ->", "门店员工不存在");
        // 修改门店员工
        sellerUserMapper.updateById(BeanUtil.copyProperties(setInfoSellerUser, SellerUser.class));
    }

    /**
     * 查询，根据手机号查询
     *
     * @param phone
     * @return
     */
    @Override
    public SellerUserDto getByPhone(String phone) {
        // 去MySQL查询
        SellerUser sellerUser = sellerUserMapper.getByPhone(phone);
        // 是否为null
        if (sellerUser == null) {
            throw new CommonException("查询，根据手机号查询 -->", "门店员工不存在");
        }
        SellerUserDto sellerUserDto = BeanUtil.copyProperties(sellerUser, SellerUserDto.class);

        return sellerUserDto;
    }

    /**
     * 查询某一门店的员工
     *
     * @param shopId
     * @param page
     * @param pageSize
     * @return
     */
    @Override
    public Page<SellerUserDto> getList(String shopId, Long page, Long pageSize) {
        // 去MySQL查询
        LambdaQueryWrapper<SellerUser> sellerUserLambdaQueryWrapper = Wrappers.lambdaQuery(SellerUser.class);
        // 获取指定门店的门店员工
        sellerUserLambdaQueryWrapper.eq(SellerUser::getShopId, shopId);
        // 排序
        sellerUserLambdaQueryWrapper.orderByAsc(SellerUser::getStatus);
        // 查到的集合对象
        Page<SellerUser> sellerUserPage = sellerUserMapper.selectPage(new Page<SellerUser>(page, pageSize), sellerUserLambdaQueryWrapper);
        List<SellerUser> records = sellerUserPage.getRecords();
        // 最终需要的数据，会放在新的page对象中
        ArrayList<SellerUserDto> sellerUserDtos = new ArrayList<>();
        // 循环拷贝数据
        for (SellerUser record : records) {
            SellerUserDto sellerUserDto = new SellerUserDto();
            BeanUtil.copyProperties(record, sellerUserDto);
            sellerUserDtos.add(sellerUserDto);
        }
        // 新的page对象，最终要返回的数据
        Page<SellerUserDto> sellerUserDtoPage = new Page<>();
        BeanUtil.copyProperties(sellerUserPage, sellerUserDtoPage, "records");
        // 将sellerUserDtos存储到新page对象中
        sellerUserDtoPage.setRecords(sellerUserDtos);
        // 返回page对象
        return sellerUserDtoPage;
    }

    /**
     * 重置密码
     *
     * @param setPwd
     * @return
     */
    @Override
    public String setPwd(SetPwd setPwd) {
        // 当前门店管理员是否存在
        SellerUser sellerUser = sellerUserMapper.selectById(setPwd.getId());
        if (sellerUser == null) {
            // 不存在，返回提示
            throw new CommonException("重置密码", "门店员工不存在");
        }
        // 存在，更新
        SellerUser sellerUser1 = BeanUtil.copyProperties(setPwd, SellerUser.class);
        sellerUser1.setPwd(MD5Str.MD5(setPwd.getPwd(), MD5_SIXTEEN));
        sellerUserMapper.updateById(sellerUser1);
        return setPwd.getPwd();
    }

    /**
     * 封装 员工对象数据并返回员工id
     *
     * @param selle
     */
    public String setSellerUser(SellerUser sellerUser) {
        String aString = redisIdWorker.nextId("selleruser");
        // id
        sellerUser.setId(aString);
        // 账号
        sellerUser.setUserName(RandomUtil.randomNumbers(11));
        // 进入待审批状态
        sellerUser.setStatus(0);
        // 账号状态正常
        sellerUser.setDelFlag(1);
        // 返回id
        return aString;
    }
}