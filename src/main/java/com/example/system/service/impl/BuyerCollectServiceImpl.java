package com.example.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.system.exception.CommonException;
import com.example.system.mapper.BuyerCollectMapper;
import com.example.system.pojo.BuyerCart;
import com.example.system.pojo.BuyerCollect;
import com.example.system.service.BuyerCollectService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@SuppressWarnings("all")
public class BuyerCollectServiceImpl implements BuyerCollectService {

    private final BuyerCollectMapper buyerCollectMapper;

    /**
     * 查询用户收藏列表
     *
     * @param page
     * @param pageSize
     * @return
     */
    @Override
    public Page<BuyerCollect> getList(Long page, Long pageSize) {
        LambdaQueryWrapper<BuyerCollect> buyerCollectLambdaQueryWrapper = Wrappers.lambdaQuery(BuyerCollect.class);
        buyerCollectLambdaQueryWrapper.orderByAsc(BuyerCollect::getCollectTime);
        Page<BuyerCollect> buyerCollectPage = buyerCollectMapper.selectPage(new Page<BuyerCollect>(page, pageSize), buyerCollectLambdaQueryWrapper);
        return buyerCollectPage;
    }
}