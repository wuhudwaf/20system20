package com.example.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.system.exception.CommonException;
import com.example.system.mapper.SellerOrderItemMapper;
import com.example.system.pojo.SellerLogistics;
import com.example.system.pojo.SellerOrderItem;
import com.example.system.service.SellerOrderItemService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@SuppressWarnings("all")
public class SellerOrderItemServiceImpl implements SellerOrderItemService {

    private final SellerOrderItemMapper sellerOrderItemMapper;


    /**
     * 商品订单列表
     *
     * @param page
     * @param pageSize
     * @return
     */
    @Override
    public Page<SellerOrderItem> getPage(Long page, Long pageSize) {
        LambdaQueryWrapper<SellerOrderItem> buyerUserLambdaQueryWrapper = Wrappers.lambdaQuery(SellerOrderItem.class);
        buyerUserLambdaQueryWrapper.orderByAsc(SellerOrderItem::getId);
        Page<SellerOrderItem> selectPage = sellerOrderItemMapper.selectPage(new Page<SellerOrderItem>(page, pageSize), buyerUserLambdaQueryWrapper);
        return selectPage;
    }
}