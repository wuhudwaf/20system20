package com.example.system.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.system.exception.CommonException;
import com.example.system.mapper.AppletInfoMapper;
import com.example.system.pojo.AppletInfo;
import com.example.system.pojo.vo.AppletInfoVo;
import com.example.system.service.AppletInfoService;
import com.example.system.utils.GetUser;
import com.example.system.utils.RedisIdWorker;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
@SuppressWarnings("all")
public class AppletInfoServiceImpl implements AppletInfoService {

    private final AppletInfoMapper appletInfoMapper;

    private final RedisIdWorker redisIdWorker;

    /**
     * 新增小程序信息
     *
     * @param appletInfo
     * @return
     */
    @Override
    public void addAppletInfo(AppletInfo appletInfo) {
        appletInfo.setInfoId(redisIdWorker.nextId("AppletInfo"));
        String userId = GetUser.getUserId();
        appletInfo.setCreateBy(userId);
        appletInfo.setUpdateBy(userId);
        appletInfo.setCreateTime(LocalDateTime.now());
        appletInfo.setLastUpdateTime(LocalDateTime.now());
        appletInfoMapper.insert(appletInfo);
    }

    /**
     * 删除小程序信息
     *
     * @param infoId
     * @return
     */
    @Override
    public void deleteAppletInfo(String infoId) {
        LambdaQueryWrapper<AppletInfo> appletInfoLambdaQueryWrapper = Wrappers.lambdaQuery(AppletInfo.class);
        appletInfoLambdaQueryWrapper.eq(AppletInfo::getInfoId, infoId);
        AppletInfo appletInfo = appletInfoMapper.selectOne(appletInfoLambdaQueryWrapper);
        if (appletInfo == null) throw new CommonException("删除小程序信息", "小程序信息不存在");
        appletInfoMapper.delete(appletInfoLambdaQueryWrapper);
    }

    /**
     * 修改小程序信息
     *
     * @param appletInfo
     * @return
     */
    @Override
    public void updateAppletInfo(AppletInfo appletInfo) {
        LambdaQueryWrapper<AppletInfo> appletInfoLambdaQueryWrapper = Wrappers.lambdaQuery(AppletInfo.class);
        appletInfoLambdaQueryWrapper.eq(AppletInfo::getInfoId, appletInfo.getInfoId());
        AppletInfo sqlAppletInfo = appletInfoMapper.selectOne(appletInfoLambdaQueryWrapper);
        if (sqlAppletInfo == null) throw new CommonException("删除小程序信息", "小程序信息不存在");
        appletInfoMapper.update(appletInfo, appletInfoLambdaQueryWrapper);
    }

    /**
     * 查看小程序信息详情
     *
     * @param infoId
     * @return
     */
    @Override
    public AppletInfoVo getAppletInfo(String infoId) {
        LambdaQueryWrapper<AppletInfo> appletInfoLambdaQueryWrapper = Wrappers.lambdaQuery(AppletInfo.class);
        appletInfoLambdaQueryWrapper.eq(AppletInfo::getInfoId, infoId);
        AppletInfo sqlAppletInfo = appletInfoMapper.selectOne(appletInfoLambdaQueryWrapper);
        AppletInfoVo appletInfoVo = BeanUtil.copyProperties(sqlAppletInfo, AppletInfoVo.class, "infoId", "appletConfigId");
        appletInfoVo.setInfoId(sqlAppletInfo.getInfoId().toString());
        appletInfoVo.setAppletConfigId(sqlAppletInfo.getAppletConfigId().toString());
        return appletInfoVo;
    }

    /**
     * 查看小程序信息列表
     *
     * @param page
     * @param pageSize
     * @return
     */
    @Override
    public Page<AppletInfoVo> getAppletInfoPage(Long page, Long pageSize) {
        LambdaQueryWrapper<AppletInfo> appletInfoLambdaQueryWrapper = Wrappers.lambdaQuery(AppletInfo.class);
        appletInfoLambdaQueryWrapper.orderByAsc(AppletInfo::getCreateTime);
        Page<AppletInfo> appletInfoPage = appletInfoMapper.selectPage(new Page<AppletInfo>(page, pageSize), appletInfoLambdaQueryWrapper);
        Page<AppletInfoVo> appletInfoVoPage = new Page<>();
        ArrayList<AppletInfoVo> appletInfoVos = new ArrayList<>();
        BeanUtil.copyProperties(appletInfoPage, appletInfoVoPage, "records");
        List<AppletInfo> records = appletInfoPage.getRecords();
        for (AppletInfo record : records) {
            AppletInfoVo appletInfoVo = BeanUtil.copyProperties(record, AppletInfoVo.class, "infoId", "appletConfigId");
            appletInfoVo.setInfoId(record.getInfoId().toString());
            appletInfoVo.setAppletConfigId(record.getAppletConfigId().toString());
            appletInfoVos.add(appletInfoVo);
        }
        appletInfoVoPage.setRecords(appletInfoVos);
        return appletInfoVoPage;
    }
}