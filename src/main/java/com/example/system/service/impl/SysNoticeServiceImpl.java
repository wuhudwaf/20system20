package com.example.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.system.exception.CommonException;
import com.example.system.mapper.SysNoticeMapper;
import com.example.system.pojo.SysNotice;
import com.example.system.service.SysNoticeService;
import com.example.system.utils.GetUser;
import com.example.system.utils.RedisIdWorker;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;

@Service
@RequiredArgsConstructor
@SuppressWarnings("all")
public class SysNoticeServiceImpl implements SysNoticeService {

    private final SysNoticeMapper sysNoticeMapper;

    private final RedisIdWorker redisIdWorker;

    /**
     * 新增通知公告
     *
     * @param sysNotice
     * @return
     */
    @Override
    @Transactional // 事务管理
    public void add(SysNotice sysNotice) {
        String userId = GetUser.getUserId();
        String id = redisIdWorker.nextId("notice");
        sysNotice.setNoticeId(id);
        sysNotice.setCreateBy(userId);
        sysNotice.setCreateTime(LocalDateTime.now());
        sysNotice.setUpdateBy(userId);
        sysNotice.setUpdateTime(LocalDateTime.now());
        sysNotice.setStatus("0");
        if (sysNoticeMapper.insert(sysNotice) <= 0) {
            throw new CommonException("新增通知公告", "网络异常，请稍后再试");
        }
    }

    /**
     * 删除指定的通知公告
     *
     * @param noticeId
     * @return
     */
    @Override
    public void delete(String noticeId) {
        LambdaQueryWrapper<SysNotice> sysNoticeLambdaQueryWrapper = Wrappers.lambdaQuery(SysNotice.class);
        sysNoticeLambdaQueryWrapper.eq(SysNotice::getNoticeId, noticeId);
        if (sysNoticeMapper.delete(sysNoticeLambdaQueryWrapper) <= 0) {
            throw new CommonException("删除指定的通知公告", "网络异常，请稍后再试");
        }
    }

    /**
     * 修改指定的通知公告
     *
     * @param sysNotice
     * @return
     */
    @Override
    public void update(SysNotice sysNotice) {
        sysNotice.setUpdateBy(GetUser.getUserId());
        sysNotice.setUpdateTime(LocalDateTime.now());
        LambdaQueryWrapper<SysNotice> sysNoticeLambdaQueryWrapper = Wrappers.lambdaQuery(SysNotice.class);
        sysNoticeLambdaQueryWrapper.eq(SysNotice::getNoticeId, sysNotice.getNoticeId());
        if (sysNoticeMapper.update(sysNotice, sysNoticeLambdaQueryWrapper) <= 0) {
            throw new CommonException("修改指定的通知公告", "网络异常，请稍后再试");
        }
    }

    /**
     * 获取通知公告列表
     *
     * @return
     */
    @Override
    public Page<SysNotice> getList(Long page, Long pageSize) {
        LambdaQueryWrapper<SysNotice> sysNoticeLambdaQueryWrapper = Wrappers.lambdaQuery(SysNotice.class);
        sysNoticeLambdaQueryWrapper.orderByDesc(SysNotice::getCreateTime);
        Page<SysNotice> sysNoticePage = sysNoticeMapper.selectPage(new Page<SysNotice>(page, pageSize), sysNoticeLambdaQueryWrapper);
        // 返回数据
        return sysNoticePage;
    }

    /**
     * 获取通知公告详情
     *
     * @param noticeId
     * @return
     */
    @Override
    public SysNotice get(String noticeId) {
        LambdaQueryWrapper<SysNotice> sysNoticeLambdaQueryWrapper = Wrappers.lambdaQuery(SysNotice.class);
        sysNoticeLambdaQueryWrapper.eq(SysNotice::getNoticeId, noticeId);
        SysNotice sysNotice = sysNoticeMapper.selectOne(sysNoticeLambdaQueryWrapper);
        if (sysNotice == null) {
            throw new CommonException("获取通知公告详情", "无效的id");
        }
        return sysNotice;
    }
}