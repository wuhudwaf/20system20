package com.example.system.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.RandomUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.system.exception.CommonException;
import com.example.system.mapper.SellerUserMapper;
import com.example.system.mapper.SysShopMapper;
import com.example.system.pojo.SellerUser;
import com.example.system.pojo.SysShop;
import com.example.system.pojo.export.ExportSysShop;
import com.example.system.pojo.vo.add.SysShopAdd;
import com.example.system.service.SysShopService;
import com.example.system.utils.GetUser;
import com.example.system.utils.RedisIdWorker;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@SuppressWarnings("all")
public class SysShopServiceImpl extends ServiceImpl<SysShopMapper, SysShop> implements SysShopService {

    private final SysShopMapper sysShopMapper;

    private final SellerUserMapper sellerUserMapper;

    private final RedisIdWorker redisIdWorker;

    /**
     * 新增门店
     *
     * @param sysShop
     * @return
     */
    @Override
    public void add(SysShop sysShop) {
        // 根据店名查询SysShop对象，判断店名是否重复
        LambdaQueryWrapper<SysShop> sysShopLambdaQueryWrapper = Wrappers.lambdaQuery(SysShop.class);
        sysShopLambdaQueryWrapper.eq(SysShop::getName, sysShop.getName());
        SysShop sysShop1 = sysShopMapper.selectOne(sysShopLambdaQueryWrapper);
        if (sysShop1 != null) throw new CommonException("新增门店", "新增失败，门店名称重复，换个名称试试");
        // 编号/门店id
        sysShop.setShopId(redisIdWorker.nextId("shop"));
        // 租户号
        sysShop.setTenantId(String.valueOf(redisIdWorker.nextId("shop")));
        // 乐观锁
        sysShop.setRevision(RandomUtil.randomInt());
        // 登录者id
        String userId = GetUser.getUserId();
        sysShop.setCreateBy(userId);
        sysShop.setUpdateBy(userId);
        sysShop.setCreateTime(LocalDateTime.now());
        sysShop.setUpdateTime(LocalDateTime.now());
        // 门店城市
        String[] split = sysShop.getShopCityS();
        StringBuffer stringBuffer = new StringBuffer();
        for (int i = 0; i < split.length; i++) {
            String s = split[i];
            if (i != split.length - 1) {
                stringBuffer.append(split[i] + ",");
            }
        }
        sysShop.setShopCity(stringBuffer.toString());
        sysShop.setAffiliationStreet(split[split.length - 1]);
        // 更新
        if (sysShopMapper.insert(sysShop) <= 0) {
            throw new CommonException("新增门店", "添加失败，请稍后再试");
        }
    }

    /**
     * 删除门店
     *
     * @param shopId
     * @return
     */
    @Override
    @Transactional // 事务管理
    public void delete(String shopId) {
        // 拿到乐观锁，同时判断门店是否存在
        SysShop byShopId = sysShopMapper.getByShopId(shopId);
        if (byShopId == null) {
            throw new CommonException("删除门店", "门店不存在");
        }

        // 门店状态为零才可删除门店
        if (byShopId.getStatus().equals(1)) throw new CommonException("删除门店", "无法删除开店状态的门店");

        // 乐观锁
        Integer revision = byShopId.getRevision();
        // 删除
        if (sysShopMapper.deleteByShopIdSysShop(shopId, revision) <= 0) {
            throw new CommonException("删除门店", "删除失败，请稍后再试");
        }

        // 删除关于门店的东西，比如分类，商品等

    }

    /**
     * 获取门店详情
     *
     * @return
     */
    @Override
    public SysShop getByShopId(String shopId) {
        // 为null，去MySQL查询
        SysShop byShopId = sysShopMapper.getByShopId(shopId);
        // 为null，向redis存入“”数据
        if (byShopId == null) throw new CommonException("获取门店详情", "门店不存在");
        String add = byShopId.getShopCity() + byShopId.getAffiliationStreet();
        byShopId.setShopCityS(add.split(","));
        // 返回对象
        return byShopId;
    }

    /**
     * 获取门店列表
     *
     * @return
     */
    @Override
    public Page<SysShop> getList(Long page, Long pageSize) {
        // 去MySQL中查询
        LambdaQueryWrapper<SysShop> sysShopLambdaQueryWrapper = Wrappers.lambdaQuery(SysShop.class);
        sysShopLambdaQueryWrapper.orderByAsc(SysShop::getOpenHours);
        Page<SysShop> sysShopPage = sysShopMapper.selectPage(new Page<>(page, pageSize), sysShopLambdaQueryWrapper);
        return sysShopPage;
    }

    /**
     * 修改门店信息
     *
     * @param sysShop
     * @return
     */
    @Override
    public void update(SysShop sysShop) {
        // 查询sysShop对象，核心是乐观锁的校验
        SysShop sqlSysShop = sysShopMapper.getByShopId(sysShop.getShopId());
        if (sqlSysShop == null) {
            throw new CommonException("修改门店信息", "门店不存在");
        }
        // 更新人
        String userId = GetUser.getUserId();
        sysShop.setUpdateBy(userId);
        sysShop.setUpdateTime(LocalDateTime.now());
        // 门店城市
        String[] split = sysShop.getShopCityS();
        StringBuffer stringBuffer = new StringBuffer();
        for (int i = 0; i < split.length; i++) {
            String s = split[i];
            if (i != split.length - 1) {
                stringBuffer.append(split[i] + ",");
            }
        }
        sysShop.setShopCity(stringBuffer.toString());
        sysShop.setAffiliationStreet(split[split.length - 1]);
        // 更新乐观锁
        sysShop.setRevision(sqlSysShop.getRevision() + 1);
        LambdaQueryWrapper<SysShop> sysShopLambdaQueryWrapper = new LambdaQueryWrapper<>();
        sysShopLambdaQueryWrapper.eq(SysShop::getShopId, sysShop.getShopId());
        sysShopLambdaQueryWrapper.eq(SysShop::getRevision, sqlSysShop.getRevision());
        // 更新
        if (sysShopMapper.update(sysShop, sysShopLambdaQueryWrapper) <= 0) {
            // 更新异常
            throw new CommonException("修改门店信息", "当前访问人数过多，请稍后再试");
        }
    }

    /**
     * 查询指定门店未审批员工列表
     *
     * @param shopId
     * @param page
     * @param pageSize
     * @return
     */
    @Override
    public Page<SellerUser> byShopIdSelectSellerUserId(String shopId, Long page, Long pageSize) {
        LambdaQueryWrapper<SellerUser> sellerUserLambdaQueryWrapper = Wrappers.lambdaQuery(SellerUser.class);
        // 指定门店
        sellerUserLambdaQueryWrapper.eq(SellerUser::getShopId, shopId);
        // 指定审批中
        sellerUserLambdaQueryWrapper.eq(SellerUser::getStatus, 0);
        // 分页对象
        Page<SellerUser> sellerUserPage = sellerUserMapper.selectPage(new Page<SellerUser>(page, pageSize), sellerUserLambdaQueryWrapper);
        // 返回数据
        return sellerUserPage;
    }

    /**
     * 批量设置门店状态(1：开店，0：闭店)
     *
     * @param shops
     * @return
     */
    @Override
    public void setStatusList(List<SysShop> shops) {
        updateBatchById(shops);
    }

    /**
     * 批量设置门店营业时间
     *
     * @param shops
     * @return
     */
    @Override
    public void setStartTimeAndEndTimeList(List<SysShop> shops) {
        updateBatchById(shops);
    }

    /**
     * 门店导出
     *
     * @return
     */
    @Override
    public List<SysShop> diyGetSysGoodsList() {
        LambdaQueryWrapper<SysShop> sysShopLambdaQueryWrapper = Wrappers.lambdaQuery(SysShop.class);
        sysShopLambdaQueryWrapper.orderByAsc(SysShop::getCreateTime);
        List<SysShop> sysShops = sysShopMapper.selectList(sysShopLambdaQueryWrapper);
        return sysShops;
    }

    /**
     * 商品导出(页数)
     *
     * @param sysShopQueryVo
     * @return
     */
    @Override
    public List<SysShop> diyGetSysShopPage(com.example.system.pojo.page.Page page) {
        if (page.getPageSize().equals(null)) page.setPageSize(10);
        if (page.getPageOne() == null || page.getPageTwo() == null) throw new CommonException("商品导出", "设置异常");
        if (page.getPageOne().equals(0) || page.getPageTwo().equals(0) || page.getPageSize().equals(0))
            throw new CommonException("商品导出", "设置异常");
        if (page.getPageOne() - page.getPageTwo() > 0) throw new CommonException("商品导出", "设置异常");
        List<SysShop> shops = new ArrayList<>();
        // 循环查询
        for (int i = page.getPageOne(); i <= page.getPageTwo(); i++) {
            LambdaQueryWrapper<SysShop> sysShopLambdaQueryWrapper = Wrappers.lambdaQuery(SysShop.class);
            sysShopLambdaQueryWrapper.orderByDesc(SysShop::getCreateTime);
            Page<SysShop> sysShopPage = sysShopMapper.selectPage(new Page<SysShop>(i, page.getPageSize()), sysShopLambdaQueryWrapper);
            List<SysShop> records = sysShopPage.getRecords();
            if (records.size() > 0) {
                // 表示有数据,循环获取数据
                for (SysShop record : records) {
                    shops.add(record);
                }
            }
        }
        return shops;
    }

    /**
     * 批量更新门店
     *
     * @param sysShopAdds
     */
    @Override
    public void insertBatchSomeColumn(List<SysShopAdd> sysShopAdds) {
        sysShopMapper.insertBatchSomeColumn(sysShopAdds);
    }

    /**
     * 门店导入
     *
     * @param sysShopAdds
     */
    @Override
    public void importShop(List<SysShopAdd> sysShopAdds) {
        for (SysShopAdd sysShopAdd : sysShopAdds) {
            LambdaQueryWrapper<SysShop> sysShopLambdaQueryWrapper = Wrappers.lambdaQuery(SysShop.class);
            sysShopLambdaQueryWrapper.eq(SysShop::getName, sysShopAdd.getName());
            SysShop sysShop = sysShopMapper.selectOne(sysShopLambdaQueryWrapper);
            if (sysShop != null) throw new CommonException("门店导入", "门店名称重复，添加失败");
            sysShopAdd.setShopId(redisIdWorker.nextId("shopId"));
            sysShopAdd.setShopId(String.valueOf(Math.round(Double.valueOf(sysShopAdd.getShopId()))));
            sysShopAdd.setRevision(1);
            sysShopAdd.setCreateTime(new Date());
            sysShopAdd.setCreateBy(GetUser.getUserId());
            sysShopAdd.setUpdateBy(GetUser.getUserId());
            sysShopAdd.setUpdateTime(new Date());
            sysShopAdd.setComprehensiveMark("0");
            // 营业时间
            String businessHours = sysShopAdd.getBusinessHours();
            StringBuffer startTime = new StringBuffer();
            StringBuffer endTime = new StringBuffer();
            int t = 0;
            for (int i = 0; i < businessHours.length(); i++) {
                char c = businessHours.charAt(i);
                if (c != '~' && c != '-' && c != '/' && t == 0) {
                    startTime.append(c);
                }
                if (c == '~' || c == '-' || c == '/') {
                    t++;
                    continue;
                }
                if (t == 1) {
                    endTime.append(c);
                }
            }
            sysShopAdd.setOpenHours(startTime.toString());
            sysShopAdd.setCloseHours(endTime.toString());
        }
        // 批量更新
        insertBatchSomeColumn(sysShopAdds);
    }


    @Override
    public List<ExportSysShop> diyGetSysShopListNew() {
        LambdaQueryWrapper<SysShop> sysShopLambdaQueryWrapper = Wrappers.lambdaQuery(SysShop.class);
        sysShopLambdaQueryWrapper.orderByAsc(SysShop::getCreateTime);
        List<SysShop> sysShops = sysShopMapper.selectList(sysShopLambdaQueryWrapper);
        // 循环拷贝
        List<ExportSysShop> exportSysShops = sysShops.stream().map(sysShop -> {
            return BeanUtil.copyProperties(sysShop, ExportSysShop.class);
        }).collect(Collectors.toList());
        return exportSysShops;
    }
}