package com.example.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.system.exception.CommonException;
import com.example.system.mapper.BuyerInviteMapper;
import com.example.system.pojo.BuyerCoupon;
import com.example.system.pojo.BuyerInvite;
import com.example.system.service.BuyerInviteService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@SuppressWarnings("all")
public class BuyerInviteServiceImpl implements BuyerInviteService {

    private final BuyerInviteMapper buyerInviteMapper;

    /**
     * 用户邀请列表
     *
     * @param page
     * @param pageSize
     * @return
     */
    @Override
    public Page<BuyerInvite> getList(Long page, Long pageSize) {
        LambdaQueryWrapper<BuyerInvite> buyerInviteLambdaQueryWrapper = Wrappers.lambdaQuery(BuyerInvite.class);
        buyerInviteLambdaQueryWrapper.orderByAsc(BuyerInvite::getInviteDate);
        Page<BuyerInvite> buyerInvitePage = buyerInviteMapper.selectPage(new Page<BuyerInvite>(page, pageSize), buyerInviteLambdaQueryWrapper);
        return buyerInvitePage;
    }
}