package com.example.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.system.exception.CommonException;
import com.example.system.mapper.SysWalletLogMapper;
import com.example.system.pojo.SysUser;
import com.example.system.pojo.SysWalletLog;
import com.example.system.service.SysWalletLogService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@SuppressWarnings("all")
public class SysWalletLogServiceImpl implements SysWalletLogService {

    private final SysWalletLogMapper sysWalletLogMapper;


    /**
     * 门店id查询余额变动日志列表
     *
     * @param shopId
     * @param page
     * @param pageSize
     * @return
     */
    @Override
    public Page<SysWalletLog> getPage(String shopId, Long page, Long pageSize) {
        LambdaQueryWrapper<SysWalletLog> sysWalletLogLambdaQueryWrapper = Wrappers.lambdaQuery(SysWalletLog.class);
        sysWalletLogLambdaQueryWrapper.orderByAsc(SysWalletLog::getId);
        // 针对某一门店查询
        sysWalletLogLambdaQueryWrapper.eq(SysWalletLog::getShopId, shopId);
        Page<SysWalletLog> sysWalletLogPage = sysWalletLogMapper.selectPage(new Page<SysWalletLog>(page, pageSize), sysWalletLogLambdaQueryWrapper);
        return sysWalletLogPage;
    }
}