package com.example.system.service.impl;

import cn.hutool.core.date.DateUtil;
import com.baomidou.mybatisplus.core.conditions.interfaces.Func;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.system.exception.CommonException;
import com.example.system.mapper.BuyerAddressMapper;
import com.example.system.mapper.BuyerMembershipRuleMapper;
import com.example.system.mapper.BuyerOrderMapper;
import com.example.system.mapper.BuyerUserMapper;
import com.example.system.pojo.*;
import com.example.system.pojo.where.BuyerUserQuery;
import com.example.system.service.BuyerUserService;
import com.example.system.utils.GetUser;
import lombok.RequiredArgsConstructor;
import org.apache.xmlbeans.impl.xb.xsdschema.Facet;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletResponse;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.*;

@Service
@RequiredArgsConstructor
@SuppressWarnings("all")
public class BuyerUserServiceImpl implements BuyerUserService {

    /**
     * 用户
     */
    private final BuyerUserMapper buyerUserMapper;

    /**
     * 会员
     */
    private final BuyerMembershipRuleMapper buyerMembershipRuleMapper;

    /**
     * 用户地址
     */
    private final BuyerAddressMapper buyerAddressMapper;

    /**
     * 订单
     */
    private final BuyerOrderMapper buyerOrderMapper;


    /**
     * 查询微信用户信息
     *
     * @param page
     * @param pageSize
     * @return
     */
    @Override
    public Page<BuyerUser> getPage(BuyerUserQuery buyerUser1) throws ParseException {
        LambdaQueryWrapper<BuyerUser> buyerUserLambdaQueryWrapper = Wrappers.lambdaQuery(BuyerUser.class);
        // 筛选,注册时间查找
        if (buyerUser1.getCreateTime() != null && !buyerUser1.getCreateTime().equals("")) {
            String createTime = buyerUser1.getCreateTime();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            String sd = sdf.format(new Date(Long.parseLong(String.valueOf(createTime))));
            buyerUserLambdaQueryWrapper.apply("date_format(create_time,'%Y-%m-%d') = '" + sd + "'");
        }
        // 用户id查找(主键)
        buyerUserLambdaQueryWrapper.eq(buyerUser1.getUserId() != null && !buyerUser1.getCreateTime().equals(""), BuyerUser::getUserId, buyerUser1.getUserId());
        // 昵称模糊查找
        buyerUserLambdaQueryWrapper.like(buyerUser1.getUserName() != null && !buyerUser1.getUserName().equals(""), BuyerUser::getUserName, buyerUser1.getUserName());
        // 推荐人查找
        buyerUserLambdaQueryWrapper.eq(buyerUser1.getInviteUserId() != null && !buyerUser1.getInviteUserId().equals(""), BuyerUser::getInviteUserId, buyerUser1.getInviteUserId());
        // 会员等级
        buyerUserLambdaQueryWrapper.eq(buyerUser1.getMembershipLevel() != null, BuyerUser::getMembershipLevel, buyerUser1.getMembershipLevel());
        // 会员状态什么玩意？干脆先加上用户状态算了,到时候再说，询问具体是什么，忽忘！！！
        buyerUserLambdaQueryWrapper.eq(buyerUser1.getStatus() != null, BuyerUser::getStatus, buyerUser1.getStatus());
        // 对消费金额区间查询 --- 区间范围
        buyerUserLambdaQueryWrapper.between(buyerUser1.getMoney1() != null && buyerUser1.getMoney2() != null,
                BuyerUser::getConsumptionAmount, buyerUser1.getMoney1(), buyerUser1.getMoney2());
        // 等级排序，高等级在上
        buyerUserLambdaQueryWrapper.orderByAsc(BuyerUser::getUserLevel);
        // 分页查询
        Page<BuyerUser> buyerUserPage = buyerUserMapper.selectPage(new Page<BuyerUser>(buyerUser1.getPage(), buyerUser1.getPageSize()), buyerUserLambdaQueryWrapper);
        List<BuyerUser> records = buyerUserPage.getRecords();
        // 没有值则直接返回
        if (records.size() == 0) return buyerUserPage;
        // 获取当前用户的会员等级，set去重
        TreeSet<Integer> integers = new TreeSet<>();
        // 获取userId
        ArrayList<String> userIds = new ArrayList<>();
        // 推荐人id
        Set<String> inviteUserId = new TreeSet<>();
        for (BuyerUser record : records) {
            integers.add(record.getMembershipLevel());
            userIds.add(record.getUserId());
            if (record.getInviteUserId() != null) {
                inviteUserId.add(record.getInviteUserId());
            }
        }
        // 拿着 integers集合指定查询
        LambdaQueryWrapper<BuyerMembershipRule> buyerMembershipRuleLambdaQueryWrapper = Wrappers.lambdaQuery(BuyerMembershipRule.class);
        buyerMembershipRuleLambdaQueryWrapper.in(BuyerMembershipRule::getMembershipLevel, integers);
        List<BuyerMembershipRule> buyerMembershipRules = buyerMembershipRuleMapper.selectList(buyerMembershipRuleLambdaQueryWrapper);
        // 封装会员等级名称
        for (BuyerUser record : records) {
            for (BuyerMembershipRule buyerMembershipRule : buyerMembershipRules) {
                if (record.getMembershipLevel().equals(buyerMembershipRule.getMembershipLevel())) {
                    record.setMembershipLevelStr(buyerMembershipRule.getMembershipName());
                    break;
                }
                if (record.getMembershipLevel().equals(0)) {
                    record.setMembershipLevelStr("普通用户");
                    break;
                }
            }
        }
        // 根据userIds批量查询订单
        LambdaQueryWrapper<BuyerOrder> buyerOrderLambdaQueryWrapper = Wrappers.lambdaQuery(BuyerOrder.class);
        buyerOrderLambdaQueryWrapper.in(BuyerOrder::getUserId, userIds);
        // 订单集合
        List<BuyerOrder> buyerOrders = buyerOrderMapper.selectList(buyerOrderLambdaQueryWrapper);
        // 用户找订单
        for (BuyerUser record : records) {
            List<BuyerOrder> buyerOrdersList = record.getBuyerOrders();
            for (BuyerOrder buyerOrder : buyerOrders) {
                if (record.getUserId().equals(buyerOrder.getUserId())) {
                    buyerOrdersList.add(buyerOrder);
                }
            }
        }
        // 封装下单次数
        for (BuyerUser record : records) {
            List<BuyerOrder> buyerOrdersList = record.getBuyerOrders();
            // 封装下单次数
            record.setNumber(buyerOrdersList.size());
            record.setBuyerOrders(null);
        }
        if (inviteUserId.size() != 0) {
            // 根据推荐人查询微信用户对象，封装用户名称
            buyerUserLambdaQueryWrapper = Wrappers.lambdaQuery(BuyerUser.class);
            buyerUserLambdaQueryWrapper.in(BuyerUser::getUserId, inviteUserId);
            List<BuyerUser> buyerUsers = buyerUserMapper.selectList(buyerUserLambdaQueryWrapper);
            // 推荐人找用户
            for (BuyerUser buyerUser : buyerUsers) {
                for (BuyerUser record : records) {
                    String userId = buyerUser.getUserId();
                    String inviteUserId1 = record.getInviteUserId();
                    if (buyerUser.getUserId().equals(record.getInviteUserId())) {
                        record.setInviteUserIdName(buyerUser.getUserName());
                    }
                }
            }
        }
        // 没有推荐人
        for (BuyerUser record : records) {
            if (record.getInviteUserIdName() == null) {
                record.setInviteUserIdName("");
            }
        }
        // 最终返回的对象
        buyerUserPage.setRecords(records);
        return buyerUserPage;
    }

    /**
     * 用户id获取用户地址列表
     *
     * @param userId
     * @return
     */
    @Override
    public List<BuyerAddress> userIdGetAddressList(String userId) {
        LambdaQueryWrapper<BuyerAddress> buyerAddressLambdaQueryWrapper = Wrappers.lambdaQuery(BuyerAddress.class);
        buyerAddressLambdaQueryWrapper.eq(BuyerAddress::getUserId, userId);
        List<BuyerAddress> buyerAddresses = buyerAddressMapper.selectList(buyerAddressLambdaQueryWrapper);
        return buyerAddresses;
    }

    /**
     * 导出全部用户
     *
     * @param response
     * @return
     */
    @Override
    public List<BuyerUser> autoGetBuyerUserList(HttpServletResponse response) {
        LambdaQueryWrapper<BuyerUser> buyerUserLambdaQueryWrapper = Wrappers.lambdaQuery(BuyerUser.class);
        // 会员等级排序
        buyerUserLambdaQueryWrapper.orderByDesc(BuyerUser::getMembershipLevel);
        // 用户积分排序
        buyerUserLambdaQueryWrapper.orderByDesc(BuyerUser::getMembershipLevel);
        // 全部的数据
        List<BuyerUser> buyerUsers = buyerUserMapper.selectList(buyerUserLambdaQueryWrapper);
        // 每有值则直接返回
        if (buyerUsers.size() == 0) return buyerUsers;
        ArrayList<String> userIds = new ArrayList<>();
        // 推荐人id
        Set<String> inviteUserId = new TreeSet<>();
        for (BuyerUser record : buyerUsers) {
            userIds.add(record.getUserId());
            if (record.getInviteUserId() != null) inviteUserId.add(record.getInviteUserId());
        }
        // 根据userIds批量查询订单
        LambdaQueryWrapper<BuyerOrder> buyerOrderLambdaQueryWrapper = Wrappers.lambdaQuery(BuyerOrder.class);
        buyerOrderLambdaQueryWrapper.in(BuyerOrder::getUserId, userIds);
        // 订单集合
        List<BuyerOrder> buyerOrders = buyerOrderMapper.selectList(buyerOrderLambdaQueryWrapper);
        // 用户找订单
        for (BuyerUser record : buyerUsers) {
            List<BuyerOrder> buyerOrdersList = record.getBuyerOrders();
            for (BuyerOrder buyerOrder : buyerOrders) {
                if (record.getUserId().equals(buyerOrder.getUserId())) {
                    buyerOrdersList.add(buyerOrder);
                }
            }
        }
        // 封装下单次数
        for (BuyerUser record : buyerUsers) {
            List<BuyerOrder> buyerOrdersList = record.getBuyerOrders();
            // 封装下单次数
            record.setNumber(buyerOrdersList.size());
            record.setBuyerOrders(null);
        }
        if (inviteUserId.size() != 0) {
            // 根据推荐人查询微信用户对象，封装用户名称
            buyerUserLambdaQueryWrapper = Wrappers.lambdaQuery(BuyerUser.class);
            buyerUserLambdaQueryWrapper.in(BuyerUser::getUserId, inviteUserId);
            List<BuyerUser> buyerUsers1 = buyerUserMapper.selectList(buyerUserLambdaQueryWrapper);
            // 推荐人找用户
            for (BuyerUser buyerUser : buyerUsers1) {
                for (BuyerUser record : buyerUsers) {
                    String userId = buyerUser.getUserId();
                    String inviteUserId1 = record.getInviteUserId();
                    if (buyerUser.getUserId().equals(record.getInviteUserId())) {
                        record.setInviteUserIdName(buyerUser.getUserName());
                    }
                }
            }
        }
        // 没有推荐人
        for (BuyerUser record : buyerUsers) {
            if (record.getInviteUserIdName() == null) {
                record.setInviteUserIdName("");
            }
        }
        return buyerUsers;
    }


    /**
     * 导出用户(分页)
     *
     * @param pageOne
     * @param pageTwo
     * @return
     */
    @Override
    public List<BuyerUser> diyGetBuyerUserPage(Integer pageOne, Integer pageTwo, Integer pageSize) {
        if (pageSize.equals(null)) pageSize = 10;
        if (pageOne == null || pageTwo == null) throw new CommonException("用户导出", "设置异常");
        if (pageOne.equals(0) || pageTwo.equals(0) || pageSize.equals(0))
            throw new CommonException("用户导出", "设置异常");
        if (pageOne - pageTwo > 0) throw new CommonException("用户导出", "设置异常");
        List<BuyerUser> buyerUsers = new ArrayList<>();
        // 循环查询
        for (int i = pageOne; i <= pageTwo; i++) {
            LambdaQueryWrapper<BuyerUser> buyerUserLambdaQueryWrapper = Wrappers.lambdaQuery(BuyerUser.class);
            // 会员等级排序
            buyerUserLambdaQueryWrapper.orderByDesc(BuyerUser::getMembershipLevel);
            // 用户积分排序
            buyerUserLambdaQueryWrapper.orderByDesc(BuyerUser::getMembershipLevel);
            Page<BuyerUser> sysGoodsPage = buyerUserMapper.selectPage(new Page<BuyerUser>(i, pageSize), buyerUserLambdaQueryWrapper);
            List<BuyerUser> records = sysGoodsPage.getRecords();
            if (records.size() > 0) {
                // 表示有数据,循环获取数据
                for (BuyerUser record : records) {
                    buyerUsers.add(record);
                }
            }
        }
        // 推荐人id
        Set<String> inviteUserId = new TreeSet<>();
        ArrayList<String> userIds = new ArrayList<>();
        for (BuyerUser record : buyerUsers) {
            userIds.add(record.getUserId());
            if (record.getInviteUserId() != null) {
                inviteUserId.add(record.getInviteUserId());
            }
        }
        // 根据userIds批量查询订单
        LambdaQueryWrapper<BuyerOrder> buyerOrderLambdaQueryWrapper = Wrappers.lambdaQuery(BuyerOrder.class);
        buyerOrderLambdaQueryWrapper.in(BuyerOrder::getUserId, userIds);
        // 订单集合
        List<BuyerOrder> buyerOrders = buyerOrderMapper.selectList(buyerOrderLambdaQueryWrapper);
        // 用户找订单
        for (BuyerUser record : buyerUsers) {
            List<BuyerOrder> buyerOrdersList = record.getBuyerOrders();
            for (BuyerOrder buyerOrder : buyerOrders) {
                if (record.getUserId().equals(buyerOrder.getUserId())) {
                    buyerOrdersList.add(buyerOrder);
                }
            }
        }
        // 封装下单次数
        for (BuyerUser record : buyerUsers) {
            List<BuyerOrder> buyerOrdersList = record.getBuyerOrders();
            // 封装下单次数
            record.setNumber(buyerOrdersList.size());
            record.setBuyerOrders(null);
        }
        if (inviteUserId.size() != 0) {
            // 根据推荐人查询微信用户对象，封装用户名称
            LambdaQueryWrapper<BuyerUser> buyerUserLambdaQueryWrapper = Wrappers.lambdaQuery(BuyerUser.class);
            buyerUserLambdaQueryWrapper.in(BuyerUser::getUserId, inviteUserId);
            List<BuyerUser> buyerUsers1 = buyerUserMapper.selectList(buyerUserLambdaQueryWrapper);
            // 推荐人找用户
            for (BuyerUser buyerUser : buyerUsers1) {
                for (BuyerUser record : buyerUsers) {
                    String userId = buyerUser.getUserId();
                    String inviteUserId1 = record.getInviteUserId();
                    if (buyerUser.getUserId().equals(record.getInviteUserId())) {
                        record.setInviteUserIdName(buyerUser.getUserName());
                    }
                }
            }
        }
        // 没有推荐人
        for (BuyerUser record : buyerUsers) {
            if (record.getInviteUserIdName() == null) {
                record.setInviteUserIdName("");
            }
        }
        return buyerUsers;
    }

    /**
     * 修改微信用户推荐人
     *
     * @param userId
     * @param inviteUserId
     * @return
     */
    @Override
    public void userIdSetInviteUserId(String userId, String inviteUserId) {
        BuyerUser buyerUser = buyerUserMapper.selectById(userId);
        if (buyerUser == null) throw new CommonException("修改推荐人", "微信用户不存在");
        buyerUser.setInviteUserId(inviteUserId);
        buyerUser.setLastUpdateTime(LocalDateTime.now());
        buyerUser.setUpdateUserId(GetUser.getUserId());
        buyerUserMapper.updateById(buyerUser);
    }

    /**
     * 封号(微信用户)
     *
     * @param userId
     * @return
     */
    @Override
    public void userIdSetStatus(String userId) {
        BuyerUser buyerUser = buyerUserMapper.selectById(userId);
        if (buyerUser == null) throw new CommonException("修改推荐人", "微信用户不存在");
        int i = 0;
        if (buyerUser.getStatus().equals(1)) {
            buyerUser.setStatus(0);
            i++;
        }
        if (buyerUser.getStatus().equals(0) && i == 0) {
            buyerUser.setStatus(1);
        }
        buyerUser.setLastUpdateTime(LocalDateTime.now());
        buyerUser.setUpdateUserId(GetUser.getUserId());
        buyerUserMapper.updateById(buyerUser);
    }

    /**
     * 删除(微信用户)
     *
     * @param userId
     * @return
     */
    @Override
    public void userIdDeleteBuyerUser(String userId) {
        BuyerUser buyerUser = buyerUserMapper.selectById(userId);
        if (buyerUser == null) throw new CommonException("修改推荐人", "微信用户不存在");
        buyerUserMapper.deleteById(userId);
    }

    /**
     * 用户名称列表(模糊查询)
     *
     * @param userName
     * @return
     */
    @Override
    public List<BuyerUser> userNameGetList(String userName) {
        LambdaQueryWrapper<BuyerUser> buyerUserLambdaQueryWrapper = Wrappers.lambdaQuery(BuyerUser.class);
        buyerUserLambdaQueryWrapper.like(BuyerUser::getUserName, userName);
        // 结果数据
        List<BuyerUser> buyerUsers = buyerUserMapper.selectList(buyerUserLambdaQueryWrapper);
        return buyerUsers;
    }

    /**
     * 根据用户id查询用户详情
     *
     * @param userId
     * @return
     */
    @Override
    public BuyerUser userIdGetBuyerUser(String userId) {
        BuyerUser buyerUser = buyerUserMapper.selectById(userId);
        if (buyerUser == null) throw new CommonException("根据用户id查询用户详情", "用户不存在");
        String inviteUserId = buyerUser.getInviteUserId();
        if (inviteUserId != null) {
            BuyerUser buyerUser1 = buyerUserMapper.selectById(inviteUserId);
            buyerUser.setInviteUserIdName(buyerUser1.getUserName());
        } else {
            buyerUser.setInviteUserIdName("");
        }
        return buyerUser;
    }
}