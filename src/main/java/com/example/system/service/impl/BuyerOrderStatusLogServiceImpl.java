package com.example.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.system.exception.CommonException;
import com.example.system.mapper.BuyerOrderStatusLogMapper;
import com.example.system.pojo.BuyerMembershipRule;
import com.example.system.pojo.BuyerOrderStatusLog;
import com.example.system.service.BuyerOrderStatusLogService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@SuppressWarnings("all")
public class BuyerOrderStatusLogServiceImpl implements BuyerOrderStatusLogService {

    private final BuyerOrderStatusLogMapper buyerOrderStatusLogMapper;


    /**
     * 订单状态变化列表
     *
     * @param page
     * @param pageSize
     * @return
     */
    @Override
    public Page<BuyerOrderStatusLog> getPage(Long page, Long pageSize) {
        LambdaQueryWrapper<BuyerOrderStatusLog> buyerUserLambdaQueryWrapper = Wrappers.lambdaQuery(BuyerOrderStatusLog.class);
        buyerUserLambdaQueryWrapper.orderByAsc(BuyerOrderStatusLog::getLogId);
        Page<BuyerOrderStatusLog> buyerOrderStatusLogPage = buyerOrderStatusLogMapper.selectPage(new Page<BuyerOrderStatusLog>(page, pageSize), buyerUserLambdaQueryWrapper);
        return buyerOrderStatusLogPage;
    }
}