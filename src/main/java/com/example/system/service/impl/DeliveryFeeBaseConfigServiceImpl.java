package com.example.system.service.impl;

import com.example.system.mapper.DeliveryFeeBaseConfigMapper;
import com.example.system.service.DeliveryFeeBaseConfigService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@SuppressWarnings("all")
public class DeliveryFeeBaseConfigServiceImpl implements DeliveryFeeBaseConfigService {

    private final DeliveryFeeBaseConfigMapper deliveryFeeBaseConfigMapper;
}