package com.example.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.system.exception.CommonException;
import com.example.system.mapper.BuyerCartMapper;
import com.example.system.pojo.BuyerCart;
import com.example.system.pojo.BuyerCartGoodsItem;
import com.example.system.service.BuyerCartService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@SuppressWarnings("all")
public class BuyerCartServiceImpl implements BuyerCartService {

    private final BuyerCartMapper buyerCartMapper;


    /**
     * 用户购物车列表
     *
     * @param page
     * @param pageSize
     * @return
     */
    @Override
    public Page<BuyerCart> getList(Long page, Long pageSize) {
        LambdaQueryWrapper<BuyerCart> buyerCartLambdaQueryWrapper = Wrappers.lambdaQuery(BuyerCart.class);
        buyerCartLambdaQueryWrapper.orderByAsc(BuyerCart::getCreateTime);
        Page<BuyerCart> buyerCartPage = buyerCartMapper.selectPage(new Page<BuyerCart>(page, pageSize), buyerCartLambdaQueryWrapper);
        return buyerCartPage;
    }
}