package com.example.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.system.exception.CommonException;
import com.example.system.mapper.GoodsQueryLogMapper;
import com.example.system.pojo.BuyerUser;
import com.example.system.pojo.GoodsQueryLog;
import com.example.system.service.GoodsQueryLogService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@SuppressWarnings("all")
public class GoodsQueryLogServiceImpl implements GoodsQueryLogService {

    private final GoodsQueryLogMapper goodsQueryLogMapper;


    /**
     * 商品搜索记录列表
     *
     * @param page
     * @param pageSize
     * @return
     */
    @Override
    public Page<GoodsQueryLog> getPage(Long page, Long pageSize) {
        LambdaQueryWrapper<GoodsQueryLog> buyerUserLambdaQueryWrapper = Wrappers.lambdaQuery(GoodsQueryLog.class);
        buyerUserLambdaQueryWrapper.orderByAsc(GoodsQueryLog::getCreateTime);
        Page<GoodsQueryLog> goodsQueryLogPage = goodsQueryLogMapper.selectPage(new Page<GoodsQueryLog>(page, pageSize), buyerUserLambdaQueryWrapper);
        return goodsQueryLogPage;
    }
}