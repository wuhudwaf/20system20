package com.example.system.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.json.JSONUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.system.dto.AddSysUser;
import com.example.system.dto.LogInUser;
import com.example.system.dto.SysUserDto;
import com.example.system.dto.UserDto;
import com.example.system.exception.CommonException;
import com.example.system.mapper.*;
import com.example.system.pojo.SysRole;
import com.example.system.pojo.SysUser;
import com.example.system.pojo.SysUserRoleRelationship;
import com.example.system.service.SysUserService;
import com.example.system.utils.*;
import lombok.RequiredArgsConstructor;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import static com.example.system.utils.Constants.*;

@Service
@RequiredArgsConstructor
@SuppressWarnings("all")
public class SysUserServiceImpl implements SysUserService {

    private final SysUserMapper sysUserMapper;

    private final SysUserRoleRelationshipMapper sysUserRoleRelationshipMapper;

    private final SysInterfaceRoleMapper sysInterfaceRoleMapper;

    private final SysInterfaceMapper sysInterfaceMapper;

    private final StringRedisTemplate stringRedisTemplate;

    private final SysRoleMapper sysRoleMapper;

    private final RedisIdWorker redisIdWorker;

    private final RedisUtils redisUtils;

    /**
     * 登录
     *
     * @param logInUser
     * @return
     */
    @Override
    public String login(LogInUser logInUser) {
        // 去MySQL查询登录者对象
        LambdaQueryWrapper<SysUser> sellerUserLambdaQueryWrapper = Wrappers.lambdaQuery(SysUser.class);
        sellerUserLambdaQueryWrapper.eq(SysUser::getUserName, logInUser.getPhone());
        SysUser sqlUser = sysUserMapper.selectOne(sellerUserLambdaQueryWrapper);
        // 判断雨
        if (sqlUser == null) {
            // 未注册
            throw new CommonException("登录", logInUser.getPhone() + " 账号不存在");
        }


        if (sqlUser.getDelFlag() == 0) {
            // 已删除
            throw new CommonException("登录", "该账号已被删除，无法登录");
        }
        // 对登录者的密码进行MD5加密，16位
        String pwd = MD5Str.MD5(logInUser.getPwd(), MD5_SIXTEEN);

        // 密码判断
//        if (!(pwd.equals(sqlUser.getPassword()))) {
//            // 密码错误
////            throw new CommonException("登录", "密码错误");
//        }
        // 拷贝 sqlUser数据到 userDto中
//        UserDto userDto = BeanUtil.copyProperties(sqlUser, UserDto.class);
        UserDto userDto = new UserDto();
        userDto.setId(sqlUser.getUserId());
        userDto.setName(sqlUser.getNickName());
        userDto.setImageAddress(sqlUser.getAvatar());
        // 生成token
        String token = TOKEN + UUID.randomUUID().toString();
        // 将用户信息到存储redis
        stringRedisTemplate.opsForValue().set(token, JSONUtil.toJsonStr(userDto), THIRTY_DAYS_TIME, TimeUnit.DAYS);
        // 返回token
        return token;
    }

    /**
     * 退出登录
     *
     * @param httpServletRequest
     * @return
     */
    @Override
    public void exit() {
        HttpServletRequest httpServletRequest = ((ServletRequestAttributes) RequestContextHolder
                .getRequestAttributes()).getRequest();
        String token = httpServletRequest.getHeader("token");
        stringRedisTemplate.delete(token);
    }

    /**
     * 获取所有的门店管理员
     *
     * @return
     */
    @Override
    public Page<SysUserDto> getShopUser(Long page, Long pageSize) {
        // 获取门店管理id集合
        LambdaQueryWrapper<SysUserRoleRelationship> sysUserRoleRelationshipLambdaQueryWrapper = Wrappers.lambdaQuery(SysUserRoleRelationship.class);
        sysUserRoleRelationshipLambdaQueryWrapper.eq(SysUserRoleRelationship::getRoleId, SHOP_ID);
        // 分页查询
        Page<SysUserRoleRelationship> sysUserRoleRelationshipPage =
                sysUserRoleRelationshipMapper.selectPage(new Page<SysUserRoleRelationship>(page, pageSize), sysUserRoleRelationshipLambdaQueryWrapper);
        if (sysUserRoleRelationshipPage.getRecords().size() == 0) {
            // 门店管理不存在
            return null;
        }
        // 拿到所有的管理员对象
        LambdaQueryWrapper<SysUser> sysUserLambdaQueryWrapper = Wrappers.lambdaQuery(SysUser.class);
        sysUserLambdaQueryWrapper.eq(SysUser::getDelFlag, 1);
        List<SysUser> sysUsers = sysUserMapper.selectList(sysUserLambdaQueryWrapper);
        // 准备需要返回的数据
        Page<SysUserDto> sysUserDtoPage = new Page<>();
        // 核心数据
        ArrayList<SysUserDto> newSysUser = new ArrayList<>();
        // 循环匹配
        for (SysUserRoleRelationship record : sysUserRoleRelationshipPage.getRecords()) { // 拥有管理员id
            for (SysUser sysUser : sysUsers) { // 管理员对象
                if (record.getUserId().equals(sysUser.getUserId())) { // 匹配,表示门店管理
                    newSysUser.add(BeanUtil.copyProperties(sysUser, SysUserDto.class));
                }
            }
        }
        // 查询门店管理员角色对象
        SysRole sysRole = sysRoleMapper.selectById(SHOP_ID);
        // 循环封装
        for (SysUserDto sysUserDto : newSysUser) {
            sysUserDto.setSysRole(sysRole);
        }
        // 匹配完毕，封装数据
        BeanUtil.copyProperties(sysUserRoleRelationshipPage, sysUserDtoPage, "records");
        sysUserDtoPage.setRecords(newSysUser);
        // 返回
        return sysUserDtoPage;
    }

    /**
     * 新增管理员
     *
     * @param addSysUser
     * @return
     */
    @Override
    @Transactional
    public void add(AddSysUser addSysUser) {
        String id = redisIdWorker.nextId("addSysUser");
        // 更新id
        addSysUser.setUserId(id);
        // 更新姓名
        addSysUser.setNickName(StringSet.strSet(addSysUser.getPhonenumber()));
        // 对密码进行加密 MD5 16位小写
        String pwd = addSysUser.getPassword();
        pwd = MD5Str.MD5(pwd, MD5_SIXTEEN);
        addSysUser.setPassword(pwd);
        addSysUser.setDelFlag('1');
        // 新增管理员
        SysUser sysUser = BeanUtil.copyProperties(addSysUser, SysUser.class);
        sysUserMapper.insert(sysUser);
        // 建立管理员角色关系
        sysUserRoleRelationshipMapper.insert(new SysUserRoleRelationship(id, addSysUser.getRoleId()));
    }

    /**
     * 查看我的信息
     *
     * @return
     */
    @Override
    public SysUserDto getMyInfo() {
        return GetUser.getSysUserDto();
    }

    /**
     * 拿到当前登录者的权限
     *
     * @param userDto
     * @return
     */
//    public List<String> getInterface(UserDto userDto) {
//        // 根据 管理员id拿到角色 id
//        SysUserRoleRelationship byUserId = sysUserRoleRelationshipMapper.getByUserId(userDto.getId());
//        // 根据角色 id拿到 权限角色关系对象包括id
//        List<SysInterfaceRole> sysInterfaceRoles = sysInterfaceRoleMapper.getByRoleId(byUserId.getRoleId());
//        // 获取所有的权限
//        List<SysInterface> sysInterfaces = sysInterfaceMapper.getList();
//        // 我的权限
//        List<String> myInterface = new ArrayList<>();
//        // 循环拿取该角色对应的权限
//        for (SysInterfaceRole sysInterfaceRole : sysInterfaceRoles) {
//            // 权限id
//            String menuId = sysInterfaceRole.getMenuId();
//            for (SysInterface sysInterface : sysInterfaces) {
//                if (menuId.equals(sysInterface.getId())) {
//                    // 添加权限
//                    myInterface.add(sysInterface.getPermissions());
//                }
//            }
//        }
//        // 权限添加完毕 返回权限
//        return myInterface;
//    }
}