package com.example.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.system.exception.CommonException;
import com.example.system.mapper.GoodsFlashSaleItemMapper;
import com.example.system.mapper.GoodsFlashSaleMapper;
import com.example.system.pojo.GoodsFlashSale;
import com.example.system.pojo.GoodsFlashSaleItem;
import com.example.system.service.GoodsFlashSaleItemService;
import com.example.system.utils.GetUser;
import com.example.system.utils.RedisIdWorker;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

/**
 * 商品限时秒杀项目信息
 */
@Service
@RequiredArgsConstructor
@SuppressWarnings("all")
public class GoodsFlashSaleItemServiceImpl extends ServiceImpl<GoodsFlashSaleItemMapper, GoodsFlashSaleItem> implements GoodsFlashSaleItemService {

    /**
     * 商品限时秒杀项目信息
     */
    private final GoodsFlashSaleItemMapper goodsFlashSaleItemMapper;

    /**
     * 商品限时秒杀
     */
    private final GoodsFlashSaleMapper goodsFlashSaleMapper;

    /**
     * 小型雪花算法
     */
    private final RedisIdWorker redisIdWorker;

    /**
     * 秒杀和商品sku关联接口
     *
     * @param list
     * @return
     */
    @Override
    public void addGoodsFlashSaleItem(GoodsFlashSaleItem goodsFlashSaleItem) {
        // 获取商品秒杀数据
        LambdaQueryWrapper<GoodsFlashSale> goodsFlashSaleLambdaQueryWrapper = Wrappers.lambdaQuery(GoodsFlashSale.class);
        goodsFlashSaleLambdaQueryWrapper.eq(GoodsFlashSale::getStatus, 1);
        // 拿到结果
        GoodsFlashSale goodsFlashSale = goodsFlashSaleMapper.selectOne(goodsFlashSaleLambdaQueryWrapper);
        // 商品秒杀是否为空，空则抛异常
        if (goodsFlashSale == null) throw new CommonException("秒杀和商品sku关联接口", "暂无商品秒杀活动，关联失败");
        // 拿到商品限时秒杀id
        String flashSaleId = goodsFlashSale.getFlashSaleId();
        // 获取登录者id
        String userId = GetUser.getUserId();
        // 生成当前时间
        LocalDateTime time = LocalDateTime.now();
        // 封装且新增商品限时秒杀项目信息
        goodsFlashSaleItem.setItemId(redisIdWorker.nextId("goodsFlashSaleItem"));
        goodsFlashSaleItem.setFlashSaleId(flashSaleId);
        goodsFlashSaleItem.setCreateTime(time);
        goodsFlashSaleItem.setCreateBy(userId);
        goodsFlashSaleItem.setLastUpdateTime(time);
        goodsFlashSaleItem.setUpdateBy(userId);
        // 新增
        goodsFlashSaleItemMapper.insert(goodsFlashSaleItem);
    }

    /**
     * 删除一个秒杀和商品sku关联
     *
     * @param skuId
     * @return
     */
    @Override
    public void deleteGoodsFlashSaleItem(String skuId) {
        // 条件构造器
        LambdaQueryWrapper<GoodsFlashSaleItem> goodsFlashSaleItemLambdaQueryWrapper = Wrappers.lambdaQuery(GoodsFlashSaleItem.class);
        // 根据商品skuid
        goodsFlashSaleItemLambdaQueryWrapper.eq(GoodsFlashSaleItem::getGoodsSkuId, skuId);
        // 查询
        GoodsFlashSaleItem goodsFlashSaleItem = goodsFlashSaleItemMapper.selectOne(goodsFlashSaleItemLambdaQueryWrapper);
        // 是否存在
        if (goodsFlashSaleItem == null) throw new CommonException("删除一个秒杀和商品sku关联", "不存在");
        // 存在，删除
        if (goodsFlashSaleItemMapper.delete(goodsFlashSaleItemLambdaQueryWrapper) <= 0)
            throw new CommonException("删除一个秒杀和商品sku关联", "网络异常，稍后再试");
    }

    /**
     * 修改商品限时秒杀项目信息
     *
     * @param goodsFlashSaleItem
     * @return
     */
    @Override
    public void updateGoodsFlashSaleItem(GoodsFlashSaleItem goodsFlashSaleItem) {
        LambdaQueryWrapper<GoodsFlashSaleItem> goodsFlashSaleItemLambdaQueryWrapper = Wrappers.lambdaQuery(GoodsFlashSaleItem.class);
        goodsFlashSaleItemLambdaQueryWrapper.eq(GoodsFlashSaleItem::getItemId, goodsFlashSaleItem.getItemId());
        goodsFlashSaleItem.setLastUpdateTime(LocalDateTime.now());
        goodsFlashSaleItem.setUpdateBy(GetUser.getUserId());
        if (goodsFlashSaleItemMapper.update(goodsFlashSaleItem, goodsFlashSaleItemLambdaQueryWrapper) <= 0)
            throw new CommonException("修改商品限时秒杀项目信息", "网络异常，稍后再试");
    }

    /**
     * 根据itemId获取商品限时秒杀项目信息
     *
     * @param itemId
     * @return
     */
    @Override
    public GoodsFlashSaleItem getGoodsFlashSaleItem(String itemId) {
        LambdaQueryWrapper<GoodsFlashSaleItem> goodsFlashSaleItemLambdaQueryWrapper = Wrappers.lambdaQuery(GoodsFlashSaleItem.class);
        goodsFlashSaleItemLambdaQueryWrapper.eq(GoodsFlashSaleItem::getItemId, itemId);
        return goodsFlashSaleItemMapper.selectOne(goodsFlashSaleItemLambdaQueryWrapper);
    }

    /**
     * 获取商品限时秒杀项目信息列表
     *
     * @param page
     * @param pageSize
     * @return
     */
    @Override
    public Page<GoodsFlashSaleItem> getGoodsFlashSaleItemPage(Long page, Long pageSize) {
        LambdaQueryWrapper<GoodsFlashSaleItem> goodsFlashSaleItemLambdaQueryWrapper = Wrappers.lambdaQuery(GoodsFlashSaleItem.class);
        goodsFlashSaleItemLambdaQueryWrapper.orderByAsc(GoodsFlashSaleItem::getCreateTime);
        Page<GoodsFlashSaleItem> goodsFlashSaleItemPage =
                goodsFlashSaleItemMapper.selectPage(new Page<GoodsFlashSaleItem>(page, pageSize), goodsFlashSaleItemLambdaQueryWrapper);
        return goodsFlashSaleItemPage;
    }
}