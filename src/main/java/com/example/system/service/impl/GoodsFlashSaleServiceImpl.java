package com.example.system.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.extra.spring.SpringUtil;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.system.dto.ReturnGoodsFlashSale;
import com.example.system.exception.CommonException;
import com.example.system.mapper.GoodsFlashSaleItemMapper;
import com.example.system.mapper.GoodsFlashSaleMapper;
import com.example.system.mapper.GoodsSkuMapper;
import com.example.system.mapper.SysGoodsMapper;
import com.example.system.pojo.GoodsFlashSale;
import com.example.system.pojo.GoodsFlashSaleItem;
import com.example.system.pojo.GoodsSku;
import com.example.system.pojo.SysGoods;
import com.example.system.pojo.vo.GoodsFlashSaleVo;
import com.example.system.service.GoodsFlashSaleService;
import com.example.system.utils.GetUser;
import com.example.system.utils.RedisIdWorker;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TreeSet;

@Service
@RequiredArgsConstructor
@SuppressWarnings("all")
public class GoodsFlashSaleServiceImpl extends ServiceImpl<SysGoodsMapper, SysGoods> implements GoodsFlashSaleService {

    /**
     * 商品限时秒杀
     */
    private final GoodsFlashSaleMapper goodsFlashSaleMapper;

    /**
     * 商品限时秒杀 商品 关联
     */
    private final GoodsFlashSaleItemMapper goodsFlashSaleItemMapper;

    /**
     * 商品sku
     */
    private final GoodsSkuMapper goodsSkuMapper;

    /**
     * 商品
     */
    private final SysGoodsMapper sysGoodsMapper;

    /**
     * 小型雪花算法
     */
    private final RedisIdWorker redisIdWorker;

    // 将商品设置为秒杀
    public void setActivity(List<GoodsFlashSaleItem> goodsFlashSaleItems) {
        ArrayList<String> goodsSkuIds = new ArrayList<>();
        for (GoodsFlashSaleItem goodsFlashSaleItem : goodsFlashSaleItems) {
            goodsSkuIds.add(goodsFlashSaleItem.getGoodsSkuId());
        }
        LambdaQueryWrapper<GoodsSku> goodsSkuLambdaQueryWrapper = Wrappers.lambdaQuery(GoodsSku.class);
        goodsSkuLambdaQueryWrapper.in(GoodsSku::getSkuId, goodsSkuIds);
        List<GoodsSku> goodsSkus = goodsSkuMapper.selectList(goodsSkuLambdaQueryWrapper);
        TreeSet<String> goodsIds = new TreeSet<>();
        for (GoodsSku skus : goodsSkus) {
            goodsIds.add(skus.getGoodsId());
        }
        // 获取商品列表
        List<SysGoods> sysGoods = sysGoodsMapper.selectBatchIds(goodsIds);
        // 修改商品activity
        for (SysGoods sysGood : sysGoods) {
            sysGood.setActivity(3);
        }
        // 更新
        updateBatchById(sysGoods);
    }

    /**
     * 将商品移除秒杀
     *
     * @param goodsFlashSaleItems
     */
    public void deleteActivity(List<GoodsFlashSaleItem> goodsFlashSaleItems) {
        ArrayList<String> goodsSkuIds = new ArrayList<>();
        for (GoodsFlashSaleItem goodsFlashSaleItem : goodsFlashSaleItems) {
            goodsSkuIds.add(goodsFlashSaleItem.getGoodsSkuId());
        }
        LambdaQueryWrapper<GoodsSku> goodsSkuLambdaQueryWrapper = Wrappers.lambdaQuery(GoodsSku.class);
        goodsSkuLambdaQueryWrapper.in(GoodsSku::getSkuId, goodsSkuIds);
        List<GoodsSku> goodsSkus = goodsSkuMapper.selectList(goodsSkuLambdaQueryWrapper);
        TreeSet<String> goodsIds = new TreeSet<>();
        for (GoodsSku skus : goodsSkus) {
            goodsIds.add(skus.getGoodsId());
        }
        // 获取商品列表
        List<SysGoods> sysGoods = sysGoodsMapper.selectBatchIds(goodsIds);
        // 修改商品activity
        for (SysGoods sysGood : sysGoods) {
            // 表示未参与
            sysGood.setActivity(1);
        }
        // 更新
        updateBatchById(sysGoods);
    }

    /**
     * 新增秒杀活动
     *
     * @param returnGoodsFlashSale
     * @return
     */
    @Transactional
    @Override
    public void addGoodsFlashSale(ReturnGoodsFlashSale returnGoodsFlashSale) {
        List<GoodsFlashSaleItem> goodsFlashSaleItems = returnGoodsFlashSale.getGoodsFlashSaleItems();
        // 是否选择sku
        if (goodsFlashSaleItems.size() == 0) throw new CommonException("新增秒杀活动", "请选择商品秒杀");
        // 对goodsFlashSaleItems进行校验,确认是否设置秒杀价格
        for (GoodsFlashSaleItem goodsFlashSaleItem : goodsFlashSaleItems) {
            // 秒杀价格是否为空
            if (goodsFlashSaleItem.getFlashSalePrice() == null) {
                throw new CommonException("新增秒杀活动", "请输入商品秒杀价");
            }
        }
        // 继续校验，新增的商品是否存在早已添加秒杀的商品
        // 根据 goodsFlashSaleItems获取skuid集合
        List<String> skuId = new ArrayList<>();
        for (GoodsFlashSaleItem goodsFlashSaleItem : goodsFlashSaleItems) {
            skuId.add(goodsFlashSaleItem.getGoodsSkuId());
        }
        LambdaQueryWrapper<GoodsFlashSaleItem> goodsFlashSaleItemLambdaQueryWrapper = Wrappers.lambdaQuery(GoodsFlashSaleItem.class);
        goodsFlashSaleItemLambdaQueryWrapper.in(GoodsFlashSaleItem::getGoodsSkuId, skuId);
        // 根据 skuId获取商品限时秒杀信息数据
        List<GoodsFlashSaleItem> sqlGoodsFlashSaleItems = goodsFlashSaleItemMapper.selectList(goodsFlashSaleItemLambdaQueryWrapper);
        // 是否能够查到数据
        if (sqlGoodsFlashSaleItems.size() > 0) {
            // 拿到 sqlGoodsFlashSaleItems 中的skuid
            TreeSet<String> skuIdSet = new TreeSet<>();
            for (GoodsFlashSaleItem sqlGoodsFlashSaleItem : sqlGoodsFlashSaleItems) {
                skuIdSet.add(sqlGoodsFlashSaleItem.getGoodsSkuId());
            }
            // 拿着skuid去goodssku表中获取数据，拿到对应的商品id
            List<GoodsSku> goodsSkus = goodsSkuMapper.selectBatchIds(skuIdSet);
            // 拿到商品id
            TreeSet<String> goodsId = new TreeSet<>();
            for (GoodsSku skus : goodsSkus) {
                goodsId.add(skus.getGoodsId());
            }
            // 拿着商品id去商品表中拿数据
            List<SysGoods> sysGoodsList = sysGoodsMapper.selectBatchIds(goodsId);
            // 获取一条
            SysGoods sysGoods = sysGoodsList.get(0);
            // 抛异常
            throw new CommonException("新增秒杀活动", sysGoods.getName() + ": " + "增在参与活动中，不可重复添加");
        }
        // 获取登录者id
        String userId = GetUser.getUserId();
        // 封装商品限时秒杀对象 returnGoodsFlashSale
        returnGoodsFlashSale.setFlashSaleId(redisIdWorker.nextId("GoodsFlashSale"));
        returnGoodsFlashSale.setStatus(0);
        returnGoodsFlashSale.setCreateTime(LocalDateTime.now());
        returnGoodsFlashSale.setLastUpdateTime(LocalDateTime.now());
        returnGoodsFlashSale.setCreateBy(userId);
        returnGoodsFlashSale.setUpdateBy(userId);
        // 循环goodsFlashSaleItems封装对象数据
        for (GoodsFlashSaleItem goodsFlashSaleItem : goodsFlashSaleItems) {
            goodsFlashSaleItem.setItemId(redisIdWorker.nextId("GoodsFlashSaleItem"));
            goodsFlashSaleItem.setFlashSaleId(returnGoodsFlashSale.getFlashSaleId());
            goodsFlashSaleItem.setCreateTime(LocalDateTime.now());
            goodsFlashSaleItem.setLastUpdateTime(LocalDateTime.now());
            goodsFlashSaleItem.setCreateBy(userId);
            goodsFlashSaleItem.setUpdateBy(userId);
        }
        GoodsFlashSale goodsFlashSale = BeanUtil.copyProperties(returnGoodsFlashSale, GoodsFlashSale.class, "startTime", "endTime");
        // 将时间戳转换为localdateTime且封装到goodsFlashSale对象中
        goodsFlashSale.setStartTime(LocalDateTime.ofInstant(Instant.ofEpochMilli(returnGoodsFlashSale.getStartTime()), ZoneId.systemDefault()));
        goodsFlashSale.setEndTime(LocalDateTime.ofInstant(Instant.ofEpochMilli(returnGoodsFlashSale.getEndTime()), ZoneId.systemDefault()));
        goodsFlashSaleMapper.insert(goodsFlashSale);
        goodsFlashSaleItemMapper.insertBatchSomeColumn(goodsFlashSaleItems);
        // 将商品设置为秒杀
        setActivity(goodsFlashSaleItems);
    }

    /**
     * 修改商品限时秒杀活动接口
     *
     * @param returnGoodsFlashSale
     * @return
     */
    @Transactional
    @Override
    public void setGoodsFlashSale(ReturnGoodsFlashSale returnGoodsFlashSale) {
        LambdaQueryWrapper<GoodsFlashSale> goodsFlashSaleLambdaQueryWrapper = new LambdaQueryWrapper<>();
        // where 判断主键，获取上架的秒杀
        goodsFlashSaleLambdaQueryWrapper.eq(GoodsFlashSale::getFlashSaleId, returnGoodsFlashSale.getFlashSaleId());
        // 查询到的数据
        GoodsFlashSale sqlGoodsFlashSale = goodsFlashSaleMapper.selectOne(goodsFlashSaleLambdaQueryWrapper);
        // 判断当前数据是否存在
        if (sqlGoodsFlashSale == null) throw new CommonException("修改商品限时秒杀活动接口", "商品限时秒杀活动不存在");
        // 有数据，表示可以更新，拷贝数据到真对象中，忽略开始时间和结束时间
        GoodsFlashSale goodsFlashSale = BeanUtil.copyProperties(returnGoodsFlashSale, GoodsFlashSale.class, "startTime", "endTime");
        // 将时间戳转换为localdateTime且封装到goodsFlashSale对象中
        Long startTime = returnGoodsFlashSale.getStartTime();
        Long endTime = returnGoodsFlashSale.getEndTime();
        goodsFlashSale.setStartTime(LocalDateTime.ofInstant(Instant.ofEpochMilli(startTime), ZoneId.systemDefault()));
        goodsFlashSale.setEndTime(LocalDateTime.ofInstant(Instant.ofEpochMilli(endTime), ZoneId.systemDefault()));
        // 更新时间，更新人
        String userId = GetUser.getUserId();
        goodsFlashSale.setUpdateBy(userId);
        goodsFlashSale.setLastUpdateTime(LocalDateTime.now());
        // 修改
        if (goodsFlashSaleMapper.update(goodsFlashSale, goodsFlashSaleLambdaQueryWrapper) <= 0)
            throw new CommonException("修改商品限时秒杀活动接口", "网络异常，稍后再试");
        List<GoodsFlashSaleItem> goodsFlashSaleItems = returnGoodsFlashSale.getGoodsFlashSaleItems();
        // 是否选择sku
        if (goodsFlashSaleItems.size() == 0) throw new CommonException("修改商品限时秒杀活动接口", "请选择商品秒杀");
        // 对goodsFlashSaleItems进行校验,确认是否设置秒杀价格
        for (GoodsFlashSaleItem goodsFlashSaleItem : goodsFlashSaleItems) {
            // 秒杀价格是否为空
            if (goodsFlashSaleItem.getFlashSalePrice() == null) {
                throw new CommonException("新增秒杀活动", "请输入商品秒杀价");
            }
        }
        // 获取这些skuId
        ArrayList<String> skuIds = new ArrayList<>();
        for (GoodsFlashSaleItem goodsFlashSaleItem : goodsFlashSaleItems) {
            skuIds.add(goodsFlashSaleItem.getGoodsSkuId());
        }
        // 拿着skuid去秒杀项目信息拿到已经添加秒杀的skuid
        LambdaQueryWrapper<GoodsFlashSaleItem> goodsFlashSaleItemLambdaQueryWrapper = Wrappers.lambdaQuery(GoodsFlashSaleItem.class);
        goodsFlashSaleItemLambdaQueryWrapper.in(GoodsFlashSaleItem::getGoodsSkuId, skuIds);
        List<GoodsFlashSaleItem> sqlGoodsFlashSaleItems = goodsFlashSaleItemMapper.selectList(goodsFlashSaleItemLambdaQueryWrapper);
        // 拿到 sqlGoodsFlashSaleItems中的skuid列表
        ArrayList<String> sqlSkuId = new ArrayList<>();
        for (GoodsFlashSaleItem sqlGoodsFlashSaleItem : sqlGoodsFlashSaleItems) {
            sqlSkuId.add(sqlGoodsFlashSaleItem.getGoodsSkuId());
        }
        ArrayList<String> updateSkuId = new ArrayList<>();
        // skuIds：总skuid | sqlSkuId：已关联秒杀的skuid
        for (String aString : sqlSkuId) {
            // for倒序遍历删除
            for (int i = skuIds.size() - 1; i >= 0; i--) {
                if (aString.equals(skuIds.get(i))) {
                    updateSkuId.add(skuIds.get(i));
                    skuIds.remove(i);
                }
            }
        }
        // 这个集合是新的值,需要更新的
        ArrayList<GoodsFlashSaleItem> updateGoodsFlashSaleItems = new ArrayList<>();
        for (String aString : updateSkuId) {
            for (GoodsFlashSaleItem goodsFlashSaleItem : goodsFlashSaleItems) {
                if (aString.equals(goodsFlashSaleItem.getGoodsSkuId())) {
                    updateGoodsFlashSaleItems.add(goodsFlashSaleItem);
                    break;
                }
            }
        }
        LambdaQueryWrapper<GoodsFlashSaleItem> goodsFlashSaleItemLambdaQueryWrapper1 = Wrappers.lambdaQuery(GoodsFlashSaleItem.class);
        goodsFlashSaleItemLambdaQueryWrapper1.eq(GoodsFlashSaleItem::getFlashSaleId, returnGoodsFlashSale.getFlashSaleId());
        goodsFlashSaleItemLambdaQueryWrapper1.in(GoodsFlashSaleItem::getGoodsSkuId, updateSkuId);
        // 这个集合就是需要更新的，秒杀值还未修改
        List<GoodsFlashSaleItem> goodsFlashSaleItems1 = goodsFlashSaleItemMapper.selectList(goodsFlashSaleItemLambdaQueryWrapper1);
        // 更新 goodsFlashSaleItems1集合秒杀值
        for (GoodsFlashSaleItem updateGoodsFlashSaleItem : updateGoodsFlashSaleItems) {
            for (GoodsFlashSaleItem goodsFlashSaleItem : goodsFlashSaleItems1) {
                if (updateGoodsFlashSaleItem.getGoodsSkuId().equals(goodsFlashSaleItem.getGoodsSkuId())) {
                    goodsFlashSaleItem.setFlashSalePrice(updateGoodsFlashSaleItem.getFlashSalePrice());
                }
            }
        }
        // 现在 skuIds集合中是新的,根据skuid，获取对应的 GoodsFlashSaleItem对象
        ArrayList<GoodsFlashSaleItem> newGoodsFlashSaleItems = new ArrayList<>();
        for (String skuId : skuIds) {
            for (GoodsFlashSaleItem goodsFlashSaleItem : goodsFlashSaleItems) {
                if (skuId.equals(goodsFlashSaleItem.getGoodsSkuId())) {
                    newGoodsFlashSaleItems.add(goodsFlashSaleItem);
                    break;
                }
            }
        }
        // 循环goodsFlashSaleItems封装对象数据
        for (GoodsFlashSaleItem goodsFlashSaleItem : newGoodsFlashSaleItems) {
            goodsFlashSaleItem.setItemId(redisIdWorker.nextId("GoodsFlashSaleItem"));
            goodsFlashSaleItem.setFlashSaleId(returnGoodsFlashSale.getFlashSaleId());
            goodsFlashSaleItem.setCreateTime(LocalDateTime.now());
            goodsFlashSaleItem.setLastUpdateTime(LocalDateTime.now());
            goodsFlashSaleItem.setCreateBy(userId);
            goodsFlashSaleItem.setUpdateBy(userId);
        }
        if (newGoodsFlashSaleItems.size() == 0) return;
        goodsFlashSaleItemMapper.insertBatchSomeColumn(newGoodsFlashSaleItems);
        // 批量更新
        GoodsFlashSaleItemServiceImpl bean = SpringUtil.getBean(GoodsFlashSaleItemServiceImpl.class);
        if (updateGoodsFlashSaleItems.size() > 0) bean.updateBatchById(goodsFlashSaleItems1);
        // 将商品设置为秒杀
        setActivity(newGoodsFlashSaleItems);
    }

    /**
     * 根据秒杀id看商品限时秒杀活动接口详情
     *
     * @param flashSaleId
     * @return
     */
    @Override
    public GoodsFlashSaleVo getGoodsFlashSale(String flashSaleId) {
        LambdaQueryWrapper<GoodsFlashSale> goodsFlashSaleLambdaQueryWrapper = Wrappers.lambdaQuery(GoodsFlashSale.class);
        goodsFlashSaleLambdaQueryWrapper.eq(GoodsFlashSale::getFlashSaleId, flashSaleId);
        // 拿到商品限时秒杀
        GoodsFlashSale goodsFlashSale = goodsFlashSaleMapper.selectOne(goodsFlashSaleLambdaQueryWrapper);
        if (goodsFlashSale == null) throw new CommonException("根据秒杀id看商品限时秒杀活动接口详情", "限时秒杀不存在");
        // 商品限时秒杀对象dto GoodsFlashSaleVo
        GoodsFlashSaleVo goodsFlashSaleVo = BeanUtil.copyProperties(goodsFlashSale, GoodsFlashSaleVo.class, "flashSaleId", "startTime", "endTime");
        goodsFlashSaleVo.setFlashSaleId(goodsFlashSale.getFlashSaleId().toString());
        // 开始时间和结束时间转换时间戳
        LocalDateTime startTime = goodsFlashSale.getStartTime();
        LocalDateTime endTime = goodsFlashSale.getEndTime();
        // 转换为时间戳且封装数据
        goodsFlashSaleVo.setStartTime(startTime.toInstant(ZoneOffset.of("+8")).toEpochMilli());
        goodsFlashSaleVo.setEndTime(endTime.toInstant(ZoneOffset.of("+8")).toEpochMilli());
        // 根据商品限时秒杀获取商品sku
        LambdaQueryWrapper<GoodsFlashSaleItem> goodsFlashSaleItemLambdaQueryWrapper = Wrappers.lambdaQuery(GoodsFlashSaleItem.class);
        goodsFlashSaleItemLambdaQueryWrapper.eq(GoodsFlashSaleItem::getFlashSaleId, goodsFlashSale.getFlashSaleId());
        // 商品sku与秒杀关联对象集合
        List<GoodsFlashSaleItem> goodsFlashSaleItems = goodsFlashSaleItemMapper.selectList(goodsFlashSaleItemLambdaQueryWrapper);
        // 根据 goodsFlashSaleItems获取 商品skuid主键集合
        TreeSet<String> skuId = new TreeSet<>();
        for (GoodsFlashSaleItem goodsFlashSaleItem : goodsFlashSaleItems) {
            skuId.add(goodsFlashSaleItem.getGoodsSkuId());
        }
        // 判断 skuId集合是否存有数据,没有就直接返回最终结果数据
        if (skuId.size() == 0) {
            // 没有商品限时秒杀项目信息，也就没有商品和sku，所以直接返回即可，也避免了报错
            return goodsFlashSaleVo;
        }
        // 根据 goodsSkusId拿到 商品sku集合对象
        List<GoodsSku> goodsSkus = goodsSkuMapper.selectBatchIds(skuId);
        // 为商品sku封装限时秒杀价格
        for (GoodsFlashSaleItem goodsFlashSaleItem : goodsFlashSaleItems) {
            for (GoodsSku skus : goodsSkus) {
                if (goodsFlashSaleItem.getGoodsSkuId().equals(skus.getSkuId())) {
                    skus.setStatus(1);
                    skus.setFlashSalePrice(goodsFlashSaleItem.getFlashSalePrice());
                }
            }
        }
        // 根据 goodsSkus拿到商品id集合
        TreeSet<String> goodsIds = new TreeSet<>();
        for (GoodsSku skus : goodsSkus) {
            goodsIds.add(skus.getGoodsId());
        }
        // 没有对应的sku
        if (goodsIds.size() == 0) return goodsFlashSaleVo;
        // 根据 goodsIds查询到商品列表
        List<SysGoods> sysGoods = sysGoodsMapper.selectBatchIds(goodsIds);
        int i = 0;
        // 将商品sku封装到商品对象中
        for (SysGoods sysGood : sysGoods) {
            i += 1;
            // 商品id
            String goodsId = sysGood.getGoodsId();
            // 商品sku列表
            List<GoodsSku> returnGoodsSkus = sysGood.getGoodsSkus();
            for (GoodsSku skus : goodsSkus) {
                // sku商品id
                String skuGoodsId = skus.getGoodsId();
                // 存储JSON对象,第一重循环即可转递完毕，因为skuVal 字段被制空，所以第二重循环继续此操作则会报错
                if (i == 1) {
                    // 表示第一重循环
                    skus.setSkuValJson(JSONObject.parseObject(skus.getSkuVal()));
                    skus.setSkuVal("");
                }
                // 当前sku对应的商品
                if (goodsId.equals(skuGoodsId)) {
                    // 存储当前商品的 商品sku
                    returnGoodsSkus.add(skus);
                }
            }
            // 封装sku列表
            sysGood.setGoodsSkus(returnGoodsSkus);
        }
        // 将商品列表封装到商品限时秒杀dto ReturnGoodsFlashSale中
        goodsFlashSaleVo.setSysGoods(sysGoods);
        return goodsFlashSaleVo;
    }

    /**
     * 开始秒杀/结束秒杀商品限时秒杀
     *
     * @param flashSaleId
     */
    @Override
    public String setStatus(String flashSaleId) {
        System.out.println("开始秒杀/结束秒杀商品限时秒杀 --> " + flashSaleId);
        LambdaQueryWrapper<GoodsFlashSale> goodsFlashSaleLambdaQueryWrapper = Wrappers.lambdaQuery(GoodsFlashSale.class);
        goodsFlashSaleLambdaQueryWrapper.eq(GoodsFlashSale::getFlashSaleId, flashSaleId);
        // 查询要修改的商品秒杀
        GoodsFlashSale goodsFlashSale = goodsFlashSaleMapper.selectOne(goodsFlashSaleLambdaQueryWrapper);
        if (goodsFlashSale == null) throw new CommonException("上架/下架商品限时秒杀", "商品限时秒杀不存在");
        // 获取当前秒杀的状态
        int status = goodsFlashSale.getStatus();
        if (status == 1) {
            // 结束秒杀
            goodsFlashSale.setStatus(2);
            goodsFlashSaleMapper.update(goodsFlashSale, goodsFlashSaleLambdaQueryWrapper);
            return "结束商品限时秒杀成功";
        }
        if (status == 2) throw new CommonException("上架/下架商品限时秒杀", "商品限时秒杀已结束，请忽重复点击");
        // 表示上架 0
        goodsFlashSaleLambdaQueryWrapper = Wrappers.lambdaQuery(GoodsFlashSale.class);
        // 获取表中已上架的商品限时秒杀
        goodsFlashSaleLambdaQueryWrapper.eq(GoodsFlashSale::getStatus, 1);
        GoodsFlashSale newGoodsFlashSale = goodsFlashSaleMapper.selectOne(goodsFlashSaleLambdaQueryWrapper);
        if (newGoodsFlashSale != null) throw new CommonException("上架/下架商品限时秒杀", "已有上架的商品限时秒杀");
        goodsFlashSaleLambdaQueryWrapper = Wrappers.lambdaQuery(GoodsFlashSale.class);
        goodsFlashSaleLambdaQueryWrapper.eq(GoodsFlashSale::getFlashSaleId, flashSaleId);
        goodsFlashSale.setStatus(1);
        goodsFlashSaleMapper.update(goodsFlashSale, goodsFlashSaleLambdaQueryWrapper);
        return "开始商品限时秒杀成功";
    }

    /**
     * 删除商品限时秒杀
     *
     * @param flashSaleId
     * @return
     */
    @Transactional
    @Override
    public void deleteGoodsFlashSale(String flashSaleId) {
        LambdaQueryWrapper<GoodsFlashSale> goodsFlashSaleLambdaQueryWrapper = Wrappers.lambdaQuery(GoodsFlashSale.class);
        goodsFlashSaleLambdaQueryWrapper.eq(GoodsFlashSale::getFlashSaleId, flashSaleId);
        // 查询要删除的商品秒杀
        GoodsFlashSale goodsFlashSale = goodsFlashSaleMapper.selectOne(goodsFlashSaleLambdaQueryWrapper);
        if (goodsFlashSale == null) throw new CommonException("删除商品限时秒杀", "商品限时秒杀不存在");
        // 存在，删除
        goodsFlashSaleMapper.delete(goodsFlashSaleLambdaQueryWrapper);
        LambdaQueryWrapper<GoodsFlashSaleItem> goodsFlashSaleItemLambdaQueryWrapper = Wrappers.lambdaQuery(GoodsFlashSaleItem.class);
        goodsFlashSaleItemLambdaQueryWrapper.eq(GoodsFlashSaleItem::getFlashSaleId, flashSaleId);
        List<GoodsFlashSaleItem> goodsFlashSaleItems = goodsFlashSaleItemMapper.selectList(goodsFlashSaleItemLambdaQueryWrapper);
        if (goodsFlashSaleItems.size() == 0) return;
        // 有秒杀sku,拿到这个itemId
        ArrayList<String> itemId = new ArrayList<>();
        for (GoodsFlashSaleItem goodsFlashSaleItem : goodsFlashSaleItems) {
            itemId.add(goodsFlashSaleItem.getItemId());
        }
        // 批量删除
        goodsFlashSaleItemMapper.deleteBatchIds(itemId);
        deleteActivity(goodsFlashSaleItems);
    }

    /**
     * 获取商品限时秒杀列表
     *
     * @param page
     * @param pageSize
     * @return
     */
    @Override
    public Page<GoodsFlashSaleVo> getGoodsFlashSalePage(Long page, Long pageSize) {
        LambdaQueryWrapper<GoodsFlashSale> goodsFlashSaleLambdaQueryWrapper = Wrappers.lambdaQuery(GoodsFlashSale.class);
        goodsFlashSaleLambdaQueryWrapper.orderByDesc(GoodsFlashSale::getStatus);
        // 获取到的数据
        Page<GoodsFlashSale> goodsFlashSalePage = goodsFlashSaleMapper.selectPage(new Page<GoodsFlashSale>(page, pageSize), goodsFlashSaleLambdaQueryWrapper);
        // 循环更新数据，最终返回的是 Page<ReturnGoodsFlashSale>
        List<GoodsFlashSale> records = goodsFlashSalePage.getRecords();
        // 需要的数据
        List<GoodsFlashSaleVo> goodsFlashSaleVos = new ArrayList<>();
        for (GoodsFlashSale record : records) {
            GoodsFlashSaleVo goodsFlashSaleVo = BeanUtil.copyProperties(record, GoodsFlashSaleVo.class, "flashSaleId", "startTime", "endTime");
            goodsFlashSaleVo.setFlashSaleId(record.getFlashSaleId().toString());
            // 转换为时间戳且封装数据
            goodsFlashSaleVo.setStartTime(record.getStartTime().toInstant(ZoneOffset.of("+8")).toEpochMilli());
            goodsFlashSaleVo.setEndTime(record.getEndTime().toInstant(ZoneOffset.of("+8")).toEpochMilli());
            goodsFlashSaleVos.add(goodsFlashSaleVo);
        }
        Page<GoodsFlashSaleVo> goodsFlashSaleVoPage = new Page<>();
        // 拷贝分页对象
        BeanUtil.copyProperties(goodsFlashSalePage, goodsFlashSaleVoPage, "records");
        goodsFlashSaleVoPage.setRecords(goodsFlashSaleVos);
        return goodsFlashSaleVoPage;
    }
}