package com.example.system.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.RandomUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.system.exception.CommonException;
import com.example.system.mapper.*;
import com.example.system.pojo.*;
import com.example.system.pojo.imports.SysGoodsImport;
import com.example.system.pojo.page.NewPage;
import com.example.system.pojo.vo.add.SysGoodsAdd;
import com.example.system.pojo.where.SysGoodsQuery;
import com.example.system.service.GoodsSkuService;
import com.example.system.service.SysGoodsService;
import com.example.system.utils.GetUser;
import com.example.system.utils.RedisIdWorker;
import com.example.system.utils.hb.HbUtils;
import lombok.RequiredArgsConstructor;
import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TreeSet;


@Service
@RequiredArgsConstructor
@SuppressWarnings("all")
public class SysGoodsServiceImpl extends ServiceImpl<SysGoodsMapper, SysGoods> implements SysGoodsService {

    /**
     * 海报生成
     */
    private final HbUtils hbUtils;

    /**
     * 商品
     */
    private final SysGoodsMapper sysGoodsMapper;

    /**
     * 商品sku表
     */
    private final GoodsSkuMapper goodsSkuMapper;

    /**
     * 商品限时秒杀
     */
    private final GoodsFlashSaleMapper goodsFlashSaleMapper;

    /**
     * 商品限时秒杀项目信息
     */
    private final GoodsFlashSaleItemMapper goodsFlashSaleItemMapper;

    /**
     * 商品类别
     */
    private final SysCategoryMapper sysCategoryMapper;

    private final SellerInventoryMapper sellerInventoryMapper;

    /**
     * 小型雪花算法
     */
    private final RedisIdWorker redisIdWorker;

    /**
     * 新增商品
     *
     * @param sysGoodsAdd
     * @return
     */
    @Override
    public void add(SysGoodsAdd sysGoodsAdd) {
        // 新增商品
        String id = redisIdWorker.nextId("goods");
        sysGoodsAdd.setGoodsId(id);
        sysGoodsAdd.setTenantId(RandomUtil.randomString(20));
        sysGoodsAdd.setSalesVolume(0);
        sysGoodsAdd.setRevision(1);
        LocalDateTime time = LocalDateTime.now();
        String userId = GetUser.getUserId();
        sysGoodsAdd.setCreateTime(time);
        sysGoodsAdd.setCreateBy(userId);
        sysGoodsAdd.setUpdateTime(time);
        sysGoodsAdd.setUpdateBy(userId);
        sysGoodsAdd.setReviewRate(0);
        sysGoodsAdd.setActivity(1);
        // 转换对象
        SysGoods sysGoods = BeanUtil.copyProperties(sysGoodsAdd, SysGoods.class);
        if (sysGoodsMapper.insert(sysGoods) <= 0) throw new CommonException("新增商品", "网络异常，稍后再试");
        // 新增商品的sku, 循环设置商品sku数据
        List<GoodsSku> goodsSkus = sysGoods.getGoodsSkus();
        // 循环封装对象数据
        for (GoodsSku skus : goodsSkus) {
            skus.setSkuId(redisIdWorker.nextId("goodsSku"));
            skus.setGoodsId(id);
            skus.setCreateBy(userId);
            skus.setCreateTime(time);
            skus.setUpdateBy(userId);
            skus.setUpdateTime(time);
        }
        // 批量插入
        goodsSkuMapper.insertBatchSomeColumn(goodsSkus);

    }

    /**
     * 删除商品
     *
     * @param goodsId
     * @return
     */
    @Override
    @Transactional
    public void delete(String goodsId) {
        // 商品构造器
        LambdaQueryWrapper<SysGoods> sysGoodsLambdaQueryWrapper = Wrappers.lambdaQuery(SysGoods.class);
        // 根据商品id
        sysGoodsLambdaQueryWrapper.eq(SysGoods::getGoodsId, goodsId);
        // 查询该商品对象
        SysGoods sqlSysGoods = sysGoodsMapper.selectOne(sysGoodsLambdaQueryWrapper);
        // 是否存在
        if (sqlSysGoods == null) throw new CommonException("删除商品", "商品不存在");
        // 存在，删除
        sysGoodsMapper.delete(sysGoodsLambdaQueryWrapper);
        // 查询对应的sku
        LambdaQueryWrapper<GoodsSku> goodsSkuLambdaQueryWrapper = Wrappers.lambdaQuery(GoodsSku.class);
        // 根据商品id
        goodsSkuLambdaQueryWrapper.eq(GoodsSku::getGoodsId, goodsId);
        // 查询到的当前商品的sku列表
        List<GoodsSku> goodsSkus = goodsSkuMapper.selectList(goodsSkuLambdaQueryWrapper);
        // 没有商品sku的话，就没有什么商品sku秒杀数据了
        if (goodsSkus.size() <= 0) return;
        // 获取skuid列表
        ArrayList<String> skuIdList = new ArrayList<>();
        for (GoodsSku skus : goodsSkus) {
            skuIdList.add(skus.getSkuId());
        }
        // 批量删除商品sku
        goodsSkuMapper.deleteBatchIds(skuIdList);
        // 查询当前运行的商品限时秒杀
        LambdaQueryWrapper<GoodsFlashSale> goodsFlashSaleLambdaQueryWrapper = Wrappers.lambdaQuery(GoodsFlashSale.class);
        // 状态 0：下架 1：上架
        goodsFlashSaleLambdaQueryWrapper.eq(GoodsFlashSale::getStatus, 1);
        // 商品秒杀对象
        GoodsFlashSale goodsFlashSale = goodsFlashSaleMapper.selectOne(goodsFlashSaleLambdaQueryWrapper);
        // 根据 skuIdList查询是否有商品限时秒杀信息数据
        LambdaQueryWrapper<GoodsFlashSaleItem> goodsFlashSaleItemLambdaQueryWrapper = Wrappers.lambdaQuery(GoodsFlashSaleItem.class);
        goodsFlashSaleItemLambdaQueryWrapper.eq(GoodsFlashSaleItem::getFlashSaleId, goodsFlashSale.getFlashSaleId());
        goodsFlashSaleItemLambdaQueryWrapper.in(GoodsFlashSaleItem::getGoodsSkuId, skuIdList);
        // 查询到的商品限时秒杀项目信息
        List<GoodsFlashSaleItem> goodsFlashSaleItems = goodsFlashSaleItemMapper.selectList(goodsFlashSaleItemLambdaQueryWrapper);
        if (goodsFlashSaleItems.size() <= 0) return;
        // 拿到商品限时秒杀项目信息id
        ArrayList<String> goodsFlashSaleItemId = new ArrayList<>();
        for (GoodsFlashSaleItem goodsFlashSaleItem : goodsFlashSaleItems) {
            goodsFlashSaleItemId.add(goodsFlashSaleItem.getItemId());
        }
        // 删除
        goodsFlashSaleItemMapper.deleteBatchIds(goodsFlashSaleItemId);
    }

    /**
     * 修改商品
     *
     * @param sysGoods
     * @return
     */
    @Override
    public void set(SysGoodsAdd sysGoodsAdd, GoodsSkuService goodsSkuService) {
        LambdaQueryWrapper<SysGoods> lambdaQueryWrapper = Wrappers.lambdaQuery(SysGoods.class);
        lambdaQueryWrapper.eq(SysGoods::getGoodsId, sysGoodsAdd.getGoodsId());
        SysGoods sqlSysGoods = sysGoodsMapper.selectOne(lambdaQueryWrapper);
        if (sqlSysGoods == null) throw new CommonException("修改商品", "商品不存在");
        sysGoodsAdd.setRevision(sqlSysGoods.getRevision() + 1);
        sysGoodsAdd.setUpdateBy(GetUser.getUserId());
        sysGoodsAdd.setUpdateTime(LocalDateTime.now());
        SysGoods sysGoods = BeanUtil.copyProperties(sysGoodsAdd, SysGoods.class);
        lambdaQueryWrapper.eq(SysGoods::getRevision, sqlSysGoods.getRevision());
        if (sysGoodsMapper.update(sysGoods, lambdaQueryWrapper) <= 0) throw new CommonException("修改商品", "网络异常");
        // 获取sku列表，批量更新
        List<GoodsSku> goodsSkus = sysGoods.getGoodsSkus();
        for (GoodsSku skus : goodsSkus) {
            skus.setUpdateTime(LocalDateTime.now());
            skus.setUpdateBy(GetUser.getUserId());
            // 更新
            if (StrUtil.isNotBlank(skus.getSkuVal()) && skus.getSkuId() != null && !skus.getSkuId().equals("")) {
                goodsSkuMapper.updateById(skus);
            }
            // 新增
            if (StrUtil.isNotBlank(skus.getSkuVal()) && skus.getSkuId() == null || skus.getSkuId().equals("")) {
                skus.setUpdateBy(GetUser.getUserId());
                skus.setCreateTime(LocalDateTime.now());
                skus.setSkuId(redisIdWorker.nextId("sysgoods"));
                skus.setGoodsId(sysGoodsAdd.getGoodsId());
                goodsSkuMapper.insert(skus);
            }
        }
    }

    /**
     * 查看商品详情(商品携带当前商品的sku列表)
     *
     * @param goodsId
     * @return
     */
    @Override
    public SysGoods getByGoodsId(String goodsId) {
        // 商品构造器
        LambdaQueryWrapper<SysGoods> lambdaQueryWrapper = Wrappers.lambdaQuery(SysGoods.class);
        // 获取指定商品
        lambdaQueryWrapper.eq(SysGoods::getGoodsId, goodsId);
        // 查询到的商品对象
        SysGoods sqlSysGoods = sysGoodsMapper.selectOne(lambdaQueryWrapper);
        // 商品是否存在
        if (sqlSysGoods == null) return null;
        // 根据商品id查询sku
        LambdaQueryWrapper<GoodsSku> goodsSkuLambdaQueryWrapper = Wrappers.lambdaQuery(GoodsSku.class);
        goodsSkuLambdaQueryWrapper.eq(GoodsSku::getGoodsId, sqlSysGoods.getGoodsId());
        // 对应的商品sku列表
        List<GoodsSku> goodsSkus = goodsSkuMapper.selectList(goodsSkuLambdaQueryWrapper);
        // 存储商品skuid
        ArrayList<String> goodsSkuList = new ArrayList<>();
        // 为商品sku状态值设置默认值,并拿到每个skuid值
        for (GoodsSku skus : goodsSkus) {
            skus.setStatus(0);
            goodsSkuList.add(skus.getSkuId());
        }
        // 根据商品秒杀对象和商品skuid列表，查询到需要的商品限时秒杀项目信息
        LambdaQueryWrapper<GoodsFlashSaleItem> goodsFlashSaleItemLambdaQueryWrapper = Wrappers.lambdaQuery(GoodsFlashSaleItem.class);
        goodsFlashSaleItemLambdaQueryWrapper.in(GoodsFlashSaleItem::getGoodsSkuId, goodsSkuList);
        // 拿到的数据就是秒杀数据，对应的skuid就是已经设置为秒杀的sku
        goodsFlashSaleItemLambdaQueryWrapper = Wrappers.lambdaQuery(GoodsFlashSaleItem.class);
        List<GoodsFlashSaleItem> goodsFlashSaleItems = goodsFlashSaleItemMapper.selectList(goodsFlashSaleItemLambdaQueryWrapper);
        // 判断每个sku是否为秒杀，并且修改sku状态值和获取商品秒杀价格
        for (GoodsFlashSaleItem goodsFlashSaleItem : goodsFlashSaleItems) {
            for (GoodsSku skus : goodsSkus) {
                if (goodsFlashSaleItem.getGoodsSkuId().equals(skus.getSkuId())) {
                    skus.setStatus(1);
                    skus.setFlashSalePrice(goodsFlashSaleItem.getFlashSalePrice());
                }
            }
        }
        // 拿到商品的sku列表
        List<GoodsSku> goodsSkus1 = sqlSysGoods.getGoodsSkus();
        for (GoodsSku skus : goodsSkus) {
            skus.setSkuValJson(JSONObject.parseObject(skus.getSkuVal()));
            goodsSkus1.add(skus);
        }
        return sqlSysGoods;
    }

    /**
     * 查看商品列表(每个商品携带当前商品的sku列表)
     *
     * @return
     */
    @Override
    public Page<SysGoods> getByGoodsPage(SysGoodsQuery sysGoodsQuery) {
        // 获取商品条件构造器
        LambdaQueryWrapper<SysGoods> lambdaQueryWrapper = Wrappers.lambdaQuery(SysGoods.class);
        // 商品名称模糊查询
        lambdaQueryWrapper.like(sysGoodsQuery.getName() != null && !sysGoodsQuery.getName().equals(""), SysGoods::getName, sysGoodsQuery.getName());
        // 商品分类查询，包括子分类
        if (sysGoodsQuery.getCateId() != null && !sysGoodsQuery.getCateId().equals("")) {
            // 当前分类id
            String cateId = sysGoodsQuery.getCateId();
            // 查询是否存在子分类
            LambdaQueryWrapper<SysCategory> sysCategoryLambdaQueryWrapper = Wrappers.lambdaQuery(SysCategory.class);
            sysCategoryLambdaQueryWrapper.eq(SysCategory::getParentId, cateId);
            List<SysCategory> sysCategories = sysCategoryMapper.selectList(sysCategoryLambdaQueryWrapper);
            // 将所有分类id存入set集合中
            TreeSet<String> sysCategorieIds = new TreeSet<>();
            for (SysCategory sysCategory : sysCategories) {
                sysCategorieIds.add(sysCategory.getCateId());
            }
            sysCategorieIds.add(cateId);
            // 循环设置条件
            lambdaQueryWrapper.eq(SysGoods::getCateId, cateId).or();
            for (String sysCategorieId : sysCategorieIds) {
                lambdaQueryWrapper.eq(SysGoods::getCateId, sysCategorieId).or();
            }
        }
        // 排序
        lambdaQueryWrapper.orderByAsc(SysGoods::getCreateTime);
        Page<SysGoods> sysGoodsPage = null;
        // 活动
        if (sysGoodsQuery.getActivity() != null) {
            // 秒杀活动
            if (sysGoodsQuery.getActivity().equals(3)) {
                lambdaQueryWrapper.eq(SysGoods::getActivity, 3);
            }
            // 其它的活动，暂未开发
            if (sysGoodsPage == null && sysGoodsQuery.getActivity().equals(2) || sysGoodsQuery.getActivity().equals(4)) {
                return sysGoodsPage;
            }
            // 未参与
            if (sysGoodsPage == null && sysGoodsQuery.getActivity().equals(1)) {
                lambdaQueryWrapper.eq(SysGoods::getActivity, 1);
            }
        }
        sysGoodsPage = sysGoodsMapper.selectPage(new Page<SysGoods>(sysGoodsQuery.getPage(), sysGoodsQuery.getPageSize()), lambdaQueryWrapper);
        // 根据商品分页获取商品列表
        List<SysGoods> goodsList = sysGoodsPage.getRecords();
        // 存储商品id
        TreeSet<String> Strings = new TreeSet<>();
        // 根据商品列表获取商品id，set去重
        for (SysGoods sysGoods : goodsList) {
            Strings.add(sysGoods.getGoodsId());
        }
        if (Strings.size() == 0) return sysGoodsPage;
        // 获取商品sku条件构造器
        LambdaQueryWrapper<GoodsSku> goodsSkuLambdaQueryWrapper = Wrappers.lambdaQuery(GoodsSku.class);
        // in 商品id(),就是根据商品id拿到需要的商品sku对象列表
        goodsSkuLambdaQueryWrapper.in(GoodsSku::getGoodsId, Strings);
        // 商品sku对象列表
        List<GoodsSku> goodsSkus = goodsSkuMapper.selectList(goodsSkuLambdaQueryWrapper);
        // 商品status默认为0
        for (GoodsSku skus : goodsSkus) {
            skus.setStatus(0);
        }
        // 目标：商品sku是否加入秒杀，更新status值
        // 获取商品skuid列表
        ArrayList<String> goodsSkuIdList = new ArrayList<>();
        for (GoodsSku skus : goodsSkus) {
            goodsSkuIdList.add(skus.getSkuId());
        }
        // 根据商品skuid列表去商品限时秒杀项目信息中查询数据
        LambdaQueryWrapper<GoodsFlashSaleItem> goodsFlashSaleItemLambdaQueryWrapper = Wrappers.lambdaQuery(GoodsFlashSaleItem.class);
        goodsFlashSaleItemLambdaQueryWrapper.in(GoodsFlashSaleItem::getGoodsSkuId, goodsSkuIdList);
        // 拿到需要的商品限时秒杀项目信息,这里面的sku都是设置为秒杀的
        List<GoodsFlashSaleItem> goodsFlashSaleItems = goodsFlashSaleItemMapper.selectList(goodsFlashSaleItemLambdaQueryWrapper);
        // 判断每个sku是否为秒杀，并且修改sku状态值和秒杀价格
        for (GoodsFlashSaleItem goodsFlashSaleItem : goodsFlashSaleItems) {
            for (GoodsSku skus : goodsSkus) {
                if (goodsFlashSaleItem.getGoodsSkuId().equals(skus.getSkuId())) {
                    skus.setStatus(1);
                    skus.setFlashSalePrice(goodsFlashSaleItem.getFlashSalePrice());
                }
            }
        }
        // j表示商品获取sku的划线价和最低限价，默认获取第一个商品sku的这两个数据
        int j = 0;
        // 目标：商品中封装商品sku列表
        // i表示外层循环次数 goodsList
        int i = 0;
        // 有商品列表，有商品sku列表，循环封装，根据商品找商品sku
        for (SysGoods sysGoods : goodsList) {
            // 第几次循环
            i += 1;
            for (GoodsSku skus : goodsSkus) {
                // 判断外层循环次数是否为第一次
                if (i == 1) {
                    // 外层第一次循环,规格值转JSON数据，并且将规格值制空
                    skus.setSkuValJson(JSONObject.parseObject(skus.getSkuVal()));
                    skus.setSkuVal("");
                    // 外层第一次循环就可以转换所有的sku对象，所以要判断去避免继续转换，从而避免报错
                }
                if (sysGoods.getGoodsId().equals(skus.getGoodsId())) {
                    List<GoodsSku> goodsSkus1 = sysGoods.getGoodsSkus();
                    goodsSkus1.add(skus);
                    sysGoods.setGoodsSkus(goodsSkus1);
                }
                if (j == 0) {
                    sysGoods.setOldPrice(skus.getOldPrice());
                    sysGoods.setPriceFloor(skus.getPriceFloor());
                    // 表示已获取，当前商品无需再次获取
                    j++;
                }
            }
            // 新的外层循环，新的商品，需重置j
            j = 0;
        }
        // 封装一级分类二级分类三级分类名称
        for (SysGoods sysGoods : goodsList) {
            LambdaQueryWrapper<SysCategory> sysCategoryLambdaQueryWrapper = Wrappers.lambdaQuery(SysCategory.class);
            if (sysGoods.getCateId() != null) {
                sysCategoryLambdaQueryWrapper.eq(SysCategory::getCateId, sysGoods.getCateId());
            }
            SysCategory sysCategory = sysCategoryMapper.selectOne(sysCategoryLambdaQueryWrapper);
            LambdaQueryWrapper<SysCategory> sysCategoryLambdaQueryWrapper1 = Wrappers.lambdaQuery(SysCategory.class);
            SysCategory sysCategory1 = null;
            if (sysCategory != null && sysCategory.getParentId() != null) {
                sysCategoryLambdaQueryWrapper1.eq(SysCategory::getCateId, sysCategory.getParentId());
                sysCategory1 = sysCategoryMapper.selectOne(sysCategoryLambdaQueryWrapper1);
            }
            LambdaQueryWrapper<SysCategory> sysCategoryLambdaQueryWrapper2 = Wrappers.lambdaQuery(SysCategory.class);
            SysCategory sysCategory2 = null;
            if (sysCategory1 != null && sysCategory1.getParentId() != null) {
                sysCategoryLambdaQueryWrapper2.eq(SysCategory::getCateId, sysCategory1.getParentId());
                sysCategory2 = sysCategoryMapper.selectOne(sysCategoryLambdaQueryWrapper2);
            }
            // 321
            if (sysCategory != null && sysCategory1 != null && sysCategory2 != null) {
                if (!sysCategory.getParentId().equals("0") && !sysCategory1.getParentId().equals("0") && sysCategory2.getParentId().equals("0")) {
                    sysGoods.setClassificationThree(sysCategory.getName());
                    sysGoods.setClassificationTwo(sysCategory1.getName());
                    sysGoods.setClassificationOne(sysCategory2.getName());
                }
            }
            // 21
            if (sysCategory != null && sysCategory1 != null) {
                if (!sysCategory.getParentId().equals("0") && sysCategory1.getParentId().equals("0")) {
                    sysGoods.setClassificationTwo(sysCategory.getName());
                    sysGoods.setClassificationOne(sysCategory1.getName());
                }
            }
            // 1
            if (sysCategory != null) {
                if (sysCategory.getParentId().equals("0")) {
                    sysGoods.setClassificationOne(sysCategory.getName());
                }
            }
        }
        return sysGoodsPage;
    }

    /**
     * 查询指定分类的商品
     *
     * @param cateId
     * @return
     */
    @Override
    public List<SysGoods> categoryIdSelectGoodsList(String cateId) {
        LambdaQueryWrapper<SysGoods> sysGoodsLambdaQueryWrapper = Wrappers.lambdaQuery(SysGoods.class);
        sysGoodsLambdaQueryWrapper.eq(SysGoods::getCateId, cateId);
        return sysGoodsMapper.selectList(sysGoodsLambdaQueryWrapper);
    }

    /**
     * 获取指定商品的sku对象列表
     *
     * @param goodsId
     * @return
     */
    @Override
    public List<GoodsSku> getGoodsSku(String goodsId) {
        LambdaQueryWrapper<GoodsSku> goodsSkuLambdaQueryWrapper = Wrappers.lambdaQuery(GoodsSku.class);
        goodsSkuLambdaQueryWrapper.eq(GoodsSku::getGoodsId, goodsId);
        List<GoodsSku> goodsSkus = goodsSkuMapper.selectList(goodsSkuLambdaQueryWrapper);
        return goodsSkus;
    }

    /**
     * 放入仓库/上架销售：1放入仓库, 2上架销售
     *
     * @param goodsId
     * @return
     */
    @Override
    public void setStatus(String goodsId) {
        LambdaQueryWrapper<SysGoods> sysGoodsLambdaQueryWrapper = Wrappers.lambdaQuery(SysGoods.class);
        sysGoodsLambdaQueryWrapper.eq(SysGoods::getGoodsId, goodsId);
        SysGoods sysGoods = sysGoodsMapper.selectOne(sysGoodsLambdaQueryWrapper);
        if (sysGoods == null) throw new CommonException("放入仓库/上架销售：1放入仓库, 2上架销售", "商品不存在");
        // 商品存在，判断商品状态，更新商品状态
        if (sysGoods.getStatus().equals(0)) {
            // 在仓库,上架销售按钮
            sysGoods.setStatus(1);
        } else {
            // 上架中，放入仓库按钮
            sysGoods.setStatus(0);
        }
        sysGoodsMapper.updateById(sysGoods);
    }

    /**
     * 筛选分页查询
     *
     * @param page
     * @param pageSize
     * @param name
     * @param cateId
     * @return
     */
    @Override
    public Page<SysGoods> getSysGoodsPageNew(Long page, Long pageSize, String name, String cateId) {
        LambdaQueryWrapper<SysGoods> sysGoodsLambdaQueryWrapper = Wrappers.lambdaQuery(SysGoods.class);
        // 商品名称模糊查询
        sysGoodsLambdaQueryWrapper.like(SysGoods::getName, name);
        // 指定商品分类
        sysGoodsLambdaQueryWrapper.eq(SysGoods::getCateId, cateId);
        Page<SysGoods> sysGoodsPage = sysGoodsMapper.selectPage(new Page<SysGoods>(page, pageSize), sysGoodsLambdaQueryWrapper);
        return sysGoodsPage;
    }

    /**
     * 全部导出
     *
     * @return
     */
    public List<SysGoodsImport> diyGetSysGoodsList() {
        LambdaQueryWrapper<SysGoods> sysGoodsLambdaQueryWrapper = Wrappers.lambdaQuery(SysGoods.class);
        sysGoodsLambdaQueryWrapper.orderByDesc(SysGoods::getCreateTime);
        // 全部的商品
        List<SysGoods> sysGoods = sysGoodsMapper.selectList(sysGoodsLambdaQueryWrapper);
        // 获取全部商品对应的sku
        ArrayList<String> goodsIds = new ArrayList<>();
        if (sysGoods.size() == 0) throw new CommonException("全部导出", "没有商品");
        for (SysGoods sysGood : sysGoods) {
            goodsIds.add(sysGood.getGoodsId());
        }
        LambdaQueryWrapper<GoodsSku> goodsSkuLambdaQueryWrapper = Wrappers.lambdaQuery(GoodsSku.class);
        goodsSkuLambdaQueryWrapper.in(GoodsSku::getGoodsId, goodsIds);
        List<GoodsSku> goodsSkus = goodsSkuMapper.selectList(goodsSkuLambdaQueryWrapper);
        // 获取全部商品对应的sku库存
        LambdaQueryWrapper<SellerInventory> sellerInventoryLambdaQueryWrapper = Wrappers.lambdaQuery(SellerInventory.class);
        sellerInventoryLambdaQueryWrapper.in(SellerInventory::getGoodsId, goodsIds);
        List<SellerInventory> sellerInventories = sellerInventoryMapper.selectList(sellerInventoryLambdaQueryWrapper);
        // 最终返回的数据
        ArrayList<SysGoodsImport> sysGoodsImports = new ArrayList<>();
        // 数据封装
        for (SysGoods sysGood : sysGoods) {
            SysGoodsImport sysGoodsImport = new SysGoodsImport();
            // 商品找sku
            for (GoodsSku skus : goodsSkus) {
                if (sysGood.getGoodsId().equals(skus.getGoodsId())) {
                    BeanUtil.copyProperties(sysGood, sysGoodsImport);
                    BeanUtil.copyProperties(skus, sysGoodsImport);
                    break;
                }
            }
            String skuVal = sysGoodsImport.getSkuVal();
            StringBuffer stringBuffer = new StringBuffer();
            for (int i = 0; i < skuVal.length(); i++) {
                char c = skuVal.charAt(i);
                if (c != '"' && c != '{' && c != '}') {
                    stringBuffer.append(c);
                }
            }
            sysGoodsImport.setSkuVal(stringBuffer.toString());
            sysGoodsImports.add(sysGoodsImport);
        }
        ArrayList<SysGoodsImport> sysGoodsImportsone = new ArrayList<>();
        // 商品找sku库存
        for (SysGoods sysGood : sysGoods) {
            SysGoodsImport sysGoodsImport = new SysGoodsImport();
            for (SellerInventory sellerInventory : sellerInventories) {
                if (sysGood.getGoodsId().equals(sellerInventory.getGoodsId())) {
                    BeanUtil.copyProperties(sysGood, sysGoodsImport);
                    BeanUtil.copyProperties(sellerInventory, sysGoodsImport);
                    break;
                }
            }
            sysGoodsImportsone.add(sysGoodsImport);
        }
        for (SysGoodsImport sysGoodsImport : sysGoodsImports) {
            for (SysGoodsImport goodsImport : sysGoodsImportsone) {
                if (sysGoodsImport.getBarCode() != null && goodsImport.getBarCode() != null && sysGoodsImport.getBarCode().equals(goodsImport.getBarCode())) {
//                    BeanUtil.copyProperties(goodsImport, sysGoodsImport);
                    sysGoodsImport.setQuantity(goodsImport.getQuantity());
                    break;
                }
            }
        }
        // 封装分类名称
        for (SysGoodsImport sysGoodsImport : sysGoodsImports) {
            LambdaQueryWrapper<SysCategory> sysCategoryLambdaQueryWrapper = Wrappers.lambdaQuery(SysCategory.class);
            sysCategoryLambdaQueryWrapper.eq(sysGoodsImport.getCateId() != null, SysCategory::getCateId, sysGoodsImport.getCateId());
            SysCategory sysCategory = sysCategoryMapper.selectOne(sysCategoryLambdaQueryWrapper);
            LambdaQueryWrapper<SysCategory> sysCategoryLambdaQueryWrapper1 = Wrappers.lambdaQuery(SysCategory.class);
            SysCategory sysCategory1 = null;
            if (sysCategory != null && sysCategory.getParentId() != null) {
                sysCategoryLambdaQueryWrapper1.eq(sysCategory.getParentId() != null, SysCategory::getCateId, sysCategory.getParentId());
                sysCategory1 = sysCategoryMapper.selectOne(sysCategoryLambdaQueryWrapper1);
            }
            LambdaQueryWrapper<SysCategory> sysCategoryLambdaQueryWrapper2 = Wrappers.lambdaQuery(SysCategory.class);
            SysCategory sysCategory2 = null;
            if (sysCategory1 != null && sysCategory1.getParentId() != null) {
                sysCategoryLambdaQueryWrapper2.eq(sysCategory1.getParentId() != null, SysCategory::getCateId, sysCategory1.getParentId());
                sysCategory2 = sysCategoryMapper.selectOne(sysCategoryLambdaQueryWrapper2);
            }
            // 321
            if (sysCategory != null && sysCategory1 != null && sysCategory2 != null) {
                if (!sysCategory.getParentId().equals("0") && !sysCategory1.getParentId().equals("0") && sysCategory2.getParentId().equals("0")) {
                    sysGoodsImport.setCateId(sysCategory.getName());
                    sysGoodsImport.setCateIdTwo(sysCategory1.getName());
                    sysGoodsImport.setCateIdOne(sysCategory2.getName());
                }
            }
            // 21
            if (sysCategory != null && sysCategory1 != null) {
                if (!sysCategory.getParentId().equals("0") && sysCategory1.getParentId().equals("0")) {
                    sysGoodsImport.setCateIdTwo(sysCategory.getName());
                    sysGoodsImport.setCateIdOne(sysCategory1.getName());
                }
            }
            // 1
            if (sysCategory != null) {
                if (sysCategory.getParentId().equals("0")) {
                    sysGoodsImport.setCateIdOne(sysCategory.getName());
                    sysGoodsImport.setCateId("");
                }
            }
        }

//        for (SysGoodsImport sysGoodsImport : sysGoodsImports) {
//            if (sysGoodsImport.getOpeningTime() != null) {
//                sysGoodsImport.setOpeningTime(sysGoodsImport.getOpeningTime());
//            }
//
//            if (sysGoodsImport.getTimedOffShelfTime() != null) {
//                sysGoodsImport.setTimedOffShelfTime(sysGoodsImport.getTimedOffShelfTime());
//            }
//        }
        return sysGoodsImports;
    }

    /**
     * 按页导出
     *
     * @param pageOne
     * @param pageTwo
     * @return
     */
    @Override
    public List<SysGoodsImport> diyGetSysGoodsPage(Integer pageOne, Integer pageTwo, Integer pageSize) {
        if (pageSize.equals(null)) pageSize = 10;
        if (pageOne == null || pageTwo == null) throw new CommonException("商品导出", "设置异常");
        if (pageOne.equals(0) || pageTwo.equals(0) || pageSize.equals(0))
            throw new CommonException("商品导出", "设置异常");
        if (pageOne - pageTwo > 0) throw new CommonException("商品导出", "设置异常");
        List<SysGoods> sysGoods = new ArrayList<>();
        // 循环查询
        for (int i = pageOne; i <= pageTwo; i++) {
            LambdaQueryWrapper<SysGoods> sysGoodsLambdaQueryWrapper = Wrappers.lambdaQuery(SysGoods.class);
            sysGoodsLambdaQueryWrapper.orderByDesc(SysGoods::getCreateTime);
            Page<SysGoods> sysGoodsPage = sysGoodsMapper.selectPage(new Page<SysGoods>(i, pageSize), sysGoodsLambdaQueryWrapper);
            List<SysGoods> records = sysGoodsPage.getRecords();
            if (records.size() > 0) {
                // 表示有数据,循环获取数据
                for (SysGoods record : records) {
                    sysGoods.add(record);
                }
            }
        }
        // 获取全部商品对应的sku
        ArrayList<String> goodsIds = new ArrayList<>();
        if (sysGoods.size() == 0) throw new CommonException("全部导出", "没有商品");
        for (SysGoods sysGood : sysGoods) {
            goodsIds.add(sysGood.getGoodsId());
        }
        LambdaQueryWrapper<GoodsSku> goodsSkuLambdaQueryWrapper = Wrappers.lambdaQuery(GoodsSku.class);
        goodsSkuLambdaQueryWrapper.in(GoodsSku::getGoodsId, goodsIds);
        List<GoodsSku> goodsSkus = goodsSkuMapper.selectList(goodsSkuLambdaQueryWrapper);
        // 获取全部商品对应的sku库存
        LambdaQueryWrapper<SellerInventory> sellerInventoryLambdaQueryWrapper = Wrappers.lambdaQuery(SellerInventory.class);
        sellerInventoryLambdaQueryWrapper.in(SellerInventory::getGoodsId, goodsIds);
        List<SellerInventory> sellerInventories = sellerInventoryMapper.selectList(sellerInventoryLambdaQueryWrapper);
        // 最终返回的数据
        ArrayList<SysGoodsImport> sysGoodsImports = new ArrayList<>();
        // 数据封装
        for (SysGoods sysGood : sysGoods) {
            SysGoodsImport sysGoodsImport = new SysGoodsImport();
            // 商品找sku
            for (GoodsSku skus : goodsSkus) {
                if (sysGood.getGoodsId().equals(skus.getGoodsId())) {
                    BeanUtil.copyProperties(sysGood, sysGoodsImport);
                    BeanUtil.copyProperties(skus, sysGoodsImport);
                    break;
                }
            }
            // 规格值处理
            String skuVal = sysGoodsImport.getSkuVal();
            StringBuffer stringBuffer = new StringBuffer();
            for (int i = 0; i < skuVal.length(); i++) {
                char c = skuVal.charAt(i);
                if (c != '"' && c != '{' && c != '}') {
                    stringBuffer.append(c);
                }
            }
            sysGoodsImport.setSkuVal(stringBuffer.toString());
            sysGoodsImports.add(sysGoodsImport);
        }
        ArrayList<SysGoodsImport> sysGoodsImportsone = new ArrayList<>();
        // 商品找sku库存
        for (SysGoods sysGood : sysGoods) {
            SysGoodsImport sysGoodsImport = new SysGoodsImport();
            for (SellerInventory sellerInventory : sellerInventories) {
                if (sysGood.getGoodsId().equals(sellerInventory.getGoodsId())) {
                    BeanUtil.copyProperties(sysGood, sysGoodsImport);
                    BeanUtil.copyProperties(sellerInventory, sysGoodsImport);
                    break;
                }
            }
            sysGoodsImportsone.add(sysGoodsImport);
        }
        for (SysGoodsImport sysGoodsImport : sysGoodsImports) {
            for (SysGoodsImport goodsImport : sysGoodsImportsone) {
                if (sysGoodsImport.getBarCode().equals(goodsImport.getBarCode())) {
//                    BeanUtil.copyProperties(goodsImport, sysGoodsImport);
                    sysGoodsImport.setQuantity(goodsImport.getQuantity());
                    break;
                }
            }
        }
        // 封装分类名称
        for (SysGoodsImport sysGoodsImport : sysGoodsImports) {
            LambdaQueryWrapper<SysCategory> sysCategoryLambdaQueryWrapper = Wrappers.lambdaQuery(SysCategory.class);
            sysCategoryLambdaQueryWrapper.eq(sysGoodsImport.getCateId() != null, SysCategory::getCateId, sysGoodsImport.getCateId());
            SysCategory sysCategory = sysCategoryMapper.selectOne(sysCategoryLambdaQueryWrapper);
            LambdaQueryWrapper<SysCategory> sysCategoryLambdaQueryWrapper1 = Wrappers.lambdaQuery(SysCategory.class);
            SysCategory sysCategory1 = null;
            if (sysCategory != null && sysCategory.getParentId() != null) {
                sysCategoryLambdaQueryWrapper1.eq(sysCategory.getParentId() != null, SysCategory::getCateId, sysCategory.getParentId());
                sysCategory1 = sysCategoryMapper.selectOne(sysCategoryLambdaQueryWrapper1);
            }
            LambdaQueryWrapper<SysCategory> sysCategoryLambdaQueryWrapper2 = Wrappers.lambdaQuery(SysCategory.class);
            SysCategory sysCategory2 = null;
            if (sysCategory1 != null && sysCategory1.getParentId() != null) {
                sysCategoryLambdaQueryWrapper2.eq(sysCategory1.getParentId() != null, SysCategory::getCateId, sysCategory1.getParentId());
                sysCategory2 = sysCategoryMapper.selectOne(sysCategoryLambdaQueryWrapper2);
            }
            // 321
            if (sysCategory != null && sysCategory1 != null && sysCategory2 != null) {
                if (!sysCategory.getParentId().equals("0") && !sysCategory1.getParentId().equals("0") && sysCategory2.getParentId().equals("0")) {
                    sysGoodsImport.setCateId(sysCategory.getName());
                    sysGoodsImport.setCateIdTwo(sysCategory1.getName());
                    sysGoodsImport.setCateIdOne(sysCategory2.getName());
                }
            }
            // 21
            if (sysCategory != null && sysCategory1 != null) {
                if (!sysCategory.getParentId().equals("0") && sysCategory1.getParentId().equals("0")) {
                    sysGoodsImport.setCateIdTwo(sysCategory.getName());
                    sysGoodsImport.setCateIdOne(sysCategory1.getName());
                }
            }
            // 1
            if (sysCategory != null) {
                if (sysCategory.getParentId().equals("0")) {
                    sysGoodsImport.setCateIdOne(sysCategory.getName());
                    sysGoodsImport.setCateId("");
                }
            }
        }

//        for (SysGoodsImport sysGoodsImport : sysGoodsImports) {
//            if (sysGoodsImport.getOpeningTime() != null) {
//                sysGoodsImport.setOpeningTime(forMat(sysGoodsImport.getOpeningTime()));
//            }
//
//            if (sysGoodsImport.getTimedOffShelfTime() != null) {
//                sysGoodsImport.setTimedOffShelfTime(forMat(sysGoodsImport.getTimedOffShelfTime()));
//            }
//        }

        return sysGoodsImports;
    }

    // 导出时时间格式化
    public String forMat(String time) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        //将字符串转换成Date类型方便SimpleDateFormat的转换
        Date date = new Date(time);
        //再将date转换成指定样式的字符串
        String format1 = format.format(date);
        return format1;
    }

    /**
     * 商品导入(废弃)
     *
     * @return
     */
    @Override
    public void autoAddSysGoodsList(List<SysGoods> sysGoods, List<GoodsSku> goodsSkus) {
        // 校验
        if (sysGoods.size() == 0) throw new CommonException("商品导入", "文件没有编写商品或标题输入错误或未选择文件");
        if (goodsSkus.size() == 0)
            throw new CommonException("商品导入", "文件没有编写商品sku或标题输入错误或未选择文件");
        // 商品找sku
        for (SysGoods sysGood : sysGoods) {
            List<GoodsSku> goodsSkusList = sysGood.getGoodsSkus();
            for (GoodsSku skus : goodsSkus) {
                if (sysGood.getRowNum().toString().equals(skus.getGoodsId())) {
                    goodsSkusList.add(skus);
                }
            }
            sysGood.setGoodsSkus(goodsSkusList);
        }
        // 找完了，做校验，添加商品规则是，每个商品至少要有一个sku对象
        for (SysGoods sysGood : sysGoods) {
            List<GoodsSku> goodsSkusList = sysGood.getGoodsSkus();
            if (goodsSkusList.size() == 0) throw new CommonException("商品导入", "请为每个商品分配对应的sku");
        }
        // 所有的商品sku数据，后续直接批量添加即可
        List<GoodsSku> newGoodsSkusList = new ArrayList<>();
        // 数据没问题，循环封装数据，然后添加即可
        for (SysGoods sysGood : sysGoods) {
            // 封装商品
            String id = redisIdWorker.nextId("goods");
            sysGood.setGoodsId(id);
            sysGood.setTenantId(RandomUtil.randomString(20));
            sysGood.setSalesVolume(0);
            sysGood.setRevision(1);
            LocalDateTime time = LocalDateTime.now();
            String userId = GetUser.getUserId();
            sysGood.setCreateTime(time);
            sysGood.setCreateBy(userId);
            sysGood.setUpdateTime(time);
            sysGood.setUpdateBy(userId);
            sysGood.setReviewRate(0);
            List<GoodsSku> goodsSkusList = sysGood.getGoodsSkus();
            // 循环封装对象数据
            for (GoodsSku skus : goodsSkusList) {
                skus.setSkuId(redisIdWorker.nextId("goodsSku"));
                skus.setGoodsId(id);
                skus.setCreateBy(userId);
                skus.setCreateTime(time);
                skus.setUpdateBy(userId);
                skus.setUpdateTime(time);
                newGoodsSkusList.add(skus);
            }
        }
        // 批量新增商品 sysGoods
        sysGoodsMapper.insertBatchSomeColumn(sysGoods);
        // 批量新增商品sku newGoodsSkusList
        goodsSkuMapper.insertBatchSomeColumn(newGoodsSkusList);
    }

    /**
     * 拿到需要更新的 SysGoodsImport对象
     *
     * @param sysGoodsImports
     */
    public void update(List<SysGoodsImport> sysGoodsImports) {
        // 商品
        ArrayList<SysGoods> sysGoodsArrayList = new ArrayList<>();
        // sku
        ArrayList<GoodsSku> goodsSkuArrayList = new ArrayList<>();
        // sku库存
        ArrayList<SellerInventory> sellerInventoryArrayList = new ArrayList<>();
        for (SysGoodsImport sysGoodsImport : sysGoodsImports) {
            sysGoodsImport.setBarCode(String.valueOf(Math.round(Double.valueOf(sysGoodsImport.getBarCode()))));
            sysGoodsImport.setNumber(String.valueOf(String.valueOf(Math.round(Double.valueOf(sysGoodsImport.getNumber())))));
            sysGoodsImport.setBarCode(String.valueOf(String.valueOf(Math.round(Double.valueOf(sysGoodsImport.getBarCode())))));
            LambdaQueryWrapper<SysGoods> sysGoodsLambdaQueryWrapper = Wrappers.lambdaQuery(SysGoods.class);
            sysGoodsLambdaQueryWrapper.eq(SysGoods::getBarCode, sysGoodsImport.getBarCode());
            // sql
            SysGoods sysGoodsSql = sysGoodsMapper.selectOne(sysGoodsLambdaQueryWrapper);
            // 分类校验
            ArrayList<String> cateIds = new ArrayList<>();
            cateIds.add(sysGoodsImport.getCateIdOne());
            cateIds.add(sysGoodsImport.getCateIdTwo());
            cateIds.add(sysGoodsImport.getCateId());
            LambdaQueryWrapper<SysCategory> sysCategoryLambdaQueryWrapper = Wrappers.lambdaQuery(SysCategory.class);
            sysCategoryLambdaQueryWrapper.in(SysCategory::getName, cateIds);
            List<SysCategory> sysCategories = sysCategoryMapper.selectList(sysCategoryLambdaQueryWrapper);
            if (sysCategories.size() < 3) throw new CommonException("商品导入", "商品分类不存在，添加失败");
            // 最低限价默认值
            if (sysGoodsImport.getPriceFloor() == null || sysGoodsImport.getPriceFloor().equals("")) {
                sysGoodsImport.setPriceFloor("0");
            }
            // 拷贝封装对象
            SysGoods sysGoods = BeanUtil.copyProperties(sysGoodsImport, SysGoods.class);
            GoodsSku goodsSku = BeanUtil.copyProperties(sysGoodsImport, GoodsSku.class);
            SellerInventory sellerInventory = BeanUtil.copyProperties(sysGoodsImport, SellerInventory.class);
            // 拿到三级分类对象
            LambdaQueryWrapper<SysCategory> sysCategoryLambdaQueryWrapper1 = Wrappers.lambdaQuery(SysCategory.class);
            sysCategoryLambdaQueryWrapper1.eq(SysCategory::getName, sysGoodsImport.getCateId());
            sysCategoryLambdaQueryWrapper1.ne(SysCategory::getParentId, 0);
            SysCategory sysCategory = sysCategoryMapper.selectOne(sysCategoryLambdaQueryWrapper1);
            // 封装数据
            {
                sysGoods.setGoodsId(redisIdWorker.nextId("sysgoods"));
                sysGoods.setCateId(sysCategory.getCateId());
                sysGoods.setRevision(1);
                sysGoods.setCreateTime(LocalDateTime.now());
                sysGoods.setUpdateTime(LocalDateTime.now());
                sysGoods.setUpdateBy(GetUser.getUserId());
                sysGoods.setCreateBy(GetUser.getUserId());
                sysGoods.setActivity(1);
                if (sysGoods.getTenantId() == null) {
                    sysGoods.setTenantId(RandomUtil.randomString(11));
                }
                sysGoods.setStartingTimeOfPurchaseRestrictionPeriod(openHours(sysGoods.getStartingTimeOfPurchaseRestrictionPeriod()));
                sysGoods.setEndTimeOfPurchaseRestrictionPeriod(openHours(sysGoods.getEndTimeOfPurchaseRestrictionPeriod()));
                BeanUtil.copyProperties(sysGoods, sysGoodsSql);
            }
            sysGoodsArrayList.add(sysGoodsSql);
            {
                // 删除老的sku
                LambdaQueryWrapper<GoodsSku> goodsSkuLambdaQueryWrapper = Wrappers.lambdaQuery(GoodsSku.class);
                goodsSkuLambdaQueryWrapper.eq(GoodsSku::getGoodsId, sysGoodsSql.getGoodsId());
                goodsSkuMapper.delete(goodsSkuLambdaQueryWrapper);
                // 下面是需要新增的sku
                goodsSku.setSkuId(redisIdWorker.nextId("goodssku"));
                goodsSku.setGoodsId(sysGoods.getGoodsId());
                goodsSku.setCreateTime(LocalDateTime.now());
                goodsSku.setUpdateTime(LocalDateTime.now());
                goodsSku.setUpdateBy(GetUser.getUserId());
                goodsSku.setCreateBy(GetUser.getUserId());
                // 规格值处理
                JSONObject jsonObject = new JSONObject();
                // key
                String unit = goodsSku.getUnit();
                String[] units = unit.split("/");
                // value
                String skuVal = goodsSku.getSkuVal();
                String[] skuVals = skuVal.split("/");
                for (int i = 0; i < units.length; i++) {
                    jsonObject.put(units[i], skuVals[i]);
                }
                goodsSku.setSkuVal(jsonObject.toJSONString());
            }
            goodsSkuArrayList.add(goodsSku);
            {
                // 删除sku库存
                LambdaQueryWrapper<SellerInventory> sellerInventoryLambdaQueryWrapper = Wrappers.lambdaQuery(SellerInventory.class);
                sellerInventoryLambdaQueryWrapper.eq(SellerInventory::getGoodsId, sysGoodsSql.getGoodsId());
                sellerInventoryMapper.delete(sellerInventoryLambdaQueryWrapper);
                // 下面是新增sku库存
                sellerInventory.setId(redisIdWorker.nextId("sellerinventory"));
                sellerInventory.setSkuId(goodsSku.getSkuId());
                sellerInventory.setGoodsId(sysGoods.getGoodsId());
                sellerInventory.setRevision(1);
            }
            sellerInventoryArrayList.add(sellerInventory);
        }
        // 拿到分类名称
        ArrayList<String> flname = new ArrayList<>();
        for (SysGoods sysGoods : sysGoodsArrayList) {
            flname.add(sysGoods.getCateId());
        }
        // 拿到所需分类，三级分类
        LambdaQueryWrapper<SysCategory> goodsSkuLambdaQueryWrapper = Wrappers.lambdaQuery(SysCategory.class);
        goodsSkuLambdaQueryWrapper.in(SysCategory::getName, flname);
        List<SysCategory> sysCategories = sysCategoryMapper.selectList(goodsSkuLambdaQueryWrapper);
        // 商品找分类id
        for (SysGoods sysGoods : sysGoodsArrayList) {
            for (SysCategory sysCategory : sysCategories) {
                if (sysGoods.getCateId().equals(sysCategory.getName())) {
                    sysGoods.setCateId(sysCategory.getCateId());
                    break;
                }
            }
        }
        // 更新商品
        for (SysGoods sysGoods : sysGoodsArrayList) {
            LambdaQueryWrapper<SysGoods> sysGoodsLambdaQueryWrapper = Wrappers.lambdaQuery(SysGoods.class);
            sysGoodsLambdaQueryWrapper.eq(SysGoods::getBarCode, sysGoods.getBarCode());
            sysGoodsMapper.update(sysGoods, sysGoodsLambdaQueryWrapper);
        }
        goodsSkuMapper.insertBatchSomeColumn(goodsSkuArrayList);
        sellerInventoryMapper.insertBatchSomeColumn(sellerInventoryArrayList);
    }

    /**
     * 商品导入
     *
     * @param sysGoodsImportxs
     */
    @Override
    @Transactional
    public void autoAddSysGoodsListTwo(List<SysGoodsImport> sysGoodsImports) throws ParseException {
        // 商品
        ArrayList<SysGoods> sysGoodsArrayList = new ArrayList<>();
        // sku
        ArrayList<GoodsSku> goodsSkuArrayList = new ArrayList<>();
        // sku库存
        ArrayList<SellerInventory> sellerInventoryArrayList = new ArrayList<>();
        // 需要更新的集合
        ArrayList<SysGoodsImport> update = new ArrayList<>();
        // 需要新增的集合
        ArrayList<SysGoodsImport> insert = new ArrayList<>();
        // 筛选，更新，新增
        for (SysGoodsImport sysGoodsImport : sysGoodsImports) {
            LambdaQueryWrapper<SysGoods> sysGoodsLambdaQueryWrapper = Wrappers.lambdaQuery(SysGoods.class);
            sysGoodsLambdaQueryWrapper.eq(SysGoods::getBarCode, sysGoodsImport.getBarCode());
            SysGoods sysGoods = sysGoodsMapper.selectOne(sysGoodsLambdaQueryWrapper);
            if (sysGoods == null) {
                // 新增
                insert.add(sysGoodsImport);
            } else {
                // 修改
                update.add(sysGoodsImport);
            }
        }
        if (insert.size() != 0) {
            // 新增的
            for (SysGoodsImport sysGoodsImport : insert) {
                sysGoodsImport.setBarCode(String.valueOf(Math.round(Double.valueOf(sysGoodsImport.getBarCode()))));
                sysGoodsImport.setNumber(String.valueOf(String.valueOf(Math.round(Double.valueOf(sysGoodsImport.getNumber())))));
                sysGoodsImport.setBarCode(String.valueOf(String.valueOf(Math.round(Double.valueOf(sysGoodsImport.getBarCode())))));
                // 商品校验
                LambdaQueryWrapper<SysGoods> sysGoodsLambdaQueryWrapper = Wrappers.lambdaQuery(SysGoods.class);
                sysGoodsLambdaQueryWrapper.eq(SysGoods::getBarCode, sysGoodsImport.getBarCode());
                SysGoods sysGoodsSql = sysGoodsMapper.selectOne(sysGoodsLambdaQueryWrapper);
                // 新增
                // 分类校验
                ArrayList<String> cateIds = new ArrayList<>();
                cateIds.add(sysGoodsImport.getCateIdOne());
                cateIds.add(sysGoodsImport.getCateIdTwo());
                cateIds.add(sysGoodsImport.getCateId());
                LambdaQueryWrapper<SysCategory> sysCategoryLambdaQueryWrapper = Wrappers.lambdaQuery(SysCategory.class);
                sysCategoryLambdaQueryWrapper.in(SysCategory::getName, cateIds);
                List<SysCategory> sysCategories = sysCategoryMapper.selectList(sysCategoryLambdaQueryWrapper);
                if (sysCategories.size() < 3) throw new CommonException("商品导入", "商品分类不存在，添加失败");
                // 最低限价默认值
                if (sysGoodsImport.getPriceFloor() == null || sysGoodsImport.getPriceFloor().equals("")) {
                    sysGoodsImport.setPriceFloor("0");
                }
                // 拷贝封装对象
                SysGoods sysGoods = BeanUtil.copyProperties(sysGoodsImport, SysGoods.class);
                GoodsSku goodsSku = BeanUtil.copyProperties(sysGoodsImport, GoodsSku.class);
                SellerInventory sellerInventory = BeanUtil.copyProperties(sysGoodsImport, SellerInventory.class);
                // 拿到三级分类对象
                LambdaQueryWrapper<SysCategory> sysCategoryLambdaQueryWrapper1 = Wrappers.lambdaQuery(SysCategory.class);
                sysCategoryLambdaQueryWrapper1.eq(SysCategory::getName, sysGoodsImport.getCateId());
                sysCategoryLambdaQueryWrapper1.ne(SysCategory::getParentId, 0);
                SysCategory sysCategory = sysCategoryMapper.selectOne(sysCategoryLambdaQueryWrapper1);
                // 封装数据
                {
                    sysGoods.setGoodsId(redisIdWorker.nextId("sysgoods"));
                    sysGoods.setCateId(sysCategory.getCateId());
                    sysGoods.setRevision(1);
                    sysGoods.setCreateTime(LocalDateTime.now());
                    sysGoods.setUpdateTime(LocalDateTime.now());
                    sysGoods.setUpdateBy(GetUser.getUserId());
                    sysGoods.setCreateBy(GetUser.getUserId());
                    sysGoods.setActivity(1);
                    sysGoods.setStartingTimeOfPurchaseRestrictionPeriod(openHours(sysGoods.getStartingTimeOfPurchaseRestrictionPeriod()));
                    sysGoods.setEndTimeOfPurchaseRestrictionPeriod(openHours(sysGoods.getEndTimeOfPurchaseRestrictionPeriod()));
                    sysGoods.setTenantId(RandomUtil.randomString(11));
                }
                sysGoodsArrayList.add(sysGoods);
                {
                    goodsSku.setSkuId(redisIdWorker.nextId("goodssku"));
                    goodsSku.setGoodsId(sysGoods.getGoodsId());
                    goodsSku.setCreateTime(LocalDateTime.now());
                    goodsSku.setUpdateTime(LocalDateTime.now());
                    goodsSku.setUpdateBy(GetUser.getUserId());
                    goodsSku.setCreateBy(GetUser.getUserId());
                    // 规格值处理
                    JSONObject jsonObject = new JSONObject();
                    // key
                    String unit = goodsSku.getUnit();
                    String[] units = unit.split("/");
                    // value
                    String skuVal = goodsSku.getSkuVal();
                    String[] skuVals = skuVal.split("/");
                    for (int i = 0; i < units.length; i++) {
                        jsonObject.put(units[i], skuVals[i]);
                    }
                    goodsSku.setSkuVal(jsonObject.toJSONString());
                }
                goodsSkuArrayList.add(goodsSku);
                {
                    sellerInventory.setId(redisIdWorker.nextId("sellerinventory"));
                    sellerInventory.setSkuId(goodsSku.getSkuId());
                    sellerInventory.setGoodsId(sysGoods.getGoodsId());
                    sellerInventory.setRevision(1);
                }
                sellerInventoryArrayList.add(sellerInventory);
            }
            sysGoodsMapper.insertBatchSomeColumn(sysGoodsArrayList);
            goodsSkuMapper.insertBatchSomeColumn(goodsSkuArrayList);
            sellerInventoryMapper.insertBatchSomeColumn(sellerInventoryArrayList);
        }
        if (update.size() != 0) {
            update(update);
        }
    }

    /**
     * 商品评价
     *
     * @return
     */
    @Override
    public List<OrderReviews> getGoodsEvaluate(NewPage newPage) {
        int page = (newPage.getPage() - 1) * newPage.getPageSize();
        newPage.setPage(page);
        List<OrderReviews> orderReviewsPage = sysGoodsMapper.getGoodsEvaluate(newPage);
        return orderReviewsPage;
    }

    /**
     * 商品海报
     *
     * @param goodsId
     * @return
     */
    @Override
    public String getPoster(Long goodsId) {
        String s = hbUtils.addImages(goodsId, redisIdWorker, sysGoodsMapper);
        return s;
    }

    /**
     * 时间获取异常解决方案
     *
     * @param openHours
     * @return
     */
    public String openHours(String openHours) {
        Date setupTime = HSSFDateUtil.getJavaDate(Double.valueOf(openHours));
        long time = setupTime.getTime();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String sd = sdf.format(new Date(Long.parseLong(String.valueOf(time))));
        return sd;
    }
}