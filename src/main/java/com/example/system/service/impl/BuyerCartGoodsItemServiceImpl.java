package com.example.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.system.exception.CommonException;
import com.example.system.mapper.BuyerCartGoodsItemMapper;
import com.example.system.pojo.BuyerAddress;
import com.example.system.pojo.BuyerCartGoodsItem;
import com.example.system.service.BuyerCartGoodsItemService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@SuppressWarnings("all")
public class BuyerCartGoodsItemServiceImpl implements BuyerCartGoodsItemService {

    private final BuyerCartGoodsItemMapper buyerCartGoodsItemMapper;


    /**
     * 购物车商品信息列表
     *
     * @param page
     * @param pageSize
     * @return
     */
    @Override
    public Page<BuyerCartGoodsItem> getList(Long page, Long pageSize) {
        LambdaQueryWrapper<BuyerCartGoodsItem> buyerCartGoodsItemLambdaQueryWrapper = Wrappers.lambdaQuery(BuyerCartGoodsItem.class);
        buyerCartGoodsItemLambdaQueryWrapper.orderByAsc(BuyerCartGoodsItem::getCreateTime);
        Page<BuyerCartGoodsItem> buyerCartLambdaQueryWrapper = buyerCartGoodsItemMapper.selectPage(new Page<BuyerCartGoodsItem>(page, pageSize), buyerCartGoodsItemLambdaQueryWrapper);
        return buyerCartLambdaQueryWrapper;
    }
}