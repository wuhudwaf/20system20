package com.example.system.service.impl;

import com.example.system.mapper.DeliveryFeeOtherConfigMapper;
import com.example.system.service.DeliveryFeeOtherConfigService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@SuppressWarnings("all")
public class DeliveryFeeOtherConfigServiceImpl implements DeliveryFeeOtherConfigService {

    private final DeliveryFeeOtherConfigMapper deliveryFeeOtherConfigMapper;
}