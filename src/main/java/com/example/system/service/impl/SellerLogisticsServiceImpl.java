package com.example.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.system.exception.CommonException;
import com.example.system.mapper.SellerLogisticsMapper;
import com.example.system.pojo.SellerInventory;
import com.example.system.pojo.SellerLogistics;
import com.example.system.service.SellerLogisticsService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@SuppressWarnings("all")
public class SellerLogisticsServiceImpl implements SellerLogisticsService {

    private final SellerLogisticsMapper sellerLogisticsMapper;


    /**
     * 物流信息列表
     *
     * @param page
     * @param pageSize
     * @return
     */
    @Override
    public Page<SellerLogistics> getPage(Long page, Long pageSize) {
        LambdaQueryWrapper<SellerLogistics> buyerUserLambdaQueryWrapper = Wrappers.lambdaQuery(SellerLogistics.class);
        buyerUserLambdaQueryWrapper.orderByAsc(SellerLogistics::getCreateTime);
        Page<SellerLogistics> sellerLogisticsPage = sellerLogisticsMapper.selectPage(new Page<SellerLogistics>(page, pageSize), buyerUserLambdaQueryWrapper);
        return sellerLogisticsPage;
    }
}