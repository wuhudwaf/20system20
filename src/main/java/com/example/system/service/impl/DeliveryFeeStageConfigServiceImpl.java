package com.example.system.service.impl;

import com.example.system.mapper.DeliveryFeeStageConfigMapper;
import com.example.system.service.DeliveryFeeStageConfigService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@SuppressWarnings("all")
public class DeliveryFeeStageConfigServiceImpl implements DeliveryFeeStageConfigService {

    private final DeliveryFeeStageConfigMapper deliveryFeeStageConfigMapper;
}