package com.example.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.system.exception.CommonException;
import com.example.system.mapper.SellerGoodsMapper;
import com.example.system.pojo.OrderReviews;
import com.example.system.pojo.SellerGoods;
import com.example.system.service.SellerGoodsService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@SuppressWarnings("all")
public class SellerGoodsServiceImpl implements SellerGoodsService {

    private final SellerGoodsMapper sellerGoodsMapper;

    /**
     * 查询门店商品列表
     *
     * @param page
     * @param pageSize
     * @return
     */
    @Override
    public Page<SellerGoods> getPage(String shopId, Long page, Long pageSize) {
        LambdaQueryWrapper<SellerGoods> sellerGoodsLambdaQueryWrapper = Wrappers.lambdaQuery(SellerGoods.class);
        // 指定店铺id
        sellerGoodsLambdaQueryWrapper.eq(SellerGoods::getShopId, shopId);
        // 排序
        sellerGoodsLambdaQueryWrapper.orderByAsc(SellerGoods::getCreateTime);
        // 查询
        Page<SellerGoods> sellerGoodsPage = sellerGoodsMapper.selectPage(new Page<SellerGoods>(page, pageSize), sellerGoodsLambdaQueryWrapper);
        // 返回
        return sellerGoodsPage;
    }
}