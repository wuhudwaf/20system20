package com.example.system.service.impl;

import com.example.system.mapper.AppletConfigMapper;
import com.example.system.service.AppletConfigService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@SuppressWarnings("all")
public class AppletConfigServiceImpl implements AppletConfigService {

    private final AppletConfigMapper appletConfigMapper;
}