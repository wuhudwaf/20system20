package com.example.system.service.impl;

import cn.hutool.json.JSONUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.system.dto.UserDto;
import com.example.system.exception.CommonException;
import com.example.system.mapper.SysBannerMapper;
import com.example.system.pojo.SysBanner;
import com.example.system.service.SysBannerService;
import com.example.system.utils.GetUser;
import com.example.system.utils.RedisIdWorker;
import com.example.system.utils.RedisUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;

@Service
@RequiredArgsConstructor
@SuppressWarnings("all")
public class SysBannerServiceImpl implements SysBannerService {

    private final SysBannerMapper sysBannerMapper;

    private final RedisIdWorker redisIdWorker;

    /**
     * 新增轮播图
     *
     * @param sysBanner
     * @return
     */
    @Override
    public void add(SysBanner sysBanner) {
        sysBanner.setBannerId(redisIdWorker.nextId("banner"));
        sysBanner.setStatus(1);
        LocalDateTime time = LocalDateTime.now();
        String userId = GetUser.getUserId();
        sysBanner.setCreateTime(time);
        sysBanner.setCreateBy(userId);
        sysBanner.setLastUpdateTime(time);
        sysBanner.setLastUpdateBy(userId);
        if (sysBannerMapper.insert(sysBanner) <= 0) throw new CommonException("新增轮播图", "网络异常，稍后再试");
    }

    /**
     * 删除轮播图
     *
     * @param bannerId
     * @return
     */
    @Override
    public void delete(String bannerId) {
        LambdaQueryWrapper<SysBanner> sysBannerLambdaQueryWrapper = Wrappers.lambdaQuery(SysBanner.class);
        sysBannerLambdaQueryWrapper.eq(SysBanner::getBannerId, bannerId);
        SysBanner sysBanner = sysBannerMapper.selectOne(sysBannerLambdaQueryWrapper);
        if (sysBanner == null) throw new CommonException("删除轮播图 ->", "轮播图不存在");
        if (sysBannerMapper.delete(sysBannerLambdaQueryWrapper) <= 0) {
            throw new CommonException("删除轮播图 -->", "网络异常，稍后再试");
        }
    }

    /**
     * 修改轮播图
     *
     * @param sysBanner
     * @return
     */
    @Override
    public void setSysBanner(SysBanner sysBanner) {
        LambdaQueryWrapper<SysBanner> sysBannerLambdaQueryWrapper = Wrappers.lambdaQuery(SysBanner.class);
        sysBannerLambdaQueryWrapper.eq(SysBanner::getBannerId, sysBanner.getBannerId());
        SysBanner sqlSysBanner = sysBannerMapper.selectOne(sysBannerLambdaQueryWrapper);
        if (sqlSysBanner == null) throw new CommonException("修改轮播图 ->", "轮播图不存在");
        sysBanner.setLastUpdateTime(LocalDateTime.now());
        sysBanner.setLastUpdateBy(GetUser.getUserId());
        if (sysBannerMapper.update(sysBanner, sysBannerLambdaQueryWrapper) <= 0) {
            throw new CommonException("修改轮播图 -->", "网络异常，稍后再试");
        }
    }

    /**
     * 查看轮播图详情
     *
     * @param bannerId
     * @return
     */
    @Override
    public SysBanner getSysBanner(String bannerId) {
        LambdaQueryWrapper<SysBanner> sysBannerLambdaQueryWrapper = Wrappers.lambdaQuery(SysBanner.class);
        sysBannerLambdaQueryWrapper.eq(SysBanner::getBannerId, bannerId);
        SysBanner sysBanner = sysBannerMapper.selectOne(sysBannerLambdaQueryWrapper);
        if (sysBanner == null) throw new CommonException("查看轮播图详情 ->", "轮播图不存在");
        return sysBanner;
    }

    /**
     * 获取轮播图列表
     *
     * @param page
     * @param pageSize
     * @return
     */
    @Override
    public Page<SysBanner> getSysBannerPage(Long page, Long pageSize) {
        LambdaQueryWrapper<SysBanner> sysBannerLambdaQueryWrapper = Wrappers.lambdaQuery(SysBanner.class);
        // status排序
        sysBannerLambdaQueryWrapper.orderByAsc(SysBanner::getSort);
        // status为空则对创建时间排序
        sysBannerLambdaQueryWrapper.orderByDesc(SysBanner::getCreateTime);
        Page<SysBanner> sysBannerPage = sysBannerMapper.selectPage(new Page<SysBanner>(page, pageSize), sysBannerLambdaQueryWrapper);
        return sysBannerPage;
    }
}