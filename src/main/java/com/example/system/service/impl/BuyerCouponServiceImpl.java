package com.example.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.system.exception.CommonException;
import com.example.system.mapper.BuyerCouponMapper;
import com.example.system.pojo.BuyerCollect;
import com.example.system.pojo.BuyerCoupon;
import com.example.system.service.BuyerCouponService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@SuppressWarnings("all")
public class BuyerCouponServiceImpl implements BuyerCouponService {

    private final BuyerCouponMapper buyerCouponMapper;

    /**
     * 用户优惠券列表
     *
     * @param page
     * @param pageSize
     * @return
     */
    @Override
    public Page<BuyerCoupon> getList(Long page, Long pageSize) {
        LambdaQueryWrapper<BuyerCoupon> buyerCouponLambdaQueryWrapper = Wrappers.lambdaQuery(BuyerCoupon.class);
        buyerCouponLambdaQueryWrapper.orderByAsc(BuyerCoupon::getCreateTime);
        Page<BuyerCoupon> buyerCouponPage = buyerCouponMapper.selectPage(new Page<BuyerCoupon>(page, pageSize), buyerCouponLambdaQueryWrapper);
        return buyerCouponPage;
    }
}