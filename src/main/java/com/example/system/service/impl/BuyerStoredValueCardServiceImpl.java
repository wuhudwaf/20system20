package com.example.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.system.exception.CommonException;
import com.example.system.mapper.BuyerStoredValueCardMapper;
import com.example.system.pojo.BuyerScore;
import com.example.system.pojo.BuyerStoredValueCard;
import com.example.system.service.BuyerStoredValueCardService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@SuppressWarnings("all")
public class BuyerStoredValueCardServiceImpl implements BuyerStoredValueCardService {

    private final BuyerStoredValueCardMapper buyerStoredValueCardMapper;


    /**
     * 用户储值卡记录列表
     *
     * @param page
     * @param pageSize
     * @return
     */
    @Override
    public Page<BuyerStoredValueCard> getPage(Long page, Long pageSize) {
        LambdaQueryWrapper<BuyerStoredValueCard> buyerUserLambdaQueryWrapper = Wrappers.lambdaQuery(BuyerStoredValueCard.class);
        buyerUserLambdaQueryWrapper.orderByAsc(BuyerStoredValueCard::getCreateTime);
        Page<BuyerStoredValueCard> buyerStoredValueCardPage = buyerStoredValueCardMapper.selectPage(new Page<BuyerStoredValueCard>(page, pageSize), buyerUserLambdaQueryWrapper);
        return buyerStoredValueCardPage;
    }
}