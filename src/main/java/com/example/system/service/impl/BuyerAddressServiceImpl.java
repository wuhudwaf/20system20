package com.example.system.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.system.mapper.BuyerAddressMapper;
import com.example.system.pojo.BuyerAddress;
import com.example.system.pojo.vo.BuyerAddressVo;
import com.example.system.service.BuyerAddressService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
@SuppressWarnings("all")
public class BuyerAddressServiceImpl implements BuyerAddressService {

    private final BuyerAddressMapper buyerAddressMapper;


    /**
     * 获取某位用户的地址信息列表
     *
     * @param userId
     * @param page
     * @param pageSize
     * @return
     */
    @Override
    public Page<BuyerAddressVo> getList(String userId, Long page, Long pageSize) {
        LambdaQueryWrapper<BuyerAddress> buyerAddressLambdaQueryWrapper = Wrappers.lambdaQuery(BuyerAddress.class);
        buyerAddressLambdaQueryWrapper.eq(BuyerAddress::getUserId, userId);
        Page<BuyerAddress> buyerAddressPage = buyerAddressMapper.selectPage(new Page<BuyerAddress>(page, pageSize), buyerAddressLambdaQueryWrapper);
        List<BuyerAddress> records = buyerAddressPage.getRecords();
        Page<BuyerAddressVo> buyerAddressVoPage = new Page<>();
        ArrayList<BuyerAddressVo> buyerAddressVos = new ArrayList<>();
        BeanUtil.copyProperties(buyerAddressPage, buyerAddressVoPage, "records");
        for (BuyerAddress record : records) {
            BuyerAddressVo buyerAddressVo = BeanUtil.copyProperties(record, BuyerAddressVo.class, "addressId", "userId");
            buyerAddressVo.setAddressId(record.getAddressId().toString());
            buyerAddressVo.setUserId(record.getUserId().toString());
            buyerAddressVos.add(buyerAddressVo);
        }
        buyerAddressVoPage.setRecords(buyerAddressVos);
        return buyerAddressVoPage;
    }
}