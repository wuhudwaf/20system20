package com.example.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.system.exception.CommonException;
import com.example.system.mapper.SysCashMapper;
import com.example.system.mapper.SysShopMapper;
import com.example.system.pojo.SysBanner;
import com.example.system.pojo.SysCash;
import com.example.system.pojo.SysShop;
import com.example.system.service.SysCashService;
import com.example.system.utils.RedisIdWorker;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

@Service
@RequiredArgsConstructor
@SuppressWarnings("all")
public class SysCashServiceImpl implements SysCashService {

    private final SysCashMapper sysCashMapper;

    private final SysShopMapper sysShopMapper;

    private final RedisIdWorker redisIdWorker;


    /**
     * 提现申请列表
     *
     * @param page
     * @param pageSize
     * @return
     */
    @Override
    public Page<SysCash> getPage(Long page, Long pageSize) {
        LambdaQueryWrapper<SysCash> buyerUserLambdaQueryWrapper = Wrappers.lambdaQuery(SysCash.class);
        buyerUserLambdaQueryWrapper.orderByAsc(SysCash::getCreateTime);
        Page<SysCash> selectPage = sysCashMapper.selectPage(new Page<SysCash>(page, pageSize), buyerUserLambdaQueryWrapper);
        // selectPage分页对象中的列表数据
        List<SysCash> records = selectPage.getRecords();
        // set集合保证id不重复
        Set<String> shopIdSet = new TreeSet<>();
        // 循环获取
        for (SysCash record : records) {
            shopIdSet.add(record.getShopId());
        }
        // 多个id批量查询 拿到多条门店数据
        List<SysShop> sysShops = sysShopMapper.selectBatchIds(shopIdSet);
        // 获取门店名称并封装
        for (SysShop sysShop : sysShops) {
            String shopName = sysShop.getName();
            for (SysCash record : records) {
                if (record.getShopId().equals(sysShop.getShopId())) {
                    record.setShopName(shopName);
                }
            }
        }
        // 更新page对象数据列表
        selectPage.setRecords(records);
        // 返回分页对象
        return selectPage;
    }

    /**
     * 新增提现申请
     *
     * @param sysCash
     * @return
     */
    @Override
    public void addSysCash(SysCash sysCash) {
        String id = redisIdWorker.nextId("SysCash");
        sysCash.setId(id);
        sysCash.setCreateTime(LocalDateTime.now());
        sysCash.setStatus(1);
        if (sysCashMapper.insert(sysCash) <= 0) throw new CommonException("新增提现申请", "网络异常，稍后再试");
    }

    /**
     * 同意申请/申请驳回
     *
     * @param id
     * @param status
     * @return
     */
    @Override
    public void deleteSysCash(String id,Integer status) {
        // 提现申请是否存在
        SysCash sysCash = sysCashMapper.selectById(id);
        if (sysCash == null) throw new CommonException("删除提现申请", "提现申请不存在");
        // 修改状态
        sysCash.setStatus(status);
        // 更新
        if (sysCashMapper.updateById(sysCash) <= 0) throw new CommonException("申请驳回", "网络异常，稍后再试");
    }

    /**
     * 提现申请详情
     *
     * @param id
     * @return
     */
    @Override
    public SysCash getBySysCashId(String id) {
        // id查到申请详情
        SysCash sysCash = sysCashMapper.selectById(id);
        // 是否存在
        if (sysCash == null) throw new CommonException("删除提现申请", "提现申请不存在");
        // 存在，获取门店详情
        SysShop byShopId = sysShopMapper.getByShopId(sysCash.getShopId());
        // 拿到门店名称
        sysCash.setShopName(byShopId.getName());
        // 返回
        return sysCash;
    }

    /**
     * 测试
     *
     * @param page
     * @param pageSize
     * @return
     */
    @Override
    public Page<SysCash> getPageTest(Long page, Long pageSize) {
        LambdaQueryWrapper<SysCash> buyerUserLambdaQueryWrapper = Wrappers.lambdaQuery(SysCash.class);
        buyerUserLambdaQueryWrapper.orderByAsc(SysCash::getCreateTime);
        Page<SysCash> selectPage = sysCashMapper.selectPage(new Page<SysCash>(page, pageSize), buyerUserLambdaQueryWrapper);
        // selectPage分页对象中的列表数据
        List<SysCash> records = selectPage.getRecords();
        // set集合保证id不重复
        Set<String> shopIdSet = new TreeSet<>();
        // 循环获取
        for (SysCash record : records) {
            shopIdSet.add(record.getShopId());
        }
        // 多个id批量查询 拿到多条门店数据
        List<SysShop> sysShops = sysShopMapper.selectBatchIds(shopIdSet);
        // 获取门店名称并封装
        for (SysShop sysShop : sysShops) {
            String shopName = sysShop.getName();
            for (SysCash record : records) {
                if (record.getShopId().equals(sysShop.getShopId())) {
                    record.setShopName(shopName);
                }
            }
        }
        // 更新page对象数据列表
        selectPage.setRecords(records);
        // 返回分页对象
        return selectPage;
    }
}