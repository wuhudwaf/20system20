package com.example.system.service.impl;

import cn.hutool.extra.spring.SpringUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.system.exception.CommonException;
import com.example.system.mapper.BuyerAddressMapper;
import com.example.system.mapper.BuyerMembershipRuleMapper;
import com.example.system.mapper.BuyerUserMapper;
import com.example.system.pojo.BuyerAddress;
import com.example.system.pojo.BuyerMembershipRule;
import com.example.system.pojo.BuyerUser;
import com.example.system.service.BuyerMembershipRuleService;
import com.example.system.utils.RedisIdWorker;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;


@Service
@RequiredArgsConstructor
@SuppressWarnings("all")
public class BuyerMembershipRuleServiceImpl implements BuyerMembershipRuleService {

    /**
     * 会员规则
     */
    private final BuyerMembershipRuleMapper buyerMembershipRuleMapper;

    /**
     * 微信用户信息表
     */
    private final BuyerUserMapper buyerUserMapper;

    /**
     * 买家地址信息表
     */
    private final BuyerAddressMapper buyerAddressMapper;

    /**
     * 添加会员规则
     *
     * @param buyerMembershipRule
     * @return
     */
    @Override
    public void add(BuyerMembershipRule buyerMembershipRule) {
        RedisIdWorker redisIdWorker = SpringUtil.getBean(RedisIdWorker.class);
        // 规则id
        buyerMembershipRule.setRuleId(redisIdWorker.nextId("buyer"));
        // 创建时间
        buyerMembershipRule.setCreateTime(LocalDateTime.now());
        // 新增
        buyerMembershipRuleMapper.insert(buyerMembershipRule);
    }

    /**
     * 删除会员规则
     *
     * @param ruleId
     * @return
     */
    @Override
    public void delete(String ruleId) {
        if (buyerMembershipRuleMapper.deleteByRuleId(ruleId) <= 0) {
            throw new CommonException("删除会员规则", "无效的id");
        }
    }

    /**
     * 修改会员规则
     *
     * @param buyerMembershipRule
     * @return
     */
    @Override
    public void set(BuyerMembershipRule buyerMembershipRule) {
        LambdaQueryWrapper<BuyerMembershipRule> buyerMembershipRuleLambdaQueryWrapper = Wrappers.lambdaQuery(BuyerMembershipRule.class);
        buyerMembershipRuleLambdaQueryWrapper.eq(BuyerMembershipRule::getRuleId, buyerMembershipRule.getRuleId());
        if (buyerMembershipRuleMapper.update(buyerMembershipRule, buyerMembershipRuleLambdaQueryWrapper) <= 0) {
            throw new CommonException("修改会员规则", "无效的id");
        }
    }

    /**
     * 查看会员规则详情
     *
     * @param ruleId
     * @return
     */
    @Override
    public BuyerMembershipRule getInfo(String ruleId) {
        BuyerMembershipRule buyerMembershipRule = buyerMembershipRuleMapper.getByRuleId(ruleId);
        if (buyerMembershipRule == null) {
            throw new CommonException("查看会员规则详情 -->", "无效的id");
        }
        // 返回拿到的对象
        return buyerMembershipRule;
    }

    /**
     * 会员等级列表
     *
     * @return
     */
    @Override
    public List<BuyerMembershipRule> getList() {
        LambdaQueryWrapper<BuyerMembershipRule> buyerMembershipRuleLambdaQueryWrapper = Wrappers.lambdaQuery(BuyerMembershipRule.class);
        buyerMembershipRuleLambdaQueryWrapper.orderByDesc(BuyerMembershipRule::getMembershipLevel);
        List<BuyerMembershipRule> buyerMembershipRulePage = buyerMembershipRuleMapper.selectList(buyerMembershipRuleLambdaQueryWrapper);
        return buyerMembershipRulePage;
    }

    @Override
    public Page<BuyerUser> getUserBuyerMembershipRulePage(Long page, Long pageSize) {
        // 微信用户信息
        LambdaQueryWrapper<BuyerUser> buyerUserLambdaQueryWrapper = Wrappers.lambdaQuery(BuyerUser.class);
        // 会员等级排序
        buyerUserLambdaQueryWrapper.orderByDesc(BuyerUser::getMembershipLevel);
        // 分页查询
        Page<BuyerUser> buyerUserPage = buyerUserMapper.selectPage(new Page<BuyerUser>(page, pageSize), buyerUserLambdaQueryWrapper);
        // 表示没有用户会员信息
        if (buyerUserPage.getRecords().size() == 0) return buyerUserPage;
        // 拿到用户的id
        ArrayList<String> userIds = new ArrayList<>();
        List<BuyerUser> records = buyerUserPage.getRecords();
        for (BuyerUser record : records) {
            userIds.add(record.getUserId());
        }
        // 批量查询用户地址
        LambdaQueryWrapper<BuyerAddress> buyerAddressLambdaQueryWrapper = Wrappers.lambdaQuery(BuyerAddress.class);
        buyerAddressLambdaQueryWrapper.in(BuyerAddress::getUserId, userIds);
        // 用户地址信息
        List<BuyerAddress> buyerAddresses = buyerAddressMapper.selectList(buyerAddressLambdaQueryWrapper);
        // 没有地址数据就不用封装，直接返回微信用户信息即可
        if (buyerAddresses.size() == 0) return buyerUserPage;
        for (BuyerUser record : records) {
            for (BuyerAddress buyerAddress : buyerAddresses) {
                if (record.getUserId().equals(buyerAddress.getUserId())) {
                    // 封装地址信息
                    StringBuffer address = new StringBuffer();
                    address.append(buyerAddress.getProvince() == null ? "" : buyerAddress.getProvince()).append(buyerAddress.getCity() == null ? "" : buyerAddress.getCity()).
                            append(buyerAddress.getCounty() == null ? "" : buyerAddress.getCounty()).append(buyerAddress.getStreet() == null ? "" : buyerAddress.getStreet())
                            .append(buyerAddress.getLastDetail() == null ? "" : buyerAddress.getLastDetail());
                    record.setAddress(address.toString());
                    break;
                }
            }
        }
        buyerUserPage.setRecords(records);
        return buyerUserPage;
    }
}