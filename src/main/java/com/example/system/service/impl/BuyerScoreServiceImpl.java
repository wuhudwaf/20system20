package com.example.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.system.mapper.BuyerScoreMapper;
import com.example.system.pojo.BuyerScore;
import com.example.system.pojo.page.NewPage;
import com.example.system.pojo.vo.BuyerUserBuyerScoreVo;
import com.example.system.service.BuyerScoreService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
@SuppressWarnings("All")
public class BuyerScoreServiceImpl implements BuyerScoreService {

    private final BuyerScoreMapper buyerScoreMapper;


    /**
     * 用户积分列表
     *
     * @param page
     * @param pageSize
     * @return
     */
    @Override
    public Page<BuyerScore> getPage(Long page, Long pageSize) {
        LambdaQueryWrapper<BuyerScore> buyerUserLambdaQueryWrapper = Wrappers.lambdaQuery(BuyerScore.class);
        buyerUserLambdaQueryWrapper.orderByAsc(BuyerScore::getCreateTime);
        Page<BuyerScore> buyerOrderStatusLogPage = buyerScoreMapper.selectPage(new Page<BuyerScore>(page, pageSize), buyerUserLambdaQueryWrapper);
        return buyerOrderStatusLogPage;
    }

    /**
     * 积分明细
     *
     * @param newPage
     * @return
     */
    @Override
    public Page<BuyerUserBuyerScoreVo> buyerUserScore(NewPage newPage) {
        int page = (newPage.getPage() - 1) * newPage.getPageSize();
        newPage.setPage(page);
        List<BuyerUserBuyerScoreVo> buyerUserBuyerScoreVos = buyerScoreMapper.buyerUserScore(newPage);
        Page<BuyerUserBuyerScoreVo> buyerUserBuyerScoreVoPage = new Page<>();
        buyerUserBuyerScoreVoPage.setRecords(buyerUserBuyerScoreVos);
        buyerUserBuyerScoreVoPage.setTotal(buyerUserBuyerScoreVos.size());
        return buyerUserBuyerScoreVoPage;
    }
}