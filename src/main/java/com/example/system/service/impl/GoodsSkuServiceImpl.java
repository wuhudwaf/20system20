package com.example.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.system.exception.CommonException;
import com.example.system.mapper.GoodsSkuMapper;
import com.example.system.pojo.GoodsSku;
import com.example.system.service.GoodsSkuService;
import com.example.system.utils.GetUser;
import com.example.system.utils.RedisIdWorker;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;

@Service
@RequiredArgsConstructor
@SuppressWarnings("all")
public class GoodsSkuServiceImpl extends ServiceImpl<GoodsSkuMapper,GoodsSku> implements GoodsSkuService {

    private final GoodsSkuMapper goodsSkuMapper;

    private final RedisIdWorker redisIdWorker;

    /**
     * 新增商品sku
     *
     * @param goodsSku
     * @return
     */
    @Override
    public void add(GoodsSku goodsSku) {
        String id = GetUser.getUserId();
        LocalDateTime timeNow = LocalDateTime.now();
        goodsSku.setSkuId(redisIdWorker.nextId("goodssuk"));
        goodsSku.setCreateBy(id);
        goodsSku.setCreateTime(timeNow);
        goodsSku.setUpdateBy(id);
        goodsSku.setUpdateTime(timeNow);
        if (goodsSkuMapper.insert(goodsSku) <= 0) throw new CommonException("新增商品sku", "网络异常，请稍后再试");
    }


    /**
     * 删除商品sku
     *
     * @param skuId
     * @return
     */
    @Override
    public void delete(String skuId) {
        GoodsSku byskuId = goodsSkuMapper.getByskuId(skuId);
        if (byskuId == null) throw new CommonException("删除商品sku", "商品规格不存在");
        LambdaQueryWrapper<GoodsSku> goodsSkuLambdaQueryWrapper = Wrappers.lambdaQuery(GoodsSku.class);
        goodsSkuLambdaQueryWrapper.eq(GoodsSku::getSkuId, skuId);
        if (goodsSkuMapper.delete(goodsSkuLambdaQueryWrapper) <= 0)
            throw new CommonException("删除商品sku", "网络异常，请稍后再试");
    }

    /**
     * 修改商品sku
     *
     * @param goodsSku
     * @return
     */
    @Override
    public void set(GoodsSku goodsSku) {
        GoodsSku byskuId = goodsSkuMapper.getByskuId(goodsSku.getSkuId());
        if (byskuId == null) throw new CommonException("修改商品sku", "商品规格不存在");
        goodsSku.setUpdateBy(GetUser.getUserId());
        goodsSku.setUpdateTime(LocalDateTime.now());
        LambdaQueryWrapper<GoodsSku> goodsSkuLambdaQueryWrapper = Wrappers.lambdaQuery(GoodsSku.class);
        goodsSkuLambdaQueryWrapper.eq(GoodsSku::getSkuId, goodsSku.getSkuId());
        if (goodsSkuMapper.update(goodsSku, goodsSkuLambdaQueryWrapper) <= 0)
            throw new CommonException("修改商品sku", "网络异常，请稍后再试");
    }

    /**
     * 查看商品sku详情
     *
     * @param skuId
     * @return
     */
    @Override
    public GoodsSku getBySkuId(String skuId) {
        GoodsSku byskuId = goodsSkuMapper.getByskuId(skuId);
        if (byskuId == null) {
            throw new CommonException("查看商品sku详情 -->", "商品sku不存在");
        }
        return byskuId;
    }

    /**
     * 商品sku列表（管理）
     *
     * @param page
     * @param pageSize
     * @return
     */
    @Override
    public Page<GoodsSku> getPage(Long page, Long pageSize) {
        LambdaQueryWrapper<GoodsSku> goodsSkuLambdaQueryWrapper = Wrappers.lambdaQuery(GoodsSku.class);
        goodsSkuLambdaQueryWrapper.orderByAsc(GoodsSku::getCreateTime);
        Page<GoodsSku> goodsSkuPage = goodsSkuMapper.selectPage(new Page<GoodsSku>(page, pageSize), goodsSkuLambdaQueryWrapper);
        return goodsSkuPage;
    }
}