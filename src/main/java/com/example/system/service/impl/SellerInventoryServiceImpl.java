package com.example.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.system.exception.CommonException;
import com.example.system.mapper.SellerInventoryMapper;
import com.example.system.pojo.SellerGoods;
import com.example.system.pojo.SellerInventory;
import com.example.system.service.SellerInventoryService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@SuppressWarnings("all")
public class SellerInventoryServiceImpl implements SellerInventoryService {

    private final SellerInventoryMapper sellerInventoryMapper;


    /**
     * 查看sku库存列表
     *
     * @param page
     * @param pageSize
     * @return
     */
    @Override
    public Page<SellerInventory> getPage(Long page, Long pageSize) {
        LambdaQueryWrapper<SellerInventory> sellerInventoryLambdaQueryWrapper = Wrappers.lambdaQuery(SellerInventory.class);
        sellerInventoryLambdaQueryWrapper.orderByAsc(SellerInventory::getId);
        Page<SellerInventory> sellerInventoryPage = sellerInventoryMapper.selectPage(new Page<SellerInventory>(page, pageSize), sellerInventoryLambdaQueryWrapper);
        return sellerInventoryPage;
    }
}