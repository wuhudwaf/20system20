package com.example.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.system.exception.CommonException;
import com.example.system.mapper.SysServiceMapper;
import com.example.system.pojo.SysRole;
import com.example.system.pojo.SysService;
import com.example.system.service.SysServiceService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@SuppressWarnings("all")
public class SysServiceServiceImpl implements SysServiceService {

    private final SysServiceMapper sysServiceMapper;


    /**
     * 售后服务列表
     *
     * @param page
     * @param pageSize
     * @return
     */
    @Override
    public Page<SysService> getPage(Long page, Long pageSize) {
        LambdaQueryWrapper<SysService> buyerUserLambdaQueryWrapper = Wrappers.lambdaQuery(SysService.class);
        buyerUserLambdaQueryWrapper.orderByAsc(SysService::getCreateTime);
        Page<SysService> selectPage = sysServiceMapper.selectPage(new Page<SysService>(page, pageSize), buyerUserLambdaQueryWrapper);
        return selectPage;
    }
}