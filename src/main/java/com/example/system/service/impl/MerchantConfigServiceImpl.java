package com.example.system.service.impl;

import com.example.system.mapper.MerchantConfigMapper;
import com.example.system.service.MerchantConfigService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@SuppressWarnings("all")
public class MerchantConfigServiceImpl implements MerchantConfigService {

    private final MerchantConfigMapper merchantConfigMapper;
}