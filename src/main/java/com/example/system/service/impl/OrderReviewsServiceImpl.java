package com.example.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.system.exception.CommonException;
import com.example.system.mapper.OrderReviewsMapper;
import com.example.system.pojo.GoodsSku;
import com.example.system.pojo.OrderReviews;
import com.example.system.service.OrderReviewsService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@SuppressWarnings("all")
public class OrderReviewsServiceImpl implements OrderReviewsService {

    private final OrderReviewsMapper orderReviewsMapper;


    /**
     * 订单评价列表
     *
     * @param page
     * @param pageSize
     * @return
     */
    @Override
    public Page<OrderReviews> getPage(Long page, Long pageSize) {
        LambdaQueryWrapper<OrderReviews> buyerUserLambdaQueryWrapper = Wrappers.lambdaQuery(OrderReviews.class);
        buyerUserLambdaQueryWrapper.orderByAsc(OrderReviews::getReviewsTime);
        Page<OrderReviews> orderReviewsPage = orderReviewsMapper.selectPage(new Page<OrderReviews>(page, pageSize), buyerUserLambdaQueryWrapper);
        return orderReviewsPage;
    }
}