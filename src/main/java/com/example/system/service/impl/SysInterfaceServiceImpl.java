package com.example.system.service.impl;

import cn.hutool.core.util.RandomUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSON;
import cn.hutool.json.JSONUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.system.exception.CommonException;
import com.example.system.mapper.SysInterfaceMapper;
import com.example.system.mapper.SysInterfaceRoleMapper;
import com.example.system.pojo.SysCoupon;
import com.example.system.pojo.SysInterface;
import com.example.system.pojo.SysInterfaceRole;
import com.example.system.service.SysInterfaceService;
import com.example.system.utils.RedisIdWorker;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.concurrent.TimeUnit;

import static com.example.system.utils.Constants.*;

@Service
@RequiredArgsConstructor
@SuppressWarnings("all")
public class SysInterfaceServiceImpl implements SysInterfaceService {

    private final SysInterfaceMapper sysInterfaceMapper;

    private final SysInterfaceRoleMapper sysInterfaceRoleMapper;

    private final RedisIdWorker redisIdWorker;


    /**
     * 新增权限
     *
     * @param sysInterface
     * @return
     */
    @Override
    public void add(SysInterface sysInterface) {
        sysInterface.setId(redisIdWorker.nextId("interface"));
        if (sysInterfaceMapper.insert(sysInterface) <= 0) {
            throw new CommonException("新增权限", "网络异常，请稍后再试");
        }
    }


    /**
     * 删除权限
     *
     * @param id
     * @return
     */
    @Override
    @Transactional
    public void delete(String id) {
        // 删除权限
        if (sysInterfaceMapper.deleteById(id) <= 0) {
            throw new CommonException("删除权限", "网络异常，请稍后再试");
        }
        // 删除权限角色关系
        LambdaQueryWrapper<SysInterfaceRole> sysInterfaceRoleLambdaQueryWrapper = Wrappers.lambdaQuery(SysInterfaceRole.class);
        sysInterfaceRoleLambdaQueryWrapper.eq(SysInterfaceRole::getMenuId, id);
        if (sysInterfaceRoleMapper.delete(sysInterfaceRoleLambdaQueryWrapper) <= 0) {
            throw new CommonException("删除权限", "网络异常，请稍后再试");
        }
    }


    /**
     * 查看权限详情
     *
     * @param id
     * @return
     */
    @Override
    public SysInterface getById(String id) {
        SysInterface sysInterface = sysInterfaceMapper.selectById(id);
        // 是否为null
        if (sysInterface == null) {
            throw new CommonException("查看权限详情 -->", "权限不存在");
        }
        // 返回
        return sysInterface;
    }

    /**
     * 查询所有权限
     *
     * @param page
     * @param pageSize
     * @return
     */
    @Override
    public Page<SysInterface> getPage(Long page, Long pageSize) {
        LambdaQueryWrapper<SysInterface> sysInterfaceLambdaQueryWrapper = Wrappers.lambdaQuery(SysInterface.class);
        sysInterfaceLambdaQueryWrapper.orderByAsc(SysInterface::getId);
        Page<SysInterface> sysInterfacePage = sysInterfaceMapper.selectPage(new Page<SysInterface>(page, pageSize), sysInterfaceLambdaQueryWrapper);
        // 返回数据
        return sysInterfacePage;
    }

    /**
     * 修改权限基本信息
     *
     * @param sysInterface
     * @return
     */
    @Override
    public void set(SysInterface sysInterface) {
        if (sysInterfaceMapper.updateById(sysInterface) <= 0) {
            throw new CommonException("修改权限基本信息 -->", "网络异常,请稍后再试");
        }
    }
}