package com.example.system.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.system.pojo.SellerLogistics;

public interface SellerLogisticsService {

    /**
     * 物流信息列表
     *
     * @param page
     * @param pageSize
     * @return
     */
    Page<SellerLogistics> getPage(Long page, Long pageSize);
}