package com.example.system.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.system.pojo.BuyerCart;

public interface BuyerCartService {

    /**
     * 用户购物车列表
     *
     * @param page
     * @param pageSize
     * @return
     */
    Page<BuyerCart> getList(Long page, Long pageSize);
}