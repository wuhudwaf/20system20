package com.example.system.service;

import com.example.system.pojo.SysCategory;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public interface SysCategoryService {

    /**
     * 添加商品类别
     *
     * @param sysCategory
     * @return
     */
    void add(SysCategory sysCategory);

    /**
     * 废弃分类
     *
     * @param cateId
     * @return
     */
    void delete(String cateId);

    /**
     * 修改分类
     *
     * @param sysCategory
     * @return
     */
    void set(SysCategory sysCategory);

    /**
     * 分类详情
     *
     * @param cateId
     * @return
     */
    SysCategory getByCateId(String cateId);

    /**
     * 查询分类列表
     * @return
     */
    List<SysCategory> getList();

    /**
     * 查询子分类（没有被废弃的）
     *
     * @param cateId
     * @return
     */
    List<SysCategory> getList(List<SysCategory> sysCategories, String cateId);

    /**
     * 查询根节点分类
     *
     * @return
     */
    List<SysCategory> getRoot();
}