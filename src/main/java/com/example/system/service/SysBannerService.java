package com.example.system.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.system.pojo.SysBanner;

import javax.servlet.http.HttpServletRequest;

public interface SysBannerService {

    /**
     * 新增轮播图
     *
     * @param sysBanner
     * @return
     */
    void add( SysBanner sysBanner);

    /**
     * 删除轮播图
     *
     * @param bannerId
     * @return
     */
    void delete(String bannerId);

    /**
     * 修改轮播图
     *
     * @param sysBanner
     * @return
     */
    void setSysBanner(SysBanner sysBanner);

    /**
     * 查看轮播图详情
     *
     * @param bannerId
     * @return
     */
    SysBanner getSysBanner(String bannerId);

    /**
     * 获取轮播图列表
     *
     * @param page
     * @param pageSize
     * @return
     */
    Page<SysBanner> getSysBannerPage(Long page, Long pageSize);
}