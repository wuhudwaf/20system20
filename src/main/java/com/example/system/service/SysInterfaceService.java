package com.example.system.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.system.pojo.SysInterface;

public interface SysInterfaceService {

    /**
     * 新增权限
     *
     * @param sysInterface
     * @return
     */
    void add(SysInterface sysInterface);

    /**
     * 删除权限
     *
     * @param id
     * @return
     */
    void delete(String id);

    /**
     * 查看权限详情
     *
     * @param id
     * @return
     */
    SysInterface getById(String id);

    /**
     * 查询所有权限
     *
     * @param page
     * @param pageSize
     * @return
     */
    Page<SysInterface> getPage(Long page, Long pageSize);

    /**
     * 修改权限基本信息
     *
     * @param sysInterface
     * @return
     */
    void set(SysInterface sysInterface);
}