package com.example.system.service;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.system.pojo.SellerOrderItem;

public interface SellerOrderItemService {

    /**
     * 商品订单列表
     *
     * @param page
     * @param pageSize
     * @return
     */
    Page<SellerOrderItem> getPage(Long page, Long pageSize);
}