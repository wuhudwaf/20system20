package com.example.system.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.system.dto.ReturnGoodsFlashSale;
import com.example.system.pojo.vo.GoodsFlashSaleVo;

public interface GoodsFlashSaleService {

    /**
     * 新增秒杀活动
     *
     * @param returnGoodsFlashSale
     * @return
     */
    void addGoodsFlashSale(ReturnGoodsFlashSale returnGoodsFlashSale);

    /**
     * 修改商品限时秒杀活动接口
     *
     * @param returnGoodsFlashSale
     * @return
     */
    void setGoodsFlashSale(ReturnGoodsFlashSale returnGoodsFlashSale);

    /**
     * 根据秒杀id看商品限时秒杀活动接口详情
     *
     * @param flashSaleId
     * @return
     */
    GoodsFlashSaleVo getGoodsFlashSale(String flashSaleId);

    /**
     * 开始秒杀/结束秒杀商品限时秒杀
     *
     * @param strFlashSaleId
     */
    String setStatus(String strFlashSaleId);

    /**
     * 删除商品限时秒杀
     *
     * @param strFlashSaleId
     * @return
     */
    void deleteGoodsFlashSale(String strFlashSaleId);

    /**
     * 获取商品限时秒杀列表
     *
     * @param page
     * @param pageSize
     * @return
     */
    Page<GoodsFlashSaleVo> getGoodsFlashSalePage(Long page, Long pageSize);
}