package com.example.system.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.system.pojo.BuyerAddress;
import com.example.system.pojo.vo.BuyerAddressVo;

public interface BuyerAddressService {

    /**
     * 获取某位用户的地址信息列表
     *
     * @param userId
     * @param page
     * @param pageSize
     * @return
     */
    Page<BuyerAddressVo> getList(String userId, Long page, Long pageSize);
}