package com.example.system.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.system.pojo.BuyerAddress;
import com.example.system.pojo.BuyerUser;
import com.example.system.pojo.where.BuyerUserQuery;

import javax.servlet.http.HttpServletResponse;
import java.text.ParseException;
import java.util.List;

public interface BuyerUserService {

    /**
     * 查询微信用户信息
     *
     * @return
     */
    Page<BuyerUser> getPage(BuyerUserQuery buyerUser) throws ParseException;

    /**
     * 用户id获取用户地址列表
     *
     * @param userId
     * @return
     */
    List<BuyerAddress> userIdGetAddressList(String userId);

    /**
     * 导出全部用户
     *
     * @param response
     * @return
     */
    List<BuyerUser> autoGetBuyerUserList(HttpServletResponse response);

    /**
     * 导出用户(分页)
     *
     * @param pageOne
     * @param pageTwo
     * @return
     */
    List<BuyerUser> diyGetBuyerUserPage(Integer pageOne, Integer pageTwo, Integer pageSize);

    /**
     * 修改微信用户推荐人
     *
     * @param userId
     * @param inviteUserId
     * @return
     */
    void userIdSetInviteUserId(String userId,String inviteUserId);

    /**
     * 封号(微信用户)
     *
     * @param userId
     * @return
     */
    void userIdSetStatus(String userId);

    /**
     * 删除(微信用户)
     *
     * @param userId
     * @return
     */
    void userIdDeleteBuyerUser(String userId);

    /**
     * 用户名称列表(模糊查询)
     *
     * @param userName
     * @return
     */
    List<BuyerUser> userNameGetList(String userName);

    /**
     * 根据用户id查询用户详情
     *
     * @param userId
     * @return
     */
    BuyerUser userIdGetBuyerUser(String userId);
}