package com.example.system.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.system.pojo.BuyerCoupon;

public interface BuyerCouponService {
    /**
     * 用户优惠券列表
     *
     * @param page
     * @param pageSize
     * @return
     */
    Page<BuyerCoupon> getList(Long page, Long pageSize);
}
