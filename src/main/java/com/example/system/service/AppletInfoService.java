package com.example.system.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.system.pojo.AppletInfo;
import com.example.system.pojo.vo.AppletInfoVo;

public interface AppletInfoService {

    /**
     * 新增小程序信息
     *
     * @param appletInfo
     * @return
     */
    void addAppletInfo(AppletInfo appletInfo);

    /**
     * 删除小程序信息
     *
     * @param infoId
     * @return
     */
    void deleteAppletInfo(String infoId);

    /**
     * 修改小程序信息
     *
     * @param appletInfo
     * @return
     */
    void updateAppletInfo(AppletInfo appletInfo);

    /**
     * 查看小程序信息详情
     *
     * @param infoId
     * @return
     */
    AppletInfoVo getAppletInfo(String infoId);

    /**
     * 查看小程序信息列表
     *
     * @param page
     * @param pageSize
     * @return
     */
    Page<AppletInfoVo> getAppletInfoPage(Long page, Long pageSize);
}