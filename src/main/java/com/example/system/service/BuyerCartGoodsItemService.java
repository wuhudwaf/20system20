package com.example.system.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.system.pojo.BuyerCartGoodsItem;

public interface BuyerCartGoodsItemService {

    /**
     * 购物车商品信息列表
     *
     * @param page
     * @param pageSize
     * @return
     */
    Page<BuyerCartGoodsItem> getList(Long page, Long pageSize);
}