package com.example.system.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.system.pojo.OrderReviews;

public interface OrderReviewsService {

    /**
     * 订单评价列表
     *
     * @param page
     * @param pageSize
     * @return
     */
    Page<OrderReviews> getPage(Long page, Long pageSize);
}