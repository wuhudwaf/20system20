package com.example.system.interceotor;

import cn.hutool.json.JSONUtil;
import com.example.system.dto.UserDto;
import com.example.system.exception.CommonException;
import com.example.system.utils.RedisUtils;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static com.example.system.utils.Constants.*;


/**
 * 拦截器，拦截需要登录的路径
 */
@SuppressWarnings({"all"})
@Slf4j
@Component
public class LoginInterceptor implements HandlerInterceptor {

    @Autowired
    private RedisUtils redisUtils;

    /**
     * 前置拦截
     *
     * @param request
     * @param response
     * @param handler
     * @return
     * @throws Exception
     */
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        // 获取令牌
        String token = request.getHeader("token");
        if (token == null) {
            // 未登录
            log.info(request.getRequestURI());
            throw new CommonException("拦截器 --> token = null", "未登录");
        }
        // 获取登录数据
        String userStr = redisUtils.getStr(token);
        if (userStr == null) {
            // 令牌已过期，请重新登录
            throw new CommonException("拦截器 --> map = null", "登录已过期，请重新登录");
        }
        UserDto userDto = JSONUtil.toBean(userStr, UserDto.class);
        // 刷新过期时间，30天
        redisUtils.setKeyTime(token, THIRTY_DAYS_TIME, TimeUnit.DAYS);
        // 拿到当前用户的权限
        List<String> urls = userDto.getUrl();
        // 获取当前访问接口
        String requestURI = request.getRequestURI();
        // 获取当前访问抽象接口
        String thisUrl = requestURI.substring(0, requestURI.indexOf("/", requestURI.indexOf("/") + 1)) + THIS_URL;
        // 权限校验
        for (String url : urls) {
            if (thisUrl.equals(url) || url.equals(ADMAIN_URL)) {
                return true;
            }
        }
        // 权限不足
        throw new CommonException("拦截器", "权限不足，无法访问");
    }
}