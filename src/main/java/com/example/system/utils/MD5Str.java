package com.example.system.utils;

import org.springframework.stereotype.Component;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import static com.example.system.utils.Constants.MD5_SIXTEEN;
import static com.example.system.utils.Constants.MD5_THIRTY_TWO;


@Component
@SuppressWarnings("All")
public class MD5Str {

    /**
     * md5加密
     * 16 32随意切换
     * 小写
     *
     * @param sourceStr
     * @return
     */
    public static String MD5(String sourceStr, String number) {
        String result = "";
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(sourceStr.getBytes());
            byte b[] = md.digest();
            int i;
            StringBuffer buf = new StringBuffer("");
            for (int offset = 0; offset < b.length; offset++) {
                i = b[offset];
                if (i < 0)
                    i += 256;
                if (i < 16)
                    buf.append("0");
                buf.append(Integer.toHexString(i));//转换成16进制编码
            }
            if (number.equals(MD5_SIXTEEN)) {
                // 16位
                result = buf.toString().substring(8, 24);
            }
            if (number.equals(MD5_THIRTY_TWO)) {
                // 32位
                result = buf.toString();
            }
        } catch (NoSuchAlgorithmException e) {
            System.out.println(e);
        }
        return result;
    }
}