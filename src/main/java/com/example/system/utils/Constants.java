package com.example.system.utils;


/**
 * 辅助工具类
 */
@SuppressWarnings("all")
public interface Constants {

    /**
     * 普通商品,key: sys:goods:page:page+pageSize
     */
    String GOODS_PAGE = "sys:goods:page:";

    /**
     * 普通商品,key: sys:goods:id
     */
    String GOODS_ID = "sys:goods:";

    /**
     * 商品sku
     */
    String GOODS_SKU_PAGE = "goods:sku:page:";

    /**
     * 商品sku
     */
    String GOODS_SKU_ID = "goods:sku:id:";

    /**
     * 商品分类key，value：sys:category:page:page+pageSize
     */
    String SYS_CATEGORY_PAGE = "sys:category:page:";

    /**
     * 商品分类key，value：sys:category:id
     */
    String SYS_CATEGORY_ID = "sys:category:";

    /**
     * 优惠券keu，sys:coupon:id
     */
    String COUPON_ID = "sys:coupon:";

    /**
     * 优惠券redis keu，value为page
     */
    String COUPON_PAGE = "sys:coupon:";

    /**
     * 权限集合，作为key，value是page
     */
    String INTERFACE_LIST = "buyer:user:";

    /**
     * 接口权限存入redis的key：buyer:user:id
     */
    String INTERFACE_KEY = "buyer:user:";

    /**
     * 门店管理集合，作为key，value是page
     */
    String SHOP_ID_LIST = "common:sys:user：";

    /**
     * 门店员工集合，作为key，value为page
     */
    String SELLER_USER = "shop:seller:user:";

    /**
     * 角色规则集合，作为key
     */
    String ROLE_LIST = "sys:role:list:";

    /**
     * 会员规则集合，作为key，page对象为value
     */
    String MEMBERSHIP_LIST = "buyer:membership:rule:list:";

    /**
     * 通知公告集合，作为key，page对象为value
     */
    String NOTICE_LIST = "sys:notice:list:";

    /**
     * 门店集合，作为key，page对象为value
     */
    String SHOP_LIST = "sys:shop:list:";

    /**
     * 门店，缓存key：sys:shop:id
     */
    String SHOP_ = "sys:shop:";

    /**
     * 通知公告,缓存key: sys:notice:id
     */
    String NOTICE_ANNOUNCEMENT = "sys:notice:";

    /**
     * 表示超级管理员id
     */
    Long ONE = 1L;

    /**
     * token
     * 最终拼接：TOKEN+UUID
     */
    String TOKEN = "token_";

    /**
     * 会员规则，存储缓存key：buyer:membership:rule:id
     */
    String BMR = "buyer:membership:rule:";

    /**
     * 门店员工，存储缓存key：seller:user:id
     * Str存储
     */
    String USER_ = "seller:user:";

    /**
     * 角色，存储缓存：sys:role:id
     * Str存储
     */
    String ROLE_ = "sys:role:";

    /**
     * 权限，存储缓存：sys:interface:id
     */
    String INTERFACE_ = "sys:interface:";

    /**
     * 三十天过期时间
     */
    Long THIRTY_DAYS_TIME = 30L;

    /**
     * 30秒过期时间
     */
    Long THIRTY_SECONDS_TIME = 30L;

    /**
     * 30分钟过期时间
     */
    Long THIRTY_MINUTES_TIME = 30L;

    /**
     * 5分钟过期时间
     */
    Long FIVE_MINUTES_TIME = 5L;

    /**
     * 接口地址/权限
     */
    String URL = "URL_";


    /**
     * 生成16位MD5
     */
    String MD5_SIXTEEN = "MD5_SIXTEEN";

    /**
     * 生成32位MD5
     */
    String MD5_THIRTY_TWO = "MD5_THIRTY_TWO";

    /**
     * 门店管理员id
     */
    Long SHOP_ID = 5L;

    /**
     * 超级管理员id
     */
    Long ADMAIN_ID = 1L;

    /**
     * 超级管理员url
     */
    String ADMAIN_URL = "/**";

    /**
     * 拦截器获取当前接口路径专用
     */
    String THIS_URL = "/**";
}