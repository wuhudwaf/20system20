package com.example.system.utils;

import com.example.system.exception.CommonException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * redis操作工具类
 */
@Component
@SuppressWarnings("all")
public class RedisUtils {

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    /**
     * 添加
     *
     * @param key
     * @param value
     */
    public void addStr(String key, String value) {
        try {
            stringRedisTemplate.opsForValue().set(key, value);
        } catch (Exception e) {
            throw new CommonException("RedisUtils.class --> addStr(String key, String value)", "网络异常，请稍后再试");
        }
    }

    /**
     * 添加
     *
     * <p>
     * 自定义过期时间
     * TimeUnit.DAYS          日的工具类
     * TimeUnit.HOURS         时的工具类
     * TimeUnit.MINUTES       分的工具类
     * TimeUnit.SECONDS       秒的工具类
     * TimeUnit.MILLISECONDS  毫秒的工具类
     *
     * @param key
     * @param value
     * @param time
     * @param timeUnit
     */
    public void addStr(String key, String value, Long time, TimeUnit timeUnit) {
        try {
            stringRedisTemplate.opsForValue().set(key, value, time, timeUnit);
        } catch (Exception e) {
            throw new CommonException("RedisUtils.class --> " +
                    "addStr(String key, String value, String time, TimeUnit timeUnit)", "网络异常，请稍后再试");
        }
    }

    /**
     * 添加
     *
     * @param key
     * @param value
     */
    public void addMap(String key, Map value) {
        try {
            stringRedisTemplate.opsForHash().putAll(key, value);
        } catch (Exception e) {
            throw new CommonException("RedisUtils.class --> " +
                    "addMap(String key, Map value)", "网络异常，请稍后再试");
        }
    }

    /**
     * 获取value
     *
     * @param key
     * @return
     */
    public Map<Object, Object> getMap(String key) {
        try {
            Map<Object, Object> entries = stringRedisTemplate.opsForHash().entries(key);
            return entries;
        } catch (Exception e) {
            throw new CommonException("RedisUtils.class --> " +
                    "getMap(String key)", "网络异常，请稍后再试");
        }
    }

    /**
     * 获取value
     *
     * @param key
     * @return
     */
    public String getStr(String key) {
        try {
            String s = stringRedisTemplate.opsForValue().get(key);
            return s;
        } catch (Exception e) {
            throw new CommonException("RedisUtils.class --> " +
                    "getStr(String key)", "网络异常，请稍后再试");
        }
    }

    /**
     * 删除
     *
     * @param key
     */
    public void remove(String key) {
        try {
            stringRedisTemplate.delete(key);
        } catch (Exception e) {
            throw new CommonException("RedisUtils.class --> " +
                    "remove(String key)", "网络异常，请稍后再试");
        }
    }

    /**
     * 设置过期时间
     *
     * @param key
     * @param time
     * @param timeUnit
     */
    public void setKeyTime(String key, Long time, TimeUnit timeUnit) {
        stringRedisTemplate.expire(key, time, timeUnit);
    }
}