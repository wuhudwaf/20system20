package com.example.system.utils.hb;

import cn.hutool.core.util.StrUtil;
import com.example.system.exception.CommonException;
import com.example.system.mapper.SysGoodsMapper;
import com.example.system.pojo.GoodsSku;
import com.example.system.pojo.vo.SysGoodsGoodsSkuVo;
import com.example.system.utils.RedisIdWorker;
import com.freewayso.image.combiner.ImageCombiner;
import com.freewayso.image.combiner.element.TextElement;
import com.freewayso.image.combiner.enums.OutputFormat;
import com.freewayso.image.combiner.enums.ZoomMode;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.awt.*;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * 商品海报生成
 */
@Component
@SuppressWarnings("all")
@RequiredArgsConstructor
public class HbUtils {

    @Value("${bjt.bjts}")
    private String baseFilePath;

    private final static String BGT_INMAGE = "bgt/";

    /**
     * 拿到商品和商品sku
     *
     * @param sysGoodsMapper
     * @param goodsId
     * @return
     */
    public SysGoodsGoodsSkuVo getSysGoodsGoodsSkuVo(SysGoodsMapper sysGoodsMapper, Long goodsId) {
        return sysGoodsMapper.getSysGoodsGoodsSkuVo(goodsId);
    }

    /**
     * 商品海报生成
     *
     * @param goodsId
     * @throws Exception
     */
    public String addImages(Long goodsId, RedisIdWorker redisIdWorker, SysGoodsMapper sysGoodsMapper) {
        // 拿到商品和商品sku
        SysGoodsGoodsSkuVo sysGoodsGoodsSkuVo = getSysGoodsGoodsSkuVo(sysGoodsMapper, goodsId);
        if (sysGoodsGoodsSkuVo == null) {
            throw new CommonException("商品海报生成", "商品不存在");
        }
        // 有的话直接返回
        if (StrUtil.isNotBlank(sysGoodsGoodsSkuVo.getPoster())) {
            return sysGoodsGoodsSkuVo.getPoster();
        }
        List<GoodsSku> orderReviews = sysGoodsGoodsSkuVo.getOrderReviews();
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder
                .getRequestAttributes()).getRequest();
        // 原价
        String oldPrice = "";
        // 现价
        String price = "";
        if (orderReviews.size() != 0) {
            GoodsSku goodsSku = orderReviews.get(0);
            oldPrice = goodsSku.getOldPrice();
            price = goodsSku.getPrice().toString();
        }
        //合成器（指定背景图和输出格式，整个图片的宽高和相关计算依赖于背景图，所以背景图的大小是个基准）
        ImageCombiner combiner = null;
        try {
            combiner = new ImageCombiner("http://admin.gz20wine.com:8081/bjt/true.jpg", OutputFormat.JPG);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        //二维码（file协议）
        String qrCodeUrl = "http://admin.gz20wine.com:8081/bjt/36940066ff196785773634369b4290c8_r.png";
        // 商品图片
        String i = sysGoodsGoodsSkuVo.getPicture();
        //加图片元素
        combiner.addImageElement(i, 130, 100, 500, 0, ZoomMode.Width);
        //加文本元素
        combiner.addTextElement(sysGoodsGoodsSkuVo.getName(), 60, 220, 625);
        // 原价
        TextElement textPrice = new TextElement("原价：" + oldPrice, 60, 270, 890);
        textPrice.setColor(Color.gray);          //灰色
        textPrice.setStrikeThrough(true);       //删除线
        combiner.addElement(textPrice);         //加入待绘制集合
        // 现价
        TextElement textPriceNow = new TextElement("现价：" + price, 50, 350, 1000);
        textPriceNow.setColor(Color.red);          //黑色
        combiner.addElement(textPriceNow);         //加入待绘制集合
        combiner.addImageElement(qrCodeUrl, 80, 900, 186, 180, ZoomMode.WidthHeight);
        //执行图片合并
        try {
            combiner.combine();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        // 创建日期文件夹
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd/");
        String format = sdf.format(new Date());
        File file = new File(baseFilePath + "/" + format + "/");
        if (!file.isDirectory()) {
            //递归生成文件夹
            file.mkdirs();
        }
        // 海报名称
        String newName = UUID.randomUUID().toString() + redisIdWorker.nextId("image") + ".jpg";
        File newFile = new File(file.getAbsolutePath() + File.separator + newName);
        try {
            combiner.save(newFile.getPath());
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        // filePath最终名称，存储到商品对象中
        String filePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + "/" + BGT_INMAGE + format + newName;
        // 更新商品的海报
        sysGoodsMapper.updateByPoster(goodsId, filePath);
        // 最终返回图片访问地址
        return filePath;
    }
}