package com.example.system.utils;

import org.springframework.stereotype.Component;

/**
 * 字符串处理工具类
 */
@Component
@SuppressWarnings("all")
public class StringSet {


    /**
     * 用来修改手机号的
     * 比如: 175****7098
     *
     * @param phone
     * @return
     */
    public static String strSet(String phone) {
        String str = phone;
        String ch = "****";
        int pos = 3;
        return str.substring(0, 3) + ch + str.substring(pos + 4);
    }
}
