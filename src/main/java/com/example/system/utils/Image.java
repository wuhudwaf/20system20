package com.example.system.utils;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Image {
    // 图片名称
    private String name;
    // 磁盘路径+名称
    private String imageAddress;
}
