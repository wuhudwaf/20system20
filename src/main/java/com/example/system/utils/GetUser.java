package com.example.system.utils;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.extra.spring.SpringUtil;
import cn.hutool.json.JSONUtil;
import com.example.system.dto.SysUserDto;
import com.example.system.dto.UserDto;
import com.example.system.exception.CommonException;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;

@SuppressWarnings("all")
public class GetUser {

    /**
     * 推荐这种方式获取bean，不要使用@Autowired了,工具类中使用
     * service实现类 和 controller 中推荐使用 @RequiredArgsConstructor注解 + final
     * 总之，不推荐使用@Autowired，网上查询说是@Autowired会空指针，
     */

    /**
     * 获取用户id
     *
     * @return
     */
    public static String getUserId() {

        HttpServletRequest httpServletRequest = ((ServletRequestAttributes) RequestContextHolder
                .getRequestAttributes()).getRequest();

        RedisUtils redisUtils = SpringUtil.getBean(RedisUtils.class);
        String token = httpServletRequest.getHeader("token");
        // 获取登录数据
        String userStr = redisUtils.getStr(token);
        if (userStr == null) throw new CommonException("获取用户id", "未登录");
        UserDto userDto = JSONUtil.toBean(userStr, UserDto.class);
        return userDto.getId().toString();
    }

    /**
     * 获取用户对象
     *
     * @return
     */
    public static SysUserDto getSysUserDto() {
        HttpServletRequest httpServletRequest = ((ServletRequestAttributes) RequestContextHolder
                .getRequestAttributes()).getRequest();

        RedisUtils redisUtils = SpringUtil.getBean(RedisUtils.class);
        String token = httpServletRequest.getHeader("token");
        // 获取登录数据
        String userStr = redisUtils.getStr(token);
        if (userStr == null) throw new CommonException("获取用户id", "未登录");
        UserDto userDto = JSONUtil.toBean(userStr, UserDto.class);
        return BeanUtil.copyProperties(userDto, SysUserDto.class);
    }

    /**
     * 获取用户对象
     *
     * @return
     */
    public static UserDto getUserDto() {
        HttpServletRequest httpServletRequest = ((ServletRequestAttributes) RequestContextHolder
                .getRequestAttributes()).getRequest();

        RedisUtils redisUtils = SpringUtil.getBean(RedisUtils.class);
        String token = httpServletRequest.getHeader("token");
        // 获取登录数据
        String userStr = redisUtils.getStr(token);
        if (userStr == null) throw new CommonException("获取用户id", "未登录");
        return JSONUtil.toBean(userStr, UserDto.class);
    }
}