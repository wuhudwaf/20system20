package com.example.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.system.pojo.SysUserRoleRelationship;
import org.apache.ibatis.annotations.Select;

@SuppressWarnings("all")
public interface SysUserRoleRelationshipMapper extends BaseMapper<SysUserRoleRelationship> {

    /**
     * 管理员 id拿到角色对象
     *
     * @param userId
     * @return
     */
    @Select("select * from sys_user_role_relationship where user_id = #{userId}")
    SysUserRoleRelationship getByUserId(String userId);
}