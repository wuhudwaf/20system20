package com.example.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.system.pojo.SysInterface;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@SuppressWarnings("all")
public interface SysInterfaceMapper extends BaseMapper<SysInterface> {
    /**
     * 拿到所有权限
     *
     * @return
     */
    @Select("select * from sys_interface")
    List<SysInterface> getList();
}