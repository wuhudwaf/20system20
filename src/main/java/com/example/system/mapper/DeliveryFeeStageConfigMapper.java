package com.example.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.system.pojo.DeliveryFeeStageConfig;

@SuppressWarnings("all")
public interface DeliveryFeeStageConfigMapper extends BaseMapper<DeliveryFeeStageConfig> {
}