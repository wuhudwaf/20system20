package com.example.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.system.pojo.GoodsSku;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@SuppressWarnings("all")
public interface GoodsSkuMapper extends BaseMapper<GoodsSku> {

    /**
     * id查询
     *
     * @param skuId
     * @return
     */
    @Select("select * from goods_sku where sku_id = #{skuId}")
    GoodsSku getByskuId(String skuId);

    /**
     * 批量插入
     *
     * @param entityList
     * @return
     */
    int insertBatchSomeColumn(List<GoodsSku> entityList);
}