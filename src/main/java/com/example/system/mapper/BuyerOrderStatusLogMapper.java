package com.example.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.system.pojo.BuyerOrderStatusLog;

@SuppressWarnings("all")
public interface BuyerOrderStatusLogMapper extends BaseMapper<BuyerOrderStatusLog> {
}