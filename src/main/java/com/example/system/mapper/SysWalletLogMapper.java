package com.example.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.system.pojo.SysWalletLog;

@SuppressWarnings("all")
public interface SysWalletLogMapper extends BaseMapper<SysWalletLog> {
}