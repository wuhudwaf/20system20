package com.example.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.system.pojo.SysUser;

public interface SysUserMapper extends BaseMapper<SysUser> {
}
