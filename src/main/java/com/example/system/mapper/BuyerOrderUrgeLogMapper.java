package com.example.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.system.pojo.BuyerOrderUrgeLog;

@SuppressWarnings("all")
public interface BuyerOrderUrgeLogMapper extends BaseMapper<BuyerOrderUrgeLog> {
}