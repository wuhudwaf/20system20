package com.example.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.system.pojo.SysBanner;

@SuppressWarnings("all")
public interface SysBannerMapper extends BaseMapper<SysBanner> {
}