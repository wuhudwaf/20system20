package com.example.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.system.pojo.SellerInventory;
import com.example.system.pojo.SysGoods;

import java.util.List;

@SuppressWarnings("all")
public interface SellerInventoryMapper extends BaseMapper<SellerInventory> {

    /**
     * 批量插入
     *
     * @param entityList
     * @return
     */
    int insertBatchSomeColumn(List<SellerInventory> entityList);
}