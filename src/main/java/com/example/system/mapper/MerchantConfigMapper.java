package com.example.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.system.pojo.MerchantConfig;

@SuppressWarnings("all")
public interface MerchantConfigMapper extends BaseMapper<MerchantConfig> {
}