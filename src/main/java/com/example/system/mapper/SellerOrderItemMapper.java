package com.example.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.system.pojo.SellerOrderItem;

@SuppressWarnings("all")
public interface SellerOrderItemMapper extends BaseMapper<SellerOrderItem> {
}