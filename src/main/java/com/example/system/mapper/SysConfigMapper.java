package com.example.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.system.pojo.SysConfig;

@SuppressWarnings("all")
public interface SysConfigMapper extends BaseMapper<SysConfig> {
}