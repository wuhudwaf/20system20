package com.example.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.system.pojo.SysNotice;

@SuppressWarnings("all")
public interface SysNoticeMapper extends BaseMapper<SysNotice> {
}
