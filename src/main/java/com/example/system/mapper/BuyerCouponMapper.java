package com.example.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.system.pojo.BuyerCoupon;

@SuppressWarnings("all")
public interface BuyerCouponMapper extends BaseMapper<BuyerCoupon> {
}