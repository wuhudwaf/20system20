package com.example.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.system.pojo.BuyerOrder;

@SuppressWarnings("all")
public interface BuyerOrderMapper extends BaseMapper<BuyerOrder> {
}