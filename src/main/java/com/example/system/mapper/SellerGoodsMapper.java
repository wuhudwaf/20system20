package com.example.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.system.pojo.SellerGoods;

@SuppressWarnings("all")
public interface SellerGoodsMapper extends BaseMapper<SellerGoods> {
}