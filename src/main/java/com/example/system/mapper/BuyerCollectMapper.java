package com.example.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.system.pojo.BuyerCollect;

@SuppressWarnings("all")
public interface BuyerCollectMapper extends BaseMapper<BuyerCollect> {
}