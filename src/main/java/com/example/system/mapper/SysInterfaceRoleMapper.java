package com.example.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.system.pojo.GoodsSku;
import com.example.system.pojo.SysInterfaceRole;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@SuppressWarnings("all")
public interface SysInterfaceRoleMapper extends BaseMapper<SysInterfaceRole> {


    /**
     * 角色 id获取对应权限对象
     *
     * @param roleId
     * @return
     */
    @Select("select * from sys_interface_role where role_id = #{roleId}")
    List<SysInterfaceRole> getByRoleId(String roleId);

    /**
     * 删除角色 id对应权限对象
     *
     * @param id
     */
    @Delete("select * from sys_interface_role where role_id = #{id}")
    int deleteByRoleId(String id);

    /**
     * 获取所有角色权限关系
     *
     * @return
     */
    @Select("select * from sys_interface_role")
    List<SysInterfaceRole> getList();

    int insertBatchSomeColumn(List<GoodsSku> entityList);
}