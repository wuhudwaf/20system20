package com.example.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.system.pojo.BuyerMembershipRule;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Select;

@SuppressWarnings("all")
public interface BuyerMembershipRuleMapper extends BaseMapper<BuyerMembershipRule> {

    /**
     * 根据 ruleId查询
     *
     * @param ruleId
     * @return
     */
    @Select("select * from buyer_membership_rule where rule_id = #{ruleId}")
    BuyerMembershipRule getByRuleId(String ruleId);

    /**
     * 根据ruleId删除
     *
     * @param ruleId
     * @return
     */
    @Delete("delete from buyer_membership_rule where rule_id = #{ruleId}")
    int deleteByRuleId(String ruleId);
}