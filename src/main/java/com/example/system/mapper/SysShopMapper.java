package com.example.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.system.pojo.GoodsSku;
import com.example.system.pojo.SysShop;
import com.example.system.pojo.vo.add.SysShopAdd;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

@SuppressWarnings("all")
public interface SysShopMapper extends BaseMapper<SysShop> {


    /**
     * 根据门店id查询
     *
     * @param shopId
     * @return
     */
    @Select("select * from sys_shop where shop_id = #{shopId}")
    SysShop getByShopId(String shopId);

    /**
     * 根据门店id删除
     *
     * @param shopId
     * @return
     */
    @Delete("delete from sys_shop where shop_id = #{shopId} and revision = #{revision}")
    int deleteByShopIdSysShop(String shopId, Integer revision);

    /**
     * 批量插入
     *
     * @param entityList
     * @return
     */
    int insertBatchSomeColumn(List<SysShopAdd> entityList);
}