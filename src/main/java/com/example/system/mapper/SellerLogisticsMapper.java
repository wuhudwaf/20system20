package com.example.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.system.pojo.SellerLogistics;

@SuppressWarnings("all")
public interface SellerLogisticsMapper extends BaseMapper<SellerLogistics> {
}