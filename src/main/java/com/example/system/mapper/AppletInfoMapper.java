package com.example.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.system.pojo.AppletInfo;

@SuppressWarnings("all")
public interface AppletInfoMapper extends BaseMapper<AppletInfo> {
}