package com.example.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.system.pojo.GoodsFlashSaleItem;
import com.example.system.pojo.GoodsSku;
import org.apache.ibatis.annotations.Delete;

import java.util.List;

@SuppressWarnings("all")
public interface GoodsFlashSaleItemMapper extends BaseMapper<GoodsFlashSaleItem> {

    /**
     * 删除 flash_sale_id
     *
     * @param flashSaleId
     * @return
     */
    @Delete("delete from goods_flash_sale_item where flash_sale_id = #{flashSaleId}")
    int deletByFlashSaleId(String flashSaleId);


    /**
     * 批量插入
     *
     * @param entityList
     * @return
     */
    int insertBatchSomeColumn(List<GoodsFlashSaleItem> entityList);
}