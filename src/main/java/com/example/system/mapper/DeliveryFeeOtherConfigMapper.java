package com.example.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.system.pojo.DeliveryFeeOtherConfig;

@SuppressWarnings("all")
public interface DeliveryFeeOtherConfigMapper extends BaseMapper<DeliveryFeeOtherConfig> {
}