package com.example.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.system.pojo.OrderReviews;

@SuppressWarnings("all")
public interface OrderReviewsMapper extends BaseMapper<OrderReviews> {
}