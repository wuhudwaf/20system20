package com.example.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.system.pojo.SysService;

@SuppressWarnings("all")
public interface SysServiceMapper extends BaseMapper<SysService> {
}