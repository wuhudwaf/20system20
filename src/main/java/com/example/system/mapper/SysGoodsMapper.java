package com.example.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.system.pojo.OrderReviews;
import com.example.system.pojo.SysGoods;
import com.example.system.pojo.page.NewPage;
import com.example.system.pojo.vo.SysGoodsGoodsSkuVo;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

@SuppressWarnings("all")
public interface SysGoodsMapper extends BaseMapper<SysGoods> {

    /**
     * 批量插入
     *
     * @param entityList
     * @return
     */
    int insertBatchSomeColumn(List<SysGoods> entityList);

    /**
     * 商品评价
     *
     * @return
     */
    List<OrderReviews> getGoodsEvaluate(NewPage newPage);

    /**
     * 拿到商品和商品sku
     *
     * @param goodsId
     * @return
     */
//    @Select("select s.poster,s.picture,s.name,g.old_price,g.price from sys_goods s join goods_sku g on g.goods_id = #{goodsId} and s.goods_id = #{goodsId}")
    SysGoodsGoodsSkuVo getSysGoodsGoodsSkuVo(Long goodsId);

    /**
     * 更新商品海报
     *
     * @param goodsId
     * @param filePath
     */
    @Update("update sys_goods set poster = #{filePath} where goods_id = #{goodsId}")
    void updateByPoster(Long goodsId, String filePath);
}