package com.example.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.system.pojo.BuyerInvite;

@SuppressWarnings("all")
public interface BuyerInviteMapper extends BaseMapper<BuyerInvite> {
}