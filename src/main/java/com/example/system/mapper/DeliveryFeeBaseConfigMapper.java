package com.example.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.system.pojo.DeliveryFeeBaseConfig;

@SuppressWarnings("all")
public interface DeliveryFeeBaseConfigMapper extends BaseMapper<DeliveryFeeBaseConfig> {
}