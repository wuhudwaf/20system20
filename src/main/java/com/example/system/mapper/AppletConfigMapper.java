package com.example.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.system.pojo.AppletConfig;

@SuppressWarnings("all")
public interface AppletConfigMapper extends BaseMapper<AppletConfig> {
}