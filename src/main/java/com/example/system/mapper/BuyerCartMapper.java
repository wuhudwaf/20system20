package com.example.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.system.pojo.BuyerCart;

@SuppressWarnings("all")
public interface BuyerCartMapper extends BaseMapper<BuyerCart> {
}