package com.example.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.system.pojo.BuyerCartGoodsItem;

@SuppressWarnings("all")
public interface BuyerCartGoodsItemMapper extends BaseMapper<BuyerCartGoodsItem> {
}