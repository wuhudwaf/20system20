package com.example.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.system.pojo.BuyerAddress;

@SuppressWarnings("all")
public interface BuyerAddressMapper extends BaseMapper<BuyerAddress> {
}