package com.example.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.system.pojo.BuyerUser;

@SuppressWarnings("all")
public interface BuyerUserMapper extends BaseMapper<BuyerUser> {
}