package com.example.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.system.pojo.GoodsFlashSale;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * 商品限时秒杀
 */
@SuppressWarnings("all")
public interface GoodsFlashSaleMapper extends BaseMapper<GoodsFlashSale> {

    /**
     * 删除 flash_sale_id
     *
     * @param flashSaleId
     * @return
     */
    @Delete("delete from goods_flash_sale where flash_sale_id = #{flashSaleId}")
    int delete(String flashSaleId);
}