package com.example.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.system.pojo.SysCash;

@SuppressWarnings("all")
public interface SysCashMapper extends BaseMapper<SysCash> {
}