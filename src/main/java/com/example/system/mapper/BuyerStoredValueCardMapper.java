package com.example.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.system.pojo.BuyerStoredValueCard;

@SuppressWarnings("all")
public interface BuyerStoredValueCardMapper extends BaseMapper<BuyerStoredValueCard> {
}