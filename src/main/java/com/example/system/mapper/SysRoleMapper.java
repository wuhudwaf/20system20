package com.example.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.system.pojo.SysRole;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@SuppressWarnings("all")
public interface SysRoleMapper extends BaseMapper<SysRole> {


    /**
     * 查询角色
     *
     * @return
     */
    @Select("select * from sys_role")
    List<SysRole> getList();
}