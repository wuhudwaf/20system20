package com.example.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.system.pojo.BuyerScore;
import com.example.system.pojo.page.NewPage;
import com.example.system.pojo.vo.BuyerUserBuyerScoreVo;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@SuppressWarnings("all")
public interface BuyerScoreMapper extends BaseMapper<BuyerScore> {

    /**
     * 积分明细
     *
     * @param newPage
     * @return
     */
    @Select("select u.user_name,u.user_id,u.mobile,bs.score,u.user_score,bs.type,bs.association_id,bs.create_time from buyer_user u left join buyer_score bs on bs.create_time = (select max(create_time) from buyer_score s where s.user_id = u.user_id) limit #{page},#{pageSize}")
    List<BuyerUserBuyerScoreVo> buyerUserScore(NewPage newPage);
}