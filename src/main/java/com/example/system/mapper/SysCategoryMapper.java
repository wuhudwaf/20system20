package com.example.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.system.pojo.SysCategory;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

@SuppressWarnings("all")
public interface SysCategoryMapper extends BaseMapper<SysCategory> {

    /**
     * 废除分类
     *
     * @param cateId
     * @return
     */
    @Update("update sys_category set status = #{2} where cate_id = #{cateId}")
    int delete(String cateId);


    /**
     * id查询
     *
     * @param cateId
     * @return
     */
    @Select("select * from sys_category where cate_id = #{cateId} and status = 1")
    SysCategory getByCateId(String cateId);

    /**
     * 查询子分类
     *
     * @param parentId
     * @return
     */
    @Select("select * from sys_category where parent_id = #{parentId} and status = 1")
    List<SysCategory> getByParentId(String parentId);

    /**
     * 查询根节点分类
     * @return
     */
    @Select("select * from sys_category where parent_id = 0 and status = 1")
    List<SysCategory> getRoot();
}