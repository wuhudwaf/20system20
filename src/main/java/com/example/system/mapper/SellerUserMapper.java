package com.example.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.system.dto.SellerUserDto;
import com.example.system.pojo.SellerUser;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@SuppressWarnings("all")
public interface SellerUserMapper extends BaseMapper<SellerUser> {

    /**
     * 根据手机号获取员工数据
     *
     * @param phone
     * @return
     */
    @Select("select * from seller_user where phone = #{phone}")
    SellerUser getByPhone(String phone);


    /**
     * 获取所有员工
     *
     * @return
     */
    @Select("select * from seller_user")
    List<SellerUserDto> getList();
}