package com.example.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.system.pojo.GoodsQueryLog;

@SuppressWarnings("all")
public interface GoodsQueryLogMapper extends BaseMapper<GoodsQueryLog> {
}