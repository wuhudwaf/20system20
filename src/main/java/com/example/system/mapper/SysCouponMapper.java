package com.example.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.system.pojo.SysCoupon;

@SuppressWarnings("all")
public interface SysCouponMapper extends BaseMapper<SysCoupon> {
}