package com.example.system.pojo;

import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

/**
 * 门店商品
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@TableName("seller_goods")
@ApiModel("门店商品")
public class SellerGoods {

    /**
     * id
     */
    @ApiModelProperty(value = "id", required = true)
    private String id;

    /**
     * 编号
     */
    @ApiModelProperty(value = "编号", required = true)
    private String goodsId;

    /**
     * 门店id
     */
    @ApiModelProperty(value = "门店id", required = true)
    private String shopId;

    /**
     * 商品名称
     */
    @ApiModelProperty(value = "商品名称", required = true)
    private String name;

    /**
     * 商品主图
     */
    @ApiModelProperty(value = "商品主图", required = true)
    private String picture;

    /**
     * 乐观锁
     */
    @ApiModelProperty(value = "乐观锁", required = true)
    private Integer revision;

    /**
     * 创建人
     */
    @ApiModelProperty(value = "创建人", required = true)
    private String createBy;

    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间", required = true)
    private LocalDateTime createTime;

    /**
     * 更新人
     */
    @ApiModelProperty(value = "更新人", required = true)
    private String updateBy;

    /**
     * 更新时间
     */
    @ApiModelProperty(value = "更新时间", required = true)
    private LocalDateTime updateTime;

    /**
     * 1上架 0下架
     */
    @ApiModelProperty(value = "1上架 0下架", required = true)
    private byte[] status;

    /**
     * 类别id
     */
    @ApiModelProperty(value = "类别id", required = true)
    private String cateId;
}