package com.example.system.pojo;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.example.system.utils.excel.ExcelExport;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * 微信用户信息表
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@TableName("buyer_user")
@ApiModel("微信用户信息")
public class BuyerUser {

    /**
     * 小程序用户表主键ID
     */
    @ExcelExport(value = "ID")
    @TableId("user_id")
    @ApiModelProperty(value = "小程序用户表主键ID", required = true)
    private String userId;

    /**
     * 小程序OPENID
     */
    @ApiModelProperty(value = "小程序OPENID", required = true)
    private String openId;

    /**
     * 用户名
     */
    @ExcelExport(value = "用户昵称")
    @ApiModelProperty(value = "用户名", required = true)
    private String userName;

    /**
     * 手机号
     */
    @ExcelExport(value = "用户电话")
    @ApiModelProperty(value = "手机号", required = true)
    private String mobile;

    /**
     * 头像
     */
    @ExcelExport(value = "用户头像")
    @ApiModelProperty(value = "头像", required = true)
    private String avatar;

    /**
     * 用户积分
     */
    @ExcelExport(value = "用户积分")
    @ApiModelProperty(value = "用户积分", required = true)
    private Integer userScore;

    /**
     * 消费金额
     */
    @ExcelExport(value = "消费金额")
    @ApiModelProperty(value = "消费金额", required = true)
    private BigDecimal consumptionAmount;

    /**
     * 用户等级
     */
    @ApiModelProperty(value = "用户等级", required = true)
    private Integer userLevel;

    /**
     * 会员等级
     */
    @ExcelExport(value = "会员等级", kv = "1-青铜会员;2-白银会员;3-黄金会员;4-铂金会员;0-普通用户")
    @ApiModelProperty(value = "会员等级", required = true)
    private Integer membershipLevel;

    /**
     * 会员等级名称
     */
    @TableField(exist = false)
    @ApiModelProperty(value = "会员等级名称", required = false)
    private String membershipLevelStr;

    /**
     * 用户状态 0：封禁 1 正常
     */
    @ApiModelProperty(value = "用户状态 0：封禁 1 正常", required = true)
    private Integer status;

    /**
     * 推荐人ID
     */
    @ApiModelProperty(value = "推荐人ID", required = true)
    private String inviteUserId;

    /**
     * 推荐人名称
     */
    @ExcelExport(value = "推荐人名称")
    @TableField(exist = false)
    private String inviteUserIdName;

    /**
     * 小程序配置信息ID
     */
    @ApiModelProperty(value = "小程序配置信息ID", required = true)
    private String configId;

    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间", required = true)
    private LocalDateTime createTime;

    /**
     * 最后修改时间
     */
    @ApiModelProperty(value = "最后修改时间", required = true)
    private LocalDateTime lastUpdateTime;

    /**
     * 更新操作人
     */
    @ApiModelProperty(value = "更新操作人", required = true)
    private String updateUserId;

    /**
     * 储值卡余额
     */
    @ApiModelProperty(value = "储值卡余额", required = true)
    private BigDecimal storedValueCard;

    /**
     * 地址
     */
    @TableField(exist = false)
    private String address;

    /**
     * 当前用户订单列表
     */
    @TableField(exist = false)
    private List<BuyerOrder> buyerOrders = new ArrayList<>();

    /**
     * 当前用户下单次数
     */
    @ExcelExport(value = "下单次数")
    @TableField(exist = false)
    private Integer number;
}