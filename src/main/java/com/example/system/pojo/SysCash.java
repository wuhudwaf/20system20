package com.example.system.pojo;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * 提现申请表
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@TableName("sys_cash")
@ApiModel("提现申请")
public class SysCash {

    /**
     * 主键
     */
    @ApiModelProperty(value = "主键", required = false)
    private String id;

    /**
     * 门店id
     */
    @NotNull(message = "请输入门店id")
    @ApiModelProperty(value = "门店id", required = true)
    private String shopId;

    /**
     * 申请时间
     */
    @ApiModelProperty(value = "申请时间", required = false)
    private LocalDateTime createTime;

    /**
     * 提现金额
     */
    @NotNull(message = "请输入提现金额")
    @ApiModelProperty(value = "提现金额", required = true)
    private BigDecimal money;

    /**
     * 1 申请中   2 申请成功   0申请驳回
     */
    @ApiModelProperty(value = "1 申请中   2 申请成功   0申请驳回", required = false)
    private Integer status;

    /**
     * 审批人
     */
    @ApiModelProperty(value = "审批人", required = false)
    private String updateUser;

    /**
     * 审批时间
     */
    @ApiModelProperty(value = "审批时间", required = false)
    private LocalDateTime updateTime;

    /**
     * 门店名称
     */
    @TableField(exist = false)
    private String shopName;
}