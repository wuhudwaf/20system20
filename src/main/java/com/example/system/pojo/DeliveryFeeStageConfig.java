package com.example.system.pojo;

import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@NoArgsConstructor
@AllArgsConstructor
@TableName("delivery_fee_stage_config")
@ApiModel("配送费阶段配置")
public class DeliveryFeeStageConfig {

    /**
     * 阶段配置ID
     */
    @ApiModelProperty(value = "阶段配置ID", required = true)
    private String stageConfigId;

    /**
     * 基础配置ID
     */
    @ApiModelProperty(value = "基础配置ID", required = true)
    private String baseConfigId;

    /**
     * 阶段配置类型 1：里程 2：重量 3：体积 4：保费
     */
    @ApiModelProperty(value = "段配置类型 1：里程 2：重量 3：体积 4：保费", required = true)
    private Integer stageConfigType;

    /**
     * 起始阶段
     */
    @ApiModelProperty(value = "起始阶段", required = true)
    private Integer startStage;

    /**
     * 结束阶段
     */
    @ApiModelProperty(value = "结束阶段", required = true)
    private Integer endStage;

    /**
     * 阶段金额
     */
    @ApiModelProperty(value = "阶段金额", required = true)
    private BigDecimal stageFee;
}