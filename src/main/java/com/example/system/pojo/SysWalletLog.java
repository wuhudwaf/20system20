package com.example.system.pojo;

import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * 门店余额变动日志
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("sys_wallet_log")
@ApiModel("门店余额变动日志")
public class SysWalletLog {

    /**
     * id
     */
    @ApiModelProperty(value = "id", required = true)
    private String id;

    /**
     * 门店id
     */
    @ApiModelProperty(value = "门店id", required = true)
    private String shopId;

    /**
     * 变更前
     */
    @ApiModelProperty(value = "变更前", required = true)
    private BigDecimal beforeMoney;

    /**
     * 金额
     */
    @ApiModelProperty(value = "金额", required = true)
    private BigDecimal money;

    /**
     * 变更后
     */
    @ApiModelProperty(value = "变更后", required = true)
    private BigDecimal afterMoney;

    /**
     * 操作类型 1：收入、2：支出
     */
    @ApiModelProperty(value = "操作类型 1：收入、2：支出", required = true)
    private Integer operateType;

    /**
     * 备注
     */
    @ApiModelProperty(value = "备注", required = true)
    private String remarks;

    /**
     * 来源1佣金 2提现 3物流费用 4订单退款
     */
    @ApiModelProperty(value = "来源1佣金 2提现 3物流费用 4订单退款", required = true)
    private Integer source;

    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间", required = true)
    private LocalDateTime createTime;

    /**
     * 订单编号
     */
    @ApiModelProperty(value = "订单编号", required = true)
    private String orderId;

    /**
     * 售后单号
     */
    @ApiModelProperty(value = "售后单号", required = true)
    private String serviceId;
}