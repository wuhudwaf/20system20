package com.example.system.pojo.page;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class Page {

    @ApiModelProperty(value = "第几页", required = false)
    private Integer page;
    @ApiModelProperty(value = "每页展示多少条", required = false)
    private Integer pageSize;
    @ApiModelProperty(value = "第几页", required = false)
    private Integer pageOne;
    @ApiModelProperty(value = "到第几页", required = false)
    private Integer pageTwo;
}
