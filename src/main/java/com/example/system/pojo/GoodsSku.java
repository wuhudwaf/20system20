package com.example.system.pojo;


import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.example.system.utils.excel.ExcelImport;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * 商品sku表
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "goods_sku", autoResultMap = true)
@ApiModel("商品sku")
public class GoodsSku implements Serializable {

    /**
     * 规格id
     */
    @TableId("sku_id")
    @ApiModelProperty(value = "规格id", required = false)
    private String skuId;

    /**
     * 商品id
     */
    @ExcelImport(value = "商品列头", required = true)
    @ApiModelProperty(value = "商品id", required = false)
    private String goodsId;

    /**
     * 规格值
     */
    @ExcelImport(value = "规格值", required = true)
    @ApiModelProperty(value = "规格值", required = false)
    private String skuVal;

    /**
     * 图片
     */
    @ExcelImport(value = "图片", required = true)
    @ApiModelProperty(value = "图片", required = false)
    private String picture;

    /**
     * 原价
     */
    @ExcelImport(value = "原价", required = true)
    @ApiModelProperty(value = "原价", required = false)
    private String oldPrice;

    /**
     * 现价
     */
    @ExcelImport(value = "现价", required = true)
    @ApiModelProperty(value = "现价", required = false)
    private BigDecimal price;

    /**
     * 限购/
     */
    @ExcelImport(value = "限购", required = true)
    @ApiModelProperty(value = "限购", required = false)
    private String stock;

    /**
     * 佣金比例
     */
    @ExcelImport(value = "佣金比例", required = true)
    @ApiModelProperty(value = "佣金比例", required = false)
    @NotNull(message = "请设置佣金比例")
    private BigDecimal commission;

    /**
     * 重量
     */
    @ApiModelProperty(value = "重量", required = false)
    private BigDecimal weight;

    /**
     * 体积
     */
    @ApiModelProperty(value = "体积", required = false)
    private BigDecimal volume;

    /**
     * 创建人
     */
    @ApiModelProperty(value = "创建人", required = false)
    private String createBy;

    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间", required = false)
    private LocalDateTime createTime;

    /**
     * 更新人
     */
    @ApiModelProperty(value = "更新人", required = false)
    private String updateBy;

    /**
     * 更新时间
     */
    @ApiModelProperty(value = "更新时间", required = false)
    private LocalDateTime updateTime;

    /**
     * 包装费
     */
    @ExcelImport(value = "包装费", required = true)
    @ApiModelProperty(value = "包装费", required = false)
    private BigDecimal packingFee;

    /**
     * 单位
     */
    @ApiModelProperty(value = "单位", required = false)
    private String unit;

    /**
     * 成本价
     */
    @ApiModelProperty(value = "成本价", required = false)
    private String costPrice;

    /**
     * 税率
     */
    @ApiModelProperty(value = "税率", required = false)
    private String taxRate;

    /**
     * 最低限价
     */
    @ApiModelProperty(value = "最低限价", required = false)
    private String priceFloor;

    /**
     * 规格值,只做数据接收，不做传参，传参时此字段许为null，比如新增，修改
     */
    @TableField(exist = false)
    private JSONObject skuValJson;

    /**
     * 状态，表示是否加入秒杀
     */
    @TableField(exist = false)
    private Integer status;

    /**
     * sku加入商品秒杀的秒杀价格
     */
    @TableField(exist = false)
    private BigDecimal flashSalePrice;
}