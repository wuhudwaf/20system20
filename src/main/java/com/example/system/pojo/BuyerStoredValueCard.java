package com.example.system.pojo;

import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * 用户储值卡记录
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("buyer_stored_value_card")
@ApiModel("用户储值卡记录")
public class BuyerStoredValueCard {

    /**
     * 记录ID
     */
    @ApiModelProperty(value = "记录ID", required = true)
    private String logId;

    /**
     * 用户ID
     */
    @ApiModelProperty(value = "用户ID", required = true)
    private String userId;

    /**
     * 操作前余额数
     */
    @ApiModelProperty(value = "操作前余额数", required = true)
    private BigDecimal beforeStoredValue;

    /**
     * 增加或减少余额数
     */
    @ApiModelProperty(value = "增加或减少余额数", required = true)
    private BigDecimal storedValue;

    /**
     * 余额类型 1：充值 2：消费
     */
    @ApiModelProperty(value = "余额类型 1：充值 2：消费", required = true)
    private Integer type;

    /**
     * 关联ID，订单ID或用户ID
     */
    @ApiModelProperty(value = "关联ID，订单ID或用户ID", required = true)
    private String associationId;

    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间", required = true)
    private LocalDateTime createTime;

    /**
     * 支付时间
     */
    @ApiModelProperty(value = "支付时间", required = true)
    private LocalDateTime payTime;

    /**
     * 支付状态 1：支付中 2：支付成功 3：支付失败
     */
    @ApiModelProperty(value = "支付状态 1：支付中 2：支付成功 3：支付失败", required = true)
    private Integer payStatus;

    /**
     * 支付订单号
     */
    @ApiModelProperty(value = "支付订单号", required = true)
    private String payOrderId;
}