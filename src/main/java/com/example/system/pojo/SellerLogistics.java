package com.example.system.pojo;

import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * 物流信息
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@TableName("seller_logistics")
@ApiModel("物流信息")
public class SellerLogistics {

    /**
     * 门店id
     */
    @ApiModelProperty(value = "门店id", required = true)
    private String shopId;

    /**
     * 物流id
     */
    @ApiModelProperty(value = "物流id", required = true)
    private String logisticsId;

    /**
     * 用户表id
     */
    @ApiModelProperty(value = "用户表id", required = true)
    private String userId;

    /**
     * 订单id
     */
    @ApiModelProperty(value = "订单id", required = true)
    private String orderId;

    /**
     * 类型
     */
    @ApiModelProperty(value = "类型", required = true)
    private Integer type;

    /**
     * 物流费用
     */
    @ApiModelProperty(value = "物流费用", required = true)
    private BigDecimal amount;

    /**
     * 收货姓名
     */
    @ApiModelProperty(value = "收货姓名", required = true)
    private String receiverName;

    /**
     * 收货电话
     */
    @ApiModelProperty(value = "收货电话", required = true)
    private String receiverPhone;

    /**
     * 省份
     */
    @ApiModelProperty(value = "省份", required = true)
    private String receiverProvince;

    /**
     * 城市
     */
    @ApiModelProperty(value = "城市", required = true)
    private String receiverCity;

    /**
     * 区/县
     */
    @ApiModelProperty(value = "区/县", required = true)
    private String receiverDistrict;

    /**
     * 街道
     */
    @ApiModelProperty(value = "街道", required = true)
    private String receiverStreet;

    /**
     * 详细地址
     */
    @ApiModelProperty(value = "详细地址", required = true)
    private String receiverLastDetail;

    /**
     * 租户号
     */
    @ApiModelProperty(value = "租户号", required = true)
    private String tenantId;

    /**
     * 乐观锁
     */
    @ApiModelProperty(value = "乐观锁", required = true)
    private Integer revision;

    /**
     * 创建人
     */
    @ApiModelProperty(value = "创建人", required = true)
    private String createBy;

    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间", required = true)
    private LocalDateTime createTime;

    /**
     * 更新人
     */
    @ApiModelProperty(value = "更新人", required = true)
    private String updateBy;

    /**
     * 更新时间
     */
    @ApiModelProperty(value = "更新时间", required = true)
    private LocalDateTime updateTime;
}