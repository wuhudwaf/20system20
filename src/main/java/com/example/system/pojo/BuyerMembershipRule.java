package com.example.system.pojo;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;


/**
 * 会员规则表
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@TableName("buyer_membership_rule")
@ApiModel("会员规则")
public class BuyerMembershipRule {

    /**
     * 规则id
     */
    @TableId("rule_id")
    @ApiModelProperty(value = "规则id", required = false)
    private String ruleId;

    /**
     * 会员等级
     */
    @ApiModelProperty(value = "会员等级", required = true)
    @NotNull(message = "请填写会员等级")
    private Integer membershipLevel;

    /**
     * 会员名称
     */
    @ApiModelProperty(value = "会员名称", required = true)
    @NotBlank(message = "请填写会员名称")
    private String membershipName;

    /**
     * 当前等级所需最少积分
     */
    @ApiModelProperty(value = "当前等级所需最少积分", required = true)
    @NotNull(message = "请填写当前等级所需最少积分")
    private Integer upgradeLowerLimit;

    /**
     * 赠送积分比例，消费1元送多少积分
     */
    @ApiModelProperty(value = "赠送积分比例，消费1元送多少积分", required = true)
    @NotNull(message = "请填写赠送积分比例")
    private Integer rewardScoreRatio;

    /**
     * 折扣比例 例如8.5折为8.5
     */
    @ApiModelProperty(value = "折扣比例 例如8.5折为8.5", required = true)
    @NotNull(message = "请填写折扣比例")
    private Double discountRatio;

    /**
     * 会员权益描述
     */
    @ApiModelProperty(value = "会员权益描述", required = true)
    @NotBlank(message = "请填写回员描述")
    private String equityDescription;

    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间", required = false)
    private LocalDateTime createTime;
}