package com.example.system.pojo;

import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

/**
 * 售后服务表
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("sys_service")
@ApiModel("售后服务")
public class SysService {

    /**
     * id
     */
    @ApiModelProperty(value = "id", required = false)
    private String serviceId;

    /**
     * 用户ID
     */
    @ApiModelProperty(value = "用户ID", required = true)
    private String userId;

    /**
     * 订单编号
     */
    @ApiModelProperty(value = "订单编号", required = true)
    private String orderId;

    /**
     * 售后类型
     */
    @ApiModelProperty(value = "售后类型", required = true)
    private String type;

    /**
     * 凭证图片
     */
    @ApiModelProperty(value = "凭证图片", required = true)
    private String pictures;

    /**
     * 售后原因
     */
    @ApiModelProperty(value = "售后原因", required = true)
    private String reason;

    /**
     * 售后状态
     */
    @ApiModelProperty(value = "售后状态", required = true)
    private Integer status;

    /**
     * 备注
     */
    @ApiModelProperty(value = "备注", required = true)
    private String remark;

    /**
     * 租户号
     */
    @ApiModelProperty(value = "租户号", required = true)
    private String tenantId;

    /**
     * 乐观锁
     */
    @ApiModelProperty(value = "乐观锁", required = true)
    private Integer revision;

    /**
     * 创建人
     */
    @ApiModelProperty(value = "创建人", required = true)
    private String createBy;

    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间", required = true)
    private LocalDateTime createTime;

    /**
     * 更新人
     */
    @ApiModelProperty(value = "更新人", required = true)
    private String updateBy;

    /**
     * 更新时间
     */
    @ApiModelProperty(value = "更新时间", required = true)
    private LocalDateTime updateYTime;

    /**
     * 门店id
     */
    @ApiModelProperty(value = "门店id", required = true)
    private String shopId;

    /**
     * 商品SkuId
     */
    @ApiModelProperty(value = "商品SkuId", required = true)
    private String goodsSkuId;

    /**
     * 退货快递公司
     */
    @ApiModelProperty(value = "退货快递公司", required = true)
    private String refundLogisticsCompany;

    /**
     * 退货备注
     */
    @ApiModelProperty(value = "退货备注", required = true)
    private String refundRemark;

    /**
     * 退货凭证图片
     */
    @ApiModelProperty(value = "退货凭证图片", required = true)
    private String refundVoucherImages;
}