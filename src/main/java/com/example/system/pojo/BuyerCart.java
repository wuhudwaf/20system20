package com.example.system.pojo;

import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * 用户购物车表
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("buyer_cart")
@ApiModel("用户购物车")
public class BuyerCart {

    /**
     * 购物车ID
     */
    @ApiModelProperty(value = "购物车ID", required = false)
    private String cartId;

    /**
     * 用户ID
     */
    @ApiModelProperty(value = "用户ID", required = true)
    private String userId;

    /**
     * 门店ID
     */
    @ApiModelProperty(value = "门店ID", required = true)
    private String shopId;

    /**
     * 总金额
     */
    @ApiModelProperty(value = "总金额", required = true)
    private BigDecimal totalPrice;

    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间", required = false)
    private LocalDateTime createTime;

    /**
     * 更新时间
     */
    @ApiModelProperty(value = "更新时间", required = false)
    private LocalDateTime lastUpdateTime;

}