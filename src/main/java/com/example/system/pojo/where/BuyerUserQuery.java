package com.example.system.pojo.where;

import com.baomidou.mybatisplus.annotation.TableId;
import com.example.system.utils.excel.ExcelExport;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
public class BuyerUserQuery {

    /**
     * 第几页
     */
    @ApiModelProperty(value = "第几页", required = true)
    @NotNull(message = "page不得为空")
    private Long page;

    /**
     * 每页展示多少条
     */
    @ApiModelProperty(value = "每页展示多少条", required = true)
    @NotNull(message = "pageSize不得为空")
    private Long pageSize;

    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间", required = false)
    private String createTime;

    /**
     * 小程序用户表主键ID
     */
    @ExcelExport(value = "ID")
    @TableId("user_id")
    @ApiModelProperty(value = "小程序用户表主键ID", required = false)
    private String userId;

    /**
     * 用户名
     */
    @ExcelExport(value = "用户昵称")
    @ApiModelProperty(value = "用户名", required = false)
    private String userName;

    /**
     * 推荐人ID
     */
    @ApiModelProperty(value = "推荐人ID", required = false)
    private String inviteUserId;

    /**
     * 会员等级
     */
    @ExcelExport(value = "会员等级", kv = "1-青铜会员;2-白银会员;3-黄金会员;4-铂金会员;0-普通用户")
    @ApiModelProperty(value = "会员等级", required = false)
    private Integer membershipLevel;

    /**
     * 用户状态 0：封禁 1 正常
     */
    @ApiModelProperty(value = "用户状态 0：封禁 1 正常", required = false)
    private Integer status;

    /**
     * 消费金额(区间值1)
     */
    @ApiModelProperty(value = "消费金额(区间值1)", required = false)
    private BigDecimal money1;

    /**
     * 费金额(区间值2)
     */
    @ApiModelProperty(value = "消费金额(区间值2)", required = false)
    private BigDecimal money2;
}