package com.example.system.pojo.where;


import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;


@Data
public class SysGoodsQuery {

    /**
     * 第几页
     */
    @ApiModelProperty(value = "第几页", required = true)
    @NotNull(message = "page不得为空")
    private Long page;

    /**
     * 每页展示多少条
     */
    @ApiModelProperty(value = "每页展示多少条", required = true)
    @NotNull(message = "pageSize不得为空")
    private Long pageSize;

    /**
     * 商品名称
     */
    @ApiModelProperty(value = "商品名称", required = false)
    private String name;

    /**
     * 类别id
     */
    @ApiModelProperty(value = "类别id", required = false)
    private String cateId;

    /**
     * 参与活动(0：全部，1：未参与，2：拼团活动，3：秒杀活动，4：砍价活动)
     */
    @ApiModelProperty(value = "参与活动(0：全部，1：未参与，2：拼团活动，3：秒杀活动，4：砍价活动)", required = false)
    private Integer activity;
}