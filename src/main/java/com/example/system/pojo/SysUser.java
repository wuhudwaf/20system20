package com.example.system.pojo;


import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 管理员
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("sys_user")
@ApiModel("管理员")
public class SysUser {

    private String userId;

    private String deptId;

    private String userName;

    private String nickName;

    private String userType;

    private String email;

    private String phonenumber;

    private char sex;

    private String avatar;

    private String password;

    private char status;

    private char delFlag;

    private String loginIp;

    private String loginDate;

    private String createBy;

    private String createTime;

    private String updateBy;

    private String updateTime;

    private String remark;
}