package com.example.system.pojo;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.example.system.utils.excel.ExcelExport;
import com.example.system.utils.excel.ExcelImport;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * 门店
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@TableName("sys_shop")
@ApiModel("门店")
public class SysShop {

    /**
     * id
     */
    @ExcelImport(value = "门店id", required = false)
    @ExcelExport(value = "主键")
    @TableId("shop_id")
    @ApiModelProperty(value = "id", required = false)
    private String shopId;

    /**
     * 门店名称
     */
    @ExcelImport(value = "门店名称", required = false)
    @ExcelExport(value = "门店名称")
    @ApiModelProperty(value = "门店名称", required = false)
    private String name;

    /**
     * 门店图片
     */
    @ExcelExport(value = "门店图片")
    @ApiModelProperty(value = "门店图片", required = false)
    private String picture;

    /**
     * 门店地址
     */
    @ExcelImport(value = "门店地址", required = false)
    @ExcelExport(value = "门店地址")
    @ApiModelProperty(value = "门店地址", required = false)
    private String address;

    /**
     * 门店负责人
     */
    @ExcelImport(value = "门店负责人/联系人/店长", required = false)
    @ExcelExport(value = "门店负责人")
    @ApiModelProperty(value = "门店负责人", required = false)
    private String admin;

    /**
     * 是否审批员工 0不审批，1审批
     */
    @ExcelExport(value = "是否审批员工", kv = "0-不审批;1-审批")
    @ApiModelProperty(value = "是否审批员工 0不审批，1审批", required = false)
    private Integer staffSet;

    /**
     * 门店电话
     */
    @ExcelImport(value = "门店服务电话", required = false)
    @ExcelExport(value = "门店电话")
    @ApiModelProperty(value = "门店电话", required = false)
    private String phone;

    /**
     * 营业开始时间
     */
    @ExcelImport(value = "营业时间", required = false)
    @ExcelExport(value = "营业开始时间")
    @ApiModelProperty(value = "营业开始时间", required = false)
    private String openHours;

    /**
     * 营业结束时间
     */
    @ExcelExport(value = "营业结束时间")
    @ApiModelProperty(value = "营业结束时间", required = false)
    private String closeHours;

    /**
     * 租户号
     */
    @ExcelExport(value = "租户号")
    @ApiModelProperty(value = "租户号", required = false)
    private String tenantId;

    /**
     * 乐观锁
     */
    @ApiModelProperty(value = "乐观锁", required = false)
    private Integer revision;

    /**
     * 创建人
     */
    @ExcelExport(value = "创建人")
    @ApiModelProperty(value = "创建人", required = false)
    private String createBy;

    /**
     * 创建时间
     */
    @ExcelExport(value = "创建时间")
    @ApiModelProperty(value = "创建时间", required = false)
    private LocalDateTime createTime;

    /**
     * 更新人
     */
    @ExcelExport(value = "更新人")
    @ApiModelProperty(value = "更新人", required = false)
    private String updateBy;

    /**
     * 更新时间
     */
    @ExcelExport(value = "更新时间")
    @ApiModelProperty(value = "更新时间", required = false)
    private LocalDateTime updateTime;

    /**
     * 门店详情
     */
    @ExcelExport(value = "门店详情")
    @ApiModelProperty(value = "门店详情", required = false)
    private String detail;

    /**
     * 门店状态 1开店 0闭店
     */
    @ExcelImport(value = "营业状态", kv = "0-闭店;1-开店", required = false)
    @ExcelExport(value = "门店状态", kv = "1-开店;0-闭店")
    @ApiModelProperty(value = "门店状态 1开店 0闭店", required = false)
    private Integer status;

    /**
     * 经度
     */
    @ExcelExport(value = "经度")
    @ApiModelProperty(value = "经度", required = false)
    private Double lat;

    /**
     * 维度
     */
    @ExcelExport(value = "维度")
    @ApiModelProperty(value = "维度", required = false)
    private String lon;

    /**
     * 门店综合评分
     */
    @ExcelExport(value = "门店综合评分")
    @ApiModelProperty(value = "门店综合评分", required = false)
    private Double comprehensiveMark;

    /**
     * 门店余额
     */
    @ExcelExport(value = "门店余额")
    @ApiModelProperty(value = "门店余额", required = false)
    private BigDecimal wallet;

    /**
     * 客服二维码地址
     */
    @ExcelExport(value = "客服二维码地址")
    @ApiModelProperty(value = "客服二维码地址", required = false)
    private String customerServiceQrCode;

    /**
     * 证件图地址
     */
    @ExcelExport(value = "证件图地址")
    @ApiModelProperty(value = "证件图地址", required = false)
    private String certificatePath;

    /**
     * 门店简称
     */
    @ExcelImport(value = "门店简称", required = false)
    @ApiModelProperty(value = "门店简称", required = false)
    private String nameSimplification;

    /**
     * 门店城市
     */
    @ExcelImport(value = "门店城市", required = false)
    @ApiModelProperty(value = "门店城市", required = false)
    private String shopCity;

    /**
     * 门店城市(页面新增时：包含市区街道)
     */
    @TableField(exist = false)
    private String[] shopCityS;
    /**
     * 所属街道
     */
    @ExcelImport(value = "所属街道", required = false)
    @ApiModelProperty(value = "所属街道", required = false)
    private String affiliationStreet;

    /**
     * 门店客服微信号
     */
    @ExcelImport(value = "门店客服微信号", required = false)
    @ApiModelProperty(value = "门店客服微信号", required = false)
    private String shopServiceWechat;

    /**
     * 是否接收门店自取
     */
    @ExcelImport(value = "是否接收门店自取", kv = "1-是;0-否", required = false)
    @ApiModelProperty(value = "是否接收门店自取", required = false)
    private String isAndNoAcceptStoreSelfPickup;

    /**
     * 法人代表
     */
    @ExcelImport(value = "法人代表", required = false)
    @ApiModelProperty(value = "法人代表", required = false)
    private String lawRepresentative;

    /**
     * 所在行政区域
     */
    @ExcelImport(value = "所在行政区域", required = false)
    @ApiModelProperty(value = "所在行政区域", required = false)
    private String administrativeRegion;

    /**
     * 门店分类
     */
    @ExcelImport(value = "门店分类", required = false)
    @ApiModelProperty(value = "门店分类", required = false)
    private String shopCategory;

    /**
     * 统一社会信用代码
     */
    @ExcelImport(value = "统一社会信用代码", required = false)
    @ApiModelProperty(value = "统一社会信用代码", required = false)
    private String creditCode;

    /**
     * 营业时间
     */
    @ExcelImport(value = "营业时间", required = false)
    @ExcelExport(value = "营业时间")
    @ApiModelProperty(value = "营业时间", required = false)
    private String businessHours;
}