package com.example.system.pojo;

import cn.hutool.extra.spring.SpringUtil;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

/**
 * 买家订单催促日志
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@TableName("buyer_order_urge_log")
@ApiModel("买家订单催促日志")
public class BuyerOrderUrgeLog {

    /**
     * 日志ID
     */
    @ApiModelProperty(value = "日志ID", required = true)
    private String logId;

    /**
     * 订单ID
     */
    @ApiModelProperty(value = "订单ID", required = true)
    private String orderId;

    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间", required = true)
    private LocalDateTime createTime;

    /**
     * 处理时间
     */
    @ApiModelProperty(value = "处理时间", required = true)
    private LocalDateTime handleTime;
}