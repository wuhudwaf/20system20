package com.example.system.pojo;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.sql.Date;
import java.time.LocalDateTime;
import java.util.*;

/**
 * 订单
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("buyer_order")
@ApiModel("订单")
public class BuyerOrder {

    /**
     * 订单id
     */
    @TableId("order_id")
    @ApiModelProperty(value = "订单id", required = false)
    private String orderId;

    /**
     * 用户id
     */
    @ApiModelProperty(value = "用户id", required = true)
    private String userId;

    /**
     * 购物车id
     */
    @ApiModelProperty(value = "购物车id", required = true)
    private String cartId;

    /**
     * 商户id
     */
    @ApiModelProperty(value = "商户id", required = true)
    private String shopId;

    /**
     * 物流id
     */
    @ApiModelProperty(value = "物流id", required = true)
    private String logisticsId;

    /**
     * 优惠券id
     */
    @ApiModelProperty(value = "优惠券id", required = true)
    private String buyerCouponId;

    /**
     * 总金额
     */
    @ApiModelProperty(value = "总金额", required = true)
    private BigDecimal totalPrice;

    /**
     * 优惠金额
     */
    @ApiModelProperty(value = "优惠金额", required = true)
    private BigDecimal couponPrice;

    /**
     * 应付金额
     */
    @ApiModelProperty(value = "应付金额", required = true)
    private BigDecimal payablePrice;

    /**
     * 支付方式
     */
    @ApiModelProperty(value = "支付方式", required = true)
    private String payMethod;

    /**
     * 支付流水号
     */
    @ApiModelProperty(value = "支付流水号", required = true)
    private String platformNumber;

    /**
     * 开票设置id
     */
    @ApiModelProperty(value = "开票设置id", required = true)
    private String invoiceTplId;

    /**
     * 订单留言备注
     */
    @ApiModelProperty(value = "订单留言备注", required = true)
    private String leaveComment;

    /**
     * 订单状态 1:待支付 2：付款中 3：待发货 4：待取货 5：已发货 6：确认收货 7：已完成 99：已取消 0：异常
     */
    @ApiModelProperty(value = "订单状态 1:待支付 2：付款中 3：待发货 4：待取货 5：已发货 6：确认收货 7：已完成 99：已取消 0：异常", required = true)
    private Integer orderStatus;

    /**
     * 支付时间
     */
    @ApiModelProperty(value = "支付时间", required = false)
    private LocalDateTime paymentTime;

    /**
     * 发货时间
     */
    @ApiModelProperty(value = "发货时间", required = false)
    private LocalDateTime sendTime;

    /**
     * 地址ID  当为快递或同城时不能为空
     */
    @ApiModelProperty(value = "地址ID  当为快递或同城时不能为空", required = true)
    private String addressId;

    /**
     * 交易完成时间
     */
    @ApiModelProperty(value = "交易完成时间", required = false)
    private LocalDateTime endTime;

    /**
     * 交易关闭时间
     */
    @ApiModelProperty(value = "交易关闭时间", required = false)
    private LocalDateTime closeTime;

    /**
     * 乐观锁
     */
    @ApiModelProperty(value = "乐观锁", required = false)
    private Integer revision;

    /**
     * 创建人
     */
    @ApiModelProperty(value = "创建人", required = false)
    private String createBy;

    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间", required = false)
    private LocalDateTime createTime;

    /**
     * 更新人
     */
    @ApiModelProperty(value = "更新人", required = false)
    private String updateBy;

    /**
     * 更新时间
     */
    @ApiModelProperty(value = "更新时间", required = false)
    private LocalDateTime updateTime;

    /**
     * 佣金总金额
     */
    @ApiModelProperty(value = "佣金总金额", required = true)
    private BigDecimal commissionPrice;

    /**
     * 1催单 0普通
     */
    @ApiModelProperty(value = "1催单 0普通", required = true)
    private Integer urge;

    /**
     * 运费
     */
    @ApiModelProperty(value = "运费", required = true)
    private BigDecimal freightPrice;

    /**
     * 优惠卷抵扣金额
     */
    @ApiModelProperty(value = "优惠卷抵扣金额", required = true)
    private BigDecimal buyerCouponPrice;

    /**
     * 会员优惠金额
     */
    @ApiModelProperty(value = "会员优惠金额", required = true)
    private BigDecimal membershipDiscountPrice;

    /**
     * 积分抵扣金额
     */
    @ApiModelProperty(value = "积分抵扣金额", required = true)
    private BigDecimal scorePrice;

    /**
     * 1快递2同城3自提
     */
    @ApiModelProperty(value = "1快递2同城3自提", required = true)
    private Integer type;

    /**
     * 退款原因
     */
    @ApiModelProperty(value = "退款原因", required = true)
    private String refundReason;

    /**
     * 退款时间
     */
    @ApiModelProperty(value = "退款时间", required = false)
    private LocalDateTime refundTime;

    /**
     * 退款订单号
     */
    @ApiModelProperty(value = "退款订单号", required = true)
    private String refundOrderNo;

    /**
     * 取消原因
     */
    @ApiModelProperty(value = "取消原因", required = true)
    private String cancelReason;

    /**
     * 自提时间
     */
    @ApiModelProperty(value = "自提时间", required = false)
    private LocalDateTime selfPickupTime;

    /**
     * 1结算 0未结算
     */
    @ApiModelProperty(value = "1结算 0未结算", required = true)
    private Integer settlementStatus;

    /**
     * 结算日期
     */
    @ApiModelProperty(value = "结算日期", required = false)
    private Date settlementDate;

    /**
     * 商品对象
     */
    @TableField(exist = false)
    private List<SysGoods> sysGoodsList = new ArrayList<>();

    /**
     * 商品订单
     */
    @TableField(exist = false)
    private List<SellerOrderItem> sellerOrderItems = new ArrayList<>();

    /**
     * 微信用户信息表
     */
    @TableField(exist = false)
    private BuyerUser buyerUser;

    /**
     * 门店对象
     */
    @TableField(exist = false)
    private Set<SysShop> sysShopSet = new HashSet<>();

    /**
     * 总销量
     */
    @TableField(exist = false)
    private Integer quantity;
}