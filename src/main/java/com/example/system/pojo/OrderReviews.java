package com.example.system.pojo;


import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

/**
 * 订单评价表
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("order_reviews")
@ApiModel("订单评价")
public class OrderReviews {

    /**
     * 评价ID
     */
    @ApiModelProperty(value = "评价ID", required = false)
    private String reviewsId;

    /**
     * 订单ID
     */
    @ApiModelProperty(value = "订单ID", required = false)
    private String orderId;

    /**
     * 门店ID
     */
    @ApiModelProperty(value = "门店ID", required = false)
    private String shopId;

    /**
     * 门店商品ID
     */
    @ApiModelProperty(value = "门店商品ID", required = false)
    private String sellerGoodsId;

    /**
     * 商品ID
     */
    @ApiModelProperty(value = "商品ID", required = false)
    private String goodsId;

    /**
     * 服务态度, 1-5依次变好
     */
    @ApiModelProperty(value = "服务态度, 1-5依次变好", required = false)
    private Integer serviceAttitude;

    /**
     * 发货速度/取货服务, 1-5依次变好
     */
    @ApiModelProperty(value = "发货速度/取货服务, 1-5依次变好", required = false)
    private Integer shippingSpeedOrPickupService;

    /**
     * 产品包装, 1-5依次变好
     */
    @ApiModelProperty(value = "产品包装, 1-5依次变好", required = false)
    private Integer goodsPackaging;

    /**
     * 评价
     */
    @ApiModelProperty(value = "评价", required = false)
    private Integer reviews;

    /**
     * 评价内容
     */
    @ApiModelProperty(value = "评价内容", required = false)
    private String reviewsContent;

    /**
     * 评价时间
     */
    @ApiModelProperty(value = "评价时间", required = false)
    private LocalDateTime reviewsTime;

    /**
     * 评价用户ID
     */
    @ApiModelProperty(value = "评价用户ID", required = false)
    private String reviewsUserId;

    /**
     * 回复
     */
    @ApiModelProperty(value = "回复", required = false)
    private String reply;

    /**
     * 评价时间
     */
    @ApiModelProperty(value = "评价时间", required = false)
    private LocalDateTime replyTime;

    /**
     * 回复操作ID
     */
    @ApiModelProperty(value = "回复操作ID", required = false)
    private String replyOperator;

    /**
     * 图片或视频地址，多个逗号分隔
     */
    @ApiModelProperty(value = "图片或视频地址，多个逗号分隔", required = false)
    private String imageOrVideoUrl;
}