package com.example.system.pojo;

import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

/**
 * 角色
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@TableName("sys_role")
@ApiModel("角色")
public class SysRole {

    /**
     * 角色id
     */
    @ApiModelProperty(value = "角色id", required = false)
    private String id;

    /**
     * 名称显示
     */
    @ApiModelProperty(value = "名称显示", required = true)
    @NotBlank(message = "名称不得为空")
    private String name;

    /**
     * 状态
     * 1显示
     * 0不显示
     */
    @ApiModelProperty(value = "状态,1显示,0不显示", required = false)
    private Integer status;
}