package com.example.system.pojo;

import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

/**
 * 购物车商品信息表
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("buyer_cart_goods_item")
@ApiModel("购物车商品信息")
public class BuyerCartGoodsItem {

    /**
     * 购物商品ID
     */
    @ApiModelProperty(value = "购物商品ID", required = false)
    private String itemId;

    /**
     * 购物车ID
     */
    @ApiModelProperty(value = "购物车ID", required = true)
    private String cartId;

    /**
     * 商品ID
     */
    @ApiModelProperty(value = "商品ID", required = true)
    private String goodsId;

    /**
     * 商品SKU ID
     */
    @ApiModelProperty(value = "商品SKU ID", required = true)
    private String goodsSkuId;

    /**
     * 商品数量
     */
    @ApiModelProperty(value = "商品数量", required = true)
    private Integer goodsNum;

    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间", required = false)
    private LocalDateTime createTime;

    /**
     * 更新时间
     */
    @ApiModelProperty(value = "更新时间", required = false)
    private LocalDateTime lastUpdateTime;
}