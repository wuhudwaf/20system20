package com.example.system.pojo;

import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

/**
 * 订单状态变化表
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@TableName("buyer_order_status_log")
@ApiModel("订单状态变化")
public class BuyerOrderStatusLog {

    /**
     * 日志ID
     */
    @ApiModelProperty(value = "日志ID", required = true)
    private String logId;

    /**
     * 订单ID
     */
    @ApiModelProperty(value = "订单ID", required = true)
    private String orderId;

    /**
     * 订单状态
     */
    @ApiModelProperty(value = "订单状态", required = true)
    private Integer orderStatus;

    /**
     * 状态信息
     */
    @ApiModelProperty(value = "状态信息", required = true)
    private String orderMsg;

    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间", required = true)
    private LocalDateTime createTime;
}