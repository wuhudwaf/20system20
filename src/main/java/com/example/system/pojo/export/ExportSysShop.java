package com.example.system.pojo.export;

import com.example.system.utils.excel.ExcelExport;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


/**
 * 门店
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ExportSysShop {

    /**
     * 门店名称
     */
    @ExcelExport(value = "门店名称")
    private String name;

    /**
     * 门店简称
     */
    @ExcelExport(value = "门店简称")
    private String nameSimplification;

    /**
     * 法人代表
     */
    @ExcelExport(value = "法人代表")
    private String lawRepresentative;

    /**
     * 统一社会信用代码
     */
    @ExcelExport(value = "统一社会信用代码")
    private String creditCode;

    /**
     * 门店地址
     */
    @ExcelExport(value = "门店地址")
    private String address;

    /**
     * 门店城市
     */
    @ExcelExport(value = "门店城市")
    private String shopCity;

    /**
     * 所在行政区域
     */
    @ExcelExport(value = "所在行政区域")
    private String administrativeRegion;

    /**
     * 所属街道
     */
    @ExcelExport(value = "所属街道")
    private String affiliationStreet;

    /**
     * 门店分类
     */
    @ExcelExport(value = "门店分类")
    private String shopCategory;

    /**
     * 营业时间
     */
    @ExcelExport(value = "营业时间")
    private String businessHours;

    /**
     * 门店状态 1开店 0闭店
     */
    @ExcelExport(value = "营业状态", kv = "1-开店;0-关店")
    private Integer status;

    /**
     * 门店服务电话
     */
    @ExcelExport(value = "门店服务电话")
    private String phone;

    /**
     * 门店负责人
     */
    @ExcelExport(value = "门店负责人")
    private String admin;

    /**
     * 门店客服微信号
     */
    @ExcelExport(value = "门店客服微信号")
    private String shopServiceWechat;

    /**
     * 是否接收门店自取
     */
    @ExcelExport(value = "是否接收门店自取", kv = "1-是;0-否")
    private String isAndNoAcceptStoreSelfPickup;
}