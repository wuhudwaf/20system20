package com.example.system.pojo;

import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@NoArgsConstructor
@AllArgsConstructor
@TableName("delivery_fee_base_config")
@ApiModel("配送费配置")
public class DeliveryFeeBaseConfig {

    /**
     * 基础配置ID
     */
    @ApiModelProperty(value = "基础配置ID", required = false)
    private String baseConfigId;

    /**
     * 厂商名称
     */
    @ApiModelProperty(value = "厂商名称", required = false)
    private String manufacturerName;

    /**
     * 厂商编码
     */
    @ApiModelProperty(value = "厂商编码", required = false)
    private String manufacturerCode;

    /**
     * 配送类型 1：快递 2：同城
     */
    @ApiModelProperty(value = "配送类型 1：快递 2：同城", required = false)
    private Integer deliveryType;

    /**
     * 状态 1：启用 0：停用
     */
    @ApiModelProperty(value = "状态 1：启用 0：停用", required = false)
    private Integer status;

    /**
     * 基础配送费
     */
    @ApiModelProperty(value = "基础配送费", required = false)
    private BigDecimal baseDeliveryFee;

    /**
     * 基础配送时效
     */
    @ApiModelProperty(value = "基础配送时效", required = false)
    private String baseDeliveryAgeing;

    /**
     * 起始夜间服务时间
     */
    @ApiModelProperty(value = "起始夜间服务时间", required = false)
    private String startNightService;

    /**
     * 结束夜间服务时间
     */
    @ApiModelProperty(value = "结束夜间服务时间", required = false)
    private String endNightService;

    /**
     * 夜间服务金额
     */
    @ApiModelProperty(value = "夜间服务金额", required = false)
    private BigDecimal nightServiceFee;
}