package com.example.system.pojo;

import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

/**
 * 用户优惠券
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@TableName("buyer_coupon")
@ApiModel("用户优惠券")
public class BuyerCoupon {

    /**
     * id
     */
    @ApiModelProperty(value = "id", required = false)
    private String id;

    /**
     * 用户id
     */
    @ApiModelProperty(value = "用户id", required = true)
    private String userId;

    /**
     * 优惠券ID
     */
    @ApiModelProperty(value = "优惠券ID", required = true)
    private String couponId;

    /**
     * 优惠券状态 1：未使用 2：已使用 3：已过期 4：已作废
     */
    @ApiModelProperty(value = "优惠券状态 1：未使用 2：已使用 3：已过期 4：已作废", required = true)
    private Integer status;

    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间", required = false)
    private LocalDateTime createTime;

    /**
     * 生效时间
     */
    @ApiModelProperty(value = "生效时间", required = true)
    private LocalDateTime effectTime;

    /**
     * 失效时间
     */
    @ApiModelProperty(value = "失效时间", required = true)
    private LocalDateTime expireTime;

    /**
     * 使用时间
     */
    @ApiModelProperty(value = "使用时间", required = true)
    private LocalDateTime usageTime;

    /**
     * 作废操作人ID
     */
    @ApiModelProperty(value = "作废操作人ID", required = false)
    private String cancelOperator;

    /**
     * 作废时间
     */
    @ApiModelProperty(value = "作废时间", required = false)
    private LocalDateTime cancelTime;

    /**
     * 获取类型 1：注册赠送 2：邀请赠送 3：开通会员赠送 4：积分兑换
     */
    @ApiModelProperty(value = "获取类型 1：注册赠送 2：邀请赠送 3：开通会员赠送 4：积分兑换", required = true)
    private Integer gainType;
}