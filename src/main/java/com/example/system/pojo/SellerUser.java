package com.example.system.pojo;

import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 门店员工
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
@TableName("seller_user")
@ApiModel("门店员工")
public class SellerUser implements Serializable {

    /**
     * 冻结和解冻
     *
     * @param id
     * @param delFlag
     */
    public SellerUser(String id, Integer delFlag) {
        this.id = id;
        this.delFlag = delFlag;
    }

    /**
     * id
     */
    @ApiModelProperty(value = "id", required = false)
    private String id;

    /**
     * 姓名
     */
    @ApiModelProperty(value = "姓名", required = true)
    @NotBlank(message = "请输入姓名")
    private String nickName;

    /**
     * 手机号
     */
    @ApiModelProperty(value = "手机号", required = true)
    @NotBlank(message = "手机号不得为空")
    @Length(min = 11, max = 11, message = "请正确输入手机号")
    private String phone;

    /**
     * 账号
     */
    @ApiModelProperty(value = "账号", required = false)
    private String userName;

    /**
     * 密码
     */
    @ApiModelProperty(value = "密码", required = true)
    private String pwd;

    /**
     * 审批 1为审批通过,0待审批
     */
    @ApiModelProperty(value = "审批 1为审批通过,0待审批", required = false)
    private Integer status;

    /**
     * 0删除 1正常
     */
    @ApiModelProperty(value = " 0删除 1正常", required = false)
    private Integer delFlag;

    /**
     * 门店id
     */
    @ApiModelProperty(value = "门店id", required = true)
    @NotNull(message = "请选择店铺")
    private String shopId;
}