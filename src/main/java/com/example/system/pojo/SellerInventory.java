package com.example.system.pojo;

import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * sku库存表
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@TableName("seller_inventory")
@ApiModel("sku库存")
public class SellerInventory {

    /**
     * 主键
     */
    @ApiModelProperty(value = "主键", required = false)
    private String id;

    /**
     * sku表id
     */
    @ApiModelProperty(value = "sku表id", required = true)
    private String skuId;

    /**
     * 商品id
     */
    @ApiModelProperty(value = "商品id", required = true)
    private String goodsId;

    /**
     * 库存
     */
    @ApiModelProperty(value = "库存", required = true)
    private String quantity;

    /**
     * 乐观锁
     */
    @ApiModelProperty(value = "乐观锁", required = false)
    private Integer revision;

    /**
     * 门店id
     */
    @ApiModelProperty(value = "门店id", required = true)
    private String shopId;
}