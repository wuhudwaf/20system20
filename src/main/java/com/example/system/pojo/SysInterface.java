package com.example.system.pojo;

import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;


/**
 * 权限/接口
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@TableName("sys_interface")
@ApiModel("权限/接口")
public class SysInterface {

    /**
     * ID
     */
    @ApiModelProperty(value = "ID", required = false)
    private String id;

    /**
     * 权限名称
     */
    @ApiModelProperty(value = "权限名称", required = true)
    @NotBlank(message = "请输入权限名称")
    private String menuName;

    /**
     * 权限路径
     */
    @ApiModelProperty(value = "权限路径", required = true)
    @NotBlank(message = "请输入权限路径")
    private String permissions;

    /**
     * 备注
     */
    @ApiModelProperty(value = "备注", required = false)
    private String remark;
}