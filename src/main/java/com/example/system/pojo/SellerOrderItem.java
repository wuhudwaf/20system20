package com.example.system.pojo;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * 商品订单表
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@TableName("seller_order_item")
@ApiModel("商品订单")
public class SellerOrderItem {

    /**
     * 订单子表id
     */
    @ApiModelProperty(value = "订单子表id", required = true)
    private String id;

    /**
     * 订单id
     */
    @ApiModelProperty(value = "订单id", required = true)
    private String orderId;

    /**
     * 用户表id
     */
    @ApiModelProperty(value = "用户表id", required = true)
    private String userId;

    /**
     * 商户id
     */
    @ApiModelProperty(value = "商户id", required = true)
    private String shopId;

    /**
     * 商品id
     */
    @ApiModelProperty(value = "商品id", required = true)
    private String goodsId;

    /**
     * sku id
     */
    @ApiModelProperty(value = "sku id", required = true)
    private String skuId;

    /**
     * 商品名称
     */
    @ApiModelProperty(value = "商品名称", required = true)
    private String goodsName;

    /**
     * 商品图片地址
     */
    @ApiModelProperty(value = "商品图片地址", required = true)
    private String goodsPicture;

    /**
     * 生成订单时的商品单价，单位是元,保留两位小数
     */
    @ApiModelProperty(value = "生成订单时的商品单价，单位是元,保留两位小数", required = true)
    private BigDecimal goodsPrice;

    /**
     * 商品数量
     */
    @ApiModelProperty(value = "商品数量", required = true)
    private Integer quantity;

    /**
     * 商品总价,单位是元,保留两位小数
     */
    @ApiModelProperty(value = "商品总价,单位是元,保留两位小数", required = true)
    private BigDecimal totalPrice;

    /**
     * 租户号
     */
    @ApiModelProperty(value = "租户号", required = true)
    private String tenantId;

    /**
     * 乐观锁
     */
    @ApiModelProperty(value = "乐观锁", required = true)
    private Integer revision;

    /**
     * 创建人
     */
    @ApiModelProperty(value = "创建人", required = true)
    private String createBy;

    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间", required = true)
    private LocalDateTime createTime;

    /**
     * 更新人
     */
    @ApiModelProperty(value = "更新人", required = true)
    private String updateBy;

    /**
     * 更新时间
     */
    @ApiModelProperty(value = "更新时间", required = true)
    private LocalDateTime updateTime;

    /**
     * 优惠金额，总优惠平均后
     */
    @ApiModelProperty(value = "优惠金额，总优惠平均后", required = true)
    private BigDecimal discountAmount;

    /**
     * 商品对象
     */
    @TableField(exist = false)
    private SysGoods sysGoods;
}