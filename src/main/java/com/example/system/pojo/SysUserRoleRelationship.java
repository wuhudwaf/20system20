package com.example.system.pojo;

import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 管理员角色关系表
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@TableName("sys_user_role_relationship")
@ApiModel("管理员角色关系")
public class SysUserRoleRelationship {

    /**
     * 管理员id
     */
    @ApiModelProperty(value = "管理员id", required = true)
    private String userId;

    /**
     * 角色id
     */
    @ApiModelProperty(value = "角色id", required = true)
    private String roleId;
}