package com.example.system.pojo;

import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
@TableName("sys_config")
@ApiModel("参数配置")
public class SysConfig {

    /**
     * 参数主键
     */
    @ApiModelProperty(value = "参数主键", required = false)
    private String configId;

    /**
     * 参数名称
     */
    @ApiModelProperty(value = "参数名称", required = false)
    private String configName;

    /**
     * 参数键名
     */
    @ApiModelProperty(value = "参数键名", required = false)
    private String configKey;

    /**
     * 参数键值
     */
    @ApiModelProperty(value = "参数键值", required = false)
    private String configValue;

    /**
     * 系统内置（y是 n否）
     */
    @ApiModelProperty(value = "系统内置（y是 n否）", required = false)
    private String configType;

    /**
     * 创建者
     */
    @ApiModelProperty(value = "创建者", required = false)
    private String createBy;

    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间", required = false)
    private LocalDateTime createTime;

    /**
     * 更新者
     */
    @ApiModelProperty(value = "更新者", required = false)
    private String updateBy;

    /**
     * 更新时间
     */
    @ApiModelProperty(value = "更新时间", required = false)
    private LocalDateTime updateTime;

    /**
     * 备注
     */
    @ApiModelProperty(value = "备注", required = false)
    private String remark;
}