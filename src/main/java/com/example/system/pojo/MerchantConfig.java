package com.example.system.pojo;

import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

/**
 * 商户信息配置
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@TableName("merchant_config")
@ApiModel("商户信息配置")
public class MerchantConfig {

    /**
     * 商户配置ID
     */
    @ApiModelProperty(value = "商户配置ID", required = false)
    private String merchantConfigId;

    /**
     * 支付商户号
     */
    @ApiModelProperty(value = "支付商户号", required = false)
    private String merchantId;

    /**
     * 支付商户KEY
     */
    @ApiModelProperty(value = "支付商户KEY", required = false)
    private String merchantKey;

    /**
     * 密钥文件存放地址
     */
    @ApiModelProperty(value = "密钥文件存放地址", required = false)
    private String certPath;

    /**
     * 小程序商户序列号
     */
    @ApiModelProperty(value = "小程序商户序列号", required = false)
    private String merchantSerialNo;

    /**
     * 状态 0：停用 1：启用
     */
    @ApiModelProperty(value = "状态 0：停用 1：启用", required = false)
    private Integer status;

    /**
     * 创建人
     */
    @ApiModelProperty(value = "创建人", required = false)
    private String createUserId;

    /**
     * 更新人
     */
    @ApiModelProperty(value = "更新人", required = false)
    private String updateUserId;

    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间", required = false)
    private LocalDateTime createTime;

    /**
     * 最后更新时间
     */
    @ApiModelProperty(value = "最后更新时间", required = false)
    private LocalDateTime lastUpdateTime;

    /**
     * 小程序配置ID
     */
    @ApiModelProperty(value = "小程序配置ID", required = false)
    private String appletConfigId;
}