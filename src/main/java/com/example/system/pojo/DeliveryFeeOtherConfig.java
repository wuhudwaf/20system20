package com.example.system.pojo;

import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@NoArgsConstructor
@AllArgsConstructor
@TableName("delivery_fee_other_config")
@ApiModel("配送费其他配置")
public class DeliveryFeeOtherConfig {

    /**
     * 其他配置ID
     */
    @ApiModelProperty(value = "其他配置ID", required = false)
    private String otherConfigId;

    /**
     * 基础配置ID
     */
    @ApiModelProperty(value = "基础配置ID", required = false)
    private String baseConfigId;

    /**
     * 阶段配置类型 1：特殊品类附加费 2：节日服务费
     */
    @ApiModelProperty(value = "阶段配置类型 1：特殊品类附加费 2：节日服务费", required = false)
    private Integer otherConfigType;

    /**
     * 其他标识
     */
    @ApiModelProperty(value = "其他标识", required = false)
    private String otherId;

    /**
     * 其他费用
     */
    @ApiModelProperty(value = "其他费用", required = false)
    private BigDecimal otherFee;
}