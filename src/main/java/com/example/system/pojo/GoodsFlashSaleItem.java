package com.example.system.pojo;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * 商品限时秒杀项目信息
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@TableName("goods_flash_sale_item")
@ApiModel("商品限时秒杀项目信息")
public class GoodsFlashSaleItem {

    /**
     * 商品限时秒杀项目信息id
     */
    @TableId("item_id")
    @ApiModelProperty(value = "商品限时秒杀项目信息id", required = false)
    private String itemId;

    /**
     * 限时秒杀ID
     */
    @ApiModelProperty(value = "限时秒杀ID", required = false)
    private String flashSaleId;

    /**
     * 商品SkuId
     */
    @NotNull(message = "商品SkuId不得为空")
    @ApiModelProperty(value = "商品SkuId", required = true)
    private String goodsSkuId;

    /**
     * 限时秒杀价格
     */
    @NotNull(message = "限时秒杀价格不得为空")
    @ApiModelProperty(value = "限时秒杀价格", required = true)
    private BigDecimal flashSalePrice;

    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间", required = false)
    private LocalDateTime createTime;

    /**
     * 最后修改时间
     */
    @ApiModelProperty(value = "最后修改时间", required = false)
    private LocalDateTime lastUpdateTime;

    /**
     * 创建人
     */
    @ApiModelProperty(value = "创建人", required = false)
    private String createBy;

    /**
     * 修改人
     */
    @ApiModelProperty(value = "修改人", required = false)
    private String updateBy;
}