package com.example.system.pojo;

import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import java.time.LocalDateTime;

/**
 * 轮播图
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@TableName("sys_banner")
@ApiModel("轮播图")
public class SysBanner {

    /**
     * 主键ID
     */
    @ApiModelProperty(value = "主键ID", required = false)
    private String bannerId;

    /**
     * 标题
     */
    @NotBlank(message = "请输入标题")
    @ApiModelProperty(value = "标题", required = true)
    private String title;

    /**
     * 图片地址
     */
    @NotBlank(message = "请输入图片地址")
    @ApiModelProperty(value = "图片地址", required = true)
    private String imagePath;

    /**
     * 跳转路径
     */
//    @NotBlank(message = "请输入跳转路径")
    @ApiModelProperty(value = "跳转路径", required = false)
    private String jumpPath;

    /**
     * 状态 0：下架 1：上架
     */
    @ApiModelProperty(value = "状态 0：下架 1：上架", required = false)
    private Integer status;

    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间", required = false)
    private LocalDateTime createTime;

    /**
     * 创建人
     */
    @ApiModelProperty(value = "创建人", required = false)
    private String createBy;

    /**
     * 最后更新时间
     */
    @ApiModelProperty(value = "最后更新时间", required = false)
    private LocalDateTime lastUpdateTime;

    /**
     * 最后更新人
     */
    @ApiModelProperty(value = "最后更新人", required = false)
    private String lastUpdateBy;

    /**
     * 排序
     */
    @ApiModelProperty(value = "排序", required = false)
    private Integer sort;
}