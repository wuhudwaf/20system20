package com.example.system.pojo;

import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

/**
 * 用户收藏
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@TableName("buyer_collect")
@ApiModel("用户收藏")
public class BuyerCollect {

    /**
     * 收藏ID
     */
    @ApiModelProperty(value = "收藏ID", required = false)
    private String collectId;

    /**
     * 用户ID
     */
    @ApiModelProperty(value = "用户ID", required = true)
    private String userId;

    /**
     * 收藏类型 1：商品 2：门店
     */
    @ApiModelProperty(value = "收藏类型 1：商品 2：门店", required = true)
    private Integer collectType;

    /**
     * 收藏内容ID
     */
    @ApiModelProperty(value = "收藏内容ID", required = true)
    private String collectObjectId;

    /**
     * 收藏时间
     */
    @ApiModelProperty(value = "收藏时间", required = false)
    private LocalDateTime collectTime;

    /**
     * 商品SkuId
     */
    @ApiModelProperty(value = "商品SkuId", required = true)
    private String goodsSkuId;
}