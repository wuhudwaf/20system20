package com.example.system.pojo.imports;

import com.alibaba.fastjson.JSONObject;
import com.example.system.utils.excel.ExcelExport;
import com.example.system.utils.excel.ExcelImport;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

/**
 * 导入导出
 */
@Data
public class SysGoodsImport {

    /**
     * 商品主键
     */
    private String goodsId;

    /**
     * 类别id
     */
    private String cateIds;

    /**
     * 商品条码
     */
    @ExcelExport(value = "商品条码")
    @ExcelImport(value = "商品条码", required = true)
    private String barCode;

    /**
     * 商品编码
     */
    @ExcelExport(value = "商品编码")
    @ExcelImport(value = "商品编码", required = true)
    private String number;

    /**
     * 商品名称
     */
    @ExcelExport(value = "商品名称")
    @ExcelImport(value = "商品名称", required = true)
    private String name;

    /**
     * 商品规格
     */
    @ExcelExport(value = "商品规格")
    @ExcelImport(value = "商品规格", required = true)
    private String skuVal;

    /**
     * 单位
     */
    @ExcelExport(value = "单位")
    @ExcelImport(value = "单位", required = true)
    private String unit;

    /**
     * 一级分类
     */
    @ExcelExport(value = "一级分类")
    @ExcelImport(value = "一级分类", required = true)
    private String cateIdOne;

    /**
     * 二级分类
     */
    @ExcelExport(value = "二级分类")
    @ExcelImport(value = "二级分类", required = true)
    private String cateIdTwo;

    /**
     * 三级分类
     */
    @ExcelExport(value = "三级分类")
    @ExcelImport(value = "三级分类", required = true)
    private String cateId;

    /**
     * 成本价
     */
    @ExcelExport(value = "成本价")
    @ExcelImport(value = "成本价", required = true)
    private String costPrice;

    /**
     * 零售价格
     */
    @ExcelExport(value = "零售价格")
    @ExcelImport(value = "零售价格", required = true)
    private BigDecimal price;

    /**
     * 库存数量
     */
    @ExcelExport(value = "库存数量")
    @ExcelImport(value = "库存数量", required = true)
    private String quantity;

    /**
     * 重量
     */
    @ExcelExport(value = "重量")
    @ExcelImport(value = "重量", required = true)
    private BigDecimal weight;

    /**
     * 税率
     */
    @ExcelExport(value = "税率")
    @ExcelImport(value = "税率", required = true)
    private String taxRate;

    /**
     * 佣金比例
     */
    @ExcelExport(value = "佣金比例")
    @ExcelImport(value = "佣金比例", required = true)
    private BigDecimal commission;

    /**
     * 最低限价
     */
    @ExcelExport(value = "最低限价")
    @ExcelImport(value = "最低限价", required = false)
    private String priceFloor;

    /**
     * 限购期限开始时间
     */
    @ExcelExport(value = "限购期限开始时间")
    @ExcelImport(value = "限购期限开始时间", required = false)
    private String startingTimeOfPurchaseRestrictionPeriod;

    /**
     * 限购期限结束时间
     */
    @ExcelExport(value = "限购期限结束时间")
    @ExcelImport(value = "限购期限结束时间", required = false)
    private String endTimeOfPurchaseRestrictionPeriod;

    /**
     * 限购方式
     */
    @ExcelExport(value = "限购方式")
    @ExcelImport(value = "限购方式", required = false)
    private String purchaseRestrictionMethod;

    /**
     * 限购次数
     */
    @ExcelExport(value = "限购次数")
    @ExcelImport(value = "限购次数", required = false)
    private String stock;

    /**
     * 配送方式
     */
    @ExcelExport(value = "配送方式")
    @ExcelImport(value = "配送方式", required = false)
    private String deliveryMethod;

    /**
     * 商品状态
     */
    @ExcelExport(value = "商品状态", kv = "0-下架;1-上架")
    @ExcelImport(value = "商品状态", kv = "0-下架;1-上架", required = false)
    private Integer status;

    /**
     * 划线价
     */
    @ExcelExport(value = "划线价")
    @ExcelImport(value = "划线价", required = false)
    private String oldPrice;

    /**
     * 开售时间
     */
    @ExcelExport(value = "开售时间")
    @ExcelImport(value = "开售时间", required = false)
    private String openingTime;

    /**
     * 定时下架时间
     */
    @ExcelExport(value = "定时下架时间")
    @ExcelImport(value = "定时下架时间", required = false)
    private String timedOffShelfTime;
}