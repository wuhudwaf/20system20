package com.example.system.pojo;

import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

/**
 * 商品搜索记录表
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("goods_query_log")
@ApiModel("商品搜索记录")
public class GoodsQueryLog {

    /**
     * 记录ID
     */
    @ApiModelProperty(value = "记录ID", required = true)
    private String logId;

    /**
     * 用户ID
     */
    @ApiModelProperty(value = "用户ID", required = true)
    private String userId;

    /**
     * 查询内容
     */
    @ApiModelProperty(value = "查询内容", required = true)
    private String queryValue;

    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间", required = true)
    private LocalDateTime createTime;
}