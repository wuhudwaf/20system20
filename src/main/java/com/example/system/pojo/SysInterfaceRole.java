package com.example.system.pojo;

import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 角色权限关联表
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@TableName("sys_interface_role")
@ApiModel("角色权限关联")
public class SysInterfaceRole {

    /**
     * 角色ID
     */
    @ApiModelProperty(value = "角色ID", required = true)
    private String roleId;

    /**
     * 接口/权限ID
     */
    @ApiModelProperty(value = "接口/权限ID", required = true)
    private String menuId;
}