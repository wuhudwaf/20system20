package com.example.system.pojo;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.List;

/**
 * 商品类别
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("sys_category")
@ApiModel("商品类别")
public class SysCategory {

    public SysCategory(String cateId, String parentId, String name, String picture, Integer status, Integer sortOrder, String tenantId, Integer revision, String createBy, LocalDateTime createTime, String updateBy, LocalDateTime updateTime) {
        this.cateId = cateId;
        this.parentId = parentId;
        this.name = name;
        this.picture = picture;
        this.status = status;
        this.sortOrder = sortOrder;
        this.tenantId = tenantId;
        this.revision = revision;
        this.createBy = createBy;
        this.createTime = createTime;
        this.updateBy = updateBy;
        this.updateTime = updateTime;
    }

    /**
     * 类别id
     */
    @TableId("cate_id")
    @ApiModelProperty(value = "类别id", required = false)
    private String cateId;

    /**
     * 父类别id当id=0时说明是根节点,一级类别
     */
    @ApiModelProperty(value = "父类别id当id=0时说明是根节点,一级类别", required = true)
    @NotNull(message = "请输入父类别id")
    private String parentId;

    /**
     * 类别名称
     */
    @ApiModelProperty(value = "类别名称", required = true)
    @NotBlank(message = "请输入类别名称")
    private String name;

    /**
     * 类别图片
     */
    @ApiModelProperty(value = "类别图片", required = true)
    @NotBlank(message = "请选择类别图片")
    private String picture;

    /**
     * 类别状态1-正常,2-已废弃
     */
    @ApiModelProperty(value = "类别状态1-正常,2-已废弃", required = false)
    private Integer status;

    /**
     * 排序编号,同类展示顺序,数值相等则自然排序
     */
    @ApiModelProperty(value = "排序编号,同类展示顺序,数值相等则自然排序", required = false)
    @NotNull(message = "请输入排序编号")
    private Integer sortOrder;

    /**
     * 租户号
     */
    @ApiModelProperty(value = "租户号", required = false)
    private String tenantId;

    /**
     * 乐观锁
     */
    @ApiModelProperty(value = "乐观锁", required = false)
    private Integer revision;

    /**
     * 创建人
     */
    @ApiModelProperty(value = "创建人", required = false)
    private String createBy;

    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间", required = false)
    private LocalDateTime createTime;

    /**
     * 更新人
     */
    @ApiModelProperty(value = "更新人", required = false)
    private String updateBy;

    /**
     * 更新时间
     */
    @ApiModelProperty(value = "更新时间", required = false)
    private LocalDateTime updateTime;

    @TableField(exist = false)
    private List<SysCategory> list;
}