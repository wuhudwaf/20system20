package com.example.system.pojo;

import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

/**
 * 用户邀请表
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@TableName("buyer_invite")
@ApiModel("用户邀请")
public class BuyerInvite {

    /**
     * 邀请ID
     */
    @ApiModelProperty(value = "邀请ID", required = false)
    private String inviteId;

    /**
     * 用户id
     */
    @ApiModelProperty(value = "用户id", required = true)
    private String userId;

    /**
     * 被邀请人id
     */
    @ApiModelProperty(value = "被邀请人id", required = true)
    private String invitedId;

    /**
     * 赠送优惠券ID
     */
    @ApiModelProperty(value = "赠送优惠券ID", required = true)
    private String giveCouponId;

    /**
     * 邀请时间
     */
    @ApiModelProperty(value = "邀请时间", required = false)
    private LocalDateTime inviteDate;

    /**
     * 奖励信息
     */
    @ApiModelProperty(value = "奖励信息", required = true)
    private String rewardInfo;
}