package com.example.system.pojo;

import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;

/**
 * 通知公告表
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@TableName("sys_notice")
@ApiModel("通知公告")
public class SysNotice {

    /**
     * 公告id
     */
    @ApiModelProperty(value = "公告id", required = false)
    private String noticeId;

    /**
     * 公告标题
     */
    @ApiModelProperty(value = "公告标题", required = true)
    @NotBlank(message = "公告标题不得为空")
    private String noticeTitle;

    /**
     * 公告类型（1通知 2公告）
     */
    @ApiModelProperty(value = "公告类型（1通知 2公告）", required = true)
    @NotBlank(message = "公告类型不得为空")
    @Size(max = 1, min = 1, message = "长度异常")
    private String noticeType;

    /**
     * 公告内容
     */

    @ApiModelProperty(value = "公告内容", required = true)
    @NotBlank(message = "公告内容不得为空")
//    @Column(name = "noticeContent",columnDefinition = "BLOB(65535)")
    private String noticeContent;

    /**
     * 公告状态（0正常 1关闭）
     */
    @ApiModelProperty(value = "公告状态（0正常 1关闭）", required = false)
    @NotBlank(message = "公告类型不得为空")
    private String status;

    /**
     * 创建者
     */
    @ApiModelProperty(value = "创建者", required = false)
    private String createBy;

    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间", required = false)
    private LocalDateTime createTime;

    /**
     * 更新者
     */
    @ApiModelProperty(value = "更新者", required = false)
    private String updateBy;

    /**
     * 更新时间
     */
    @ApiModelProperty(value = "更新时间", required = false)
    private LocalDateTime updateTime;

    /**
     * 备注
     */
    @ApiModelProperty(value = "备注", required = true)
    @NotBlank(message = "备注不得为空")
    private String remark;

}