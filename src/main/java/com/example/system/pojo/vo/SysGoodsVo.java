package com.example.system.pojo.vo;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.example.system.pojo.GoodsFlashSaleItem;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Data
public class SysGoodsVo {

    /**
     * 编号
     */
    @TableId("goods_id")
    @ApiModelProperty(value = "id", required = false)
    private String goodsId;

    /**
     * 类别id
     */
    @ApiModelProperty(value = "类别id", required = true)
    @NotNull(message = "请选择分类")
    private String cateId;

    /**
     * 商品主图
     */
    @ApiModelProperty(value = "商品主图", required = true)
    @NotBlank(message = "请选择商品主图")
    private String picture;

    /**
     * 商品名称
     */
    @ApiModelProperty(value = "商品名称", required = true)
    @NotBlank(message = "请输入商品主图")
    private String name;

    /**
     * 商品编号
     */
    @ApiModelProperty(value = "商品编号", required = true)
    @NotNull(message = "请输入商品编号")
    private Integer number;

    /**
     * 轮播图
     */
    @ApiModelProperty(value = "轮播图", required = true)
    @NotNull(message = "请选择轮播图")
    private String otherPictures;

    /**
     * 爆款推荐
     */
    @ApiModelProperty(value = "爆款推荐", required = true)
    private Integer bktj;

    /**
     * 热销榜单
     */
    @ApiModelProperty(value = "热销榜单", required = true)
    private Integer rxbd;

    /**
     * 名酒榜单
     */
    @ApiModelProperty(value = "名酒榜单", required = true)
    private Integer mjbd;

    /**
     * 超值特惠
     */
    @ApiModelProperty(value = "超值特惠", required = true)
    private Integer czth;

    /**
     * 特价专区
     */
    @ApiModelProperty(value = "特价专区", required = true)
    private Integer tjzq;

    /**
     * 销量
     */
    @ApiModelProperty(value = "销量", required = false)
    private Integer salesVolume;

    /**
     * 租户号
     */
    @ApiModelProperty(value = "租户号", required = false)
    private String tenantId;

    /**
     * 乐观锁
     */
    @ApiModelProperty(value = "乐观锁", required = false)
    private Integer revision;

    /**
     * 创建人
     */
    @ApiModelProperty(value = "创建人", required = false)
    private String createBy;

    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间", required = false)
    private LocalDateTime createTime;

    /**
     * 更新人
     */
    @ApiModelProperty(value = "更新人", required = false)
    private String updateBy;

    /**
     * 更新时间
     */
    @ApiModelProperty(value = "更新时间", required = false)
    private LocalDateTime updateTime;

    /**
     * 细节
     */
    @ApiModelProperty(value = "细节", required = false)
    private String detail;

    /**
     * 1 仓库中, 2 销售中
     */
    @ApiModelProperty(value = "商品状态", required = true)
    @NotNull(message = "请选择商品状态")
    private Integer status;

    /**
     * 好评率
     */
    @ApiModelProperty(value = "好评率", required = false)
    private float reviewRate;

    /**
     * 库存
     */
    @ApiModelProperty(value = "库存", required = true)
    @NotNull(message = "请输入库存")
    private Integer inventory;

    /**
     * 商品限时秒杀项目信息
     */
    @TableField(exist = false)
    @ApiModelProperty(value = "商品限时秒杀项目信息", required = false)
    private List<GoodsFlashSaleItem> goodsFlashSaleItems = new ArrayList<>();
}