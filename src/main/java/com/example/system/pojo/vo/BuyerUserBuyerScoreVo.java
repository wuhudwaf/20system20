package com.example.system.pojo.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 用户积分明细
 */
@Data
@ApiModel("用户积分明细")
public class BuyerUserBuyerScoreVo {

    /**
     * 用户昵称
     */
    @ApiModelProperty("用户昵称")
    private String userName;

    /**
     * 用户编号
     */
    @ApiModelProperty("用户编号")
    private String userId;

    /**
     * 手机号码
     */
    @ApiModelProperty("手机号码")
    private String mobile;

    /**
     * 变动积分
     */
    @ApiModelProperty("变动积分")
    private Integer score;

    /**
     * 剩余积分
     */
    @ApiModelProperty("剩余积分")
    private Integer userScore;

    /**
     * 变动类型
     *  1：新增 2：减少
     */
    @ApiModelProperty("变动类型")
    private Integer type;

    /**
     * 来源单号
     */
    @ApiModelProperty("来源单号")
    private String associationId;

    /**
     * 记录时间
     */
    @ApiModelProperty("记录时间")
    private String createTime;
}