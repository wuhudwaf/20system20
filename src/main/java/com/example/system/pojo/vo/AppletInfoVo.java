package com.example.system.pojo.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;

@Data
public class AppletInfoVo {

    /**
     * 信息ID
     */
    @ApiModelProperty(value = "信息ID", required = false)
    private String infoId;

    /**
     * 小程序配置ID
     */
    @ApiModelProperty(value = "小程序配置ID", required = true)
    private String appletConfigId;

    /**
     * 客服电话，多个电话以逗号分割
     */
    @ApiModelProperty(value = "客服电话，多个电话以逗号分割", required = true)
    private String customerServiceTelephone;

    /**
     * 用户服务协议
     */
    @ApiModelProperty(value = "用户服务协议", required = true)
    private String userServiceAgreement;

    /**
     * 隐私政策
     */
    @ApiModelProperty(value = "隐私政策", required = true)
    private String privacyPolicy;

    /**
     * 企业信息
     */
    @ApiModelProperty(value = "企业信息", required = true)
    private String enterpriseInformation;

    /**
     * 创建人
     */
    @ApiModelProperty(value = "创建人", required = false)
    private String createBy;

    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间", required = false)
    private LocalDateTime createTime;

    /**
     * 更新人
     */
    @ApiModelProperty(value = "更新人", required = false)
    private String updateBy;

    /**
     * 最后更新时间
     */
    @ApiModelProperty(value = "最后更新时间", required = false)
    private LocalDateTime lastUpdateTime;

    /**
     * 会员协议
     */
    @ApiModelProperty(value = "会员协议", required = true)
    private String membershipAgreement;
}
