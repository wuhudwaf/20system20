package com.example.system.pojo.vo;

import com.example.system.pojo.BuyerUser;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class BuyerUserVo {

    /**
     * 总金额
     */
    private BigDecimal money;

    /**
     * 用户集合
     */
    private List<BuyerUser> buyerUsers;
}