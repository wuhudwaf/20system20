package com.example.system.pojo.vo.query;

import com.baomidou.mybatisplus.annotation.TableId;
import com.example.system.pojo.page.Page;
import com.example.system.utils.excel.ExcelExport;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
public class SysShopQueryVo extends Page {

    /**
     * id
     */
    @ExcelExport(value = "主键")
    @TableId("shop_id")
    @ApiModelProperty(value = "id", required = false)
    private String shopId;

    /**
     * 门店名称
     */
    @ExcelExport(value = "门店名称")
    @ApiModelProperty(value = "门店名称", required = true)
    @NotBlank(message = "请输入门店名称")
    private String name;

    /**
     * 门店图片
     */
    @ExcelExport(value = "门店图片")
    @ApiModelProperty(value = "门店图片", required = true)
    @NotBlank(message = "请输入门店图片")
    private String picture;

    /**
     * 门店地址
     */
    @ExcelExport(value = "门店地址")
    @ApiModelProperty(value = "门店地址", required = true)
    @NotBlank(message = "请输入门店地址")
    private String address;

    /**
     * 门店负责人
     */
    @ExcelExport(value = "门店负责人")
    @ApiModelProperty(value = "门店负责人", required = true)
    @NotBlank(message = "请输入门店负责人")
    private String admin;

    /**
     * 是否审批员工 0不审批，1审批
     */
    @ExcelExport(value = "是否审批员工",kv = "0-不审批;1-审批")
    @ApiModelProperty(value = "是否审批员工 0不审批，1审批", required = true)
    @NotNull(message = "请选择是否审批员工")
    private Integer staffSet;

    /**
     * 门店电话
     */
    @ExcelExport(value = "门店电话")
    @ApiModelProperty(value = "门店电话", required = true)
    @NotBlank(message = "请输入门店电话")
    private String phone;

    /**
     * 营业开始时间
     */
    @ExcelExport(value = "营业开始时间")
    @ApiModelProperty(value = "营业开始时间", required = true)
    @NotBlank(message = "请输入营业开始时间")
    private String openHours;

    /**
     * 营业结束时间
     */
    @ExcelExport(value = "营业结束时间")
    @ApiModelProperty(value = "营业结束时间", required = true)
    @NotBlank(message = "请输入营业结束时间")
    private String closeHours;

    /**
     * 租户号
     */
    @ExcelExport(value = "租户号")
    @ApiModelProperty(value = "租户号", required = false)
    private String tenantId;

    /**
     * 乐观锁
     */
    @ApiModelProperty(value = "乐观锁", required = false)
    private Integer revision;

    /**
     * 创建人
     */
    @ExcelExport(value = "创建人")
    @ApiModelProperty(value = "创建人", required = false)
    private String createBy;

    /**
     * 创建时间
     */
    @ExcelExport(value = "创建时间")
    @ApiModelProperty(value = "创建时间", required = false)
    private LocalDateTime createTime;

    /**
     * 更新人
     */
    @ExcelExport(value = "更新人")
    @ApiModelProperty(value = "更新人", required = false)
    private String updateBy;

    /**
     * 更新时间
     */
    @ExcelExport(value = "更新时间")
    @ApiModelProperty(value = "更新时间", required = false)
    private LocalDateTime updateTime;

    /**
     * 门店详情
     */
    @ExcelExport(value = "门店详情")
    @ApiModelProperty(value = "门店详情", required = false)
//    @NotBlank(message = "请输入门店名称")
    private String detail;

    /**
     * 门店状态 1开店 0闭店
     */
    @ExcelExport(value = "门店状态",kv = "1-开店;0-闭店")
    @ApiModelProperty(value = "门店状态 1开店 0闭店", required = true)
    @NotNull(message = "请选择门店状态")
    private Integer status;

    /**
     * 经度
     */
    @ExcelExport(value = "经度")
    @ApiModelProperty(value = "经度", required = false)
//    @NotNull(message = "请输入门店经度")
    private Double lat;

    /**
     * 维度
     */
    @ExcelExport(value = "维度")
    @ApiModelProperty(value = "维度", required = false)
//    @NotBlank(message = "请输入门店维度")
    private String lon;

    /**
     * 门店综合评分
     */
    @ExcelExport(value = "门店综合评分")
    @ApiModelProperty(value = "门店综合评分", required = false)
    private Double comprehensiveMark;

    /**
     * 门店余额
     */
    @ExcelExport(value = "门店余额")
    @ApiModelProperty(value = "门店余额", required = true)
    @NotNull(message = "请输入门店余额")
    private BigDecimal wallet;

    /**
     * 客服二维码地址
     */
    @ExcelExport(value = "客服二维码地址")
    @ApiModelProperty(value = "客服二维码地址", required = true)
    @NotBlank(message = "请输入客服二维码地址")
    private String customerServiceQrCode;

    /**
     * 证件图地址
     */
    @ExcelExport(value = "证件图地址")
    @ApiModelProperty(value = "证件图地址", required = true)
    @NotBlank(message = "请输入证件图地址")
    private String certificatePath;
}