package com.example.system.pojo.vo;

import com.example.system.pojo.GoodsSku;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

/**
 * 商品 商品sku一对多
 */
@Data
public class SysGoodsGoodsSkuVo {

    /**
     * id
     */
    private String goodsId;

    /**
     * 分类id
     */
    private String cateId;

    /**
     * 商品主图
     */
    private String picture;

    /**
     * 商品名称
     */
    private String name;

    /**
     * 商品编号
     */
    private String number;

    /**
     * 轮播图
     */
    private String otherPictures;

    /**
     * 爆款推荐
     */
    private Integer bktj;

    /**
     * 热销榜单
     */
    private Integer rxbd;

    /**
     * 名酒榜单
     */
    private Integer mjbd;

    /**
     * 超值特惠
     */
    private Integer czth;

    /**
     * 特价专区
     */
    private Integer tjzq;

    /**
     * 销量
     */
    private Integer salesVolume;

    /**
     * 租户号
     */
    private String tenantId;

    /**
     * 乐观锁
     */
    private Integer revision;

    /**
     * 创建人
     */
    private String createBy;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 更新人
     */
    private String updateBy;

    /**
     * 更新时间
     */
    private LocalDateTime updateTime;

    /**
     * 细节
     */
    private String detail;

    /**
     * 1 放入仓库, 2 上架销售
     */
    private Integer status;

    /**
     * 好评率
     */
    private float reviewRate;

    /**
     * 库存
     */
    private Integer inventory;

    /**
     * 参与活动(0：全部，1：未参与，2：拼团活动，3：秒杀活动，4：砍价活动)
     */
    private Integer activity;

    /**
     * 限购期限开始时间
     */
    private String startingTimeOfPurchaseRestrictionPeriod;

    /**
     * 限购期限结束时间
     */
    private String endTimeOfPurchaseRestrictionPeriod;

    /**
     * 限购方式
     */
    private String purchaseRestrictionMethod;

    /**
     * 配送方式
     */
    private String deliveryMethod;

    /**
     * 开售时间
     */
    private Date openingTime;

    /**
     * 定时下架时间
     */
    private Date timedOffShelfTime;

    /**
     * 商品条码
     */
    private String barCode;

    /**
     * 海报
     */
    private String poster;

    /**
     * 商品Sku集合
     */
    private List<GoodsSku> orderReviews;
}