package com.example.system.pojo.vo.add;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.example.system.pojo.GoodsSku;
import com.example.system.utils.excel.ExcelExport;
import com.example.system.utils.excel.ExcelImport;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SysGoodsAdd {

    /**
     * 编号
     */
    @TableId("goods_id")
    @ApiModelProperty(value = "id", required = false)
    private String goodsId;

    /**
     * 类别id
     */
    @ExcelImport(value = "类别", required = true, unique = true)
    @ApiModelProperty(value = "类别id", required = false)
    private String cateId;

    /**
     * 商品主图
     */
    @ExcelExport(value = "商品图片")
    @ExcelImport(value = "商品图片", required = true)
    @ApiModelProperty(value = "商品主图", required = false)
    private String picture;

    /**
     * 商品名称
     */
    @ExcelImport(value = "商品名称", required = true)
    @ExcelExport(value = "商品名称")
    @ApiModelProperty(value = "商品名称", required = false)
    private String name;

    /**
     * 商品编号
     */
    @ExcelImport(value = "商品编号", required = true, unique = true)
    @ApiModelProperty(value = "商品编号", required = false)
    private Integer number;

    /**
     * 轮播图
     */
    @ExcelImport(value = "轮播图", required = true)
    @ApiModelProperty(value = "轮播图", required = false)
    private String otherPictures;

    /**
     * 爆款推荐
     */
    @ExcelImport(value = "爆款推荐", kv = "1-是;0-否", required = true)
    @ExcelExport(value = "爆款推荐", kv = "1-是;0-否")
    @ApiModelProperty(value = "爆款推荐", required = false)
    private Integer bktj;

    /**
     * 热销榜单
     */
    @ExcelImport(value = "热销榜单", kv = "1-是;0-否", required = true)
    @ExcelExport(value = "热销榜单", kv = "1-是;0-否")
    @ApiModelProperty(value = "热销榜单", required = false)
    private Integer rxbd;

    /**
     * 名酒榜单
     */
    @ExcelImport(value = "名酒榜单", kv = "1-是;0-否", required = true)
    @ExcelExport(value = "名酒榜单", kv = "1-是;0-否")
    @ApiModelProperty(value = "名酒榜单", required = false)
    private Integer mjbd;

    /**
     * 超值特惠
     */
    @ExcelImport(value = "超值特惠", kv = "1-是;0-否", required = true)
    @ExcelExport(value = "超值特惠", kv = "1-是;0-否")
    @ApiModelProperty(value = "超值特惠", required = false)
    private Integer czth;

    /**
     * 特价专区
     */
    @ExcelImport(value = "特价专区", kv = "1-是;0-否", required = true)
    @ExcelExport(value = "特价专区", kv = "1-是;0-否")
    @ApiModelProperty(value = "特价专区", required = false)
    private Integer tjzq;

    /**
     * 销量
     */
    @ExcelExport(value = "销量")
    @ApiModelProperty(value = "销量", required = false)
    private Integer salesVolume;

    /**
     * 租户号
     */
    @ExcelExport(value = "租户号")
    @ApiModelProperty(value = "租户号", required = false)
    private String tenantId;

    /**
     * 乐观锁
     */
    @ApiModelProperty(value = "乐观锁", required = false)
    private Integer revision;

    /**
     * 创建人
     */
    @ApiModelProperty(value = "创建人", required = false)
    private String createBy;

    /**
     * 创建时间
     */
    @ExcelExport(value = "创建时间")
    @ApiModelProperty(value = "创建时间", required = false)
    private LocalDateTime createTime;

    /**
     * 更新人
     */
    @ApiModelProperty(value = "更新人", required = false)
    private String updateBy;

    /**
     * 更新时间
     */
    @ApiModelProperty(value = "更新时间", required = false)
    private LocalDateTime updateTime;

    /**
     * 细节
     */
    @ApiModelProperty(value = "细节", required = false)
    private String detail;

    /**
     * 1 放入仓库, 2 上架销售
     */
    @ExcelImport(value = "结果", kv = "1-放入仓库;2-上架销售", required = false)
    @ApiModelProperty(value = "商品状态", required = true)
    private Integer status;

    /**
     * 好评率
     */
    @ExcelExport(value = "好评率")
    @ApiModelProperty(value = "好评率", required = false)
    private float reviewRate;

    /**
     * 库存
     */
    @ExcelImport(value = "库存", required = true)
    @ApiModelProperty(value = "库存", required = false)
    private Integer inventory;

    /**
     * 参与活动(0：全部，1：未参与，2：拼团活动，3：秒杀活动，4：砍价活动)
     */
    @ApiModelProperty(value = "参与活动(0：全部，1：未参与，2：拼团活动，3：秒杀活动，4：砍价活动)", required = false)
    private Integer activity;

    /**
     * 商品sku列表
     */
    @ApiModelProperty(value = "商品sku列表", required = false)
    private List<GoodsSku> goodsSkus = new ArrayList<>();
}