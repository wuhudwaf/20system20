package com.example.system.pojo.vo;

import com.baomidou.mybatisplus.annotation.TableField;
import com.example.system.pojo.GoodsFlashSaleItem;
import com.example.system.pojo.SysGoods;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * 好像返回用的
 */
@Data
public class GoodsFlashSaleVo {

    /**
     * 限时秒杀ID
     */
    @ApiModelProperty(value = "限时秒杀ID", required = false)
    private String flashSaleId;

    /**
     * 限时秒杀名称
     */
    @NotBlank(message = "请输入限时秒杀名称")
    @ApiModelProperty(value = "限时秒杀名称", required = true)
    private String flashSaleName;

    /**
     * 状态 0：下架 1：上架 2：已结束
     */
    @ApiModelProperty(value = "状态 0：未开启秒杀 1：开始秒杀 2：已结束", required = false)
    private Integer status;

    /**
     * 开始时间
     */
    @NotNull(message = "请输入开始时间")
    @ApiModelProperty(value = "开始时间", required = true)
    private Long startTime;

    /**
     * 结束时间
     */
    @NotNull(message = "请输入结束时间")
    @ApiModelProperty(value = "结束时间", required = true)
    private Long endTime;

    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间", required = false)
    private LocalDateTime createTime;

    /**
     * 最后修改时间
     */
    @ApiModelProperty(value = "最后修改时间", required = false)
    private LocalDateTime lastUpdateTime;

    /**
     * 创建人
     */
    @ApiModelProperty(value = "创建人", required = false)
    private String createBy;

    /**
     * 修改人
     */
    @ApiModelProperty(value = "修改人", required = false)
    private String updateBy;

    /**
     * 商品限时秒杀项目信息列表
     */
    @TableField(exist = false)
    @ApiModelProperty(value = "商品限时秒杀项目信息列表", required = true)
    private List<GoodsFlashSaleItem> goodsFlashSaleItems = new ArrayList<>();

    /**
     * 商品列表
     */
    @TableField(exist = false)
    private List<SysGoods> sysGoods = new ArrayList<>();
}