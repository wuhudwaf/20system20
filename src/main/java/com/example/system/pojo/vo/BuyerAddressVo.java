package com.example.system.pojo.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;

@Data
public class BuyerAddressVo {

    /**
     * 地址id
     */
    @ApiModelProperty(value = "ID", required = false)
    private String addressId;

    /**
     * 用户id
     */
    @ApiModelProperty(value = "用户id", required = true)
    private String userId;

    /**
     * 省
     */
    @ApiModelProperty(value = "省", required = true)
    private String province;

    /**
     * 市
     */
    @ApiModelProperty(value = "市", required = true)
    private String city;

    /**
     * 区/县
     */
    @ApiModelProperty(value = "区/县", required = true)
    private String county;

    /**
     * 街道
     */
    @ApiModelProperty(value = "街道", required = true)
    private String street;

    /**
     * 详细信息
     */
    @ApiModelProperty(value = "详细信息", required = true)
    private String lastDetail;

    /**
     * 是否默认 1：默认地址 0 非默认地址
     */
    @ApiModelProperty(value = "是否默认 1：默认地址 0 非默认地址", required = true)
    private Integer type;

    /**
     * 收货姓名
     */
    @ApiModelProperty(value = "收货姓名", required = true)
    private String receiverName;

    /**
     * 收货电话
     */
    @ApiModelProperty(value = "收货电话", required = true)
    private String receiverPhone;

    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间", required = false)
    private LocalDateTime createTime;

    /**
     * 更新时间
     */
    @ApiModelProperty(value = "更新时间", required = false)
    private LocalDateTime lastUpdateTime;

    /**
     * 删除标识 0：已经删除 1：没删除（默认）
     */
    @ApiModelProperty(value = "删除标识 0：已经删除 1：没删除（默认）", required = false)
    private Integer deleteFlag;
}