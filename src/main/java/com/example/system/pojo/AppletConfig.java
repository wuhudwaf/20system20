package com.example.system.pojo;

import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

/**
 * 小程序配置
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@TableName("applet_config")
@ApiModel("小程序配置")
public class AppletConfig {

    /**
     * 配置ID主键
     */
    @ApiModelProperty(value = "配置ID主键", required = false)
    private String configId;

    /**
     * 小程序名称
     */
    @ApiModelProperty(value = "小程序名称", required = true)
    private String appletName;

    /**
     * 小程序类型
     */
    @ApiModelProperty(value = "小程序类型", required = false)
    private String appletType;

    /**
     * 小程序APP_ID
     */
    @ApiModelProperty(value = "小程序APP_ID", required = false)
    private String app_id;

    /**
     * 小程序密钥
     */
    @ApiModelProperty(value = "小程序密钥", required = false)
    private String appletSecret;

    /**
     * 小程序微信token
     */
    @ApiModelProperty(value = "小程序微信token", required = false)
    private String appletToken;

    /**
     * 状态（1：正常，0：停用）
     */
    @ApiModelProperty(value = "状态（1：正常，0：停用）", required = false)
    private Integer status;

    /**
     * 创建人ID
     */
    @ApiModelProperty(value = "创建人ID", required = false)
    private String createUserId;

    /**
     * 修改人ID
     */
    @ApiModelProperty(value = "修改人ID", required = false)
    private String updateUserId;

    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间", required = false)
    private LocalDateTime createTime;

    /**
     * 最后更新时间
     */
    @ApiModelProperty(value = "最后更新时间", required = false)
    private LocalDateTime lastUpdateTime;
}