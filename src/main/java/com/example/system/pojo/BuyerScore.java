package com.example.system.pojo;

import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

/**
 * 用户积分表
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@TableName("buyer_score")
@ApiModel("用户积分")
public class BuyerScore {

    /**
     * 主键ID
     */
    @ApiModelProperty(value = "主键ID", required = true)
    private String scoreId;

    /**
     * 用户ID
     */
    @ApiModelProperty(value = "用户ID", required = true)
    private String userId;

    /**
     * 操作前积分数
     */
    @ApiModelProperty(value = "操作前积分数", required = true)
    private Integer beforeScore;

    /**
     * 增加或减少积分数
     */
    @ApiModelProperty(value = "增加或减少积分数", required = true)
    private Integer score;

    /**
     * 积分类型  1：支付订单送积分  2：兑换优惠券消耗 3：注册赠送积分 4：邀请赠送积分 5：退款回收积分 6：购物消耗积分
     */
    @ApiModelProperty(value = "积分类型  1：支付订单送积分  2：兑换优惠券消耗 3：注册赠送积分 4：邀请赠送积分 5：退款回收积分 6：购物消耗积分", required = true)
    private Integer scoreType;

    /**
     * 关联ID，订单ID或优惠券ID
     */
    @ApiModelProperty(value = " 关联ID，订单ID或优惠券ID", required = true)
    private String associationId;

    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间", required = true)
    private LocalDateTime createTime;

    /**
     * 类型 1：新增 2：减少
     */
    @ApiModelProperty(value = "类型 1：新增 2：减少", required = true)
    private Integer type;
}