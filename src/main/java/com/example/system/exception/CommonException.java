package com.example.system.exception;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 统一异常
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@SuppressWarnings("all")
public class CommonException extends RuntimeException {

    /**
     * 哪里出现的异常
     */
    private String head;

    /**
     * 异常信息
     */
    private String message;
}
