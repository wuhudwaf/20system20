package com.example.system.dto;

import com.example.system.pojo.SysUser;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

/**
 * 新增管理员专用
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("新增管理员专用")
public class AddSysUser extends SysUser {

    /**
     * 角色id
     */
    @ApiModelProperty(value = "角色id", required = true)
    @NotNull(message = "角色id不得为空")
    private String roleId;
}