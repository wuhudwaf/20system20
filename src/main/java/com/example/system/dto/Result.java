package com.example.system.dto;

import io.swagger.models.auth.In;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Result {

    public static final int RESPONSE_SUCCESS = 200;
    public static final int RESPONSE_FAILURE = -1;

    private Boolean success;
    private String errorMsg;
    private Object data;
    private String total;
    private Integer code;

    public static Result ok() {
        return new Result(true, null, null, null,RESPONSE_SUCCESS);
    }

    public static Result ok(Object data) {
        return new Result(true, null, data, null,RESPONSE_SUCCESS);
    }

    public static Result ok(List<?> data, String total) {
        return new Result(true, null, data, total,RESPONSE_SUCCESS);
    }

    public static Result fail(String errorMsg) {
        return new Result(false, errorMsg, null, null,RESPONSE_FAILURE);
    }
}