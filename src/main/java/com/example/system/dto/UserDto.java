package com.example.system.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;


/**
 * 管理员登录成功后存储的登录数据
 */
@Data
@ApiModel("管理员登录成功后存储的登录数据")
public class UserDto {

    /**
     * id
     */
    @ApiModelProperty(value = "id", required = true)
    private String id;

    /**
     * 姓名
     */
    @ApiModelProperty(value = "姓名", required = true)
    private String name;

    /**
     * 头像地址
     */
    @ApiModelProperty(value = "头像地址", required = true)
    private String imageAddress;

    /**
     * 我的权限
     */
    @ApiModelProperty(value = "我的权限", required = true)
    private List<String> url;

    /**
     * 总权限
     */
    @ApiModelProperty(value = "总权限", required = true)
    private List<InterfaceDto> urlAll;

}