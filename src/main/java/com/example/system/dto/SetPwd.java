package com.example.system.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;

/**
 * 重置门店员工密码专用
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("重置门店员工密码专用")
public class SetPwd {

    /**
     * id
     */
    @ApiModelProperty(value = "id", required = true)
    private String id;

    /**
     * 密码
     */
    @ApiModelProperty(value = "密码", required = true)
    @NotBlank(message = "密码不得为空")
    @Length(min = 6, max = 16, message = "密码长度异常")
    private String pwd;

}