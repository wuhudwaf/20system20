package com.example.system.dto;

import com.baomidou.mybatisplus.annotation.TableName;
import com.example.system.pojo.GoodsFlashSale;
import com.example.system.pojo.GoodsFlashSaleItem;
import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * 添加商品限时秒杀
 */
@AllArgsConstructor
@NoArgsConstructor
@TableName("goods_flash_sale")
@ApiModel("添加商品限时秒杀")
public class AddGoodsFlashSale extends GoodsFlashSale {

    /**
     *
     */
    private List<GoodsFlashSaleItem> goodsFlashSaleItems;
}