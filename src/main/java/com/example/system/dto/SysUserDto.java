package com.example.system.dto;

import com.example.system.pojo.SysRole;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 管理员，返回给前端专用
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("管理员，返回给前端专用")
public class SysUserDto {

    /**
     * id
     */
    @ApiModelProperty(value = "id", required = true)
    private String id;

    /**
     * 姓名
     */
    @ApiModelProperty(value = "姓名", required = true)
    private String name;

    /**
     * 头像地址
     */
    @ApiModelProperty(value = "头像地址", required = true)
    private String imageAddress;

    /**
     * 角色对象
     */
    @ApiModelProperty(value = "角色对象", required = true)
    private SysRole sysRole;
}