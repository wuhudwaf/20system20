package com.example.system.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 当前角色对应的接口
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("当前角色对应的接口")
public class InterfaceDto {

    /**
     * 接口ID
     */
    @ApiModelProperty(value = "接口ID", required = true)
    private String id;

    /**
     * 名称
     */
    @ApiModelProperty(value = "名称", required = true)
    private String menuName;
}