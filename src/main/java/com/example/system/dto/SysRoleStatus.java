package com.example.system.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

/**
 * 启用/关闭角色
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("启用/关闭角色")
public class SysRoleStatus {

    /**
     * id
     */
    @ApiModelProperty(value = "id", required = true)
    @NotNull(message = "id不得为空")
    private String id;

    /**
     * 状态
     * 0关闭
     * 1启用
     */
    @ApiModelProperty(value = "状态,0关闭,1启用", required = true)
    @NotNull(message = "状态不得为空")
    private Integer status;
}