package com.example.system.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;

/**
 * 修改门店员工
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("修改门店员工")
public class SetInfoSellerUser {

    /**
     * id
     */
    @ApiModelProperty(value = "id", required = true)
    private String id;

    /**
     * 姓名
     */
    @ApiModelProperty(value = "姓名", required = true)
    @NotBlank(message = "请输入姓名")
    private String nickName;

    /**
     * 手机号
     */
    @ApiModelProperty(value = "手机号", required = true)
    @Length(min = 11, max = 11, message = "请正确输入手机号")
    private String phone;
}
