package com.example.system.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Data
public class SysCouponDto {

    /**
     * 优惠劵id
     */
    @ApiModelProperty(value = "优惠劵id", required = false)
    private String couponId;

    /**
     * 优惠券名称
     */
    @ApiModelProperty(value = "优惠券名称", required = true)
    @NotBlank(message = "请输入优惠券名称")
    private String couponName;

    /**
     * 优惠金额
     */
    @ApiModelProperty(value = "优惠金额", required = true)
    @NotBlank(message = "请输入优惠金额")
    private String couponPrice;

    /**
     * 满减金额
     */
    @ApiModelProperty(value = "满减金额", required = true)
    @NotBlank(message = "请输入满减金额")
    private String couponAmount;

    /**
     * 优惠类型
     */
    @ApiModelProperty(value = "优惠类型", required = true)
    @NotBlank(message = "请选择优惠类型")
    private String couponType;

    /**
     * 有效期
     */
    @ApiModelProperty(value = "有效期", required = true)
    @NotNull(message = "请输入有效期")
    private Integer useTime;

    /**
     * 开始时间
     */
    @ApiModelProperty(value = "开始时间", required = true)
    @NotNull(message = "请设置开始时间")
    private Long startTime;

    /**
     * 结束时间
     */
    @ApiModelProperty(value = "结束时间", required = true)
    @NotNull(message = "请设置结束时间")
    private Long endTime;

    /**
     * 租户号
     */
    @ApiModelProperty(value = "租户号", required = false)
    private String tenantId;

    /**
     * 乐观锁
     */
    @ApiModelProperty(value = "乐观锁", required = false)
    private Integer revision;

    /**
     * 创建人
     */
    @ApiModelProperty(value = "创建人", required = false)
    private String createBy;

    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间", required = false)
    private LocalDateTime createTime;

    /**
     * 更新人
     */
    @ApiModelProperty(value = "更新人", required = false)
    private String updateBy;

    /**
     * 更新时间
     */
    @ApiModelProperty(value = "更新时间", required = false)
    private LocalDateTime updateTime;

    /**
     * 状态 0：下架 1：上架
     */
    @ApiModelProperty(value = "状态 0：下架 1：上架", required = false)
    @NotNull(message = "请选择状态")
    private Integer status;

    /**
     * 是否允许兑换 1：可以 0：不可以
     */
    @ApiModelProperty(value = "是否允许兑换 1：可以 0：不可以", required = true)
    @NotNull(message = "请选择是否允许兑换")
    private Integer allowRedeemed;

    /**
     * 兑换所需积分
     */
    @ApiModelProperty(value = "兑换所需积分", required = true)
    private Integer redeemNeedScore;
}