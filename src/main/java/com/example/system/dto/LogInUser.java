package com.example.system.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;

/**
 * 登录专用
 */
@Data
@ApiModel("管理员登录")
public class LogInUser {
    /**
     * 手机号
     */
    private String phone;

    /**
     * 账号
     */
//    @NotBlank(message = "密码不得为空")
//    @ApiModelProperty(value = "账号", required = true)
//    private String name;

    /**
     * 密码
     */
    @ApiModelProperty(value = "密码", required = true)
    @NotBlank(message = "密码不得为空")
    @Length(min = 6, max = 16, message = "密码长度异常")
    private String pwd;

}