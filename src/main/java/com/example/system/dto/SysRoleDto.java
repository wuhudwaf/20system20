package com.example.system.dto;

import com.example.system.pojo.SysRole;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * 角色详情/新增
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("角色详情/新增")
public class SysRoleDto extends SysRole {

    /**
     * 当前角色对应的接口
     */
    @ApiModelProperty(value = "当前角色对应的接口", required = true)
    private List<InterfaceDto> interfaceDto;

}