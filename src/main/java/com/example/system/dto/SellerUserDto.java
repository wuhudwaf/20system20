package com.example.system.dto;

import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;

/**
 * 门店员工缓存对象
 * 新增的用户都会被缓存redis中，时间30分钟
 * SellerUserDto则是缓存对象
 * 注意缓存穿透问题
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@TableName("seller_user")
@ApiModel("门店员工")
public class SellerUserDto {

    /**
     * id
     */
    @ApiModelProperty(value = "id", required = false)
    private String id;

    /**
     * 姓名
     */
    @ApiModelProperty(value = "姓名", required = false)
    private String nickName;

    /**
     * 手机号
     */
    @ApiModelProperty(value = "手机号", required = true)
    @NotBlank(message = "手机号不得为空")
    @Length(min = 11, max = 11, message = "请正确输入手机号")
    private String phone;

    /**
     * 账号
     */
    @ApiModelProperty(value = "账号", required = false)
    private String userName;

    /**
     * 状态
     */
    @ApiModelProperty(value = "状态", required = false)
    private Integer status;

    /**
     * 0删除 1正常
     */
    @ApiModelProperty(value = "0删除 1正常", required = false)
    private Integer delFlag;

    /**
     * 门店id
     */
    @ApiModelProperty(value = "门店id", required = true)
    private String shopId;
}
