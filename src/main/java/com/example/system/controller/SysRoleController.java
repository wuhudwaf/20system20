package com.example.system.controller;

import com.example.system.dto.Result;
import com.example.system.dto.SysRoleDto;
import com.example.system.dto.SysRoleStatus;
import com.example.system.service.SysRoleService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * 角色管理
 */
@Api(tags = "角色管理")
@Validated
@RestController
@SuppressWarnings("all")
@RequiredArgsConstructor
@RequestMapping("/sys/role")
public class SysRoleController {

    private final SysRoleService sysRoleService;


    /**
     * 查询角色列表
     *
     * @return
     */
    @ApiOperation(value = "查询角色列表", notes = "查询角色列表")
    @GetMapping("/getPage")
    public Result getPage(@ApiParam(name = "page", value = "第几页", required = true) @RequestParam @NotNull(message = "page不得为空") Long page,
                          @ApiParam(name = "pageSize", value = "每页展示多少条", required = true) @RequestParam @NotNull(message = "pageSize不得为空") Long pageSize) {
        return Result.ok(sysRoleService.getPage(page, pageSize));
    }

    /**
     * 删除角色
     *
     * @param id
     * @return
     */
    @ApiOperation(value = "删除角色", notes = "删除角色")
    @DeleteMapping("/delete")
    public Result delete(@ApiParam(name = "id", value = "角色id", required = true) @RequestParam @NotNull(message = "id不得为空") String id) {
        sysRoleService.delete(id);
        return Result.ok("删除成功");
    }

    /**
     * 查看角色详情 (回显数据)
     *
     * @param id
     * @return
     */
    @ApiOperation(value = "查看角色详情", notes = "查看角色详情")
    @GetMapping("/getInfo")
    public Result getInfo(@ApiParam(name = "id", value = "角色id", required = true) @RequestParam @NotNull(message = "id不得为空") String id) {
        return Result.ok(sysRoleService.getInfo(id));
    }

    /**
     * 启用/关闭角色
     *
     * @param sysRoleStatus
     * @return
     */
    @ApiOperation(value = "启用/关闭角色", notes = "启用/关闭角色")
    @PutMapping("/setStatus")
    public Result setStatus(@RequestBody @Valid SysRoleStatus sysRoleStatus) {
        sysRoleService.setStatus(sysRoleStatus);
        return Result.ok("操作成功");
    }

    /**
     * 新增角色
     *
     * @param sysRoleDto
     * @return
     */
    @ApiOperation(value = "新增角色", notes = "新增角色")
    @PostMapping("/add")
    public Result add(@RequestBody @Valid SysRoleDto sysRoleDto) {
        sysRoleService.add(sysRoleDto);
        return Result.ok("新增成功");
    }

    /**
     * 编辑角色
     *
     * @param sysRoleDto
     * @return
     */
    @ApiOperation(value = "编辑角色", notes = "编辑角色")
    @PutMapping("/set")
    public Result set(@RequestBody @Valid SysRoleDto sysRoleDto) {
        sysRoleService.set(sysRoleDto);
        return Result.ok("修改成功");
    }

    /**
     * 重置角色权限
     *
     * @return
     */
    @ApiOperation(value = "重置角色权限", notes = "重置角色权限")
    @PutMapping("/resetting")
    public Result resetting(@ApiParam(name = "id", value = "角色id", required = true) @RequestParam @NotNull(message = "角色id不得为空") String id) {
        sysRoleService.resetting(id);
        return Result.ok("重置成功");
    }

    /**
     * 查询所有角色，新增管理员时选择可以使用的角色
     *
     * @return
     */
    @ApiOperation(value = "查询所有角色，新增管理员时选择可以使用的角色", notes = "查询所有角色，新增管理员时选择可以使用的角色")
    @GetMapping("/getList")
    public Result getList() {
        return Result.ok(sysRoleService.getList());
    }
}