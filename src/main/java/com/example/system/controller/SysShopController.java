package com.example.system.controller;

import com.example.system.dto.Result;
import com.example.system.pojo.SysShop;
import com.example.system.pojo.export.ExportSysShop;
import com.example.system.pojo.page.Page;
import com.example.system.pojo.vo.add.SysShopAdd;
import com.example.system.service.SysShopService;
import com.example.system.utils.RedisIdWorker;
import com.example.system.utils.excel.ExcelUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;


/**
 * 门店管理
 */
@Api(tags = "门店管理")
@Validated
@RestController
@SuppressWarnings("all")
@RequiredArgsConstructor
@RequestMapping("/shop/sys/shop")
public class SysShopController {

    private final SysShopService sysShopService;

    private final RedisIdWorker redisIdWorker;


    /**
     * 新增门店
     *
     * @param sysShop
     * @return
     */
    @ApiOperation(value = "新增门店", notes = "新增门店")
    @PostMapping("/add")
    public Result add(@RequestBody @Valid SysShop sysShop) {
        sysShopService.add(sysShop);
        return Result.ok("门店添加成功");
    }

    /**
     * 删除门店
     *
     * @param shopId
     * @return
     */
    @ApiOperation(value = "删除门店", notes = "删除门店")
    @DeleteMapping("/delete")
    public Result delete(@ApiParam(name = "shopId", value = "门店id", required = true) @RequestParam @NotNull(message = "门店id不得为空") String shopId) {
        sysShopService.delete(shopId);
        return Result.ok("门店删除成功");
    }

    /**
     * 获取门店详情
     *
     * @return
     */
    @ApiOperation(value = "获取门店详情", notes = "获取门店详情")
    @GetMapping("/getByShopId")
    public Result getByShopId(@ApiParam(name = "shopId", value = "门店id", required = true) @RequestParam @NotNull(message = "门店id不得为空") String shopId) {
        return Result.ok(sysShopService.getByShopId(shopId));
    }

    /**
     * 获取门店列表
     *
     * @return
     */
    @ApiOperation(value = "获取门店列表", notes = "获取门店列表")
    @GetMapping("/getList")
    public Result getList(@ApiParam(name = "page", value = "第几页", required = true) @RequestParam @NotNull(message = "page不得为空") Long page, @ApiParam(name = "pageSize", value = "每页展示多少条", required = true) @NotNull(message = "pageSize不得为空") Long pageSize) {
        return Result.ok(sysShopService.getList(page, pageSize));
    }

    /**
     * 修改门店信息
     *
     * @param sysShop
     * @return
     */
    @ApiOperation(value = "修改门店信息", notes = "修改门店信息")
    @PutMapping("/update")
    public Result update(@RequestBody @Valid SysShop sysShop) {
        sysShopService.update(sysShop);
        return Result.ok("门店修改成功");
    }

    /**
     * 查询指定门店未审批员工列表
     *
     * @param shopId
     * @param page
     * @param pageSize
     * @return
     */
    @ApiOperation(value = "查询指定门店未审批员工列表", notes = "查询指定门店未审批员工列表")
    @PutMapping("/byShopIdSelectSellerUserId")
    public Result byShopIdSelectSellerUserId(@ApiParam(name = "shopId", value = "门店id", required = true) @RequestParam @NotNull(message = "shopId不得为空") String shopId, @ApiParam(name = "page", value = "第几页") @RequestParam @NotNull(message = "page不得为空") Long page, @ApiParam(name = "pageSize", value = "每页多少条数据") @RequestParam @NotNull(message = "pageSize不得为空") Long pageSize) {
        return Result.ok(sysShopService.byShopIdSelectSellerUserId(shopId, page, pageSize));
    }

    /**
     * 批量设置门店状态(1：开店，0：闭店)
     *
     * @param shops
     * @return
     */
    @ApiOperation(value = "批量设置门店状态(1：开店，0：闭店)", notes = "批量设置门店状态(1：开店，0：闭店)")
    @PutMapping("/setStatusList")
    public Result setStatusList(@ApiParam(name = "shops", value = "门店对象集合（数组）", required = true) @RequestBody List<SysShop> shops) {
        sysShopService.setStatusList(shops);
        return Result.ok("操作成功");
    }

    /**
     * 批量设置门店营业时间
     *
     * @param shops
     * @return
     */
    @ApiOperation(value = "批量设置门店营业时间", notes = "批量设置门店营业时间")
    @PutMapping("/setStartTimeAndEndTimeList")
    public Result setStartTimeAndEndTimeList(@ApiParam(name = "shops", value = "门店对象集合（数组）", required = true) @RequestBody List<SysShop> shops) {
        sysShopService.setStartTimeAndEndTimeList(shops);
        return Result.ok("操作成功");
    }

    @ApiOperation(value = "门店导出(全部)[导入字段与导出一致]", notes = "门店导出(全部)[导入字段与导出一致]")
    @GetMapping("/diyGetSysShopListNew")
    public Result diyGetSysShopListNew(HttpServletResponse response) {
        // 表示导出全部
        List<ExportSysShop> shops = sysShopService.diyGetSysShopListNew();
        ExcelUtils.export(response, "shop", shops, ExportSysShop.class);
        return Result.ok("操作成功");
    }

    /**
     * 门店导出
     *
     * @param response
     * @return
     */
    @ApiOperation(value = "门店导出(全部)", notes = "门店导出(全部)")
    @GetMapping("/diyGetSysShopList")
    public Result diyGetSysShopList(HttpServletResponse response) {
        // 表示导出全部
        List<SysShop> shops = sysShopService.diyGetSysGoodsList();
        ExcelUtils.export(response, "shop", shops, SysShop.class);
        return Result.ok("操作成功");
    }

    /**
     * 门店导出(页数)
     *
     * @param response
     * @param sysShopQueryVo
     * @return
     */
    @ApiOperation(value = "门店导出(页数)", notes = "门店导出(页数)")
    @GetMapping("/diyGetSysShopPage")
    public Result diyGetSysShopPage(HttpServletResponse response, Page page) {
        List<SysShop> shops = sysShopService.diyGetSysShopPage(page);
        ExcelUtils.export(response, "shop", shops, SysShop.class);
        return Result.ok("操作成功");
    }

    /**
     * 门店导入
     *
     * @param shopFile
     * @return
     * @throws Exception
     */
    @PostMapping("/importShop")
    @ApiOperation(value = "门店导入", notes = "门店导入")
    public Result importShop(@ApiParam(name = "shopFile", value = "门店数据文件", required = true) @RequestPart("shopFile") MultipartFile shopFile) throws Exception {
        sysShopService.importShop(ExcelUtils.readMultipartFile(shopFile, SysShopAdd.class));
        return Result.ok("操作成功");
    }

    /**
     * 文件导入日期问题，转换为上传输入时间
     * yyyy-MM-dd HH:mm:ss
     *
     * @param openHours
     * @return
     */
//    public String openHours(String openHours) {
//        Date setupTime = HSSFDateUtil.getJavaDate(Double.valueOf(openHours));
//        long time = setupTime.getTime();
//        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
//        String sd = sdf.format(new Date(Long.parseLong(String.valueOf(time))));
//        return sd;
//    }
}