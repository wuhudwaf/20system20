package com.example.system.controller;

import com.example.system.service.DeliveryFeeBaseConfigService;
import io.swagger.annotations.Api;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Api(tags = "配送费配置")
@Validated
@RestController
@SuppressWarnings("all")
@RequiredArgsConstructor
@RequestMapping("/delivery/fee/base/config")
public class DeliveryFeeBaseConfigController {

    private final DeliveryFeeBaseConfigService deliveryFeeBaseConfigService;
}