package com.example.system.controller;

import com.example.system.dto.Result;
import com.example.system.pojo.BuyerMembershipRule;
import com.example.system.service.BuyerMembershipRuleService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * 会员管理
 */
@Api(tags = "会员管理")
@Validated
@RestController
@SuppressWarnings("all")
@RequiredArgsConstructor
@RequestMapping("/buyer/membership/rule")
public class BuyerMembershipRuleController {

    private final BuyerMembershipRuleService buyerMembershipRuleService;

    /**
     * 添加会员规则
     *
     * @param buyerMembershipRule
     * @return
     */
    @ApiOperation(value = "添加会员规则", notes = "添加会员规则")
    @PostMapping("/add")
    public Result add(@RequestBody @Valid BuyerMembershipRule buyerMembershipRule) {
        buyerMembershipRuleService.add(buyerMembershipRule);
        return Result.ok("添加成功");
    }

    /**
     * 删除会员规则
     *
     * @param ruleId
     * @return
     */
    @ApiOperation(value = "删除会员规则", notes = "删除会员规则")
    @DeleteMapping("/delete")
    public Result delete(@ApiParam(name = "ruleId", value = "会员规则id", required = true) @RequestParam @NotNull(message = "id不得为空") String ruleId) {
        buyerMembershipRuleService.delete(ruleId);
        return Result.ok("删除成功");
    }

    /**
     * 修改会员规则
     *
     * @param buyerMembershipRule
     * @return
     */
    @ApiOperation(value = "修改会员规则", notes = "修改会员规则")
    @PutMapping("/set")
    public Result set(@RequestBody @Valid BuyerMembershipRule buyerMembershipRule) {
        buyerMembershipRuleService.set(buyerMembershipRule);
        return Result.ok("修改成功");
    }

    /**
     * 查看员规则详情
     *
     * @param ruleId
     * @return
     */
    @ApiOperation(value = "查看员规则详情", notes = "查看员规则详情")
    @GetMapping("/getInfo")
    public Result getInfo(@ApiParam(name = "ruleId", value = "会员规则id", required = true) @RequestParam @NotNull(message = "id不得为空") String ruleId) {
        return Result.ok(buyerMembershipRuleService.getInfo(ruleId));
    }

    /**
     * 会员等级列表
     *
     * @return
     */
    @ApiOperation(value = "会员等级列表", notes = "会员等级列表")
    @GetMapping("/getList")
    public Result getList() {
        return Result.ok(buyerMembershipRuleService.getList());
    }

//    @ApiOperation(value = "会员用户信息列表", notes = "会员用户信息列表")
//    @GetMapping("/getUserBuyerMembershipRulePage")
//    public Result getUserBuyerMembershipRulePage(@ApiParam(name = "page", value = "第几页", required = true) @RequestParam @NotNull(message = "page不得为空") Long page,
//                                                 @ApiParam(name = "pageSize", value = "每页展示多少条", required = true) @NotNull(message = "pageSize不得为空") Long pageSize) {
//        return Result.ok(buyerMembershipRuleService.getUserBuyerMembershipRulePage(page, pageSize));
//    }
}