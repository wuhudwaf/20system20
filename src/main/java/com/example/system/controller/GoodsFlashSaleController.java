package com.example.system.controller;

import com.example.system.dto.Result;
import com.example.system.dto.ReturnGoodsFlashSale;
import com.example.system.pojo.GoodsFlashSale;
import com.example.system.service.GoodsFlashSaleService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * 商品限时秒杀
 */
@Api(tags = "商品限时秒杀")
@Validated
@RestController
@SuppressWarnings("all")
@RequiredArgsConstructor
@RequestMapping("/goods/flash/sale")
public class GoodsFlashSaleController {

    private final GoodsFlashSaleService goodsFlashSaleService;

    /**
     * 新增秒杀活动
     *
     * @param returnGoodsFlashSale
     * @return
     */
    @ApiOperation("新增秒杀活动")
    @PostMapping("/addGoodsFlashSale")
    public Result addGoodsFlashSale(@RequestBody @Valid ReturnGoodsFlashSale returnGoodsFlashSale) {
        goodsFlashSaleService.addGoodsFlashSale(returnGoodsFlashSale);
        return Result.ok("新增成功");
    }

    /**
     * 修改商品限时秒杀活动接口
     *
     * @param goodsFlashSale
     * @return
     */
    @ApiOperation("修改商品限时秒杀活动接口")
    @PutMapping("/setGoodsFlashSale")
    public Result setGoodsFlashSale(@RequestBody @Valid ReturnGoodsFlashSale returnGoodsFlashSale) {
        goodsFlashSaleService.setGoodsFlashSale(returnGoodsFlashSale);
        return Result.ok("修改成功");
    }

    /**
     * 根据秒杀id看商品限时秒杀活动接口详情
     *
     * @param flashSaleId
     * @return
     */
    @ApiOperation("根据秒杀id看商品限时秒杀活动接口详情")
    @GetMapping("/getGoodsFlashSale")
    public Result getGoodsFlashSale(@ApiParam(name = "flashSaleId", value = "商品限时秒杀id", required = true)
                                    @RequestParam
                                    @NotNull(message = "flashSaleId不得为空") String flashSaleId) {
        return Result.ok(goodsFlashSaleService.getGoodsFlashSale(flashSaleId));
    }

    /**
     * 开始秒杀/结束秒杀商品限时秒杀
     *
     * @param flashSaleId
     * @return
     */
    @ApiOperation("开始秒杀/结束秒杀商品限时秒杀")
    @PutMapping("/setStatus")
    public Result setStatus(@ApiParam(name = "flashSaleId", value = "商品限时秒杀id", required = true)
                            @RequestParam
                            @NotBlank(message = "商品限时秒杀id不得为空") String flashSaleId) {
        return Result.ok(goodsFlashSaleService.setStatus(flashSaleId));
    }

    /**
     * 删除商品限时秒杀
     *
     * @param flashSaleId
     * @return
     */
    @ApiOperation("删除商品限时秒杀")
    @DeleteMapping("/deleteGoodsFlashSale")
    public Result deleteGoodsFlashSale(@ApiParam(name = "flashSaleId", value = "商品限时秒杀id", required = true)
                                       @RequestParam
                                       @NotBlank(message = "商品限时秒杀id不得为空") String flashSaleId) {
        goodsFlashSaleService.deleteGoodsFlashSale(flashSaleId);
        return Result.ok("删除成功");
    }

    /**
     * 获取商品限时秒杀列表
     *
     * @param page
     * @param pageSize
     * @return
     */
    @ApiOperation("获取商品限时秒杀列表")
    @GetMapping("/getGoodsFlashSalePage")
    public Result getGoodsFlashSalePage(@ApiParam(name = "page", value = "第几页", required = true) @RequestParam @NotNull(message = "page不得为空") Long page,
                                        @ApiParam(name = "pageSize", value = "每页展示多少条", required = true) @RequestParam @NotNull(message = "page不得为空") Long pageSize) {
        return Result.ok(goodsFlashSaleService.getGoodsFlashSalePage(page, pageSize));
    }
}