package com.example.system.controller;

import com.example.system.service.DeliveryFeeOtherConfigService;
import io.swagger.annotations.Api;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Api(tags = "配送费其他配置")
@Validated
@SuppressWarnings("all")
@RequestMapping("/delivery/fee/other/config")
@RestController
@RequiredArgsConstructor
public class DeliveryFeeOtherConfigController {

    private final DeliveryFeeOtherConfigService deliveryFeeOtherConfigService;
}