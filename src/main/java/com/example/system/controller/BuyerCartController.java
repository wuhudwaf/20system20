package com.example.system.controller;

import com.example.system.dto.Result;
import com.example.system.service.BuyerCartService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotNull;

/**
 * 用户购物车表
 */
@Api(tags = "用户购物车")
@Validated
@RestController
@SuppressWarnings("all")
@RequiredArgsConstructor
@RequestMapping("/buyer/cart")
public class BuyerCartController {

    private final BuyerCartService buyerCartService;

    /**
     * 用户购物车列表
     *
     * @param page
     * @param pageSize
     * @return
     */
    @ApiOperation(value = "用户购物车列表", notes = "用户购物车列表")
    @GetMapping("/getList")
    public Result getList(@ApiParam(name = "page", value = "第几页", required = true) @RequestParam @NotNull(message = "page不得为空") Long page,
                          @ApiParam(name = "pageSize", value = "每页展示多少条", required = true) @RequestParam @NotNull(message = "page不得为空") Long pageSize) {
        return Result.ok(buyerCartService.getList(page, pageSize));
    }
}