package com.example.system.controller;


import com.example.system.dto.Result;
import com.example.system.dto.SetInfoSellerUser;
import com.example.system.pojo.SellerUser;
import com.example.system.dto.SetPwd;
import com.example.system.service.SellerUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import org.hibernate.validator.constraints.Length;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * 门店员工管理
 */
@Api(tags = "门店员工管理")
@Validated
@RestController
@SuppressWarnings("all")
@RequiredArgsConstructor
@RequestMapping("/shop/seller/user")
public class SellerUserController {

    private final SellerUserService sellerUserService;


    /**
     * 新增门店员工
     *
     * @param sellerUser
     * @return
     */
    @ApiOperation(value = "新增门店员工", notes = "新增门店员工")
    @PostMapping("/add")
    public Result add(@RequestBody @Valid SellerUser sellerUser) {
        return Result.ok("密码[牢记] --> " + sellerUserService.add(sellerUser));
    }

    /**
     * 冻结门店员工
     *
     * @param id
     * @return
     */
    @ApiOperation(value = "冻结门店员工", notes = "冻结门店员工")
    @PutMapping("/delete")
    public Result delete(@ApiParam(name = "id", value = "门店员工id", required = true) @RequestParam @NotNull(message = "id不得为空") String id) {
        sellerUserService.delete(id);
        return Result.ok("冻结成功");
    }

    /**
     * 解冻
     *
     * @param id
     * @return
     */
    @ApiOperation(value = "解冻", notes = "解冻")
    @PutMapping("/set")
    public Result set(@ApiParam(name = "id", value = "门店员工id", required = true) @RequestParam @NotNull(message = "id不得为空") String id) {
        sellerUserService.set(id);
        return Result.ok("解冻成功");
    }

    /**
     * 数据回显 (修改门店员工信息)
     *
     * @param id
     * @return
     */
    @ApiOperation(value = "门店员工详情", notes = "门店员工详情")
    @GetMapping("/get")
    public Result get(@ApiParam(name = "id", value = "门店员工id", required = true) @RequestParam @NotNull(message = "id不得为空") String id) {
        return Result.ok(sellerUserService.get(id));
    }

    /**
     * 修改门店员工
     *
     * @param setInfoSellerUser
     * @return
     */
    @ApiOperation(value = "修改门店员工", notes = "修改门店员工")
    @PutMapping("/setInfo")
    public Result setInfo(@RequestBody @Valid SetInfoSellerUser setInfoSellerUser) {
        sellerUserService.setInfo(setInfoSellerUser);
        return Result.ok("修改成功");
    }


    /**
     * 查询，根据手机号查询
     *
     * @param phone
     * @return
     */
    @ApiOperation(value = "查询，根据手机号查询", notes = "查询，根据手机号查询")
    @GetMapping("/getByPhone")
    public Result getByPhone(@ApiParam(name = "phone", value = "手机号", required = true) @RequestParam @NotBlank(message = "请填写手机号")
                             @Length(min = 11, max = 11, message = "请正确输入手机号")
                             String phone) {
        return Result.ok(sellerUserService.getByPhone(phone));
    }

    /**
     * 查询某一门店的员工
     *
     * @param shopId
     * @param page
     * @param pageSize
     * @return
     */
    @ApiOperation(value = "查询某一门店的员工", notes = "查询某一门店的员工")
    @GetMapping("/getList")
    public Result getList(
            @ApiParam(name = "shopId", value = "门店id", required = true) @RequestParam @NotNull(message = "shopId不得为空") String shopId,
            @ApiParam(name = "page", value = "第几页", required = true) @RequestParam @NotNull(message = "page不得为空") Long page,
            @ApiParam(name = "pageSize", value = "每页展示多少条", required = true) @RequestParam @NotNull(message = "pageSize不得为空") Long pageSize) {
        return Result.ok(sellerUserService.getList(shopId, page, pageSize));
    }

    /**
     * 重置密码
     *
     * @param setPwd
     * @return
     */
    @ApiOperation(value = "重置密码", notes = "重置密码")
    @PutMapping("/setPwd")
    public Result setPwd(@ApiParam(name = "setPwd", value = "密码", required = true) @RequestBody @Valid() SetPwd setPwd) {
        return Result.ok("密码 --> " + sellerUserService.setPwd(setPwd));
    }
}