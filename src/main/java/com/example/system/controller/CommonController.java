//package com.example.system.controller;
//
//
//import com.example.system.dto.Result;
//import com.example.system.utils.Image;
//
//import io.swagger.annotations.Api;
//import io.swagger.annotations.ApiOperation;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.web.bind.annotation.PostMapping;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
//import org.springframework.web.multipart.MultipartFile;
//
//import java.io.File;
//import java.io.IOException;
//import java.util.UUID;
//
//@Slf4j
//@SuppressWarnings({"All"})
//@Api(tags = "图片上传接口")
//@RestController
//@RequestMapping("/common")
//// 图片上传
//public class CommonController {
//
//    @Value("${img.path}")
//    private String file;
//
//    @Value("${path.name}")
//    private String path;
//
//    @ApiOperation(value = "选择图片后调用此接口", notes = "选择图片后调用此接口")
//    @PostMapping("/upload")
//    public Result upload(MultipartFile file) {
//        // 上传图片名称
//        String originalFilename = file.getOriginalFilename();
//        String img = originalFilename.substring(originalFilename.lastIndexOf("."));
//
//        // UUID + img = 全新名称
//        String newImg = UUID.randomUUID().toString() + img;
//
//        // 判断是否文件是否存在, 未存在则创建
//        File newFile = new File(this.file);
//        if (!newFile.exists()) {
//            newFile.mkdirs();
//        }
//
//        try {
//            file.transferTo(new File(this.file + newImg));
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        // path
//        Image image = new Image();
//        image.setName(newImg);
//        image.setImageAddress(path + newImg);
//        return Result.ok(image.getImageAddress());
//    }
//}