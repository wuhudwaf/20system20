package com.example.system.controller;

import com.example.system.dto.Result;
import com.example.system.service.BuyerCouponService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotNull;

/**
 * 用户优惠券
 */
@Api(tags = "用户优惠券")
@Validated
@RestController
@SuppressWarnings("all")
@RequiredArgsConstructor
@RequestMapping("/buyer/coupon")
public class BuyerCouponController {

    private final BuyerCouponService buyerCouponService;

    /**
     * 用户优惠券列表
     *
     * @param page
     * @param pageSize
     * @return
     */
    @ApiOperation(value = "用户优惠券列表", notes = "用户优惠券列表")
    @GetMapping("/getList")
    public Result getList(@ApiParam(name = "page", value = "第几页", required = true) @RequestParam @NotNull(message = "page不得为空") Long page,
                          @ApiParam(name = "pageSize", value = "每页展示多少条", required = true) @RequestParam @NotNull(message = "page不得为空") Long pageSize) {
        return Result.ok(buyerCouponService.getList(page, pageSize));
    }
}