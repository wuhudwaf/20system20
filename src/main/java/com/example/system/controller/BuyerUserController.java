package com.example.system.controller;

import com.example.system.dto.Result;
import com.example.system.pojo.BuyerUser;
import com.example.system.pojo.where.BuyerUserQuery;
import com.example.system.service.BuyerUserService;
import com.example.system.utils.excel.ExcelUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.text.ParseException;
import java.util.List;

/**
 * 微信用户信息表
 */
@Api(tags = "微信用户信息")
@Validated
@RestController
@SuppressWarnings("all")
@RequiredArgsConstructor
@RequestMapping("/root/buyer/user")
public class BuyerUserController {

    private final BuyerUserService buyerUserService;

    /**
     * 查询微信用户信息
     *
     * @return
     */
    @ApiOperation(value = "查询微信用户信息", notes = "查询微信用户信息")
    @GetMapping("/getPage")
    public Result getPage(@ApiParam(name = "buyerUser", value = "", required = true) @Valid BuyerUserQuery buyerUser) throws ParseException {
        return Result.ok(buyerUserService.getPage(buyerUser));
    }

    /**
     * 用户id获取用户地址列表
     *
     * @param userId
     * @return
     */
    @GetMapping("/userIdGetAddressList")
    @ApiOperation(value = "用户id获取用户地址列表", notes = "用户id获取用户地址列表")
    public Result userIdGetAddressList(@ApiParam(name = "userId", value = "用户id", required = true) @RequestParam @NotNull(message = "page不得为空") String userId) {
        return Result.ok(buyerUserService.userIdGetAddressList(userId));
    }

    /**
     * 导出全部用户
     *
     * @param response
     * @return
     */
    @ApiOperation(value = "导出全部用户", notes = "导出全部用户")
    @GetMapping("/autoGetBuyerUserList")
    public Result autoGetBuyerUserList(HttpServletResponse response) {
        List<BuyerUser> buyerUsers = buyerUserService.autoGetBuyerUserList(response);
        // 导出
        ExcelUtils.export(response, "用户表", buyerUsers, BuyerUser.class);
        return Result.ok("操作成功");
    }

    /**
     * 导出用户(分页)
     *
     * @param pageOne
     * @param response
     * @param pageTwo
     * @return
     */
    @ApiOperation(value = "导出用户(分页)", notes = "导出用户(分页)")
    @GetMapping("/diyGetBuyerUserPage")
    public Result diyGetBuyerUserPage(@ApiParam(name = "pageOne", value = "第几页", required = true) @RequestParam Integer pageOne,
                                      HttpServletResponse response,
                                      @ApiParam(name = "pageTwo", value = "至第几页", required = true) @RequestParam Integer pageTwo,
                                      @ApiParam(name = "pageSize", value = "每页多少条", required = true) @RequestParam Integer pageSize) {
        List<BuyerUser> buyerUsers = buyerUserService.diyGetBuyerUserPage(pageOne, pageTwo, pageSize);
        ExcelUtils.export(response, "微信用户表", buyerUsers, BuyerUser.class);
        return Result.ok("操作成功");
    }

    /**
     * 修改微信用户推荐人
     *
     * @param userId
     * @param inviteUserId
     * @return
     */
    @ApiOperation(value = "修改微信用户推荐人", notes = "修改微信用户推荐人")
    @PutMapping("/userIdSetInviteUserId")
    public Result userIdSetInviteUserId(@ApiParam(name = "userId", value = "微信用户id", required = true)
                                        @RequestParam
                                        @NotBlank(message = "微信用户id不得为空") String userId,
                                        @ApiParam(name = "inviteUserId", value = "推荐人id", required = true)
                                        @RequestParam
                                        @NotBlank(message = "推荐人id不得为空") String inviteUserId) {
        buyerUserService.userIdSetInviteUserId(userId, inviteUserId);
        return Result.ok("操作成功");
    }

    /**
     * 封号(微信用户)
     *
     * @param userId
     * @return
     */
    @ApiOperation(value = "封号(微信用户) 0：封禁 1 正常", notes = "封号(微信用户) 0：封禁 1 正常")
    @PutMapping("/userIdSetStatus")
    public Result userIdSetStatus(@ApiParam(name = "userId", value = "微信用户id", required = true)
                                  @RequestParam
                                  @NotBlank(message = "微信用户id不得为空") String userId) {
        buyerUserService.userIdSetStatus(userId);
        return Result.ok("操作成功");
    }

    /**
     * 删除(微信用户)
     *
     * @param userId
     * @return
     */
    @DeleteMapping("/userIdDeleteBuyerUser")
    @ApiOperation(value = "删除(微信用户)", notes = "删除(微信用户)")
    public Result userIdDeleteBuyerUser(@ApiParam(name = "userId", value = "微信用户id", required = true)
                                        @RequestParam
                                        @NotBlank(message = "微信用户id不得为空") String userId) {
        buyerUserService.userIdDeleteBuyerUser(userId);
        return Result.ok("操作成功");
    }

    /**
     * 用户名称列表(模糊查询)
     *
     * @param userName
     * @return
     */
    @ApiOperation(value = "用户名称列表(模糊查询)", notes = "用户名称列表(模糊查询)")
    @GetMapping("/userNameGetList")
    public Result userNameGetList(@ApiParam(name = "userName", value = "微信用户名称", required = true)
                                  @RequestParam
                                  @NotBlank(message = "微信用户id不得为空") String userName) {
        return Result.ok(buyerUserService.userNameGetList(userName));
    }

    /**
     * 根据用户id查询用户详情
     *
     * @param userId
     * @return
     */
    @ApiOperation(value = "根据用户id查询用户详情", notes = "根据用户id查询用户详情")
    @GetMapping("/userIdGetBuyerUser")
    public Result userIdGetBuyerUser(@ApiParam(name = "userId", value = "微信用户id", required = true)
                                     @RequestParam
                                     @NotBlank(message = "微信用户id不得为空") String userId) {
        return Result.ok(buyerUserService.userIdGetBuyerUser(userId));
    }
}