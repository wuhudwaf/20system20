package com.example.system.controller;

import com.example.system.dto.AddSysUser;
import com.example.system.dto.Result;
import com.example.system.service.SysUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * 管理员管理
 */
@Api(tags = "管理员管理")
@Validated
@RestController
@SuppressWarnings("all")
@RequiredArgsConstructor
@RequestMapping("/sys/user")
public class SysUserController {

    private final SysUserService sysUserService;

    /**
     * 获取所有的门店管理员
     *
     * @return
     */
    @ApiOperation(value = "获取所有的门店管理员", notes = "获取所有的门店管理员")
    @GetMapping("/getShopUser")
    public Result getShopUser(@ApiParam(name = "page", value = "第几页", required = true) @RequestParam @NotNull(message = "page不得为空") Long page, @ApiParam(name = "pageSize", value = "每页多少条", required = true) @RequestParam @NotNull(message = "pageSize不得为空") Long pageSize) {
        return Result.ok(sysUserService.getShopUser(page, pageSize));
    }

    /**
     * 新增管理员
     *
     * @param addSysUser
     * @return
     */
    @ApiOperation(value = "新增管理员", notes = "新增管理员")
    @PostMapping("/add")
    public Result add(@RequestBody @Valid AddSysUser addSysUser) {
        sysUserService.add(addSysUser);
        return Result.ok("添加成功");
    }


//    @ApiOperation(value = "新增管理员", notes = "新增管理员")
//    @GetMapping("/getPage")
//    public Result getPage() {
//
//    }
}