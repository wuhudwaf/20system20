package com.example.system.controller;


import com.example.system.dto.Result;
import com.example.system.service.BuyerOrderStatusLogService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotNull;

/**
 * 订单状态变化表
 */
@Api(tags = "订单状态变化")
@Validated
@RestController
@SuppressWarnings("all")
@RequiredArgsConstructor
@RequestMapping("/order/buyer/order/status/log")
public class BuyerOrderStatusLogController {

    private final BuyerOrderStatusLogService buyerOrderStatusLogService;


    /**
     * 订单状态变化列表
     *
     * @param page
     * @param pageSize
     * @return
     */
    @ApiOperation(value = "订单状态变化列表", notes = "订单状态变化列表")
    @GetMapping("/getPage")
    public Result getPage(@ApiParam(name = "page", value = "第几页", required = true) @RequestParam @NotNull(message = "page不得为空") Long page,
                          @ApiParam(name = "pageSize", value = "每页展示多少条", required = true) @RequestParam @NotNull(message = "pageSize不得为空") Long pageSize) {
        return Result.ok(buyerOrderStatusLogService.getPage(page, pageSize));
    }
}