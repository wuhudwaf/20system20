package com.example.system.controller;

import com.example.system.dto.Result;
import com.example.system.pojo.AppletInfo;
import com.example.system.service.AppletInfoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@Api(tags = "小程序信息")
@Validated
@RestController
@SuppressWarnings("all")
@RequiredArgsConstructor
@RequestMapping("/applet/info")
public class AppletInfoController {

    private final AppletInfoService appletInfoService;

    /**
     * 新增小程序信息
     *
     * @param appletInfo
     * @return
     */
    @PostMapping("/addAppletInfo")
    @ApiOperation(value = "新增小程序信息", notes = "新增小程序信息")
    public Result addAppletInfo(@ApiParam(name = "appletInfo", value = "小程序信息表", required = true) @RequestBody @Valid AppletInfo appletInfo) {
        appletInfoService.addAppletInfo(appletInfo);
        return Result.ok("新增成功");
    }

    /**
     * 删除小程序信息
     *
     * @param infoId
     * @return
     */
    @DeleteMapping("/deleteAppletInfo")
    @ApiOperation(value = "删除小程序信息", notes = "删除小程序信息")
    public Result deleteAppletInfo(@ApiParam(name = "infoId", value = "小程序信息id", required = true) @RequestParam @NotNull(message = "infoId不得为空") String infoId) {
        appletInfoService.deleteAppletInfo(infoId);
        return Result.ok("删除成功");
    }

    /**
     * 修改小程序信息
     *
     * @param appletInfo
     * @return
     */
    @PutMapping("/updateAppletInfo")
    @ApiOperation(value = "修改小程序信息", notes = "修改小程序信息")
    public Result updateAppletInfo(@ApiParam(name = "appletInfo", value = "小程序信息表", required = true) @RequestBody @Valid AppletInfo appletInfo) {
        appletInfoService.updateAppletInfo(appletInfo);
        return Result.ok("修改成功");
    }

    /**
     * 查看小程序信息详情
     *
     * @param infoId
     * @return
     */
    @GetMapping("/getAppletInfo")
    @ApiOperation(value = "查看小程序信息详情", notes = "查看小程序信息详情")
    public Result getAppletInfo(@ApiParam(name = "infoId", value = "小程序信息id", required = true) @RequestParam @NotNull(message = "infoId不得为空") String infoId) {
        return Result.ok(appletInfoService.getAppletInfo(infoId));
    }

    /**
     * 查看小程序信息列表
     *
     * @param page
     * @param pageSize
     * @return
     */
    @GetMapping("/getAppletInfoPage")
    @ApiOperation(value = "查看小程序信息列表", notes = "查看小程序信息列表")
    public Result getAppletInfoPage(@ApiParam(name = "page", value = "第几页", required = true) @RequestParam @NotNull(message = "page不得为空") Long page,
                                    @ApiParam(name = "pageSize", value = "每页展示多少条", required = true) @RequestParam @NotNull(message = "page不得为空") Long pageSize) {
        return Result.ok(appletInfoService.getAppletInfoPage(page, pageSize));
    }
}