package com.example.system.controller;

import com.example.system.dto.Result;
import com.example.system.pojo.SysCategory;
import com.example.system.service.SysCategoryService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;

/**
 * 商品类别
 */
@Api(tags = "商品类别")
@Validated
@RestController
@SuppressWarnings("all")
@RequiredArgsConstructor
@RequestMapping("/goods/sys/category")
public class SysCategoryController {

    private final SysCategoryService sysCategoryService;

    /**
     * 添加商品类别
     *
     * @param sysCategory
     * @return
     */
    @ApiOperation(value = "添加商品类别", notes = "添加商品类别")
    @PostMapping("/add")
    public Result add(@RequestBody @Valid SysCategory sysCategory) {
        sysCategoryService.add(sysCategory);
        return Result.ok("添加成功");
    }

    /**
     * 废弃分类
     *
     * @param cateId
     * @return
     */
    @ApiOperation(value = "废弃分类", notes = "废弃分类")
    @DeleteMapping("/delete")
    public Result delete(@ApiParam(name = "cateId", value = "分类id", required = true) @RequestParam @NotNull(message = "请选择要废除的分类") String cateId) {
        sysCategoryService.delete(cateId);
        return Result.ok("删除成功");
    }

    /**
     * 修改分类
     *
     * @param sysCategory
     * @return
     */
    @ApiOperation(value = "修改分类", notes = "修改分类")
    @PutMapping("/set")
    public Result set(@RequestBody SysCategory sysCategory) {
        sysCategoryService.set(sysCategory);
        return Result.ok("修改成功");
    }

    /**
     * 分类详情
     *
     * @param cateId
     * @return
     */
    @ApiOperation(value = "分类详情", notes = "分类详情")
    @GetMapping("/getByCateId")
    public Result getByCateId(@ApiParam(name = "cateId", value = "分类id", required = true) @RequestParam @NotNull(message = "类别id不得为空") String cateId) {
        return Result.ok(sysCategoryService.getByCateId(cateId));
    }


    /**
     * 查询分类列表
     * @return
     */
    @ApiOperation(value = "查询分类列表", notes = "查询分类列表")
    @GetMapping("/getC")
    public Result getList() {
        return Result.ok(sysCategoryService.getList());
    }

    /**
     * 查询子分类（没有被废弃的）
     *
     * @return
     */
    @ApiOperation(value = "查询某一分类的子分类（没有被废弃的）", notes = "查询某一分类的子分类（没有被废弃的）")
    @GetMapping("/getList")
    public Result getList(@ApiParam(name = "cateId", value = "分类id", required = true) @RequestParam @NotNull(message = "cateId不得为空") String cateId) {
        return Result.ok(sysCategoryService.getList(new ArrayList<SysCategory>(), cateId));
    }

    /**
     * 查询根节点分类
     *
     * @return
     */
    @ApiOperation(value = "查询根节点分类", notes = "查询根节点分类")
    @GetMapping("/getRoot")
    public Result getRoot() {
        return Result.ok(sysCategoryService.getRoot());
    }
}