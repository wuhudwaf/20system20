package com.example.system.controller;

import com.example.system.dto.Result;
import com.example.system.dto.SysCouponDto;
import com.example.system.pojo.SysCoupon;
import com.example.system.service.SysCouponService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * 优惠劵
 */
@Api(tags = "优惠劵")
@Validated
@RestController
@SuppressWarnings("all")
@RequiredArgsConstructor
@RequestMapping("/sys/coupon")
public class SysCouponController {

    private final SysCouponService sysCouponService;

    /**
     * 添加优惠券
     *
     * @param sysCoupon
     * @return
     */
    @ApiOperation(value = "添加优惠券", notes = "添加优惠券")
    @PostMapping("/add")
    public Result add(@RequestBody @Valid SysCouponDto sysCouponDto) {
        sysCouponService.add(sysCouponDto);
        return Result.ok("添加成功");
    }

    /**
     * 删除优惠券
     *
     * @param couponId
     * @return
     */
    @ApiOperation(value = "删除优惠券", notes = "删除优惠券")
    @DeleteMapping("/delete")
    public Result delete(@ApiParam(name = "couponId", value = "优惠券id", required = true) @RequestParam @NotNull(message = "id不得为空") String couponId) {
        sysCouponService.delete(couponId);
        return Result.ok("删除成功");
    }

    /**
     * 修改优惠券
     *
     * @param sysCoupon
     * @return
     */
    @ApiOperation(value = "修改优惠券", notes = "修改优惠券")
    @PutMapping("/update")
    public Result update(@RequestBody @Valid SysCouponDto sysCouponDto) {
        sysCouponService.update(sysCouponDto);
        return Result.ok("修改成功");
    }

    /**
     * 分页查询优惠券
     *
     * @param page
     * @param pageSize
     * @return
     */
    @ApiOperation(value = "查询优惠券列表", notes = "查询优惠券列表")
    @GetMapping("/getPage")
    public Result getPage(@ApiParam(name = "page", value = "第几页", required = true) @RequestParam @NotNull(message = "page不得为空") Long page,
                          @ApiParam(name = "pageSize", value = "每页展示多少条", required = true) @RequestParam @NotNull(message = "pageSize不得为空") Long pageSize) {
        return Result.ok(sysCouponService.getPage(page, pageSize));
    }

    /**
     * 查看优惠券详情
     *
     * @param couponId
     * @return
     */
    @ApiOperation(value = "查看优惠券详情", notes = "查看优惠券详情")
    @GetMapping("/get")
    public Result get(@ApiParam(name = "couponId", value = "优惠券id", required = true) @RequestParam @NotNull(message = "id不得为空") String couponId) {
        return Result.ok(sysCouponService.get(couponId));
    }

    /**
     * 批量设置上/下架
     *
     * @param sysCoupons
     * @return
     */
    @ApiOperation(value = "批量设置上/下架", notes = "批量设置上/下架")
    @PutMapping("/setStatusList")
    public Result setStatusList(@ApiParam(name = "couponId", value = "优惠券对象集合（数组）", required = true) @RequestBody List<SysCoupon> sysCoupons) {
        sysCouponService.setStatusList(sysCoupons);
        return Result.ok("修改成功");
    }
}