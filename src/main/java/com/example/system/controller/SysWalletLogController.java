package com.example.system.controller;

import com.example.system.dto.Result;
import com.example.system.service.SysWalletLogService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotNull;

/**
 * 门店余额变动日志
 */
@Api(tags = "门店余额变动日志")
@Validated
@RestController
@SuppressWarnings("all")
@RequiredArgsConstructor
@RequestMapping("/shop/sys/wallet/log")
public class SysWalletLogController {

    private final SysWalletLogService sysWalletLogService;

    /**
     * 门店id查询余额变动日志列表
     *
     * @param shopId
     * @param page
     * @param pageSize
     * @return
     */
    @ApiOperation(value = "门店id查询余额变动日志列表", notes = "门店id查询余额变动日志列表")
    @GetMapping("/getPage")
    public Result getPage(@ApiParam(name = "shopId", value = "门店id", required = true) @RequestParam @NotNull(message = "门店id不得为空") String shopId,
                          @ApiParam(name = "page", value = "第几页", required = true) @RequestParam @NotNull(message = "page不得为空") Long page,
                          @ApiParam(name = "pageSize", value = "每页多少条", required = true) @RequestParam @NotNull(message = "pageSize不得为空") Long pageSize) {
        return Result.ok(sysWalletLogService.getPage(shopId, page, pageSize));
    }
}