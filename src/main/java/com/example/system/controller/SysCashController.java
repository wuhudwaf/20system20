package com.example.system.controller;

import com.example.system.dto.Result;
import com.example.system.pojo.SysCash;
import com.example.system.service.SysCashService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * 提现申请表
 */
@Api(tags = "提现申请")
@Validated
@RestController
@SuppressWarnings("all")
@RequiredArgsConstructor
@RequestMapping("/sys/cash")
public class SysCashController {

    private final SysCashService sysCashService;

    /**
     * 测试
     *
     * @param page
     * @param pageSize
     * @return
     */
//    @ApiOperation(value = "测试,提现申请列表", notes = "测试,提现申请列表")
//    @GetMapping("/getPageTest")
//    public Result getPageTest(@ApiParam(name = "page", value = "第几页", required = true) @RequestParam @NotNull(message = "page不得为空") Long page,
//                              @ApiParam(name = "pageSize", value = "每页展示多少条", required = true) @RequestParam @NotNull(message = "pageSize不得为空") Long pageSize) {
//        return Result.ok(sysCashService.getPageTest(page, pageSize));
//    }

    /**
     * 提现申请列表
     *
     * @param page
     * @param pageSize
     * @return
     */
    @ApiOperation(value = "提现申请列表", notes = "提现申请列表")
    @GetMapping("/getPage")
    public Result getPage(@ApiParam(name = "page", value = "第几页", required = true) @RequestParam @NotNull(message = "page不得为空") Long page,
                          @ApiParam(name = "pageSize", value = "每页展示多少条", required = true) @RequestParam @NotNull(message = "pageSize不得为空") Long pageSize) {
        return Result.ok(sysCashService.getPage(page, pageSize));
    }

    /**
     * 新增提现申请
     *
     * @param sysCash
     * @return
     */
//    @PostMapping("/addSysCash")
//    @ApiOperation(value = "新增提现申请", notes = "新增提现申请")
//    public Result addSysCash(@RequestBody @Valid SysCash sysCash) {
//        sysCashService.addSysCash(sysCash);
//        return Result.ok("申请成功");
//    }

    /**
     * 同意申请/申请驳回
     *
     * @param id
     * @param status
     * @return
     */
    @PutMapping("/deleteSysCash")
    @ApiOperation(value = "同意申请/申请驳回", notes = "同意申请/申请驳回")
    public Result updateSysCash(@ApiParam(name = "id", value = "提现申请id", required = true) @RequestParam @NotNull(message = "提现申请id不得为空") String id,
                                @ApiParam(name = "status", value = "1 申请中   2 申请成功   0申请驳回", required = true) @RequestParam @NotNull(message = "提现申请id不得为空") Integer status) {
        sysCashService.deleteSysCash(id, status);
        return Result.ok("删除成功");
    }

    /**
     * 提现申请详情
     *
     * @param id
     * @return
     */
//    @ApiOperation(value = "提现申请详情", notes = "提现申请详情")
//    @GetMapping("/getBySysCashId")
//    public Result getBySysCashId(@ApiParam(name = "id", value = "提现申请id", required = true) @RequestParam @NotNull(message = "提现申请id不得为空") String id) {
//        return Result.ok(sysCashService.getBySysCashId(id));
//    }
}