package com.example.system.controller;


import com.example.system.dto.Result;
import com.example.system.pojo.SysBanner;
import com.example.system.service.SysBannerService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * 轮播图
 */
@Api(tags = "轮播图管理")
@Validated
@RestController
@SuppressWarnings("all")
@RequiredArgsConstructor
@RequestMapping("/goods/sys/banner")
public class SysBannerController {

    private final SysBannerService sysBannerService;

    /**
     * 新增轮播图
     *
     * @param sysBanner
     * @return
     */
    @ApiOperation("新增轮播图")
    @PostMapping("/addSysBanner")
    public Result addSysBanner(@RequestBody @Valid SysBanner sysBanner) {
        sysBannerService.add(sysBanner);
        return Result.ok("轮播图新增成功");
    }

    /**
     * 删除轮播图
     *
     * @param bannerId
     * @return
     */
    @ApiOperation("删除轮播图")
    @DeleteMapping("/deleteSysBanner")
    public Result deleteSysBanner(@ApiParam(name = "bannerId", value = "轮播图id", required = true) @RequestParam @NotNull(message = "bannerId不得为空") String bannerId) {
        sysBannerService.delete(bannerId);
        return Result.ok("轮播图删除成功");
    }

    /**
     * 修改轮播图
     *
     * @param sysBanner
     * @return
     */
    @ApiOperation("修改轮播图")
    @PutMapping("/setSysBanner")
    public Result setSysBanner(@RequestBody @Valid SysBanner sysBanner) {
        sysBannerService.setSysBanner(sysBanner);
        return Result.ok("轮播图修改成功");
    }

    /**
     * 查看轮播图详情
     *
     * @param bannerId
     * @return
     */
    @ApiOperation("查看轮播图详情")
    @GetMapping("/getSysBanner")
    public Result getSysBanner(@ApiParam(name = "bannerId", value = "轮播图id", required = true) @RequestParam @NotNull(message = "bannerId不得为空") String bannerId) {
        return Result.ok(sysBannerService.getSysBanner(bannerId));
    }

    /**
     * 获取轮播图列表
     *
     * @param page
     * @param pageSize
     * @return
     */
    @ApiOperation("获取轮播图列表")
    @GetMapping("/getSysBannerPage")
    public Result getSysBannerPage(@ApiParam(name = "page", value = "第几页", required = true) @RequestParam @NotNull(message = "page不得为空") Long page,
                                   @ApiParam(name = "pageSize", value = "每页展示多少条", required = true) @RequestParam @NotNull(message = "pageSize不得为空") Long pageSize) {
        return Result.ok(sysBannerService.getSysBannerPage(page, pageSize));
    }
}