package com.example.system.controller;

import com.example.system.dto.Result;
import com.example.system.pojo.page.Page;
import com.example.system.service.BuyerOrderService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;

/**
 * 订单
 */
@Api(tags = "管理订单")
@Validated
@RestController
@SuppressWarnings("all")
@RequiredArgsConstructor
@RequestMapping("/order/buyer/order")
public class BuyerOrderController {

    private final BuyerOrderService buyerOrderService;

    /**
     * 查询订单列表
     *
     * @param page
     * @param pageSize
     * @return
     */
    @ApiOperation(value = "查询订单", notes = "查询订单")
    @GetMapping("/getPage")
    public Result getPage(@ApiParam(name = "page", value = "第几页", required = true) @RequestParam @NotNull(message = "page不得为空") Long page,
                          @ApiParam(name = "pageSize", value = "每页展示多少条", required = true) @RequestParam @NotNull(message = "pageSize不得为空") Long pageSize) {
        return Result.ok(buyerOrderService.getPage(page, pageSize));
    }

    /**
     * 取消订单
     *
     * @param orderId
     * @return
     */
    @PutMapping("/setOrderStatus")
    @ApiOperation(value = "取消订单", notes = "取消订单")
    public Result setOrderStatus(@ApiParam(name = "orderId", value = "订单id", required = true)
                                 @RequestParam
                                 @NotNull(message = "订单id不得为空") String orderId) {
        buyerOrderService.setOrderStatus(orderId);
        return Result.ok("取消成功");
    }

    /**
     * 退款记录
     *
     * @param page
     * @return
     */
    @GetMapping("/getRefundLog")
    @ApiOperation(value = "退款记录", notes = "退款记录")
    public Result getRefundLog(Page page) {
        return Result.ok(buyerOrderService.getRefundLog(page));
    }
}