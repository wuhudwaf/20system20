package com.example.system.controller;

import com.example.system.dto.Result;
import com.example.system.pojo.SysInterface;
import com.example.system.service.SysInterfaceService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * 权限管理
 */
@Api(tags = "权限管理")
@Validated
@RestController
@SuppressWarnings("all")
@RequiredArgsConstructor
@RequestMapping("/buyer/user")
public class SysInterfaceController {

    private final SysInterfaceService sysInterfaceService;


    /**
     * 新增权限
     *
     * @param sysInterface
     * @return
     */
    @ApiOperation(value = "新增权限", notes = "新增权限")
    @PostMapping("/add")
    public Result add( @RequestBody @Valid SysInterface sysInterface) {
        sysInterfaceService.add(sysInterface);
        return Result.ok("新政成功");
    }

    /**
     * 删除权限
     *
     * @param id
     * @return
     */
    @ApiOperation(value = "删除权限", notes = "删除权限")
    @DeleteMapping("/delete")
    public Result delete(@ApiParam(name = "id", value = "权限id", required = true) @RequestParam @NotNull(message = "id不得为空") String id) {
        sysInterfaceService.delete(id);
        return Result.ok("删除成功");
    }

    /**
     * 修改权限基本信息
     *
     * @param sysInterface
     * @return
     */
    @ApiOperation(value = "修改权限基本信息", notes = "修改权限基本信息")
    @PutMapping("/set")
    public Result set(@RequestBody @Valid SysInterface sysInterface) {
        sysInterfaceService.set(sysInterface);
        return Result.ok("修改成功");
    }

    /**
     * 查看权限详情
     *
     * @param id
     * @return
     */
    @ApiOperation(value = "查看权限详情", notes = "查看权限详情")
    @GetMapping("/getById")
    public Result getById(@ApiParam(name = "id", value = "权限id", required = true) @RequestParam @NotNull(message = "id不得为空") String id) {
        return Result.ok(sysInterfaceService.getById(id));
    }

    /**
     * 查询所有权限
     *
     * @param page
     * @param pageSize
     * @return
     */
    @ApiOperation(value = "查询所有权限", notes = "查询所有权限")
    @GetMapping("/getPage")
    public Result getPage(@ApiParam(name = "page", value = "第几页", required = true) @RequestParam @NotNull(message = "id不得为空") Long page,
                          @ApiParam(name = "pageSize", value = "每页展示多少条", required = true) @RequestParam @NotNull(message = "id不得为空") Long pageSize) {
        return Result.ok(sysInterfaceService.getPage(page, pageSize));
    }
}