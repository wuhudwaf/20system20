package com.example.system.controller;

import com.example.system.dto.Result;
import com.example.system.pojo.page.NewPage;
import com.example.system.service.BuyerScoreService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotNull;

/**
 * 用户积分表
 */
@Api(tags = "用户积分")
@Validated
@RestController
@SuppressWarnings("all")
@RequiredArgsConstructor
@RequestMapping("/buyer/score")
public class BuyerScoreController {

    private final BuyerScoreService buyerScoreService;

    /**
     * 用户积分列表
     *
     * @param page
     * @param pageSize
     * @return
     */
    @ApiOperation(value = "用户积分列表", notes = "用户积分列表")
    @GetMapping("/getPage")
    public Result getPage(@ApiParam(name = "page", value = "第几页", required = true) @RequestParam @NotNull(message = "page不得为空") Long page,
                          @ApiParam(name = "pageSize", value = "每页展示多少条", required = true) @RequestParam @NotNull(message = "pageSize不得为空") Long pageSize) {
        return Result.ok(buyerScoreService.getPage(page, pageSize));
    }

    /**
     * 积分明细
     *
     * @param newPage
     * @return
     */
    @GetMapping("/buyerUserScore")
    @ApiOperation(value = "积分明细", notes = "积分明细")
    public Result buyerUserScore(NewPage newPage) {
        return Result.ok(buyerScoreService.buyerUserScore(newPage));
    }
}