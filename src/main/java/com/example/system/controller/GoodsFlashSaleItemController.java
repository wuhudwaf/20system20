package com.example.system.controller;

import com.example.system.dto.Result;
import com.example.system.pojo.GoodsFlashSaleItem;
import com.example.system.service.GoodsFlashSaleItemService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * 秒杀和商品sku关联
 */
@Api(tags = "秒杀和商品sku关联")
@Validated
@RestController
@SuppressWarnings("all")
@RequiredArgsConstructor
@RequestMapping("/goods/flash/sale/item")
public class GoodsFlashSaleItemController {

    private final GoodsFlashSaleItemService goodsFlashSaleItemService;


    /**
     * 秒杀和商品sku关联接口,单关联
     *
     * @param list
     * @return
     */
    @ApiOperation(value = "秒杀和商品sku关联接口,单关联", notes = "秒杀和商品sku关联接口,单关联")
    @PostMapping("/addGoodsFlashSaleItem")
    public Result addGoodsFlashSaleItem(
            @ApiParam(name = "goodsFlashSaleItem", value = "商品限时秒杀项目信息对象", required = true)
            @RequestBody @Valid GoodsFlashSaleItem goodsFlashSaleItem) {
        goodsFlashSaleItemService.addGoodsFlashSaleItem(goodsFlashSaleItem);
        return Result.ok("创建成功");
    }

    /**
     * 删除一个秒杀和商品sku关联
     *
     * @param skuId
     * @return
     */
    @ApiOperation(value = "根据skuId删除一个秒杀和商品sku关联", notes = "根据skuId删除一个秒杀和商品sku关联")
    @DeleteMapping("/deleteGoodsFlashSaleItem")
    public Result deleteGoodsFlashSaleItem(@ApiParam(name = "skuId", value = "商品skuId", required = true)
                                           @RequestParam @NotNull(message = "商品限时秒杀项目信息id不得为空") String skuId) {
        goodsFlashSaleItemService.deleteGoodsFlashSaleItem(skuId);
        return Result.ok("删除成功");
    }

    /**
     * 修改商品限时秒杀项目信息
     *
     * @param goodsFlashSaleItem
     * @return
     */
    @ApiOperation(value = "根据itemId修改商品限时秒杀项目信息", notes = "根据itemId修改商品限时秒杀项目信息")
    @PutMapping("/updateGoodsFlashSaleItem")
    public Result updateGoodsFlashSaleItem(@ApiParam(name = "goodsFlashSaleItem", value = "商品限时秒杀项目信息对象", required = true) @RequestBody @Valid GoodsFlashSaleItem goodsFlashSaleItem) {
        goodsFlashSaleItemService.updateGoodsFlashSaleItem(goodsFlashSaleItem);
        return Result.ok("修改成功");
    }

    /**
     * 根据itemId获取商品限时秒杀项目信息
     *
     * @param itemId
     * @return
     */
    @ApiOperation(value = "根据itemId获取商品限时秒杀项目信息", notes = "根据itemId获取商品限时秒杀项目信息")
    @GetMapping("/getGoodsFlashSaleItem")
    public Result getGoodsFlashSaleItem(@ApiParam(name = "itemId", value = "商品限时秒杀项目信息id", required = true) @RequestParam @NotNull(message = "商品限时秒杀项目信息id不得为空") String itemId) {
        return Result.ok(goodsFlashSaleItemService.getGoodsFlashSaleItem(itemId));
    }

    /**
     * 获取商品限时秒杀项目信息列表
     *
     * @param page
     * @param pageSize
     * @return
     */
    @ApiOperation(value = "获取商品限时秒杀项目信息列表", notes = "获取商品限时秒杀项目信息列表")
    @GetMapping("/getGoodsFlashSaleItemPage")
    public Result getGoodsFlashSaleItemPage(@ApiParam(name = "page", value = "第几页", required = true)
                                            @RequestParam @NotNull(message = "page不得为空") Long page,
                                            @ApiParam(name = "pageSize", value = "每页展示多少条", required = true)
                                            @RequestParam @NotNull(message = "pageSize不得为空") Long pageSize) {
        return Result.ok(goodsFlashSaleItemService.getGoodsFlashSaleItemPage(page, pageSize));
    }
}