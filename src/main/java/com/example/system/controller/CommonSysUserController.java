package com.example.system.controller;

import com.example.system.dto.LogInUser;
import com.example.system.dto.Result;
import com.example.system.service.SysUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

/**
 * 管理员登录
 */
@Api(tags = "管理员的基本操作")
@Validated
@RestController
@SuppressWarnings("all")
@RequiredArgsConstructor
@RequestMapping("/common/sys/user")
public class CommonSysUserController {

    private final SysUserService sysUserService;

    /**
     * 登录
     *
     * @param logInUser
     * @return
     */
    @ApiOperation(value = "登录", notes = "登录")
    @PostMapping("/login")
    public Result login(@RequestBody @Valid LogInUser logInUser) {
        return Result.ok(sysUserService.login(logInUser));
    }

    /**
     * 退出登录
     *
     * @param httpServletRequest
     * @return
     */
    @ApiOperation(value = "退出登录", notes = "退出登录")
    @DeleteMapping("/exit")
    public Result exit() {
        sysUserService.exit();
        return Result.ok("退出成功");
    }

    /**
     * 查看我的信息
     *
     * @return
     */
    @ApiOperation(value = "查看我的信息", notes = "查看我的信息")
    @GetMapping("/getMyInfo")
    public Result getMyInfo() {
        return Result.ok(sysUserService.getMyInfo());
    }
}