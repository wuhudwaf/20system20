package com.example.system.controller;

import com.example.system.dto.Result;
import com.example.system.service.FileUploadService;
import com.example.system.utils.PicUploadResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

@Api(tags = "图片上传接口")
@RestController
@RequiredArgsConstructor
@SuppressWarnings("all")
@RequestMapping("/common")
public class FileUploadController {
    // 允许上传的格式 图片形式
    private static final String[] IMAGE_TYPE = new String[]{".bmp", ".jpg", ".jpeg", ".png"};

    @Resource
    private FileUploadService fileUploadService;

    /**
     * 单图片上传
     * @param file
     * @param request
     * @return
     */
    @ApiOperation(value = "选择图片后调用此接口", notes = "选择图片后调用此接口")
    @PostMapping("/upload")
    public Result uploadImg(@RequestParam("file") MultipartFile file, HttpServletRequest request) {
        boolean isFlag = false;
        for (String type : IMAGE_TYPE) {
            System.out.println(file.getOriginalFilename());
            if (StringUtils.endsWithIgnoreCase(file.getOriginalFilename(), type)) {
                isFlag = true;
                break;
            }
        }

        if (isFlag) {

            PicUploadResult picUploadResult = fileUploadService.uplodadImg(file, request);
            boolean isLegal = picUploadResult.isLegal();

            if (isLegal) {
//                Map resMap = new HashMap<>();
//                resMap.put("imgPath", picUploadResult.getImgPath());
                return Result.ok(picUploadResult.getImgPath());
            } else {
                return Result.fail("图片上传有误");
            }
        } else {
            return Result.fail("上传的图片格式必须为:bmp,jpg,jpeg,png");
        }

    }

//    @PostMapping("/uploadManyImg")
    public Result uploadManyImg(@RequestParam("files") MultipartFile[] files, HttpServletRequest request) {
        boolean isFlag = false;
        for (MultipartFile uploadFile : files) {
            for (String type : IMAGE_TYPE) {
                if (StringUtils.endsWithIgnoreCase(uploadFile.getOriginalFilename(), type)) {
                    isFlag = true;
                    break;
                }
            }
        }

        if (isFlag) {
            PicUploadResult picUploadResult = fileUploadService.uploadManyImg(files, request);
            boolean isLegal = picUploadResult.isLegal();

            if (isLegal) {
                Map resMap = new HashMap<>();
                resMap.put("imgPaths", picUploadResult.getImgPahts());
                return Result.ok(resMap);
            } else {
                return Result.fail("图片上传有误");
            }
        } else {
            return Result.fail("上传的图片格式必须为:bmp,jpg,jpeg,png");
        }
    }
}