package com.example.system.controller;

import com.example.system.dto.Result;
import com.example.system.pojo.SysGoods;
import com.example.system.pojo.imports.SysGoodsImport;
import com.example.system.pojo.page.NewPage;
import com.example.system.pojo.vo.add.SysGoodsAdd;
import com.example.system.pojo.where.SysGoodsQuery;
import com.example.system.service.GoodsSkuService;
import com.example.system.service.SysGoodsService;
import com.example.system.utils.excel.ExcelUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * 商品
 */
@Api(tags = "商品管理")
@Validated
@RestController
@SuppressWarnings("all")
@RequiredArgsConstructor
@RequestMapping("/goods/sys/goods")
public class SysGoodsController {

    private final SysGoodsService sysGoodsService;

    private final GoodsSkuService goodsSkuService;

    /**
     * 新增商品
     *
     * @param sysGoods
     * @return
     */
    @ApiOperation(value = "新增商品", notes = "新增商品")
    @PostMapping("/add")
    public Result add(@RequestBody @Valid SysGoodsAdd sysGoodsAdd) {
        sysGoodsService.add(sysGoodsAdd);
        return Result.ok("添加成功");
    }

    /**
     * 获取指定商品的sku对象列表
     *
     * @param goodsId
     * @return
     */
    @ApiOperation(value = "获取指定商品的sku对象列表", notes = "获取指定商品的sku对象列表")
    @GetMapping("/getGoodsSku")
    public Result getGoodsSku(@ApiParam(name = "goodsId", value = "商品id", required = true) @RequestParam @NotNull(message = "商品id不得为空") String goodsId) {
        return Result.ok(sysGoodsService.getGoodsSku(goodsId));
    }

    /**
     * 删除商品
     *
     * @param goodsId
     * @return
     */
    @ApiOperation(value = "删除商品", notes = "删除商品")
    @DeleteMapping("/delete")
    public Result delete(@ApiParam(name = "goodsId", value = "商品id", required = true) @RequestParam @NotNull(message = "") String goodsId) {
        sysGoodsService.delete(goodsId);
        return Result.ok("删除成功");
    }

    /**
     * 修改商品
     *
     * @param sysGoods
     * @return
     */
    @ApiOperation(value = "修改商品", notes = "修改商品")
    @PutMapping("/set")
    public Result set(@RequestBody @Valid SysGoodsAdd sysGoods) {
        sysGoodsService.set(sysGoods, goodsSkuService);
        return Result.ok("修改成功");
    }

    /**
     * 查看商品详情(商品携带当前商品的sku列表)
     *
     * @param goodsId
     * @return
     */
    @ApiOperation(value = "查看商品详情(商品携带当前商品的sku列表)", notes = "查看商品详情(商品携带当前商品的sku列表)")
    @GetMapping("/getByGoodsId")
    public Result getByGoodsId(@ApiParam(name = "goodsId", value = "商品id", required = true) @RequestParam @NotNull(message = "goodsId不得为空") String goodsId) {
        return Result.ok(sysGoodsService.getByGoodsId(goodsId));
    }

    /**
     * 查看商品列表(每个商品携带当前商品的sku列表)
     *
     * @return
     */
    @ApiOperation(value = "查看商品列表(每个商品携带当前商品的sku列表)", notes = "查看商品列表(每个商品携带当前商品的sku列表)")
    @GetMapping("/getByGoodsPage")
    public Result getByGoodsPage(@ApiParam(name = "sysGoodsQuery", value = "", required = true) @Valid SysGoodsQuery sysGoodsQuery) {
        return Result.ok(sysGoodsService.getByGoodsPage(sysGoodsQuery));
    }

    /**
     * 查询指定分类的商品
     *
     * @param cateId
     * @return
     */
    @ApiOperation(value = "查询指定分类的商品", notes = "查询指定分类的商品")
    @GetMapping("/categoryIdSelectGoodsList")
    public Result categoryIdSelectGoodsList(@ApiParam(name = "cateId", value = "商品分类id", required = true) @RequestParam @NotNull(message = "商品分类id不得为空") String cateId) {
        return Result.ok(sysGoodsService.categoryIdSelectGoodsList(cateId));
    }

    /**
     * 放入仓库/上架销售：1放入仓库, 2上架销售
     *
     * @param goodsId
     * @return
     */
    @PutMapping("/setStatus")
    @ApiOperation(value = "放入仓库/上架销售：1放入仓库, 2上架销售", notes = "放入仓库/上架销售：1放入仓库, 2上架销售")
    public Result setStatus(@ApiParam(name = "goodsId", value = "商品id", required = true) @RequestParam @NotNull(message = "商品id不得为空") String goodsId) {
        sysGoodsService.setStatus(goodsId);
        return Result.ok("操作成功");
    }

    /**
     * 商品导入
     *
     * @param goodsFile
     * @param goodsSkuFile
     * @return
     * @throws Exception
     */
    @ApiOperation(value = "商品导入", notes = "商品导入")
    @PostMapping("/autoAddSysGoodsListTwo")
    public Result autoAddSysGoodsListTwo(@ApiParam(name = "goodsFile", value = "商品数据文件", required = true) @RequestPart("goodsFile") MultipartFile goodsFile) throws Exception {
        sysGoodsService.autoAddSysGoodsListTwo(ExcelUtils.readMultipartFile(goodsFile, SysGoodsImport.class));
        return Result.ok("操作成功");
    }

    /**
     * 商品导出
     *
     * @param fileName
     * @param response
     * @return
     */
    @ApiOperation(value = "商品导出(全部)", notes = "商品导出(全部)")
    @GetMapping("/diyGetSysGoodsList")
    public Result diyGetSysGoodsList(@ApiParam(name = "fileName", value = "导出的文件名称", required = true) @RequestParam String fileName,
                                     HttpServletResponse response) {
        // 表示导出全部
        List<SysGoodsImport> sysGoods = sysGoodsService.diyGetSysGoodsList();
        ExcelUtils.export(response, fileName, sysGoods, SysGoods.class);
        return Result.ok("操作成功");
    }

    /**
     * 商品导出(页数)
     *
     * @param pageOne
     * @param response
     * @param pageTwo
     * @return
     */
    @ApiOperation(value = "商品导出(页数)", notes = "商品导出(页数)")
    @GetMapping("/diyGetSysGoodsPage")
    public Result diyGetSysGoodsPage(@ApiParam(name = "pageOne", value = "第几页", required = true) @RequestParam Integer pageOne,
                                     HttpServletResponse response,
                                     @ApiParam(name = "pageTwo", value = "至第几页", required = true) @RequestParam Integer pageTwo,
                                     @ApiParam(name = "pageSize", value = "每页多少条", required = true) @RequestParam Integer pageSize) {
        List<SysGoodsImport> sysGoods = sysGoodsService.diyGetSysGoodsPage(pageOne, pageTwo, pageSize);
        ExcelUtils.export(response, "商品表", sysGoods, SysGoods.class);
        return Result.ok("操作成功");
    }

    /**
     * 商品评价
     *
     * @param newPage
     * @return
     */
    @ApiOperation(value = "商品评价", notes = "商品评价")
    @GetMapping("/getGoodsEvaluate")
    public Result getGoodsEvaluate(NewPage newPage) {
        return Result.ok(sysGoodsService.getGoodsEvaluate(newPage));
    }

    /**
     * 商品海报
     *
     * @param goodsId
     * @return
     */
    @ApiOperation(value = "商品海报", notes = "商品海报")
    @GetMapping("/getPoster")
    public Result getPoster(@ApiParam(name = "goodsId", value = "商品id", required = true) @RequestParam Long goodsId){
        return Result.ok(sysGoodsService.getPoster(goodsId));
    }
}