package com.example.system.controller;

import com.example.system.dto.Result;
import com.example.system.service.SellerGoodsService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotNull;

/**
 * 门店商品
 */
@Api(tags = "门店商品")
@Validated
@RestController
@SuppressWarnings("all")
@RequiredArgsConstructor
@RequestMapping("/shop/seller/goods")
public class SellerGoodsController {

    private final SellerGoodsService sellerGoodsService;

    /**
     * 查询指定门店商品列表
     *
     * @param page
     * @param pageSize
     * @return
     */
    @ApiOperation(value = "查询指定门店商品列表", notes = "查询指定门店商品列表")
    @GetMapping("/getPage")
    public Result getPage(
            @ApiParam(name = "shopId", value = "门店id", required = true) @RequestParam @NotNull(message = "门店id不得为空") String shopId,
            @ApiParam(name = "page", value = "第几页", required = true) @RequestParam @NotNull(message = "page不得为空") Long page,
            @ApiParam(name = "pageSize", value = "每页展示多少条", required = true) @RequestParam @NotNull(message = "pageSize不得为空") Long pageSize) {
        return Result.ok(sellerGoodsService.getPage(shopId, page, pageSize));
    }


}