package com.example.system.controller;

import com.example.system.dto.Result;
import com.example.system.pojo.GoodsSku;
import com.example.system.service.GoodsSkuService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * 商品sku表
 */
@Api(tags = "商品sku")
@Validated
@RestController
@SuppressWarnings("all")
@RequiredArgsConstructor
@RequestMapping("/goods/sku")
public class GoodsSkuController {

    private final GoodsSkuService goodsSkuService;


    /**
     * 纯新增商品sku，测试是否能够存储 json数据
     *
     * @param goodsSku
     * @return
     */
//    @ApiOperation(value = "纯新增商品sku，测试是否能够存储 json数据", notes = "纯新增商品sku，测试是否能够存储 json数据")
//    @PostMapping("/add")
//    public Result add(@RequestBody @Valid GoodsSku goodsSku) {
//        goodsSkuService.add(goodsSku);
//        return Result.ok("新增成功");
//    }

    /**
     * 删除商品sku
     *
     * @param skuId
     * @return
     */
    @ApiOperation(value = "删除商品sku", notes = "删除商品sku")
    @DeleteMapping("/delete")
    public Result delete(@ApiParam(name = "skuId", value = "商品skuId", required = true) @RequestParam @NotNull(message = "skuId不得为空") String skuId) {
        goodsSkuService.delete(skuId);
        return Result.ok("删除成功");
    }

    /**
     * 修改商品sku
     *
     * @param goodsSku
     * @return
     */
    @ApiOperation(value = "修改商品sku", notes = "修改商品sku")
    @PutMapping("/set")
    public Result set(
           @RequestBody @Valid GoodsSku goodsSku) {
        goodsSkuService.set( goodsSku);
        return Result.ok("修改成功");
    }

    /**
     * 查看商品sku详情
     *
     * @param skuId
     * @return
     */
    @ApiOperation(value = "查看商品sku详情", notes = "查看商品sku详情")
    @GetMapping("/getBySkuId")
    public Result getBySkuId(@ApiParam(name = "skuId", value = "商品skuId", required = true) @RequestParam @NotNull(message = "skuId不得为空") String skuId) {
        return Result.ok(goodsSkuService.getBySkuId(skuId));
    }

    /**
     * 商品sku列表（管理）
     *
     * @param page
     * @param pageSize
     * @return
     */
    @ApiOperation(value = "商品sku列表（管理）", notes = "商品sku列表（管理）")
    @GetMapping("/getPage")
    public Result getPage(@ApiParam(name = "page", value = "第几页", required = true) @RequestParam @NotNull(message = "page不得为空") Long page,
                          @ApiParam(name = "pageSize", value = "每页展示多少条", required = true) @RequestParam @NotNull(message = "pageSize不得为空") Long pageSize) {
        return Result.ok(goodsSkuService.getPage(page, pageSize));
    }
}