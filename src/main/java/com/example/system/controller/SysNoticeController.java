package com.example.system.controller;

import com.example.system.dto.Result;
import com.example.system.pojo.SysNotice;
import com.example.system.service.SysNoticeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * 公告
 */
@Api(tags = "通知公告")
@Validated
@RestController
@SuppressWarnings("all")
@RequiredArgsConstructor
@RequestMapping("/sys/notice")
public class SysNoticeController {

    private final SysNoticeService sysNoticeService;

    /**
     * 新增通知公告
     *
     * @param sysNotice
     * @return
     */
    @ApiOperation(value = "新增通知公告", notes = "新增通知公告")
    @PostMapping("/add")
    public Result add(@RequestBody @Valid SysNotice sysNotice) {
        sysNoticeService.add(sysNotice);
        return Result.ok("添加成功");
    }

    /**
     * 删除指定的通知公告
     *
     * @param noticeId
     * @return
     */
    @ApiOperation(value = "删除指定的通知公告", notes = "删除指定的通知公告")
    @DeleteMapping("/delete")
    public Result delete(@ApiParam(name = "noticeId", value = "公告id", required = true) @RequestParam @NotNull(message = "id不得为空") String noticeId) {
        sysNoticeService.delete(noticeId);
        return Result.ok("删除成功");
    }

    /**
     * 修改指定的通知公告
     *
     * @param sysNotice
     * @return
     */
    @ApiOperation(value = "修改指定的通知公告", notes = "修改指定的通知公告")
    @PutMapping("/update")
    public Result update(@RequestBody @Valid SysNotice sysNotice) {
        sysNoticeService.update(sysNotice);
        return Result.ok("修改成功");
    }

    /**
     * 获取通知公告列表
     *
     * @return
     */
    @ApiOperation(value = "获取通知公告列表", notes = "获取通知公告列表")
    @GetMapping("/getList")
    public Result getList(@ApiParam(name = "page", value = "第几页", required = true) @RequestParam @NotNull(message = "page不得为空") Long page,
                          @ApiParam(name = "pageSize", value = "每页展示多少条", required = true) @RequestParam @NotNull(message = "pageSize不得为空") Long pageSize) {
        return Result.ok(sysNoticeService.getList(page, pageSize));
    }

    /**
     * 获取通知公告详情
     *
     * @param noticeId
     * @return
     */
    @ApiOperation(value = "获取通知公告详情", notes = "获取通知公告详情")
    @GetMapping("/get")
    public Result get(@ApiParam(name = "noticeId", value = "公告id", required = true) @RequestParam @NotNull(message = "id不得为空") String noticeId) {
        return Result.ok(sysNoticeService.get(noticeId));
    }
}