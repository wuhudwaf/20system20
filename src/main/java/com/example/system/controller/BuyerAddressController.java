package com.example.system.controller;

import com.example.system.dto.Result;
import com.example.system.service.BuyerAddressService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotNull;

/**
 * 买家地址信息表
 */
@Api(tags = "用户地址信息")
@Validated
@RestController
@SuppressWarnings("all")
@RequiredArgsConstructor
@RequestMapping("/buyer/address")
public class BuyerAddressController {

    private final BuyerAddressService buyerAddressService;

    /**
     * 获取某位用户的地址信息列表
     *
     * @param userId
     * @param page
     * @param pageSize
     * @return
     */
    @ApiOperation(value = "获取某位用户的地址信息列表", notes = "获取某位用户的地址信息列表")
    @GetMapping("/getByUserId")
    public Result getByUserId(
            @ApiParam(name = "userId", value = "用户id", required = true) @RequestParam @NotNull(message = "用户id不得为空") String userId,
            @ApiParam(name = "page", value = "第几页", required = true) @RequestParam @NotNull(message = "用户id不得为空") Long page,
            @ApiParam(name = "pageSize", value = "每页展示多少条", required = true) @RequestParam @NotNull(message = "用户id不得为空") Long pageSize) {
        return Result.ok(buyerAddressService.getList(userId, page, pageSize));
    }
}