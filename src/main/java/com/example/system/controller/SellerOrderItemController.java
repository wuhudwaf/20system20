package com.example.system.controller;

import com.example.system.dto.Result;
import com.example.system.service.SellerOrderItemService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotNull;

/**
 * 商品订单表
 */
@Api(tags = "商品订单")
@Validated
@RestController
@SuppressWarnings("all")
@RequiredArgsConstructor
@RequestMapping("/goods/seller/order/item")
public class SellerOrderItemController {

    private final SellerOrderItemService sellerOrderItemService;


    /**
     * 商品订单列表
     *
     * @param page
     * @param pageSize
     * @return
     */
    @ApiOperation(value = "商品订单列表", notes = "商品订单列表")
    @GetMapping("/getPage")
    public Result getPage(@ApiParam(name = "page", value = "第几页", required = true) @RequestParam @NotNull(message = "page不得为空") Long page,
                          @ApiParam(name = "pageSize", value = "每页展示多少条", required = true) @RequestParam @NotNull(message = "pageSize不得为空") Long pageSize) {
        return Result.ok(sellerOrderItemService.getPage(page, pageSize));
    }
}