//package com.example.system;
//
//import com.example.system.utils.RedisIdWorker;
//import com.freewayso.image.combiner.ImageCombiner;
//import com.freewayso.image.combiner.element.TextElement;
//import com.freewayso.image.combiner.enums.OutputFormat;
//import com.freewayso.image.combiner.enums.ZoomMode;
//import lombok.RequiredArgsConstructor;
//import org.junit.jupiter.api.Test;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.boot.test.context.SpringBootTest;
//
//import javax.servlet.http.HttpServletRequest;
//import java.awt.*;
//import java.io.File;
//import java.text.SimpleDateFormat;
//import java.util.Date;
//import java.util.UUID;
//
//
//@SpringBootTest
//@RequiredArgsConstructor
//class ApplicationTests {
//
//    @Value("${bjt.bjts}")
//    private String baseFilePath;
//
//    private final static String BGT_INMAGE = "bgt/";
//
//    private final RedisIdWorker redisIdWorker;
//
//    @Test
//    void contextLoads(HttpServletRequest request) throws Exception {
//
//        //合成器（指定背景图和输出格式，整个图片的宽高和相关计算依赖于背景图，所以背景图的大小是个基准）
//        ImageCombiner combiner = new ImageCombiner("http://admin.gz20wine.com:8081/bjt/true.jpg", OutputFormat.JPG);
//
//        //二维码（file协议）
//        String qrCodeUrl = "http://admin.gz20wine.com:8081/bjt/36940066ff196785773634369b4290c8_r.png";
//
//        // 商品图片
//        String i = "http://admin.gz20wine.com:8081/file/2023/09/06/69877048-933e-4423-8139-798893672e37.jpg";
//
//        //加图片元素
//        combiner.addImageElement(i, 130, 100, 500, 0, ZoomMode.Width);
//        //加文本元素
//        combiner.addTextElement("收藏酒", 60, 220, 625);
//
//        TextElement textPrice = new TextElement("￥1290", 60, 350, 890);
//        textPrice.setColor(Color.gray);          //灰色
//        textPrice.setStrikeThrough(true);       //删除线
//        combiner.addElement(textPrice);         //加入待绘制集合
//
//        TextElement textPriceNow = new TextElement("￥80", 50, 450, 1000);
//        textPriceNow.setColor(Color.red);          //黑色
//        textPriceNow.setStrikeThrough(true);       //删除线
//        combiner.addElement(textPriceNow);         //加入待绘制集合
//
//        combiner.addImageElement(qrCodeUrl, 80, 900, 186, 180, ZoomMode.WidthHeight);
//
//        //执行图片合并
//        combiner.combine();
//
//        // 创建日期文件夹
//        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd/");
//        String format = sdf.format(new Date());
//        File file = new File(baseFilePath + "/" + format + "/");
//        if (!file.isDirectory()) {
//            //递归生成文件夹
//            file.mkdirs();
//        }
//
//        // 海报名称
//        String newName = UUID.randomUUID().toString() + redisIdWorker.nextId("image");
//
//        File newFile = new File(file.getAbsolutePath() + File.separator + newName);
//
//        combiner.save(newFile.getPath());
//
//        // filePath最终名称，存储到商品对象中
//        String filePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + "/" + BGT_INMAGE + format + newName;
//
//        //存储
////        combiner.save("C:\\Users\\EDY\\Desktop\\" + newName);
//
//    }
//}